use sc_client_api::AuxStore;
use sc_consensus_manual_seal::{ConsensusDataProvider, Error};
use std::{
    sync::{atomic, Arc},
    time::SystemTime,
};

use frame_benchmarking::frame_support::sp_runtime::traits::Block;
use sc_consensus_aura::{
    slot_duration, CompatibleDigestItem, InherentDataProvider,
    INHERENT_IDENTIFIER as AURA_INHERENT_ID,
};

use sp_api::{ProvideRuntimeApi, TransactionFor};
use sp_blockchain::{HeaderBackend, HeaderMetadata};
use sp_consensus::{BlockImportParams, SlotData};
use sp_consensus_aura::{
    inherents::AuraInherentData, sr25519::AuthorityId, sr25519::AuthorityPair, AuraApi,
};
use sp_core::Public;
use sp_inherents::{InherentData, InherentDataProviders, InherentIdentifier, ProvideInherentData};
use sp_keystore::SyncCryptoStore;
use sp_runtime::{
    generic::{BlockId, Digest},
    traits::{Block as BlockT, DigestFor, DigestItemFor, Header, Zero},
};
use sp_timestamp::{InherentError, InherentType, INHERENT_IDENTIFIER};
use std::marker::PhantomData;

/// This is used to assist the manual seal engine in processing blocks
pub struct AuraConsensusDataProvider<B: BlockT, C> {
    _block: PhantomData<B>,

    _client: PhantomData<C>,
}

impl<B, C> AuraConsensusDataProvider<B, C>
where
    B: BlockT,
    C: AuxStore
        + HeaderBackend<B>
        + ProvideRuntimeApi<B>
        + HeaderMetadata<B, Error = sp_blockchain::Error>,
    C::Api: AuraApi<B, AuthorityId, Error = sp_blockchain::Error>,
{
    pub fn new(
        client: Arc<C>,
        keystore: Arc<dyn SyncCryptoStore>,
        provider: &InherentDataProviders,
        authorities: Vec<AuthorityId>,
    ) -> Result<Self, Error> {
        // ensure authorities are properly configured for this chain
        validate_authorities(authorities.as_slice(), keystore.clone())?;

        let slot_duration = slot_duration(&*client).expect("slot duration available");

        let timestamp_provider = SlotTimestampProvider::new(client, *slot_duration)?;

        provider.register_provider(timestamp_provider)?;

        if !provider.has_provider(&AURA_INHERENT_ID) {
            provider
                .register_provider(InherentDataProvider::new(slot_duration.slot_duration()))
                .map_err(|e| Error::StringError(e.into_string()))?;
        }

        Ok(Self {
            _block: Default::default(),
            _client: Default::default(),
        })
    }
}

fn validate_authorities(
    authorities: &[AuthorityId],
    keystore: Arc<dyn SyncCryptoStore>,
) -> Result<(), Error> {
    if authorities.is_empty() {
        return Err(Error::StringError(
            "Cannot supply empty authority set!".into(),
        ));
    }

    if authorities.len() > 1 {
        return Err(Error::StringError(
            "Cannot support multiple authorities currently".into(),
        ));
    }

    // Ensure the keys for the configured authority are actually available
    if let Some(authority_id) = authorities.get(0) {
        if !SyncCryptoStore::has_keys(
            keystore.as_ref(),
            &[(
                authority_id.to_raw_vec(),
                sp_application_crypto::key_types::AURA,
            )],
        ) {
            return Err(Error::StringError(
                "Configured authority key isn't in the keystore".into(),
            ));
        }
    }
    Ok(())
}

impl<B, C> ConsensusDataProvider<B> for AuraConsensusDataProvider<B, C>
where
    B: BlockT,
    C: AuxStore
        + HeaderBackend<B>
        + HeaderMetadata<B, Error = sp_blockchain::Error>
        + ProvideRuntimeApi<B>,
    C::Api: AuraApi<B, AuthorityId>,
{
    type Transaction = TransactionFor<C, B>;

    fn create_digest(
        &self,
        _: &<B as Block>::Header,
        inherents: &InherentData,
    ) -> Result<DigestFor<B>, Error> {
        let slot = inherents.aura_inherent_data()?;

        let logs =
            vec![<DigestItemFor<B> as CompatibleDigestItem<AuthorityPair>>::aura_pre_digest(slot)];

        Ok(Digest { logs })
    }

    fn append_block_import(
        &self,
        _: &<B as Block>::Header,
        _: &mut BlockImportParams<B, Self::Transaction>,
        _: &InherentData,
    ) -> Result<(), Error> {
        // nothing to do for aura
        Ok(())
    }
}

/// Provide duration since unix epoch in millisecond for timestamp inherent.
/// Mocks the timestamp inherent to always produce the timestamp for the next babe slot.
struct SlotTimestampProvider {
    time: atomic::AtomicU64,
    slot_duration: u64,
}

impl SlotTimestampProvider {
    /// create a new mocked time stamp provider.
    fn new<B, C>(client: Arc<C>, slot_duration: u64) -> Result<Self, Error>
    where
        B: BlockT,
        C: AuxStore + HeaderBackend<B> + ProvideRuntimeApi<B>,
        C::Api: AuraApi<B, AuthorityId>,
    {
        let info = client.info();

        // looks like this isn't the first block, rehydrate the fake time.
        // otherwise we'd be producing blocks for older slots.
        let duration = if info.best_number != Zero::zero() {
            let header = client.header(BlockId::Hash(info.best_hash))?.unwrap();
            let slot = find_pre_digest::<B>(&header).unwrap();
            // add the slot duration so there's no collision of slots
            (slot * slot_duration) + slot_duration
        } else {
            // this is the first block, use the correct time.
            let now = SystemTime::now();
            now.duration_since(SystemTime::UNIX_EPOCH)
                .map_err(|err| Error::StringError(format!("{}", err)))?
                .as_millis() as u64
        };

        Ok(Self {
            time: atomic::AtomicU64::new(duration),
            slot_duration,
        })
    }
}

impl ProvideInherentData for SlotTimestampProvider {
    fn inherent_identifier(&self) -> &'static InherentIdentifier {
        &INHERENT_IDENTIFIER
    }

    fn provide_inherent_data(
        &self,
        inherent_data: &mut InherentData,
    ) -> Result<(), sp_inherents::Error> {
        // we update the time here.
        let duration: InherentType = self
            .time
            .fetch_add(self.slot_duration, atomic::Ordering::SeqCst);
        inherent_data.put_data(INHERENT_IDENTIFIER, &duration)?;
        Ok(())
    }

    fn error_to_string(&self, error: &[u8]) -> Option<String> {
        InherentError::try_from(&INHERENT_IDENTIFIER, error).map(|e| format!("{:?}", e))
    }
}

fn find_pre_digest<B: BlockT>(header: &B::Header) -> Result<u64, String> {
    if header.number().is_zero() {
        return Ok(0u64);
    }

    let mut pre_digest: Option<u64> = None;
    for log in header.digest().logs() {
        trace!(target: "aura", "Checking log {:?}", log);
        match (
            CompatibleDigestItem::<AuthorityPair>::as_aura_pre_digest(log),
            pre_digest.is_some(),
        ) {
            (Some(_), true) => return Err(aura_err::<B>("MultipleHeaders".to_string())),
            (None, _) => trace!(target: "aura", "Ignoring digest not meant for us"),
            (s, false) => pre_digest = s.map(Into::into),
        }
    }
    pre_digest.ok_or_else(|| aura_err::<B>("NoDigestFound".to_string()))
}

fn aura_err<B: BlockT>(error: String) -> String {
    debug!(target: "aura", "{}", error);
    error
}
