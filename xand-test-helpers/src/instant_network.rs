use crate::{
    key_ring::XandKeyRing,
    node::{BlockSeal, ExternalityEnv, ExtrinsicSubmitter, FullClient, Node, NodeConfig},
};
use chrono::Utc;
use futures::{channel::oneshot::Sender, future::ready, FutureExt, StreamExt};
use jsonrpc_core::{serde_json, MetaIoHandler};
use petri::petri_persisted_state::PetriPersistedState;
use petri::snapshot::file_snapshotter::{
    FileManagerImpl, FileReaderImpl, FileSnapshotter, FileWriterImpl, SchedulerImpl,
};
use petri::{IdTagCache, Petri, TransactionCache, TxoCache};
use rand::rngs::OsRng;
use sc_client_api::execution_extensions::ExecutionStrategies;
use sc_finality_grandpa::AuthorityId as GrandpaId;
use sp_consensus_aura::sr25519::AuthorityId as AuraId;
use sp_core::{sr25519, Pair};
use sp_state_machine::ExecutionStrategy;
use std::iter::repeat;
use std::path::PathBuf;
use std::sync::atomic::AtomicUsize;
use std::{
    collections::HashMap,
    future::Future,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    time::Duration,
};
use tokio::{runtime::Handle, task::JoinHandle};
use tokio_stream::wrappers::IntervalStream;
use tpfs_krypt::KeyType;
use url::Url;
use xand_api::clear_txo_store::SubstrateClientClearTxOStore;
use xand_api::container::{
    make_server_test, ClearRedeemBuilderProdDeps, CreateBuilderProdDeps, RedeemBuilderProdDeps,
    SendBuilderProdDeps,
};
use xand_api::health::{HealthMonitorImpl, StartupState};
use xand_api::{
    finalized_head_listener, keys::WrappedNetworkKeyResolver, listen_for_initial_block_information,
    XandApiSvc, XandNetworkKeyResolver,
};
use xand_api_client::{ReconnectingXandApiClient, TransactionId, XandApiClientTrait};
use xand_financial_client::{
    clear_redeem_builder::ClearRedeemBuilderImpl, create_builder::CreateBuilderImpl,
    RedeemBuilderImpl, SendBuilderImpl,
};
use xand_financial_client_adapter::{
    financial_client_adapter::new_redeem_mgr, financial_client_adapter::AdapterExtDecoderConfig,
    krypt_adapter::KryptKeyStore, new_create_mgr, new_send_mgr,
};
use xand_models::ExtrinsicDecoderImpl;
use xandstrate::chain_spec::{
    dev_account_key, dev_encryption_key, dev_root_key, get_account_id_from_phrase, get_from_phrase,
    testnet_genesis, AuthorityKeys, ChainSpec,
};
use xandstrate_client::{test_helpers::create_key_manager, KeyMgrSigner, SubstrateClient};
use xandstrate_runtime::GenesisConfig;

pub struct Network {
    pub block_seal: BlockSeal,
    pub externality_env: ExternalityEnv,
    pub api_services: HashMap<XandKeyRing, ReconnectingXandApiClient>,
    pub extrinsic_submitter: ExtrinsicSubmitter,
}

pub struct NetworkBuilder {
    auto_seal: bool,
    use_emission_rewards: bool,
    api_services: HashMap<XandKeyRing, ServiceConfig>,
}

impl NetworkBuilder {
    pub fn add_service<S>(mut self, config: S) -> Self
    where
        S: Into<ServiceConfig>,
    {
        let config = config.into();
        self.api_services.insert(config.account, config);
        self
    }

    pub fn set_auto_seal(mut self, b: bool) -> Self {
        self.auto_seal = b;
        self
    }

    pub fn set_use_emission_rewards(mut self, b: bool) -> Self {
        self.use_emission_rewards = b;
        self
    }

    pub async fn run<F, R>(self, closure: F) -> R::Output
    where
        F: FnOnce(Network) -> R + Send,
        R: Future,
    {
        crate::logging::init_instant_network_logger();
        let chain_spec = network_builder_chainspec(self.use_emission_rewards);

        let node_config = NodeConfig {
            chain_spec: Box::new(chain_spec),
            execution_strategies: ExecutionStrategies {
                syncing: ExecutionStrategy::NativeWhenPossible,
                importing: ExecutionStrategy::NativeWhenPossible,
                block_construction: ExecutionStrategy::AlwaysWasm,
                offchain_worker: ExecutionStrategy::NativeWhenPossible,
                other: ExecutionStrategy::NativeWhenPossible,
            },
            auto_seal: self.auto_seal,
        };

        let apis = self.api_services.clone();
        let node = Node::new(tokio::runtime::Handle::current(), node_config).unwrap();
        let rpc_handler = node.rpc_handler();
        let substrate_client = node.client();
        let (servers, clients) = futures::stream::iter(apis.values())
            .fold(
                (Vec::new(), HashMap::new()),
                |(mut servers, mut clients), config| {
                    let config = config.clone();
                    let substrate_client = substrate_client.clone();
                    let rpc_handler = rpc_handler.clone();
                    async move {
                        let (server, client) =
                            start_xand_api(&config, substrate_client, rpc_handler).await;
                        servers.push(server);
                        clients.insert(config.account, client);
                        (servers, clients)
                    }
                },
            )
            .await;
        let network = Network {
            block_seal: node.block_seal(),
            externality_env: node.externality_env(),
            api_services: clients,
            extrinsic_submitter: node.extrinsic_submitter(),
        };
        let result = closure(network).await;
        for server in servers {
            server.stop().await;
        }
        result
    }
}

static NUM_NODES: AtomicUsize = AtomicUsize::new(1);

fn network_builder_chainspec(use_emissions: bool) -> ChainSpec {
    let mut props = serde_json::map::Map::new();
    // Ensure our default "unit" size in the UI is reasonable (with 2, our "cents" will show up
    // as "dollars" in the UI by default)
    props.insert("tokenDecimals".to_string(), 2.into());
    // Give a nice little X symbol for our currency! I hope you parse unicode!
    props.insert("tokenSymbol".to_string(), "🅧".into());
    let num_nodes = 1;
    let genesis_conf = network_builder_genesis_config(use_emissions);

    xandstrate::chain_spec::NUM_NODES.store(num_nodes, Ordering::SeqCst);
    ChainSpec::from_genesis(
        &format!("Local Confidentiality Testnet - {} Nodes", num_nodes),
        "confidential",
        sc_service::ChainType::Development,
        genesis_conf,
        vec![],
        None,
        None,
        Some(props),
        None,
    )
}

fn generate_validator_keys_from_phrase(phrase: &str) -> AuthorityKeys {
    (
        get_account_id_from_phrase::<sr25519::Public>(phrase),
        get_from_phrase::<AuraId>(&format!("{}aura", phrase)),
        get_from_phrase::<GrandpaId>(phrase),
    )
}

pub fn network_builder_genesis_config(configure_rewards: bool) -> impl Fn() -> GenesisConfig {
    const PAC_ENCRYPTION_KEY: &str = "E8dqeGXb6mmbGVr9anBYP4CukgPC2z9REB4ed2z6m8iP";
    const BIGGIE_ENCRYPTION_KEY: &str = "2YafchPNHsesJe29KeSsB2z5MmPANAzTtnTdYrXjmDSJ";
    const TRUST_ENCRYPTION_KEY: &str = "FrbTj38hA43D4cazHAuF8CQ8SmavEzV9M9vuVjFKAUWA";
    const XAND_ENCRYPTION_KEY: &str = "5y8g94FZsyTfJf6EeVdu733guRZGenXdTdftUSawaPiR";
    const XAND1_ENCRYPTION_KEY: &str = "HASJE7dXifTMGP835rS27rdXMY62EmuDZyGjHXjCrBJ1";

    let auth_keys: Vec<_> = (1..=NUM_NODES.load(Ordering::SeqCst))
        .zip(repeat("Xand"))
        .map(|(i, s)| generate_validator_keys_from_phrase(&format!("//{}{}", s, i)))
        .collect();
    move || {
        testnet_genesis(
            auth_keys.clone(),
            dev_root_key().public().0.into(),
            vec![dev_account_key("2Pac"), dev_account_key("Biggie")],
            dev_account_key("Trust"),
            None,
            vec![],
            vec![
                (
                    dev_account_key("2Pac"),
                    dev_encryption_key(PAC_ENCRYPTION_KEY),
                ),
                (
                    dev_account_key("Biggie"),
                    dev_encryption_key(BIGGIE_ENCRYPTION_KEY),
                ),
                (
                    dev_account_key("Trust"),
                    dev_encryption_key(TRUST_ENCRYPTION_KEY),
                ),
                (
                    dev_account_key("Xand"),
                    dev_encryption_key(XAND_ENCRYPTION_KEY),
                ),
                (
                    dev_account_key("Xand1"),
                    dev_encryption_key(XAND1_ENCRYPTION_KEY),
                ),
            ],
            configure_rewards,
        )
    }
}

impl Default for NetworkBuilder {
    fn default() -> Self {
        NetworkBuilder {
            auto_seal: true,
            use_emission_rewards: false,
            api_services: Default::default(),
        }
    }
}

struct XandApiServerInner {
    petri_shutdown: Arc<AtomicBool>,
    petri_handle: JoinHandle<()>,
    xand_api_shutdown: Sender<()>,
    xand_api_handle: JoinHandle<()>,
}

impl XandApiServerInner {
    // This must return a `Future` and not be marked `async` as the synchronous part of the work
    // needs to happen when the function is called (i.e. even if the return value is not awaited).
    fn stop(self) -> impl Future<Output = ()> {
        // send shutdown signals
        self.xand_api_shutdown.send(()).unwrap();
        self.petri_shutdown.store(true, Ordering::SeqCst);
        let petri_handle = self.petri_handle;
        let xand_api_handle = self.xand_api_handle;
        async move {
            let _ = xand_api_handle.await;
            let _ = petri_handle.await;
        }
    }
}

struct XandApiServer {
    inner: Option<XandApiServerInner>,
    port: u16,
}

impl XandApiServer {
    pub async fn start(
        config: &ServiceConfig,
        client: Arc<FullClient>,
        rpc_handler: Arc<MetaIoHandler<sc_rpc::Metadata, sc_rpc_server::RpcMiddleware>>,
    ) -> Self {
        let txo_cache = TxoCache::default();
        let id_tag_cache = IdTagCache::new(OsRng::default());
        let key_manager = Arc::from(create_key_manager(
            std::iter::once((KeyType::SubstrateSr25519, config.account.seed_phrase()))
                .chain(
                    config
                        .account
                        .encryption_secret()
                        .map(|s| (KeyType::SharedEncryptionX25519, s)),
                )
                .collect(),
        ));
        let key_store = Arc::new(KryptKeyStore::from(Arc::clone(&key_manager)));
        let substrate_client = Arc::new(SubstrateClient::connect_local(rpc_handler).await.unwrap());
        // setup create manager
        let create_manager = new_create_mgr(key_manager.clone(), substrate_client.clone());
        // setup redeem manager
        let redeem_manager = new_redeem_mgr(key_manager.clone(), substrate_client.clone());
        // setup send manager
        let send_manager = new_send_mgr(key_manager.clone(), substrate_client.clone());
        // setup txn cache
        let transaction_cache = Arc::new(TransactionCache::with_transaction_limit(usize::MAX));
        let encryption_key_resolver = WrappedNetworkKeyResolver::from(XandNetworkKeyResolver::new(
            substrate_client.clone(),
            key_manager.clone(),
        ));
        let extrinsic_decoder = ExtrinsicDecoderImpl::<AdapterExtDecoderConfig>::new(
            create_manager.clone(),
            send_manager,
            redeem_manager.clone(),
        );
        let create_builder = CreateBuilderImpl::<CreateBuilderProdDeps> {
            id_tag_selector: id_tag_cache.clone(),
            key_store: key_store.clone(),
            encryption_provider: key_store.clone(),
            network_key_resolver: encryption_key_resolver.clone(),
            member_context: substrate_client.clone().into(),
            rng: futures::lock::Mutex::new(OsRng::default()),
        };
        let send_builder = SendBuilderImpl::<SendBuilderProdDeps>::new(
            id_tag_cache.clone(),
            key_store.clone(),
            txo_cache.clone(),
            key_store.clone(),
            encryption_key_resolver.clone(),
            substrate_client.clone().into(),
            OsRng::default().into(),
        );
        let petri_shutdown = Arc::new(AtomicBool::new(false));

        let state = PetriPersistedState::new(
            transaction_cache.clone(),
            txo_cache.clone(),
            id_tag_cache.clone(),
        );

        let snapshot_dir = PathBuf::default();
        let wait_time = Duration::from_secs(3600);
        let file_snapshotter = FileSnapshotter::<
            FileWriterImpl,
            FileReaderImpl,
            FileManagerImpl,
            SchedulerImpl,
        >::new_standard(snapshot_dir, wait_time);

        let mut petri = Petri::new(
            client,
            extrinsic_decoder,
            petri_shutdown.clone(),
            Some(file_snapshotter),
            state,
        );

        let petri_handle = tokio::spawn(async move {
            petri.start().await;
        });
        let redeem_builder = RedeemBuilderImpl::<RedeemBuilderProdDeps> {
            txo_repo: txo_cache.clone(),
            id_tag_selector: id_tag_cache.clone(),
            key_store: key_store.clone(),
            member_context: substrate_client.clone().into(),
            encryption_provider: key_store.clone(),
            network_key_resolver: encryption_key_resolver.clone(),
            rng: OsRng::default().into(),
        };

        let clear_txo_repo = SubstrateClientClearTxOStore::new(substrate_client.clone());

        let clear_redeem_builder = ClearRedeemBuilderImpl::<ClearRedeemBuilderProdDeps> {
            clear_txo_repo,
            encryption_provider: key_store.clone(),
            network_key_resolver: encryption_key_resolver.clone(),
            rng: OsRng::default().into(),
        };

        let clear_txo_store = SubstrateClientClearTxOStore::new(substrate_client.clone());

        let (initial_block_sender, initial_block_receiver) =
            tokio::sync::watch::channel::<Option<u32>>(None);
        let last_block_info =
            finalized_head_listener::spawn_data_info_listener(substrate_client.clone());

        tokio::spawn(listen_for_initial_block_information(
            last_block_info.clone(),
            initial_block_sender,
        ));

        let startup_state = StartupState {
            block_number: Some(initial_block_receiver),
            startup_timestamp: Utc::now(),
        };

        let last_block_info =
            finalized_head_listener::spawn_data_info_listener(substrate_client.clone());
        let health_monitor =
            HealthMonitorImpl::new(transaction_cache.clone(), startup_state, last_block_info);

        let svc = XandApiSvc::new(
            substrate_client,
            transaction_cache,
            KeyMgrSigner::new(key_manager),
            encryption_key_resolver,
            create_manager,
            redeem_manager,
            txo_cache,
            clear_txo_store,
            create_builder,
            send_builder,
            redeem_builder,
            clear_redeem_builder,
            health_monitor,
        )
        .await;
        let (xand_api_shutdown, signal) = futures::channel::oneshot::channel();
        let signal = signal.map(|r| r.unwrap());

        let (server_fut, port) = make_server_test(0, svc, signal, None)
            .await
            .expect("Xand API server can start");
        info!("Started Xand API server on port {}", port);
        let rt = Handle::current();
        let xand_api_handle = rt.spawn(server_fut.map(|_| ()));

        let inner = Some(XandApiServerInner {
            petri_shutdown,
            petri_handle,
            xand_api_shutdown,
            xand_api_handle,
        });
        XandApiServer { inner, port }
    }

    pub async fn stop(mut self) {
        if let Some(inner) = self.inner.take() {
            inner.stop().await;
        }
    }
}

impl Drop for XandApiServer {
    fn drop(&mut self) {
        if let Some(inner) = self.inner.take() {
            // Just drop the Future as it can't be awaited here. No resource is leaked. There is
            // just no synchronization with the completion of any task internal to the server.
            let _ = inner.stop();
        }
    }
}

async fn start_xand_api(
    config: &ServiceConfig,
    substrate_client: Arc<FullClient>,
    rpc_handler: Arc<MetaIoHandler<sc_rpc::Metadata, sc_rpc_server::RpcMiddleware>>,
) -> (XandApiServer, ReconnectingXandApiClient) {
    let server = XandApiServer::start(config, substrate_client, rpc_handler).await;
    let url = format!("http://127.0.0.1:{}", server.port)
        .parse::<Url>()
        .unwrap();
    let client = ReconnectingXandApiClient::stub(url).await;
    (server, client)
}

#[derive(Clone, Debug)]
pub struct ServiceConfig {
    account: XandKeyRing,
}

impl From<XandKeyRing> for ServiceConfig {
    fn from(account: XandKeyRing) -> Self {
        ServiceConfig { account }
    }
}

pub async fn wait_until_transaction_cached<C>(client: &C, txn_id: &TransactionId)
where
    C: XandApiClientTrait,
{
    let cached = IntervalStream::new(tokio::time::interval(Duration::from_secs(2)))
        .take_while(move |_| async move { client.get_transaction_details(txn_id).await.is_err() })
        .for_each(|_| ready(()));
    if tokio::time::timeout(Duration::from_secs(60), cached)
        .await
        .is_err()
    {
        panic!("Waiting for transaction {} to be cached timed out", txn_id);
    }
}

#[cfg(test)]
mod tests {
    use crate::{instant_network::NetworkBuilder, key_ring::XandKeyRing};
    use xand_api_client::{Send, XandApiClientTrait, XandTransaction};

    #[tokio::test]
    async fn can_submit_txn_with_xand_api() {
        let actual_balance = NetworkBuilder::default()
            .add_service(XandKeyRing::Tupac)
            .add_service(XandKeyRing::Biggie)
            .run(|network| async move {
                let tupac_client = network.api_services.get(&XandKeyRing::Tupac).unwrap();
                let biggie_client = network.api_services.get(&XandKeyRing::Biggie).unwrap();
                let _ = tupac_client
                    .submit_transaction_wait_final(
                        XandKeyRing::Tupac.into(),
                        XandTransaction::Send(Send {
                            destination_account: XandKeyRing::Biggie.into(),
                            amount_in_minor_unit: 0,
                        }),
                    )
                    .await
                    .unwrap();
                biggie_client
                    .get_balance(&XandKeyRing::Biggie.address().to_string())
                    .await
                    .unwrap()
            })
            .await;
        assert_eq!(actual_balance, Some(0));
    }
}
