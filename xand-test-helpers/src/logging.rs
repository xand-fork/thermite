use std::{
    fs::{self, File, OpenOptions},
    path::PathBuf,
    sync::Once,
};
use tracing_subscriber::EnvFilter;

/// Initialize the logger for instant network tests
pub(crate) fn init_instant_network_logger() {
    static LOG_INIT: Once = Once::new();
    LOG_INIT.call_once(|| {
        let filter = EnvFilter::from_default_env();
        tracing_subscriber::fmt().with_env_filter(filter).init();
    });
}

pub fn init_logger() {
    init_instant_network_logger();
}

pub fn get_log_file_handle_for(filename: &str) -> File {
    let mut log_file_handle = mkdir_p_default_log_dir();
    log_file_handle.push(filename);

    // Print location for when tests error
    print_tail_command(log_file_handle.as_os_str().to_str().unwrap());

    OpenOptions::new()
        .create(true)
        .append(true)
        .truncate(false)
        .open(log_file_handle)
        .unwrap()
}

/// Print log file location for when tests error
fn mkdir_p_default_log_dir() -> PathBuf {
    let mut d = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    d.push("logs");
    info!("Checking or creating log directory: {:?}", d);
    fs::create_dir_all(&d).unwrap();
    d
}

/// Print log file location for when tests error
fn print_tail_command(filepath: &str) {
    info!(
        "Log file at {:?}\nFollow it with: tail -f {:?}",
        filepath, filepath
    );
}
