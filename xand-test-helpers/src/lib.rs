//! This crate provides testing helpers meant to be used during integration testing

#![forbid(unsafe_code)]

#[macro_use]
extern crate log;

mod aura_provider;
pub mod instant_network;
mod key_ring;
pub mod logging;
pub mod node;
mod test;
mod utils;

pub use instant_network::{wait_until_transaction_cached, Network, NetworkBuilder};
pub use key_ring::XandKeyRing;
pub use logging::init_logger;
pub use xand_models::BankAccountId;
pub use xandstrate_client;
pub use xandstrate_client::CallExt;

pub const TEST_TIMEOUT_SECS: Duration = Duration::from_secs(30);

use futures::TryStreamExt;
use rand::Rng;
use sp_core::Pair;
use std::time::Duration;
use url::Url;
use xand_runtime_models::FiatReqCorrelationId;
use xandstrate::chain_spec::dev_root_key;
use xandstrate::SUBSTRATE_STARTING_WS_RPC_PORT;

use xandstrate_client::{
    test_helpers::create_key_signer, KeyMgrSigner, SubstrateClient, SubstrateClientInterface,
    SubstrateEventProvider, UnsignedExtrinsic,
};
use xandstrate_runtime::extrinsic_builder::set_validator_emission_rate_extrinsic;

/// Generates random correlation_id for fiat requests
pub fn random_16_byte_correlation_id() -> FiatReqCorrelationId {
    rand::thread_rng().gen::<[u8; 16]>()
}

/// A simple way to ensure some repeatedly-called function doesn't run for longer than a provided
/// timeout. Can be called three different ways:
///
/// # Examples
/// In this example, the code inside the brackets is run repeatedly until `ctr == 5`
/// ```
/// # #[macro_use] extern crate xand_test_helpers;
/// # #[macro_use] extern crate xand_utils;
/// # use xand_utils::timeout;
/// # use std::time::Duration;
/// # fn main() {
/// // This example uses the default overall timeout (30s) and default interval (10ms)
/// let mut ctr = 0;
/// test_timeout!({
///     ctr += 1;
///     ctr == 5
/// });
///
/// // Here, we specify how long the macro should wait between calls to the function
/// ctr = 0;
/// test_timeout!(Duration::from_millis(1), {
///     ctr += 1;
///     ctr == 5
/// });
///
/// // Finally, you can also specify how long the overall timeout should be
/// ctr = 0;
/// test_timeout!(Duration::from_secs(100), Duration::from_millis(1), {
///     ctr += 1;
///     ctr == 5
/// });
/// # }
/// ```
///
#[macro_export]
macro_rules! test_timeout {
    ($timeout:expr, $interval:expr, $f:block) => {
        ::xand_utils::timeout!($timeout, $interval, $f);
    };
    ($interval:expr, $f:block) => {
        ::xand_utils::timeout!($crate::TEST_TIMEOUT_SECS, $interval, $f);
    };
    ($f:block) => {
        ::xand_utils::timeout!($crate::TEST_TIMEOUT_SECS, $f);
    };
}

/// Extension crate for the substrate client providing functionality helpful during testing
#[async_trait::async_trait]
pub trait SubsClientExt {
    async fn wait_until_final(&self, bnum: u32, timeout: Duration);
}

#[async_trait::async_trait]
impl SubsClientExt for SubstrateClient {
    async fn wait_until_final(&self, bnum: u32, timeout: Duration) {
        let task = self
            .subscribe_heads_until(bnum)
            .await
            .expect("Error subscribing heads")
            .try_collect::<Vec<_>>();
        let task = tokio::time::timeout(timeout, task).await;
        task.unwrap_or_else(|_| {
            panic!(
                "Timed out waiting for {} to become final after {:?}",
                bnum, timeout
            )
        })
        .expect("Substrate error watching for final heads");
    }
}

/// Util method to get a client connected to the 0th validator
pub async fn get_connected_client() -> SubstrateClient {
    let validator_0_ws_url = Url::parse(&format!(
        "ws://127.0.0.1:{}",
        SUBSTRATE_STARTING_WS_RPC_PORT
    ))
    .unwrap();
    SubstrateClient::connect(validator_0_ws_url).await.unwrap()
}

pub struct TestClientsManager {
    pub substrate_client: SubstrateClient,
    pub key_manager: KeyMgrSigner,
}

impl TestClientsManager {
    pub async fn new() -> Self {
        info!("Creating client manager");
        TestClientsManager {
            substrate_client: get_connected_client().await,
            key_manager: create_key_signer(),
        }
    }

    pub async fn verify_can_submit_any_transaction_successfully(&self) {
        let client = get_connected_client().await;
        let root = dev_root_key().public();
        let update_emission_extrinsic =
            set_validator_emission_rate_extrinsic(100u64, 1u32).into_sudo();
        let key_manager = create_key_signer();
        let extrinsic = client
            .make_signable(&root, update_emission_extrinsic)
            .await
            .unwrap();

        let res = client
            .submit_and_watch_extrinsic(&extrinsic.sign(&key_manager).unwrap())
            .await
            .unwrap()
            .wait_until_committed()
            .await
            .unwrap();

        let is_success = client.is_extrinsic_success(res).await.unwrap();
        assert!(is_success, "Failed to complete a transaction")
    }

    pub async fn submit_extrinsic(&self, ext: UnsignedExtrinsic) {
        let signed_ext = ext.sign(&self.key_manager).unwrap();
        self.substrate_client
            .submit_and_watch_extrinsic(&signed_ext)
            .await
            .unwrap()
            .wait_until_committed()
            .await
            .unwrap();
    }

    pub async fn wait_for_finalize_block(&self, block_num: u32, timeout: u64) {
        info!("Waiting for {} block(s) to be finalized", block_num);
        self.substrate_client
            .wait_until_final(block_num, Duration::from_secs(timeout))
            .await;
    }
}
