To address RUSTSEC-2022-0035, we forked websocket 0.26.5 and aliased it as 0.24.999 to be compatible with the websocket
dependency that substrate requires (v0.24.0). In doing this, we lose automated audits on the websocket dependency. To
solve this, this crate will pull in websocket 0.26.5 so that it is included in thermite's automated cargo audit.