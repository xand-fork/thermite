#[derive(Debug, serde::Serialize, serde::Deserialize, PartialEq, Eq, Default)]
pub struct Claims {
    /// Subject - defined in jwt standard
    pub sub: String,
    /// Expiration time - defined in jwt standard
    pub exp: u64,

    pub company: String,
}
