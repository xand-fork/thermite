//! The conversions in this module require linking in the entire runtime, and thus are
//! not enabled by default. In order to access the conversions, you must use this crate
//! with the `runtime` feature enabled.
//!
//! This is necessary to decode our transactions. In theory, we can do this without depending
//! on the runtime, but it would involve a substantial amount of work.

pub use xandstrate_runtime::TimestampCall;

use crate::financial_client_mapping::errors::TxnConversionError::FinancialClientError;
use crate::CallDecodeResult::{CouldNotDecode, RootFinancialTransaction};
use crate::{
    convert_type,
    substrate_mapping::{hash_substrate_encodeable, to_platform::map_h256_to_transaction_id},
    substrate_primitives::ToAddress,
    *,
};
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use derive_more::Constructor;
use sp_core::crypto::Ss58Codec;
use std::convert::{TryFrom, TryInto};
use xand_financial_client::{CreateManager, RedeemManager, SendManager};
use xand_public_key::public_key_to_address;
use xandstrate_runtime::{
    self as xandstrate, opaque::get_signer_pubkey_from_extrinsic, Call as AnySubstrateCall,
    ConfidentialityCall, EncryptionPubKey, NetworkVotingCall, Runtime, UncheckedExtrinsic,
    XandCall, XandValidatorsCall,
};
use CallDecodeResult::{DecodedFinancialTransaction, DecodedXandTransaction};

#[async_trait]
pub trait ExtrinsicDecoder {
    async fn decode_extrinsic(
        &self,
        input: UncheckedExtrinsic,
        timestamp: DateTime<Utc>,
    ) -> Result<ExtrinsicDecodeResult, TxnConversionError>;

    async fn decode_call(
        &self,
        call: xandstrate::Call,
    ) -> Result<CallDecodeResult, TxnConversionError>;
}

pub trait ExtrinsicDecoderConfig {
    type CreateManager: CreateManager + Send + Sync;
    type SendManager: SendManager + Send + Sync;
    type RedeemManager: RedeemManager + Send + Sync;
}

#[derive(Clone, Constructor)]
pub struct ExtrinsicDecoderImpl<Config: ExtrinsicDecoderConfig> {
    pub create_manager: Config::CreateManager,
    pub send_manager: Config::SendManager,
    pub redeem_manager: Config::RedeemManager,
}

#[async_trait]
impl<T: ExtrinsicDecoderConfig> ExtrinsicDecoder for ExtrinsicDecoderImpl<T> {
    async fn decode_extrinsic(
        &self,
        input: UncheckedExtrinsic,
        timestamp: DateTime<Utc>,
    ) -> Result<ExtrinsicDecodeResult, TxnConversionError> {
        let hash = hash_substrate_encodeable(&input);
        let transaction_id = map_h256_to_transaction_id(hash);
        let signer_address;
        match self.decode_call(input.function.clone()).await? {
            CouldNotDecode(financial_tx) => Ok(ExtrinsicDecodeResult::CouldNotDecode(financial_tx)),
            DecodedXandTransaction(tx) => {
                signer_address = get_signer_pubkey_from_extrinsic(&input)
                    .map(|x| x.to_ss58check())
                    .ok_or(TxnConversionError::ExtrinsicMissingSigner)?
                    .try_into()?;
                Ok(ExtrinsicDecodeResult::AccountBased(Transaction {
                    signer_address,
                    transaction_id,
                    status: TransactionStatus::Committed,
                    txn: tx,
                    timestamp,
                }))
            }
            DecodedFinancialTransaction(signer_address, xand_tx, financial_tx) => {
                Ok(ExtrinsicDecodeResult::RingSignature(
                    Transaction {
                        signer_address,
                        transaction_id,
                        status: TransactionStatus::Committed,
                        txn: xand_tx,
                        timestamp,
                    },
                    financial_tx,
                ))
            }
            RootFinancialTransaction(financial_tx) => {
                Ok(ExtrinsicDecodeResult::RootTransaction(financial_tx))
            }
        }
    }

    async fn decode_call(
        &self,
        call: xandstrate::Call,
    ) -> Result<CallDecodeResult, TxnConversionError> {
        match call {
            AnySubstrateCall::Confidentiality(confidentiality_call) => match confidentiality_call {
                ConfidentialityCall::request_create(d) => {
                    let tx = d.0;
                    let financial_txn = self
                        .create_manager
                        .reveal_create_request(tx)
                        .await
                        .map_err(|e| FinancialClientError {
                            source: Box::new(e),
                        })?;
                    match &financial_txn.signer {
                        Some(addr) => Ok(DecodedFinancialTransaction(
                            public_key_to_address(addr),
                            financial_txn.txn.clone().try_into()?,
                            financial_txn,
                        )),
                        None => Ok(CouldNotDecode(Some(financial_txn))),
                    }
                }
                ConfidentialityCall::confirm_create(d) => {
                    let tx = d.0;
                    Ok(DecodedXandTransaction(XandTransaction::FulfillCreate(
                        tx.try_into()?,
                    )))
                }
                ConfidentialityCall::cancel_pending_create(d) => {
                    let tx = d.0;
                    Ok(DecodedXandTransaction(XandTransaction::CancelCreate(
                        tx.try_into()?,
                    )))
                }
                ConfidentialityCall::__Ignore(..) => Err(TxnConversionError::NotAXandTransaction {
                    info: format!("{:?}", confidentiality_call),
                }),
                ConfidentialityCall::send(send) => {
                    let send = send.0;
                    let financial_txn = self
                        .send_manager
                        .reveal_send(send)
                        .await
                        .map_err(|e| FinancialClientError { source: e.into() })?;
                    match financial_txn.signer {
                        Some(signer) => Ok(DecodedFinancialTransaction(
                            public_key_to_address(&signer),
                            financial_txn.txn.clone().try_into()?,
                            financial_txn,
                        )),
                        None => Ok(CouldNotDecode(Some(financial_txn))),
                    }
                }
                ConfidentialityCall::redeem(redeem) => {
                    let redeem = redeem.0;
                    let financial_txn = self
                        .redeem_manager
                        .reveal_redeem(redeem)
                        .await
                        .map_err(|e| FinancialClientError { source: e.into() })?;
                    match financial_txn.signer {
                        Some(signer) => Ok(DecodedFinancialTransaction(
                            public_key_to_address(&signer),
                            financial_txn.txn.clone().try_into()?,
                            financial_txn,
                        )),
                        None => Ok(CouldNotDecode(Some(financial_txn))),
                    }
                }
                ConfidentialityCall::redeem_fulfill(d) => {
                    let tx = d.0;
                    Ok(DecodedXandTransaction(XandTransaction::FulfillRedeem(
                        tx.try_into()?,
                    )))
                }
                ConfidentialityCall::cancel_pending_redeem(d) => {
                    let tx = d.0;
                    Ok(DecodedXandTransaction(XandTransaction::CancelRedeem(
                        tx.try_into()?,
                    )))
                }
                ConfidentialityCall::reward(d) => {
                    let tx = d.0;
                    let financial_txn: xand_financial_client::models::Transaction = tx.into();
                    Ok(RootFinancialTransaction(financial_txn))
                }
                ConfidentialityCall::validator_redeem(d) => {
                    let redeem = d.0;
                    let financial_txn: xand_financial_client::models::Transaction = redeem.into();
                    match financial_txn.signer {
                        Some(signer) => Ok(DecodedFinancialTransaction(
                            public_key_to_address(&signer),
                            financial_txn.txn.clone().try_into()?,
                            financial_txn,
                        )),
                        None => Ok(CouldNotDecode(Some(financial_txn))),
                    }
                }
                ConfidentialityCall::set_pending_create_expire_time(d) => {
                    Ok(DecodedXandTransaction(
                        XandTransaction::SetPendingCreateExpire(SetPendingCreateExpire {
                            expire_in_milliseconds: d,
                        }),
                    ))
                }
            },
            // Unwrap sudo call and recursively convert
            AnySubstrateCall::Sudo(pallet_sudo::Call::sudo(sudocall)) => {
                self.decode_call(*sudocall).await
            }
            _ => decode_non_confidential_call(call).map(DecodedXandTransaction),
        }
    }
}

fn decode_xand_call(call: XandCall<Runtime>) -> Result<XandTransaction, TxnConversionError> {
    match call {
        XandCall::register_account_as_member(id, key) => {
            Ok(XandTransaction::RegisterMember(RegisterAccountAsMember {
                address: id.to_ss58check().try_into()?,
                encryption_key: key,
            }))
        }
        XandCall::remove_member(id) => Ok(XandTransaction::RemoveMember(RemoveMember {
            address: id.to_ss58check().try_into()?,
        })),
        XandCall::exit_member(id) => Ok(XandTransaction::ExitMember(ExitMember {
            address: id.to_ss58check().try_into()?,
        })),
        XandCall::set_trust_node_id(id, key) => Ok(XandTransaction::SetTrust(SetTrustNodeId {
            address: id.to_ss58check().try_into()?,
            encryption_key: key,
        })),
        XandCall::set_limited_agent_id(id) => {
            let address = match id {
                Some(addr) => Some(addr.to_ss58check().try_into()?),
                None => None,
            };
            Ok(XandTransaction::SetLimitedAgent(SetLimitedAgentId {
                address,
            }))
        }
        XandCall::set_member_encryption_key(key) => {
            Ok(XandTransaction::SetMemberEncryptionKey(SetMemberEncKey {
                key,
            }))
        }
        XandCall::set_trust_encryption_key(key) => {
            Ok(XandTransaction::SetTrustEncryptionKey(SetTrustEncKey {
                key,
            }))
        }
        XandCall::allowlist_cidr_block(cidr_block) => {
            Ok(XandTransaction::AllowlistCidrBlock(AllowlistCidrBlock {
                cidr_block,
            }))
        }
        XandCall::remove_allowlist_cidr_block(cidr_block) => Ok(
            XandTransaction::RemoveAllowlistCidrBlock(RemoveAllowlistCidrBlock { cidr_block }),
        ),
        XandCall::root_allowlist_cidr_block(account, cidr_block) => Ok(
            XandTransaction::RootAllowlistCidrBlock(RootAllowlistCidrBlock {
                account: account.to_ss58check().try_into()?,
                cidr_block,
            }),
        ),
        XandCall::root_remove_allowlist_cidr_block(account, cidr_block) => Ok(
            XandTransaction::RootRemoveAllowlistCidrBlock(RootRemoveAllowlistCidrBlock {
                account: account.to_ss58check().try_into()?,
                cidr_block,
            }),
        ),
        XandCall::remove_self() => Ok(XandTransaction::WithdrawFromNetwork),
        XandCall::upgrade_runtime(_, _) => unimplemented!(), // TODO - implement transaction for upgrade and remove set_code
        // This is for sure impossible, but included so we don't have a blanket _
        // arm that might hide errors for new transaction types
        XandCall::__Ignore(_, _) => Err(TxnConversionError::NotAXandTransaction {
            info: format!("{:?}", call),
        }),
    }
}

fn decode_xand_validators_call(
    call: XandValidatorsCall<Runtime>,
) -> Result<XandTransaction, TxnConversionError> {
    match call {
        XandValidatorsCall::add_authority_key(account_id) => {
            Ok(XandTransaction::AddAuthorityKey(AddAuthorityKey {
                account_id: account_id.to_ss58check().try_into()?,
            }))
        }
        XandValidatorsCall::remove_authority_key(account_id) => {
            Ok(XandTransaction::RemoveAuthorityKey(RemoveAuthorityKey {
                account_id: account_id.to_ss58check().try_into()?,
            }))
        }
        // This is for sure impossible, but included so we don't have a blanket _
        // arm that might hide errors for new transaction types
        XandValidatorsCall::__Ignore(_, _) => Err(TxnConversionError::NotAXandTransaction {
            info: format!("{:?}", call),
        }),
    }
}

fn decode_network_voting_call(
    call: NetworkVotingCall<Runtime>,
) -> Result<XandTransaction, TxnConversionError> {
    match call {
        NetworkVotingCall::submit_proposal(proposal) => {
            let call = *proposal;
            let txn = decode_non_confidential_call(call)?;
            let txn_type = crate::TransactionType::from(&txn);
            let proposed_action = AdministrativeTransaction::from_transaction(txn)
                .ok_or(TxnConversionError::ExpectedAdminTransaction { txn_type })?;
            Ok(XandTransaction::SubmitProposal(SubmitProposal {
                proposed_action,
            }))
        }
        NetworkVotingCall::vote_proposal(id, vote) => {
            Ok(XandTransaction::VoteProposal(VoteProposal { id, vote }))
        }
        // This is for sure impossible, but included so we don't have a blanket _
        // arm that might hide errors for new transaction types
        NetworkVotingCall::__Ignore(_, _) => Err(TxnConversionError::NotAXandTransaction {
            info: format!("{:?}", call),
        }),
    }
}

fn decode_validator_emissions_call(
    call: xandstrate::validator_emissions::Call<Runtime>,
) -> Result<XandTransaction, TxnConversionError> {
    match call {
        xandstrate::validator_emissions::Call::set_validator_emission_rate(
            minor_units_per_emission,
            block_quota,
        ) => Ok(XandTransaction::SetValidatorEmissionRate(
            SetValidatorEmissionRate {
                minor_units_per_emission,
                block_quota,
            },
        )),
        xandstrate::validator_emissions::Call::__Ignore(_, _) => {
            Err(TxnConversionError::NotAXandTransaction {
                info: format!("{:?}", call),
            })
        }
    }
}

pub fn decode_non_confidential_call(
    call: xandstrate::Call,
) -> Result<XandTransaction, TxnConversionError> {
    match call {
        AnySubstrateCall::XandStrate(call) => decode_xand_call(call),
        AnySubstrateCall::XandValidators(call) => decode_xand_validators_call(call),
        AnySubstrateCall::NetworkVoting(call) => decode_network_voting_call(call),
        AnySubstrateCall::ValidatorEmissions(call) => decode_validator_emissions_call(call),
        AnySubstrateCall::Sudo(pallet_sudo::Call::sudo(call)) => {
            decode_non_confidential_call(*call)
        }
        AnySubstrateCall::Session(pallet_session::Call::set_keys(keys, ..)) => {
            Ok(XandTransaction::RegisterSessionKeys(RegisterSessionKeys {
                block_production_pubkey: sp_core::sr25519::Public::from(keys.aura).to_address(),
                block_finalization_pubkey: sp_core::ed25519::Public::from(keys.grandpa)
                    .to_address(),
            }))
        }
        AnySubstrateCall::Confidentiality(_) => {
            Err(TxnConversionError::UnexpectedConfidentialTransaction)
        }
        AnySubstrateCall::Authorship(_)
        | AnySubstrateCall::Grandpa(_)
        | AnySubstrateCall::RandomnessCollectiveFlip(_)
        | AnySubstrateCall::Session(_)
        | AnySubstrateCall::Sudo(_)
        | AnySubstrateCall::System(_)
        | AnySubstrateCall::Timestamp(_) => Err(TxnConversionError::NotAXandTransaction {
            info: format!("{:?}", call),
        }),
        #[cfg(feature = "test-upgrade-pallet")]
        AnySubstrateCall::TestUpgrade(_) => Err(TxnConversionError::NotAXandTransaction {
            info: format!("{:?}", call),
        }),
    }
}

#[allow(clippy::large_enum_variant)]
#[derive(Debug)]
pub enum ExtrinsicDecodeResult {
    CouldNotDecode(Option<xand_financial_client::models::Transaction>),
    AccountBased(Transaction),
    RingSignature(Transaction, xand_financial_client::models::Transaction),
    RootTransaction(xand_financial_client::models::Transaction),
}

impl ExtrinsicDecodeResult {
    pub fn get_financial_transaction(&self) -> Option<&xand_financial_client::models::Transaction> {
        match &self {
            ExtrinsicDecodeResult::CouldNotDecode(tx) => tx.as_ref(),
            ExtrinsicDecodeResult::AccountBased(_) => None,
            ExtrinsicDecodeResult::RingSignature(_, tx) => Some(tx),
            ExtrinsicDecodeResult::RootTransaction(tx) => Some(tx),
        }
    }

    pub fn get_xand_transaction(&self) -> Option<&Transaction> {
        match &self {
            ExtrinsicDecodeResult::CouldNotDecode(_) => None,
            ExtrinsicDecodeResult::AccountBased(tx) => Some(tx),
            ExtrinsicDecodeResult::RingSignature(tx, _) => Some(tx),
            ExtrinsicDecodeResult::RootTransaction(_) => None,
        }
    }
}

pub enum CallDecodeResult {
    CouldNotDecode(Option<xand_financial_client::models::Transaction>),
    DecodedXandTransaction(XandTransaction),
    DecodedFinancialTransaction(
        Address,
        XandTransaction,
        xand_financial_client::models::Transaction,
    ),
    RootFinancialTransaction(xand_financial_client::models::Transaction),
}

impl TryFrom<CallDecodeResult> for XandTransaction {
    type Error = TxnConversionError;

    fn try_from(res: CallDecodeResult) -> Result<Self, Self::Error> {
        match res {
            CouldNotDecode(Some(txn)) => Err(TxnConversionError::ConfidentialTransactionError {
                txn: Box::new(txn.txn),
            }),
            CouldNotDecode(None) => Err(TxnConversionError::UnexpectedConfidentialTransaction),
            DecodedXandTransaction(tx) => Ok(tx),
            DecodedFinancialTransaction(_, tx, _) => Ok(tx),
            RootFinancialTransaction(_) => {
                Err(TxnConversionError::UnexpectedConfidentialTransaction)
            }
        }
    }
}

/// Extracts the timestamp intrinsic from a list of (in/ex)trinsics in a block
pub fn timestamp_from_extrinsics<'a, I>(extrinsics: I) -> Option<DateTime<Utc>>
where
    I: IntoIterator<Item = &'a UncheckedExtrinsic>,
{
    extrinsics
        .into_iter()
        .find_map(|e| match e {
            UncheckedExtrinsic {
                function: xandstrate_runtime::Call::Timestamp(TimestampCall::set(t)),
                ..
            } => Some(convert_type(*t).ok()?),
            _ => None,
        })
        .map(Transaction::timestamp_from_unix_time_millis)
}

impl From<EncryptionPubKey> for EncryptionKey {
    fn from(pub_key: EncryptionPubKey) -> Self {
        let bytes = pub_key.as_bytes();
        EncryptionKey(*bytes)
    }
}
