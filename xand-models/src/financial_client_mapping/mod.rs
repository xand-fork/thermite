pub mod errors;

use crate::financial_client_mapping::errors::TxnConversionError;
use crate::{
    BankAccountId, BankAccountInfo, CancelPendingCreate, CancelPendingRedeem,
    EncryptionKey as XMEncryptionKey, FulfillPendingCreate, FulfillPendingRedeem,
    PendingCreate as XMPendingCreate, PendingRedeem, SendSchema as XandSend, XandTransaction,
};
use std::convert::{TryFrom, TryInto};
use tpfs_krypt::Encrypted;
use xand_financial_client::models::{
    PendingClearRedeem, PendingConfidentialRedeem, PendingRedeem as XFCPendingRedeem,
};
use xand_financial_client::{
    errors::FinancialClientError,
    models::{
        BankAccount, ConfidentialPayload, EncryptionKey, FinancialTransaction,
        PendingCreate as FCPendingCreate, SendModel,
    },
};
use xand_ledger::{
    CashConfirmationTransaction, CreateCancellationTransaction, RedeemCancellationTransaction,
    RedeemFulfillmentTransaction,
};

impl TryFrom<FinancialTransaction> for XandTransaction {
    type Error = TxnConversionError;

    fn try_from(tx: FinancialTransaction) -> Result<Self, Self::Error> {
        Ok(match tx {
            FinancialTransaction::CreateRequest(d) => {
                XandTransaction::CreatePendingCreate(d.try_into()?)
            }
            FinancialTransaction::Create(create) => XandTransaction::FulfillCreate(create.into()),
            FinancialTransaction::CreateCancellation(create_cancellation) => {
                XandTransaction::CancelCreate(create_cancellation.into())
            }
            FinancialTransaction::Send(send) => {
                let send = send.ok_or_else(|| TxnConversionError::FinancialClientError {
                    source: FinancialClientError::UnauthorizedDecryption.into(),
                })?;
                XandTransaction::Send(send.into())
            }
            FinancialTransaction::Redeem(redeem) => {
                XandTransaction::CreatePendingRedeem(redeem.into())
            }
            FinancialTransaction::RedeemFulfillment(redeem_fulfill) => {
                XandTransaction::FulfillRedeem(redeem_fulfill.into())
            }
            FinancialTransaction::RedeemCancellation(cancellation) => {
                XandTransaction::CancelRedeem(cancellation.into())
            }
            FinancialTransaction::RewardValidator(_) => {
                return Err(TxnConversionError::NotAXandTransaction {
                    info: "A RewardTransaction cannot be converted to a XandTransaction"
                        .to_string(),
                });
            }
        })
    }
}

impl From<ConfidentialPayload<BankAccount>> for BankAccountInfo {
    fn from(bank_account: ConfidentialPayload<BankAccount>) -> Self {
        match bank_account {
            ConfidentialPayload::Decrypted(dec) => BankAccountInfo::Unencrypted(dec.into()),
            ConfidentialPayload::Encrypted(enc) => Encrypted::from_bytes_unchecked(enc.as_slice())
                .map(BankAccountInfo::Encrypted)
                .unwrap_or_else(|_| BankAccountInfo::Malformed(enc.into())),
        }
    }
}

impl From<BankAccount> for BankAccountId {
    fn from(account: BankAccount) -> Self {
        BankAccountId {
            routing_number: account.routing_number,
            account_number: account.account_number,
        }
    }
}

impl From<EncryptionKey> for XMEncryptionKey {
    fn from(key: EncryptionKey) -> Self {
        XMEncryptionKey(*key.as_bytes())
    }
}

impl From<XMEncryptionKey> for EncryptionKey {
    fn from(key: XMEncryptionKey) -> Self {
        key.0.into()
    }
}

impl From<FCPendingCreate> for XMPendingCreate {
    fn from(d: FCPendingCreate) -> XMPendingCreate {
        XMPendingCreate {
            amount_in_minor_unit: d.amount_in_minor_unit,
            correlation_id: d.correlation_id.into(),
            account: d.account.into(),
            completing_transaction: None,
        }
    }
}

impl From<CashConfirmationTransaction> for FulfillPendingCreate {
    fn from(create: CashConfirmationTransaction) -> Self {
        FulfillPendingCreate {
            correlation_id: create.correlation_id.into(),
        }
    }
}

impl From<CreateCancellationTransaction> for CancelPendingCreate {
    fn from(create_cancellation: CreateCancellationTransaction) -> Self {
        CancelPendingCreate {
            correlation_id: create_cancellation.correlation_id.into(),
            reason: create_cancellation.reason,
        }
    }
}

impl From<SendModel> for XandSend {
    fn from(send: SendModel) -> Self {
        let destination_account = xand_public_key::public_key_to_address(&send.recipient);
        XandSend {
            destination_account,
            amount_in_minor_unit: send.amount_in_minor_unit,
        }
    }
}

impl From<XFCPendingRedeem> for PendingRedeem {
    fn from(red: XFCPendingRedeem) -> Self {
        match red {
            XFCPendingRedeem::Confidential(r) => r.into(),
            XFCPendingRedeem::Clear(r) => PendingRedeem {
                amount_in_minor_unit: r.amount_in_minor_unit,
                correlation_id: r.correlation_id.into(),
                account: r.account.into(),
                completing_transaction: None,
            },
        }
    }
}

impl From<PendingConfidentialRedeem> for PendingRedeem {
    fn from(r: PendingConfidentialRedeem) -> Self {
        Self {
            amount_in_minor_unit: r.amount_in_minor_unit,
            correlation_id: r.correlation_id.into(),
            account: r.account.into(),
            completing_transaction: None,
        }
    }
}

impl From<PendingClearRedeem> for PendingRedeem {
    fn from(r: PendingClearRedeem) -> Self {
        Self {
            amount_in_minor_unit: r.amount_in_minor_unit,
            correlation_id: r.correlation_id.into(),
            account: r.account.into(),
            completing_transaction: None,
        }
    }
}

impl From<RedeemFulfillmentTransaction> for FulfillPendingRedeem {
    fn from(fulfillment: RedeemFulfillmentTransaction) -> Self {
        FulfillPendingRedeem {
            correlation_id: fulfillment.correlation_id.into(),
        }
    }
}

impl From<RedeemCancellationTransaction> for CancelPendingRedeem {
    fn from(cancellation: RedeemCancellationTransaction) -> Self {
        CancelPendingRedeem {
            correlation_id: cancellation.correlation_id.into(),
            reason: cancellation.reason,
        }
    }
}
