use crate::BankAccountInfo;
use xand_api_proto::proto_models::{
    errors::EncryptionError, BankAccountId, BankAccountInfo as ClientBankAccountInfo,
};

impl From<crate::BankAccountInfo> for ClientBankAccountInfo {
    fn from(val: BankAccountInfo) -> Self {
        match val {
            BankAccountInfo::Encrypted(_) => {
                ClientBankAccountInfo::Encrypted(EncryptionError::KeyNotFound)
            }
            BankAccountInfo::Malformed(_) => {
                ClientBankAccountInfo::Encrypted(EncryptionError::MessageMalformed)
            }
            BankAccountInfo::Unencrypted(info) => ClientBankAccountInfo::Unencrypted({
                BankAccountId {
                    routing_number: info.routing_number.to_string(),
                    account_number: info.account_number,
                }
            }),
        }
    }
}
