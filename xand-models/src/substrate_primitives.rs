//! Module is only compiled when `sp-core` or `runtime-conversions` features are
//! enabled. Uses the substrate_primitives crate to help with conversions, etc.

use crate::EncryptionKey;
use sp_core::{
    crypto::{AccountId32, Ss58Codec},
    ed25519, sr25519, Pair,
};
use std::convert::TryInto;
use tpfs_krypt::secrecy::Secret;
use tpfs_krypt::{KeyPair, KeyPairInstance, KeyType};
use xand_address::Address;

/// Generates a sr25519 keypair using substrate's default dev passphrase, and the provided string
/// as a sub-path. Note that this is *very slow* and should not be done in a tight loop in tests.
///
/// # Only use this for testing!
pub fn dev_account_key(s: &str) -> sr25519::Pair {
    sr25519::Pair::from_string(&format!("//{}", s), None)
        .expect("Any string is a valid subpath for an account key")
}

/// # Only use this for testing!
pub fn dev_address(s: &str) -> Address {
    dev_account_key(s).public().to_address()
}

/// # Only use this for testing!
pub fn dev_encryption_key(s: &str) -> EncryptionKey {
    // hd paths are not supported for encryption keys
    let kp =
        KeyPairInstance::from_secret(Secret::new(s.to_string()), KeyType::SharedEncryptionX25519)
            .unwrap();

    kp.public().as_slice().try_into().unwrap()
}

/// Trait to convert to `Address`
pub trait ToAddress {
    fn to_address(&self) -> Address;
}

impl ToAddress for sr25519::Public {
    fn to_address(&self) -> Address {
        self.to_ss58check()
            .try_into()
            .expect("sr25519::Public is convertible to Address")
    }
}

impl ToAddress for ed25519::Public {
    fn to_address(&self) -> Address {
        self.to_ss58check()
            .try_into()
            .expect("ed25519::Public is convertible to Address")
    }
}

impl ToAddress for AccountId32 {
    fn to_address(&self) -> Address {
        self.to_ss58check()
            .try_into()
            .expect("AccountId32 is convertible to Address")
    }
}
