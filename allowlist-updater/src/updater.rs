use crate::{
    errors::*, logging_events::LoggingEvent, publisher::AllowListPublisher,
    retriever::CidrBlockRetriever,
};
use std::{thread::sleep, time::Duration};

/// Generic updater. Will retrieve CIDR blocks using specified `CidrBlockRetriever` and publish those
/// blocks with specified `AllowListPublisher` on a specified `cadence` in seconds.
pub struct AllowListUpdater<R, P>
where
    R: CidrBlockRetriever,
    P: AllowListPublisher,
{
    retriever: R,
    publisher: P,
    cadence: Duration,
}

impl<R: CidrBlockRetriever, P: AllowListPublisher> AllowListUpdater<R, P> {
    pub fn new(retriever: R, publisher: P, cadence: Duration) -> Self {
        Self {
            retriever,
            publisher,
            cadence,
        }
    }

    pub fn run(&self) {
        loop {
            match self.update() {
                Ok(_) => (),
                Err(e) => error!(LoggingEvent::AllowlistUpdaterError(e)),
            }
            sleep(self.cadence);
        }
    }

    pub fn update(&self) -> Result<()> {
        let blocks = self.retriever.retrieve_cidr_blocks()?;
        self.publisher.publish(blocks)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pseudo::Mock;
    use xand_runtime_models::CidrBlock;

    pub struct MockPublisher {
        pub mock_publish: Mock<Vec<CidrBlock>, Result<()>>,
    }

    impl MockPublisher {
        pub fn new(res: Result<()>) -> Self {
            Self {
                mock_publish: Mock::new(res),
            }
        }
    }

    impl AllowListPublisher for MockPublisher {
        fn publish(&self, blocks: Vec<CidrBlock>) -> Result<()> {
            self.mock_publish.call(blocks)
        }
    }

    pub struct MockRetriever {
        mock_retrieve_cidr_blocks: Mock<(), Result<Vec<CidrBlock>>>,
    }

    impl MockRetriever {
        pub fn new(blocks: Result<Vec<CidrBlock>>) -> Self {
            Self {
                mock_retrieve_cidr_blocks: Mock::new(blocks),
            }
        }
    }

    impl CidrBlockRetriever for MockRetriever {
        fn retrieve_cidr_blocks(&self) -> Result<Vec<CidrBlock>> {
            self.mock_retrieve_cidr_blocks.call(())
        }
    }

    #[test]
    fn test_update() {
        let all_blocks = Vec::new();
        let retriever = MockRetriever::new(Ok(all_blocks));
        let publisher = MockPublisher::new(Ok(()));
        let cadence = Duration::from_secs(1);
        let updater = AllowListUpdater::new(retriever, publisher, cadence);
        assert!(updater.update().is_ok());
    }
}
