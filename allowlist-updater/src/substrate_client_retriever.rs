use crate::errors::*;
use crate::retriever::CidrBlockRetriever;

use futures::executor::block_on;
use snafu::ResultExt;
use xand_runtime_models::CidrBlock;
use xandstrate_client::SubstrateClientInterface;

pub struct JsonRpcRetriever<C: SubstrateClientInterface> {
    client: C,
}

impl<C: SubstrateClientInterface> JsonRpcRetriever<C> {
    pub fn new(client: C) -> Self {
        Self { client }
    }
}

impl<C: SubstrateClientInterface> CidrBlockRetriever for JsonRpcRetriever<C> {
    fn retrieve_cidr_blocks(&self) -> Result<Vec<CidrBlock>> {
        block_on(self.client.get_allowlisted_cidr_blocks()).context(Rpc)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pseudo::Mock;
    use xandstrate_client::{mock::MockSubstrateClient, xand_models::dev_address};

    #[test]
    fn test_retrieve_cidr_blocks() {
        let block1: CidrBlock = [1, 2, 3, 4, 5].into();
        let block2: CidrBlock = [5, 4, 3, 2, 1].into();
        let tupac = dev_address("2Pac");
        let biggie = dev_address("Biggie");
        let blocks = vec![(tupac, block1), (biggie, block2)];
        let client = MockSubstrateClient {
            mock_get_cidr_blocks_per_address: Mock::new(Ok(blocks)),
            ..Default::default()
        };
        let retriever = JsonRpcRetriever::new(client);
        let got_blocks = retriever.retrieve_cidr_blocks().unwrap();
        assert!(got_blocks.contains(&block1));
        assert!(got_blocks.contains(&block2));
        assert_eq!(got_blocks.len(), 2);
    }
}
