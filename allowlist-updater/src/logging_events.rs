use crate::errors::Error;

#[logging_event]
pub enum LoggingEvent {
    AllowlistUpdaterError(Error),
}
