//! # Allowlist Sidecar Updater
//!
//! This app is for updating allowlisted CIDR blocks. It does this by retrieving the blocks and publishing
//! them on a regular cadence.
//!
//! The default retriever uses the substrate client to get the CIDR blocks via the validator JSON RPC.
//!
//! The default publisher formats the CIDR blocks for a nginx config file and writes to specified file.
//!
//! Additional configuration data will be loaded from a config file. The path to the config file is
//! passed as the first argument of the executable.
//!
//! e.g. `cargo run ./config`
//!

#![forbid(unsafe_code)]

pub(crate) mod config;
pub mod errors;
pub(crate) mod logging_events;
pub mod updater;

pub mod nginx_config_publisher;
pub mod publisher;

pub mod retriever;
pub mod substrate_client_retriever;

extern crate config as cfg;
#[macro_use]
extern crate tpfs_logger_port;
#[macro_use]
extern crate clap;

use crate::config::{config_reader, SubstrateClientRetriever};
use clap::App as Clapp;
use panics::set_log_panics_hook;

fn main() {
    set_log_panics_hook();

    tpfs_logger_log4rs_adapter::init_with_default_config().expect("Must be able to init logger");

    let version = env!("CARGO_PKG_VERSION");
    let matches = clap_app!(@app(Clapp::new("ALLOWLIST UPDATER"))
        (version: version)
        (about: "Updating allowlist configuration for nginx server with CIDR blocks")
        (@arg config_path: +takes_value default_value("config")
        "Set to a directory containing config files (ex: /some/place/with/config)"))
    .get_matches();
    let config_path = matches.value_of("config_path").unwrap();
    let config: SubstrateClientRetriever =
        config_reader(config_path).expect("No config at specified location");
    let updater = config.create_updater().unwrap();
    updater.run();
}
