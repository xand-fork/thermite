use crate::{errors::XandApiErrors, logging_events::LoggingEvent};
use futures::{
    channel::{mpsc, oneshot},
    future::ready,
    SinkExt, Stream, StreamExt,
};
use petri::HistoricalStore;
use std::{collections::HashMap, mem, sync::Arc, time::Duration};
use tokio_stream::wrappers::IntervalStream;
use tonic::{Code, Status};
use tpfs_logger_port::{error, info};
use tracing_futures::Instrument;
use xand_api_proto::{self as xap, StatusExt, TransactionStatus, TransactionUpdate};
use xand_models::TransactionId;
use xandstrate_client::{
    extrinsic_states::TransactionStatus as ExtTransactionStatus, sr25519, KeyMgrSigner,
    SubmittedExtrinsic, SubstrateClientInterface, UncheckedExtrinsic,
};

/// Contains services to process enqueued transaction requests and submit them to substrate.
///
/// Transaction requests are pushed into a main queue and handled asynchronously. Requests are
/// dequeued from this main queue one by one and either enqueued into a per-issuer queue or
/// submitted to substrate directly when the issuer is unknown.
///
/// The per-issuer queues ensure that, for a given issuer, transaction `n + 1` is submitted to
/// substrate only after transaction `n` is committed or rejected, which helps with assigning the
/// right account nonce with each transaction given that substrate expects them to be a sequence of
/// increasing and consecutive numbers.
pub(crate) struct SubmissionHandler<C, H> {
    substrate_client: Arc<C>,
    txn_cache: Arc<H>,
    key_manager: KeyMgrSigner,
}

#[derive(Debug)]
pub(crate) enum SubmissionRequest {
    NeedsSigning(crate::proto_help::CallWrapper, sr25519::Public),
    DontSign(crate::proto_help::CallWrapper),
}

#[derive(derive_more::Constructor)]
pub(crate) struct Submission {
    request: SubmissionRequest,
    notify: mpsc::Sender<Result<TransactionUpdate, Status>>,
    tracing_span: tracing::Span,
}

impl<C, H> SubmissionHandler<C, H>
where
    C: SubstrateClientInterface + 'static,
    H: HistoricalStore + 'static,
{
    pub fn new(substrate_client: Arc<C>, txn_cache: Arc<H>, key_manager: KeyMgrSigner) -> Self {
        SubmissionHandler {
            substrate_client,
            txn_cache,
            key_manager,
        }
    }

    /// Dequeues submissions from the main queue.
    pub async fn run<S>(self, submissions: S)
    where
        S: Stream<Item = Submission>,
    {
        let handler = Arc::new(self);
        let mut issuer_queues = HashMap::new();
        futures::pin_mut!(submissions);
        while let Some(submission) = submissions.next().await {
            handler
                .clone()
                .dispatch_submission(submission, &mut issuer_queues)
                .await;
        }
    }

    /// Enqueues the submission into a per-issuer queue, or processes it if its issuer is unknown.
    async fn dispatch_submission(
        self: Arc<Self>,
        submission: Submission,
        issuer_queues: &mut HashMap<sr25519::Public, mpsc::Sender<Submission>>,
    ) {
        let issuer = match submission.request {
            SubmissionRequest::NeedsSigning(_, issuer) => Some(issuer),
            SubmissionRequest::DontSign(_) => None,
        };
        match issuer {
            Some(issuer) => {
                let _ = issuer_queues
                    .entry(issuer)
                    .or_insert_with(|| self.make_issuer_queue())
                    .send(submission)
                    .await;
            }
            None => {
                let task = async move {
                    let task =
                        self.perform_submission(submission.request, submission.notify, || ());
                    task.instrument(submission.tracing_span).await
                };
                tokio::spawn(task);
            }
        }
    }

    /// Returns a per-issuer transaction queue and spawns a task to dequeue and process
    /// transactions for this issuer.
    ///
    /// Adapts the reading end of the per-issuer queue so that each request is turned into a
    /// `Future` that completes when the status update stream for this request is exhausted.
    ///
    /// The stream also needs to enforce that request `n + 1` is not submitted before request `n`
    /// is committed or rejected. That's what the oneshot channel is for. When transaction `n` is
    /// committed or otherwise done, the oneshot sender is dropped, causing the request stream
    /// waiting on the oneshot receiver to resume and yield the next request ready to be submitted
    /// to substrate.
    fn make_issuer_queue(self: Arc<Self>) -> mpsc::Sender<Submission> {
        const ISSUER_QUEUE_CAPACITY: usize = 100;
        let (sender, receiver) = mpsc::channel::<Submission>(ISSUER_QUEUE_CAPACITY);
        // `previous_txn_committed` is captured by the closure passed to `then`. It needs to have
        // an initial value and be ready (achieved by immediately dropping its associated sender).
        // Each yielded request replaces it with a new oneshot receiver that becomes ready when the
        // transaction is committed or done.
        let (_, mut previous_txn_committed) = oneshot::channel::<()>();
        let receiver = receiver.then(move |submission| {
            let (on_committed_sender, on_committed_receiver) = oneshot::channel();
            let previous_txn_done =
                mem::replace(&mut previous_txn_committed, on_committed_receiver);
            let handler = self.clone();
            // Note to clippy: A `Future` yielding a `Future` can be useful. Here it is used to
            // pop submissions from the stream only when they are ready to be submitted.
            #[allow(clippy::async_yields_async)]
            async move {
                let _ = previous_txn_done.await;
                async move {
                    let task =
                        handler.perform_submission(submission.request, submission.notify, || {
                            drop(on_committed_sender)
                        });
                    task.instrument(submission.tracing_span).await
                }
            }
        });
        let submissions = receiver.for_each_concurrent(None, |f| f);
        tokio::spawn(submissions);
        sender
    }

    /// Builds the extrinsic corresponding to the request, signs it if needed, submits it to
    /// substrate and forwards transaction status updates to the requester.
    async fn perform_submission<F>(
        &self,
        request: SubmissionRequest,
        mut notify: mpsc::Sender<Result<TransactionUpdate, Status>>,
        on_committed: F,
    ) where
        F: FnOnce(),
    {
        let extrinsic = match request {
            SubmissionRequest::NeedsSigning(call, issuer) => match self.sign(call, issuer).await {
                Ok(extrinsic) => extrinsic,
                Err(e) => {
                    let _ = notify.send(Err(e.into())).await;
                    return;
                }
            },
            SubmissionRequest::DontSign(call) => match self.to_unsigned_extrinsic(call).await {
                Ok(extrinsic) => extrinsic,
                Err(e) => {
                    let _ = notify.send(Err(e.into())).await;
                    return;
                }
            },
        };
        let submitted_ext = match self
            .substrate_client
            .submit_and_watch_extrinsic(&extrinsic)
            .await
        {
            Ok(submission) => submission,
            Err(e) => {
                let _ = notify.send(Err(XandApiErrors::from(e).into())).await;
                return;
            }
        };
        let transaction_id = TransactionId::from(submitted_ext.hash);
        let task =
            self.send_transaction_updates(submitted_ext, transaction_id, notify, on_committed);
        task.await
    }

    async fn to_unsigned_extrinsic(
        &self,
        call: crate::proto_help::CallWrapper,
    ) -> Result<UncheckedExtrinsic, XandApiErrors> {
        Ok(self.substrate_client.make_unsignable(call.0).await?)
    }

    async fn sign(
        &self,
        call: crate::proto_help::CallWrapper,
        issuer: sr25519::Public,
    ) -> Result<UncheckedExtrinsic, XandApiErrors> {
        let extrinsic = self
            .substrate_client
            .make_signable(&issuer, call.0)
            .await?
            .sign(&self.key_manager)?;
        Ok(extrinsic)
    }

    async fn send_transaction_updates<F>(
        &self,
        submitted_ext: SubmittedExtrinsic,
        transaction_id: TransactionId,
        mut notify: mpsc::Sender<Result<TransactionUpdate, Status>>,
        on_committed: F,
    ) where
        F: FnOnce(),
    {
        let mut on_committed = Some(on_committed);
        let mut updates = submitted_ext.get_stream();
        while let Some(update) = updates.next().await {
            let (update, done) = match update {
                Ok(status) => {
                    self.parse_transaction_update(&transaction_id, status, &mut on_committed)
                        .await
                }
                Err(subs_client_err) => {
                    let error_message = format!("{:?}", subs_client_err);
                    error!(LoggingEvent::SubstrateClientError(error_message.clone()));
                    (Err(Status::new(Code::Internal, error_message)), true)
                }
            };
            if done && update.is_ok() {
                let cached = self.txn_cache.wait_for_transaction(transaction_id.clone());
                let status_update_receiver_closed =
                    IntervalStream::new(tokio::time::interval(Duration::from_secs(30)))
                        .take_while(|_| ready(!notify.is_closed()))
                        .map(|_| ())
                        .for_each(|_| ready(()));
                futures::future::select(cached, status_update_receiver_closed).await;
            }
            let failed_to_send_update = match update.transpose() {
                Some(update) => notify.send(update).await.is_err(),
                None => false,
            };
            if done || failed_to_send_update {
                break;
            }
        }
    }

    async fn parse_transaction_update<F>(
        &self,
        transaction_id: &TransactionId,
        status: ExtTransactionStatus,
        on_committed: &mut Option<F>,
    ) -> (Result<Option<TransactionUpdate>, Status>, bool)
    where
        F: FnOnce(),
    {
        let status = match status {
            ExtTransactionStatus::Future | ExtTransactionStatus::Ready => {
                Ok(Some(xap::TransactionAccepted {}.into()))
            }
            ExtTransactionStatus::Broadcast(broadcast) => Ok(Some(
                xap::TransactionBroadcast {
                    validator_ids: broadcast,
                }
                .into(),
            )),
            ExtTransactionStatus::InBlock(_in_block) => {
                Ok(Some(xap::TransactionCommitted {}.into()))
            }
            ExtTransactionStatus::Retracted(..) => Ok(None),
            ExtTransactionStatus::FinalityTimeout(_block) => Err(Status::deadline_exceeded(
                format!("Finality timed out for transaction {}", transaction_id,),
            )),
            ExtTransactionStatus::Finalized(_block) => {
                Ok(Some(xap::TransactionFinalized {}.into()))
            }
            ExtTransactionStatus::Usurped(_) => Ok(None),
            ExtTransactionStatus::Dropped => Ok(Some(
                xap::TransactionDropped {
                    reason: "Unknown".into(),
                }
                .into(),
            )),
            ExtTransactionStatus::Invalid => Ok(Some(
                xap::TransactionInvalid {
                    reason: "Unknown".into(),
                }
                .into(),
            )),
        };
        match status {
            Ok(Some(status)) => {
                let committed = matches!(status, xap::transaction_status::Status::Committed(_));
                let done = status.is_final();
                if done || committed {
                    info!(LoggingEvent::TransactionStatus {
                        status: format!("{:?}", status),
                    });
                }
                if committed {
                    if let Some(notify_committed) = on_committed.take() {
                        notify_committed();
                    }
                }
                let update = TransactionUpdate {
                    id: transaction_id.to_string(),
                    status: Some(TransactionStatus {
                        status: Some(status),
                    }),
                };
                (Ok(Some(update)), done)
            }
            Ok(None) => (Ok(None), false),
            Err(e) => (Err(e), true),
        }
    }
}
