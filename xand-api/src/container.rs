use crate::api_middleware::HealthCheckMiddlewareLayer;
use crate::{
    clear_txo_store::{ClearTxOStore, SubstrateClientClearTxOStore},
    finalized_head_listener,
    health::HealthMonitorImpl,
    jwt::JwtAuthInterceptor,
    keys::{NetworkKeyResolver, WrappedNetworkKeyResolver},
    listen_for_initial_block_information, HealthMonitor, ServerResult, StartupState, XandApiConfig,
    XandApiServer, XandApiSvc, XandNetworkKeyResolver,
};
use chrono::Utc;
use futures::lock::Mutex;
use futures::TryFutureExt;
use petri::{HistoricalStore, IdTagCache, TransactionCache, TxoCache, TxoStore};
use rand::rngs::OsRng;
use std::future::Future;
use std::net::Ipv4Addr;
use std::sync::Arc;
use tokio::net::TcpListener;
use tokio_stream::wrappers::TcpListenerStream;
use tonic::transport::Server;
use tpfs_krypt::KeyManagement;
use xand_financial_client::clear_redeem_builder::{
    ClearRedeemBuilder, ClearRedeemBuilderDeps, ClearRedeemBuilderImpl,
};
use xand_financial_client::{
    create_builder::{CreateBuilder, CreateBuilderDeps, CreateBuilderImpl},
    CreateManager, RedeemBuilder, RedeemBuilderDeps, RedeemBuilderImpl, RedeemManager, SendBuilder,
    SendBuilderDeps, SendBuilderImpl,
};
use xand_financial_client_adapter::{
    krypt_adapter::KryptKeyStore, member_context::SubstrateClientMemberContext,
};
use xandstrate_client::{
    ConfidentialClientInterface, KeyMgrSigner, SubstrateClient, SubstrateClientInterface,
};

#[allow(clippy::too_many_arguments)]
pub async fn start_server<C, R>(
    cfg: XandApiConfig,
    id_tag_selector: IdTagCache<OsRng>,
    transaction_cache: Arc<TransactionCache>,
    substrate_client: Arc<SubstrateClient>,
    key_manager: Arc<dyn KeyManagement>,
    txo_cache: TxoCache,
    key_store: Arc<KryptKeyStore>,
    create_manager: C,
    redeem_manager: R,
) -> Result<(), Box<dyn std::error::Error>>
where
    C: CreateManager + Send + Sync + 'static,
    R: RedeemManager + Send + Sync + 'static,
{
    let addr = format!("0.0.0.0:{}", cfg.port).parse()?;
    let encryption_key_resolver = WrappedNetworkKeyResolver::from(XandNetworkKeyResolver::new(
        substrate_client.clone(),
        key_manager.clone(),
    ));

    let create_builder = CreateBuilderImpl::<CreateBuilderProdDeps> {
        id_tag_selector: id_tag_selector.clone(),
        key_store: key_store.clone(),
        encryption_provider: key_store.clone(),
        network_key_resolver: encryption_key_resolver.clone(),
        member_context: substrate_client.clone().into(),
        rng: Mutex::new(OsRng::default()),
    };
    let send_builder = SendBuilderImpl::<SendBuilderProdDeps>::new(
        id_tag_selector.clone(),
        key_store.clone(),
        txo_cache.clone(),
        key_store.clone(),
        encryption_key_resolver.clone(),
        substrate_client.clone().into(),
        OsRng::default().into(),
    );
    let redeem_builder = RedeemBuilderImpl::<RedeemBuilderProdDeps> {
        txo_repo: txo_cache.clone(),
        id_tag_selector: id_tag_selector.clone(),
        key_store: key_store.clone(),
        member_context: substrate_client.clone().into(),
        encryption_provider: key_store.clone(),
        network_key_resolver: encryption_key_resolver.clone(),
        rng: OsRng::default().into(),
    };

    let clear_txo_repo = SubstrateClientClearTxOStore::new(substrate_client.clone());

    let clear_redeem_builder = ClearRedeemBuilderImpl::<ClearRedeemBuilderProdDeps> {
        clear_txo_repo,
        encryption_provider: key_store,
        network_key_resolver: encryption_key_resolver.clone(),
        rng: OsRng::default().into(),
    };

    let clear_txo_store = SubstrateClientClearTxOStore::new(substrate_client.clone());

    let (initial_block_sender, initial_block_receiver) =
        tokio::sync::watch::channel::<Option<u32>>(None);
    let startup_state = StartupState {
        block_number: Some(initial_block_receiver),
        startup_timestamp: Utc::now(),
    };

    let last_block_info =
        finalized_head_listener::spawn_data_info_listener(substrate_client.clone());
    tokio::spawn(listen_for_initial_block_information(
        last_block_info.clone(),
        initial_block_sender,
    ));

    let health_monitor =
        HealthMonitorImpl::new(transaction_cache.clone(), startup_state, last_block_info);

    let api_svc = XandApiSvc::new(
        substrate_client,
        transaction_cache,
        KeyMgrSigner::new(key_manager),
        encryption_key_resolver,
        create_manager,
        redeem_manager,
        txo_cache,
        clear_txo_store,
        create_builder,
        send_builder,
        redeem_builder,
        clear_redeem_builder,
        health_monitor.clone(),
    )
    .await;

    // Enable JWT auth if secret was specified
    let interceptor = match cfg.jwt_secret_path {
        Some(path) => JwtAuthInterceptor::using_file(path)?,
        None => JwtAuthInterceptor::no_auth(),
    };
    let api_server = XandApiServer::with_interceptor(api_svc, interceptor.into_interceptor());

    let health_check_middleware_layer = HealthCheckMiddlewareLayer::new(health_monitor);

    Server::builder()
        .layer(health_check_middleware_layer)
        .add_service(api_server)
        .serve(addr)
        .await?;

    Ok(())
}

pub async fn start_server_test<F, S, H, E, C, R, T, ClearTxOs, B, P, A, V, HM>(
    port: u16,
    api_svc: XandApiSvc<S, H, E, C, R, T, ClearTxOs, B, P, A, V, HM>,
    shutdown_signal: F,
    maybe_jwt: Option<Vec<u8>>,
) -> Result<(), Box<dyn std::error::Error>>
where
    S: SubstrateClientInterface + ConfidentialClientInterface + Sync + Send + 'static,
    H: HistoricalStore + Sync + Send + 'static,
    E: NetworkKeyResolver + Sync + Send + 'static,
    C: CreateManager + Sync + Send + 'static,
    R: RedeemManager + Sync + Send + 'static,
    F: Future<Output = ()>,
    T: TxoStore + Send + Sync + 'static,
    ClearTxOs: ClearTxOStore + Send + Sync + 'static,
    B: CreateBuilder + Sync + Send + 'static,
    P: SendBuilder + Send + Sync + 'static,
    A: RedeemBuilder + Send + Sync + 'static,
    V: ClearRedeemBuilder + Send + Sync + 'static,
    HM: HealthMonitor + Send + Sync + 'static,
{
    let (task, _) = make_server_test(port, api_svc, shutdown_signal, maybe_jwt).await?;
    task.await
}

pub async fn make_server_test<F, S, H, E, C, R, T, ClearTxOs, B, P, A, V, HM>(
    port: u16,
    api_svc: XandApiSvc<S, H, E, C, R, T, ClearTxOs, B, P, A, V, HM>,
    shutdown_signal: F,
    maybe_jwt: Option<Vec<u8>>,
) -> ServerResult<(impl Future<Output = ServerResult<()>>, u16)>
where
    S: SubstrateClientInterface + ConfidentialClientInterface + Sync + Send + 'static,
    H: HistoricalStore + Sync + Send + 'static,
    E: NetworkKeyResolver + Sync + Send + 'static,
    C: CreateManager + Sync + Send + 'static,
    R: RedeemManager + Sync + Send + 'static,
    F: Future<Output = ()>,
    T: TxoStore + Send + Sync + 'static,
    ClearTxOs: ClearTxOStore + Send + Sync + 'static,
    B: CreateBuilder + Sync + Send + 'static,
    P: SendBuilder + Send + Sync + 'static,
    A: RedeemBuilder + Send + Sync + 'static,
    V: ClearRedeemBuilder + Send + Sync + 'static,
    HM: HealthMonitor + Send + Sync + 'static,
{
    // Enable JWT auth if secret was specified
    let interceptor = match maybe_jwt {
        Some(jwt_secret) => JwtAuthInterceptor::using_static(jwt_secret),
        None => JwtAuthInterceptor::no_auth(),
    };
    let api_server = XandApiServer::with_interceptor(api_svc, interceptor.into_interceptor());

    let listener = TcpListener::bind((Ipv4Addr::LOCALHOST, port)).await?;
    let port = listener.local_addr()?.port();
    let task = Server::builder()
        .add_service(api_server)
        .serve_with_incoming_shutdown(TcpListenerStream::new(listener), shutdown_signal)
        .map_err(Into::into);
    Ok((task, port))
}

pub struct CreateBuilderProdDeps;

impl CreateBuilderDeps for CreateBuilderProdDeps {
    type IdentityTagSelector = IdTagCache<OsRng>;
    type KeyStore = Arc<KryptKeyStore>;
    type NetworkKeyResolver = WrappedNetworkKeyResolver<XandNetworkKeyResolver<SubstrateClient>>;
    type EncryptionProvider = Arc<KryptKeyStore>;
    type MemberContext = SubstrateClientMemberContext<SubstrateClient>;
    type Rng = OsRng;
}

pub struct SendBuilderProdDeps;

impl SendBuilderDeps for SendBuilderProdDeps {
    type KeyStore = Arc<KryptKeyStore>;
    type TxoRepo = TxoCache;
    type Rng = OsRng;
    type NetworkKeyResolver = WrappedNetworkKeyResolver<XandNetworkKeyResolver<SubstrateClient>>;
    type EncryptionProvider = Arc<KryptKeyStore>;
    type IdentityTagSelector = IdTagCache<OsRng>;
    type MemberContext = SubstrateClientMemberContext<SubstrateClient>;
}

pub struct RedeemBuilderProdDeps;

impl RedeemBuilderDeps for RedeemBuilderProdDeps {
    type TxoRepo = TxoCache;
    type IdentityTagSelector = IdTagCache<OsRng>;
    type KeyStore = Arc<KryptKeyStore>;
    type MemberContext = SubstrateClientMemberContext<SubstrateClient>;
    type EncryptionProvider = Arc<KryptKeyStore>;
    type NetworkKeyResolver = WrappedNetworkKeyResolver<XandNetworkKeyResolver<SubstrateClient>>;
    type Rng = OsRng;
}

pub struct ClearRedeemBuilderProdDeps;

impl ClearRedeemBuilderDeps for ClearRedeemBuilderProdDeps {
    type ClearTxoRepo = SubstrateClientClearTxOStore<SubstrateClient>;
    type EncryptionProvider = Arc<KryptKeyStore>;
    type NetworkKeyResolver = WrappedNetworkKeyResolver<XandNetworkKeyResolver<SubstrateClient>>;
    type Rng = OsRng;
}
