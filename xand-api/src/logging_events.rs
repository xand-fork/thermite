use tpfs_logger_port::logging_event;

#[logging_event]
pub enum LoggingEvent {
    UsingConfidentialityMode(bool),
    /// Can happen if multiple requests are submitted in quick succession or across multiple
    /// instances of the substrate client
    AccountCorrelationIdCollision {
        transaction_id: String,
        account: String,
    },
    SubstrateClientError(String),
    UnknownStatus(String),
    /// logged when submitting a transaction without a signature
    UnknownSignatory {
        transaction_id: String,
    },
    ConnectionFailure {
        message: String,
    },
    UnknownProposalAction {
        action: String,
    },
    InitialStartupInformationFailure(String),
    EnqueuingRequest,
    TransactionStatus {
        status: String,
    },
    NegativeValidatorElapsedBlocksWarning {
        message: String,
        current_block: u32,
        initial_block: u32,
    },
}
