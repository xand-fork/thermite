use serde::{Deserialize, Serialize};
use std::path::PathBuf;

#[derive(Clone, Debug, Deserialize, PartialEq, Eq, Serialize)]
pub struct XandApiConfig {
    /// Which port does grpc listen on
    pub port: u16,
    /// If specified, this is the path to the (symmetric) secret that will be used for JWT tokens.
    pub jwt_secret_path: Option<PathBuf>,
}

impl Default for XandApiConfig {
    fn default() -> Self {
        Self {
            port: 50051,
            jwt_secret_path: None,
        }
    }
}
