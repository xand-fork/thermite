//! Convert between types in [xand_api_proto::xand_model] and [::xand_model].

use std::convert::TryInto;
use xand_api_proto::proto_models;
use xand_api_proto::proto_models::WithdrawFromNetwork;
use xand_models::{
    AdministrativeTransaction, BankAccountId, BankAccountInfo, CreateCompletion, RedeemCompletion,
    TransactionStatus, XandTransaction,
};
use xand_runtime_models::{CidrBlock, ProposalStage};

// Bidirectional `std::convert::From` type conversions involving foreign types leads to orphan
// rule errors at compile-time. A conventional way to work around this is to create newtype
// wrappers for each of the foreign types and define local trait implementations using the
// newtypes instead.
//
// The authors of this module instead opted to resolve the orphan rule by defining local
// traits resembling `std::convert::{From,Into}` and implementing conversions in terms of
// those instead. The benefit is rather than having 4*N types for roughly the same N concepts
// we limit it to 2*N, at the cost of having to use a slightly different conversion interface.
//
// In hindsight, it may have been better to implement proc-macros which can implement these
// conversions per pair of types and enable custom implementations whenever the
// commonalities between the types diverge. On the other hand, these explicit conversion
// implementations make it clearer what needs to change when we change types in the
// xand-models crate or the xand_models module in the xand-api-proto crate, especially
// compared to proc-macros that would obscure the details.

/// Local trait that behaves like [std::convert::From].
pub(crate) trait FromWrapper<T> {
    fn convert_from(input: T) -> Self;
}

/// Local trait that behaves like [std::convert::Into].
pub(crate) trait IntoWrapper<T> {
    fn convert_into(self) -> T;
}

impl<T, U> IntoWrapper<U> for T
where
    U: FromWrapper<T>,
{
    fn convert_into(self) -> U {
        FromWrapper::convert_from(self)
    }
}

/* xand_models -> proto_models */

impl FromWrapper<xand_models::Transaction> for proto_models::Transaction {
    fn convert_from(input: xand_models::Transaction) -> Self {
        proto_models::Transaction {
            signer_address: input.signer_address,
            transaction_id: input.transaction_id.convert_into(),
            status: input.status.convert_into(),
            txn: input.txn.convert_into(),
            timestamp: input.timestamp,
        }
    }
}

impl FromWrapper<xand_models::TransactionType> for proto_models::TransactionType {
    fn convert_from(input: xand_models::TransactionType) -> Self {
        match input {
            xand_models::TransactionType::RegisterMember => Self::RegisterMember,
            xand_models::TransactionType::RegisterSessionKeys => Self::RegisterSessionKeys,
            xand_models::TransactionType::RemoveMember => Self::RemoveMember,
            xand_models::TransactionType::ExitMember => Self::ExitMember,
            xand_models::TransactionType::SetLimitedAgent => Self::SetLimitedAgent,
            xand_models::TransactionType::SetValidatorEmissionRate => {
                Self::SetValidatorEmissionRate
            }
            xand_models::TransactionType::SetTrust => Self::SetTrust,
            xand_models::TransactionType::SetMemberEncryptionKey => Self::SetMemberEncryptionKey,
            xand_models::TransactionType::SetTrustEncryptionKey => Self::SetTrustEncryptionKey,
            xand_models::TransactionType::SetPendingCreateExpire => {
                Self::SetPendingCreateRequestExpire
            }
            xand_models::TransactionType::Send => Self::Send,
            xand_models::TransactionType::CreatePendingCreate => Self::CreateRequest,
            xand_models::TransactionType::FulfillCreate => Self::CashConfirmation,
            xand_models::TransactionType::CancelCreate => Self::CreateCancellation,
            xand_models::TransactionType::CancelRedeem => Self::RedeemCancellation,
            xand_models::TransactionType::CreatePendingRedeem => Self::RedeemRequest,
            xand_models::TransactionType::FulfillRedeem => Self::RedeemFulfillment,
            xand_models::TransactionType::AddAuthorityKey => Self::AddAuthorityKey,
            xand_models::TransactionType::RemoveAuthorityKey => Self::RemoveAuthorityKey,
            xand_models::TransactionType::AllowlistCidrBlock => Self::AllowlistCidrBlock,
            xand_models::TransactionType::RemoveAllowlistCidrBlock => {
                Self::RemoveAllowlistCidrBlock
            }
            xand_models::TransactionType::RootAllowlistCidrBlock => Self::RootAllowlistCidrBlock,
            xand_models::TransactionType::RootRemoveAllowlistCidrBlock => {
                Self::RootRemoveAllowlistCidrBlock
            }
            xand_models::TransactionType::SubmitProposal => Self::SubmitProposal,
            xand_models::TransactionType::VoteProposal => Self::VoteProposal,
            xand_models::TransactionType::WithdrawFromNetwork => Self::WithdrawFromNetwork,
        }
    }
}

impl FromWrapper<xand_models::TransactionId> for proto_models::TransactionId {
    fn convert_from(input: xand_models::TransactionId) -> Self {
        input
            .decode()
            .try_into()
            .expect("transaction ID validity should be preserved")
    }
}

impl FromWrapper<xand_models::TransactionStatus> for proto_models::TransactionStatus {
    fn convert_from(input: xand_models::TransactionStatus) -> Self {
        match input {
            TransactionStatus::Unknown => proto_models::TransactionStatus::Unknown,
            TransactionStatus::Pending => proto_models::TransactionStatus::Pending,
            TransactionStatus::Invalid(s) => proto_models::TransactionStatus::Invalid(s),
            TransactionStatus::Committed => proto_models::TransactionStatus::Committed,
            TransactionStatus::Finalized => proto_models::TransactionStatus::Finalized,
        }
    }
}

impl FromWrapper<xand_models::XandTransaction> for proto_models::XandTransaction {
    fn convert_from(input: xand_models::XandTransaction) -> Self {
        match input {
            XandTransaction::RegisterMember(d) => {
                proto_models::XandTransaction::RegisterMember(d.convert_into())
            }
            XandTransaction::RemoveMember(d) => {
                proto_models::XandTransaction::RemoveMember(d.convert_into())
            }
            XandTransaction::ExitMember(d) => {
                proto_models::XandTransaction::ExitMember(d.convert_into())
            }
            XandTransaction::SetTrust(d) => {
                proto_models::XandTransaction::SetTrust(d.convert_into())
            }
            XandTransaction::SetLimitedAgent(d) => {
                proto_models::XandTransaction::SetLimitedAgent(d.convert_into())
            }
            XandTransaction::SetValidatorEmissionRate(d) => {
                proto_models::XandTransaction::SetValidatorEmissionRate(d.convert_into())
            }
            XandTransaction::SetMemberEncryptionKey(d) => {
                proto_models::XandTransaction::SetMemberEncryptionKey(d.convert_into())
            }
            XandTransaction::SetTrustEncryptionKey(d) => {
                proto_models::XandTransaction::SetTrustEncryptionKey(d.convert_into())
            }
            XandTransaction::SetPendingCreateExpire(d) => {
                proto_models::XandTransaction::SetPendingCreateRequestExpire(d.convert_into())
            }
            XandTransaction::Send(d) => proto_models::XandTransaction::Send(d.convert_into()),
            XandTransaction::CreatePendingCreate(d) => {
                proto_models::XandTransaction::CreateRequest(d.convert_into())
            }
            XandTransaction::FulfillCreate(d) => {
                proto_models::XandTransaction::CashConfirmation(d.convert_into())
            }
            XandTransaction::CancelCreate(d) => {
                proto_models::XandTransaction::CreateCancellation(d.convert_into())
            }
            XandTransaction::CancelRedeem(d) => {
                proto_models::XandTransaction::RedeemCancellation(d.convert_into())
            }
            XandTransaction::CreatePendingRedeem(d) => {
                proto_models::XandTransaction::RedeemRequest(d.convert_into())
            }
            XandTransaction::FulfillRedeem(d) => {
                proto_models::XandTransaction::RedeemFulfillment(d.convert_into())
            }
            XandTransaction::AddAuthorityKey(d) => {
                proto_models::XandTransaction::AddAuthorityKey(d.convert_into())
            }
            XandTransaction::RemoveAuthorityKey(d) => {
                proto_models::XandTransaction::RemoveAuthorityKey(d.convert_into())
            }
            XandTransaction::AllowlistCidrBlock(d) => {
                proto_models::XandTransaction::AllowlistCidrBlock(d.convert_into())
            }
            XandTransaction::RemoveAllowlistCidrBlock(d) => {
                proto_models::XandTransaction::RemoveAllowlistCidrBlock(d.convert_into())
            }
            XandTransaction::RootAllowlistCidrBlock(d) => {
                proto_models::XandTransaction::RootAllowlistCidrBlock(d.convert_into())
            }
            XandTransaction::RootRemoveAllowlistCidrBlock(d) => {
                proto_models::XandTransaction::RootRemoveAllowlistCidrBlock(d.convert_into())
            }
            XandTransaction::SubmitProposal(d) => {
                proto_models::XandTransaction::SubmitProposal(d.convert_into())
            }
            XandTransaction::VoteProposal(d) => {
                proto_models::XandTransaction::VoteProposal(d.convert_into())
            }
            XandTransaction::RegisterSessionKeys(d) => {
                proto_models::XandTransaction::RegisterSessionKeys(d.convert_into())
            }
            XandTransaction::WithdrawFromNetwork => {
                proto_models::XandTransaction::WithdrawFromNetwork(WithdrawFromNetwork {})
            }
        }
    }
}

impl FromWrapper<xand_models::ProposalStage> for proto_models::ProposalStage {
    fn convert_from(input: xand_models::ProposalStage) -> Self {
        match input {
            ProposalStage::Proposed => Self::Proposed,
            ProposalStage::Accepted => Self::Accepted,
            ProposalStage::Rejected => Self::Rejected,
            ProposalStage::Invalid => Self::Invalid,
        }
    }
}

impl FromWrapper<xand_models::RegisterAccountAsMember> for proto_models::RegisterAccountAsMember {
    fn convert_from(input: xand_models::RegisterAccountAsMember) -> Self {
        Self {
            address: input.address,
            encryption_key: input.encryption_key.convert_into(),
        }
    }
}

impl FromWrapper<xand_models::RemoveMember> for proto_models::RemoveMember {
    fn convert_from(input: xand_models::RemoveMember) -> Self {
        Self {
            address: input.address,
        }
    }
}

impl FromWrapper<xand_models::ExitMember> for proto_models::ExitMember {
    fn convert_from(input: xand_models::ExitMember) -> Self {
        Self {
            address: input.address,
        }
    }
}

impl FromWrapper<xand_models::SetTrustNodeId> for proto_models::SetTrustNodeId {
    fn convert_from(input: xand_models::SetTrustNodeId) -> Self {
        Self {
            address: input.address,
            encryption_key: input.encryption_key.convert_into(),
        }
    }
}

impl FromWrapper<xand_models::SetLimitedAgentId> for proto_models::SetLimitedAgentId {
    fn convert_from(input: xand_models::SetLimitedAgentId) -> Self {
        Self {
            address: input.address,
        }
    }
}

impl FromWrapper<xand_models::SetValidatorEmissionRate> for proto_models::SetValidatorEmissionRate {
    fn convert_from(input: xand_models::SetValidatorEmissionRate) -> Self {
        Self {
            block_quota: input.block_quota,
            minor_units_per_emission: input.minor_units_per_emission,
        }
    }
}

impl FromWrapper<xand_models::SetMemberEncKey> for proto_models::SetMemberEncKey {
    fn convert_from(input: xand_models::SetMemberEncKey) -> Self {
        Self {
            key: input.key.convert_into(),
        }
    }
}

impl FromWrapper<xand_models::SetTrustEncKey> for proto_models::SetTrustEncKey {
    fn convert_from(input: xand_models::SetTrustEncKey) -> Self {
        Self {
            key: input.key.convert_into(),
        }
    }
}

impl FromWrapper<xand_models::SetPendingCreateExpire>
    for proto_models::SetPendingCreateRequestExpire
{
    fn convert_from(input: xand_models::SetPendingCreateExpire) -> Self {
        Self {
            expire_in_milliseconds: input.expire_in_milliseconds,
        }
    }
}

impl FromWrapper<xand_models::SendSchema> for proto_models::Send {
    fn convert_from(input: xand_models::SendSchema) -> Self {
        Self {
            destination_account: input.destination_account,
            amount_in_minor_unit: input.amount_in_minor_unit,
        }
    }
}

impl FromWrapper<xand_models::PendingCreate> for proto_models::PendingCreateRequest {
    fn convert_from(input: xand_models::PendingCreate) -> Self {
        Self {
            amount_in_minor_unit: input.amount_in_minor_unit,
            correlation_id: input.correlation_id.convert_into(),
            account: input.account.convert_into(),
            completing_transaction: input.completing_transaction.map(IntoWrapper::convert_into),
        }
    }
}

impl FromWrapper<xand_models::FulfillPendingCreate> for proto_models::CashConfirmation {
    fn convert_from(input: xand_models::FulfillPendingCreate) -> Self {
        Self {
            correlation_id: input.correlation_id.convert_into(),
        }
    }
}

impl FromWrapper<xand_ledger::CreateCancellationReason> for proto_models::CreateCancellationReason {
    fn convert_from(r: xand_ledger::CreateCancellationReason) -> Self {
        match r {
            xand_ledger::CreateCancellationReason::Expired => Self::Expired,
            xand_ledger::CreateCancellationReason::InvalidData => Self::InvalidData,
            xand_ledger::CreateCancellationReason::BankNotFound => Self::BankNotFound,
        }
    }
}

impl FromWrapper<xand_models::CancelPendingCreate> for proto_models::CreateCancellation {
    fn convert_from(input: xand_models::CancelPendingCreate) -> Self {
        Self {
            correlation_id: input.correlation_id.convert_into(),
            reason: input.reason.convert_into(),
        }
    }
}

impl FromWrapper<xand_ledger::RedeemCancellationReason> for proto_models::RedeemCancellationReason {
    fn convert_from(r: xand_ledger::RedeemCancellationReason) -> Self {
        match r {
            xand_ledger::RedeemCancellationReason::InvalidData => Self::InvalidData,
            xand_ledger::RedeemCancellationReason::AccountNotAllowed => Self::AccountNotAllowed,
        }
    }
}

impl FromWrapper<xand_models::CancelPendingRedeem> for proto_models::RedeemCancellation {
    fn convert_from(input: xand_models::CancelPendingRedeem) -> Self {
        Self {
            correlation_id: input.correlation_id.convert_into(),
            reason: input.reason.convert_into(),
        }
    }
}

impl FromWrapper<xand_models::PendingRedeem> for proto_models::PendingRedeemRequest {
    fn convert_from(input: xand_models::PendingRedeem) -> Self {
        Self {
            amount_in_minor_unit: input.amount_in_minor_unit,
            correlation_id: input.correlation_id.convert_into(),
            account: input.account.convert_into(),
            completing_transaction: input.completing_transaction.map(IntoWrapper::convert_into),
        }
    }
}

impl FromWrapper<xand_models::FulfillPendingRedeem> for proto_models::RedeemFulfillment {
    fn convert_from(input: xand_models::FulfillPendingRedeem) -> Self {
        Self {
            correlation_id: input.correlation_id.convert_into(),
        }
    }
}

impl FromWrapper<xand_models::AddAuthorityKey> for proto_models::AddAuthorityKey {
    fn convert_from(input: xand_models::AddAuthorityKey) -> Self {
        Self {
            account_id: input.account_id,
        }
    }
}

impl FromWrapper<xand_models::RemoveAuthorityKey> for proto_models::RemoveAuthorityKey {
    fn convert_from(input: xand_models::RemoveAuthorityKey) -> Self {
        Self {
            account_id: input.account_id,
        }
    }
}

impl FromWrapper<xand_models::AllowlistCidrBlock> for proto_models::AllowlistCidrBlock {
    fn convert_from(input: xand_models::AllowlistCidrBlock) -> Self {
        Self {
            cidr_block: input.cidr_block.convert_into(),
        }
    }
}

impl FromWrapper<xand_models::RemoveAllowlistCidrBlock> for proto_models::RemoveAllowlistCidrBlock {
    fn convert_from(input: xand_models::RemoveAllowlistCidrBlock) -> Self {
        Self {
            cidr_block: input.cidr_block.convert_into(),
        }
    }
}

impl FromWrapper<xand_models::RootAllowlistCidrBlock> for proto_models::RootAllowlistCidrBlock {
    fn convert_from(input: xand_models::RootAllowlistCidrBlock) -> Self {
        Self {
            account: input.account,
            cidr_block: input.cidr_block.convert_into(),
        }
    }
}

impl FromWrapper<xand_models::RootRemoveAllowlistCidrBlock>
    for proto_models::RootRemoveAllowlistCidrBlock
{
    fn convert_from(input: xand_models::RootRemoveAllowlistCidrBlock) -> Self {
        Self {
            account: input.account,
            cidr_block: input.cidr_block.convert_into(),
        }
    }
}

impl FromWrapper<xand_models::SubmitProposal> for proto_models::SubmitProposal {
    fn convert_from(input: xand_models::SubmitProposal) -> Self {
        Self {
            proposed_action: input.proposed_action.convert_into(),
        }
    }
}

impl FromWrapper<xand_models::VoteProposal> for proto_models::VoteProposal {
    fn convert_from(input: xand_models::VoteProposal) -> Self {
        Self {
            id: input.id,
            vote: input.vote,
        }
    }
}

impl FromWrapper<xand_models::RegisterSessionKeys> for proto_models::RegisterSessionKeys {
    fn convert_from(input: xand_models::RegisterSessionKeys) -> Self {
        Self {
            block_production_pubkey: input.block_production_pubkey,
            block_finalization_pubkey: input.block_finalization_pubkey,
        }
    }
}

impl FromWrapper<xand_models::CreateCompletion> for proto_models::CreateRequestCompletion {
    fn convert_from(input: xand_models::CreateCompletion) -> Self {
        match input {
            CreateCompletion::Confirmation(d) => Self::Confirmation(d.convert_into()),
            CreateCompletion::Cancellation(d) => Self::Cancellation(d.convert_into()),
            CreateCompletion::Expiration(d) => Self::Expiration(d.convert_into()),
        }
    }
}

impl FromWrapper<xand_models::X25519PublicKey> for proto_models::PublicKey {
    fn convert_from(input: xand_models::X25519PublicKey) -> Self {
        input.as_bytes().into()
    }
}

impl FromWrapper<xand_models::CorrelationId> for proto_models::CorrelationId {
    fn convert_from(input: xand_models::CorrelationId) -> Self {
        input
            .to_bytes()
            .try_into()
            .expect("correlation id should be compatible")
    }
}

impl FromWrapper<xand_models::BankAccountInfo> for proto_models::BankAccountInfo {
    fn convert_from(input: xand_models::BankAccountInfo) -> Self {
        match input {
            BankAccountInfo::Unencrypted(d) => Self::Unencrypted(d.convert_into()),
            BankAccountInfo::Encrypted(_) => {
                Self::Encrypted(proto_models::errors::EncryptionError::KeyNotFound)
            }
            BankAccountInfo::Malformed(_) => {
                Self::Encrypted(proto_models::errors::EncryptionError::MessageMalformed)
            }
        }
    }
}

impl FromWrapper<xand_models::CidrBlock> for proto_models::CidrBlock {
    fn convert_from(input: CidrBlock) -> Self {
        Self(input.0)
    }
}

impl FromWrapper<xand_models::RedeemCompletion> for proto_models::RedeemRequestCompletion {
    fn convert_from(input: RedeemCompletion) -> Self {
        match input {
            RedeemCompletion::Confirmation(d) => Self::Confirmation(d.convert_into()),
            RedeemCompletion::Cancellation(d) => Self::Cancellation(d.convert_into()),
        }
    }
}

impl FromWrapper<xand_models::BankAccountId> for proto_models::BankAccountId {
    fn convert_from(input: BankAccountId) -> Self {
        Self {
            routing_number: input.routing_number,
            account_number: input.account_number,
        }
    }
}

impl FromWrapper<xand_models::AdministrativeTransaction>
    for proto_models::AdministrativeTransaction
{
    fn convert_from(input: AdministrativeTransaction) -> Self {
        match input {
            AdministrativeTransaction::RegisterAccountAsMember(d) => {
                Self::RegisterAccountAsMember(d.convert_into())
            }
            AdministrativeTransaction::SetTrust(d) => Self::SetTrust(d.convert_into()),
            AdministrativeTransaction::AddAuthorityKey(d) => {
                Self::AddAuthorityKey(d.convert_into())
            }
            AdministrativeTransaction::RemoveAuthorityKey(d) => {
                Self::RemoveAuthorityKey(d.convert_into())
            }
            AdministrativeTransaction::RootAllowlistCidrBlock(d) => {
                Self::RootAllowlistCidrBlock(d.convert_into())
            }
            AdministrativeTransaction::RootRemoveAllowlistCidrBlock(d) => {
                Self::RootRemoveAllowlistCidrBlock(d.convert_into())
            }
            AdministrativeTransaction::RemoveMember(d) => Self::RemoveMember(d.convert_into()),
            AdministrativeTransaction::SetLimitedAgent(d) => {
                Self::SetLimitedAgent(d.convert_into())
            }
            AdministrativeTransaction::SetValidatorEmissionRate(d) => {
                Self::SetValidatorEmissionRate(d.convert_into())
            }
        }
    }
}

/* proto_models -> xand_models */

impl FromWrapper<proto_models::Transaction> for xand_models::Transaction {
    fn convert_from(input: proto_models::Transaction) -> Self {
        Self {
            signer_address: input.signer_address,
            transaction_id: input.transaction_id.convert_into(),
            status: input.status.convert_into(),
            txn: input.txn.convert_into(),
            timestamp: input.timestamp,
        }
    }
}

impl FromWrapper<proto_models::TransactionId> for xand_models::TransactionId {
    fn convert_from(input: proto_models::TransactionId) -> Self {
        input
            .decode()
            .try_into()
            .expect("transaction ID validity should be preserved")
    }
}

impl FromWrapper<proto_models::TransactionStatus> for xand_models::TransactionStatus {
    fn convert_from(input: proto_models::TransactionStatus) -> Self {
        match input {
            proto_models::TransactionStatus::Unknown => xand_models::TransactionStatus::Unknown,
            proto_models::TransactionStatus::Pending => xand_models::TransactionStatus::Pending,
            proto_models::TransactionStatus::Invalid(s) => {
                xand_models::TransactionStatus::Invalid(s)
            }
            proto_models::TransactionStatus::Committed => xand_models::TransactionStatus::Committed,
            proto_models::TransactionStatus::Finalized => xand_models::TransactionStatus::Finalized,
        }
    }
}

impl FromWrapper<proto_models::XandTransaction> for xand_models::XandTransaction {
    fn convert_from(input: proto_models::XandTransaction) -> Self {
        match input {
            proto_models::XandTransaction::RegisterMember(d) => {
                Self::RegisterMember(d.convert_into())
            }
            proto_models::XandTransaction::RemoveMember(d) => Self::RemoveMember(d.convert_into()),
            proto_models::XandTransaction::ExitMember(d) => Self::ExitMember(d.convert_into()),
            proto_models::XandTransaction::SetTrust(d) => Self::SetTrust(d.convert_into()),
            proto_models::XandTransaction::SetLimitedAgent(d) => {
                Self::SetLimitedAgent(d.convert_into())
            }
            proto_models::XandTransaction::SetValidatorEmissionRate(d) => {
                Self::SetValidatorEmissionRate(d.convert_into())
            }
            proto_models::XandTransaction::SetMemberEncryptionKey(d) => {
                Self::SetMemberEncryptionKey(d.convert_into())
            }
            proto_models::XandTransaction::SetTrustEncryptionKey(d) => {
                Self::SetTrustEncryptionKey(d.convert_into())
            }
            proto_models::XandTransaction::SetPendingCreateRequestExpire(d) => {
                Self::SetPendingCreateExpire(d.convert_into())
            }
            proto_models::XandTransaction::Send(d) => Self::Send(d.convert_into()),
            proto_models::XandTransaction::CreateRequest(d) => {
                Self::CreatePendingCreate(d.convert_into())
            }
            proto_models::XandTransaction::CashConfirmation(d) => {
                Self::FulfillCreate(d.convert_into())
            }
            proto_models::XandTransaction::CreateCancellation(d) => {
                Self::CancelCreate(d.convert_into())
            }
            proto_models::XandTransaction::RedeemCancellation(d) => {
                Self::CancelRedeem(d.convert_into())
            }
            proto_models::XandTransaction::RedeemRequest(d) => {
                Self::CreatePendingRedeem(d.convert_into())
            }
            proto_models::XandTransaction::RedeemFulfillment(d) => {
                Self::FulfillRedeem(d.convert_into())
            }
            proto_models::XandTransaction::AddAuthorityKey(d) => {
                Self::AddAuthorityKey(d.convert_into())
            }
            proto_models::XandTransaction::RemoveAuthorityKey(d) => {
                Self::RemoveAuthorityKey(d.convert_into())
            }
            proto_models::XandTransaction::AllowlistCidrBlock(d) => {
                Self::AllowlistCidrBlock(d.convert_into())
            }
            proto_models::XandTransaction::RemoveAllowlistCidrBlock(d) => {
                Self::RemoveAllowlistCidrBlock(d.convert_into())
            }
            proto_models::XandTransaction::RootAllowlistCidrBlock(d) => {
                Self::RootAllowlistCidrBlock(d.convert_into())
            }
            proto_models::XandTransaction::RootRemoveAllowlistCidrBlock(d) => {
                Self::RootRemoveAllowlistCidrBlock(d.convert_into())
            }
            proto_models::XandTransaction::SubmitProposal(d) => {
                Self::SubmitProposal(d.convert_into())
            }
            proto_models::XandTransaction::VoteProposal(d) => Self::VoteProposal(d.convert_into()),
            proto_models::XandTransaction::RegisterSessionKeys(d) => {
                Self::RegisterSessionKeys(d.convert_into())
            }
            proto_models::XandTransaction::RuntimeUpgrade(_) => {
                unimplemented!()
            }
            proto_models::XandTransaction::WithdrawFromNetwork(_) => Self::WithdrawFromNetwork,
        }
    }
}

impl FromWrapper<proto_models::Send> for xand_models::SendSchema {
    fn convert_from(input: proto_models::Send) -> Self {
        Self {
            destination_account: input.destination_account,
            amount_in_minor_unit: input.amount_in_minor_unit,
        }
    }
}

impl FromWrapper<proto_models::PendingCreateRequest> for xand_models::PendingCreate {
    fn convert_from(input: proto_models::PendingCreateRequest) -> Self {
        Self {
            amount_in_minor_unit: input.amount_in_minor_unit,
            correlation_id: input.correlation_id.convert_into(),
            account: input.account.convert_into(),
            completing_transaction: input.completing_transaction.map(IntoWrapper::convert_into),
        }
    }
}

impl FromWrapper<proto_models::PendingRedeemRequest> for xand_models::PendingRedeem {
    fn convert_from(input: proto_models::PendingRedeemRequest) -> Self {
        Self {
            amount_in_minor_unit: input.amount_in_minor_unit,
            correlation_id: input.correlation_id.convert_into(),
            account: input.account.convert_into(),
            completing_transaction: input.completing_transaction.map(IntoWrapper::convert_into),
        }
    }
}

impl FromWrapper<proto_models::CashConfirmation> for xand_models::FulfillPendingCreate {
    fn convert_from(input: proto_models::CashConfirmation) -> Self {
        Self {
            correlation_id: input.correlation_id.convert_into(),
        }
    }
}

impl FromWrapper<proto_models::RedeemFulfillment> for xand_models::FulfillPendingRedeem {
    fn convert_from(input: proto_models::RedeemFulfillment) -> Self {
        Self {
            correlation_id: input.correlation_id.convert_into(),
        }
    }
}

impl FromWrapper<proto_models::CreateRequestCompletion> for xand_models::CreateCompletion {
    fn convert_from(input: proto_models::CreateRequestCompletion) -> Self {
        match input {
            proto_models::CreateRequestCompletion::Confirmation(d) => {
                Self::Confirmation(d.convert_into())
            }
            proto_models::CreateRequestCompletion::Cancellation(d) => {
                Self::Cancellation(d.convert_into())
            }
            proto_models::CreateRequestCompletion::Expiration(d) => {
                Self::Expiration(d.convert_into())
            }
        }
    }
}

impl FromWrapper<proto_models::RedeemRequestCompletion> for xand_models::RedeemCompletion {
    fn convert_from(input: proto_models::RedeemRequestCompletion) -> Self {
        match input {
            proto_models::RedeemRequestCompletion::Confirmation(d) => {
                Self::Confirmation(d.convert_into())
            }
            proto_models::RedeemRequestCompletion::Cancellation(d) => {
                Self::Cancellation(d.convert_into())
            }
        }
    }
}

impl FromWrapper<proto_models::CreateCancellationReason> for xand_ledger::CreateCancellationReason {
    fn convert_from(r: proto_models::CreateCancellationReason) -> Self {
        match r {
            proto_models::CreateCancellationReason::Expired => Self::Expired,
            proto_models::CreateCancellationReason::InvalidData => Self::InvalidData,
            proto_models::CreateCancellationReason::BankNotFound => Self::BankNotFound,
        }
    }
}

impl FromWrapper<proto_models::CreateCancellation> for xand_models::CancelPendingCreate {
    fn convert_from(input: proto_models::CreateCancellation) -> Self {
        Self {
            correlation_id: input.correlation_id.convert_into(),
            reason: input.reason.convert_into(),
        }
    }
}

impl FromWrapper<proto_models::RedeemCancellationReason> for xand_ledger::RedeemCancellationReason {
    fn convert_from(r: proto_models::RedeemCancellationReason) -> Self {
        match r {
            proto_models::RedeemCancellationReason::InvalidData => Self::InvalidData,
            proto_models::RedeemCancellationReason::AccountNotAllowed => Self::AccountNotAllowed,
        }
    }
}

impl FromWrapper<proto_models::RedeemCancellation> for xand_models::CancelPendingRedeem {
    fn convert_from(input: proto_models::RedeemCancellation) -> Self {
        Self {
            correlation_id: input.correlation_id.convert_into(),
            reason: input.reason.convert_into(),
        }
    }
}

impl FromWrapper<proto_models::BankAccountInfo> for xand_models::BankAccountInfo {
    fn convert_from(input: proto_models::BankAccountInfo) -> Self {
        match input {
            proto_models::BankAccountInfo::Unencrypted(d) => Self::Unencrypted(d.convert_into()),
            //TODO: This invariant could be handled differently. It is not desirable, but tests cover this case to ensure it cannot happen in production.
            proto_models::BankAccountInfo::Encrypted(_) => unimplemented!("cannot convert encrypted bank account info error into encrypted payload with meaningful information")
        }
    }
}

impl FromWrapper<proto_models::BankAccountId> for xand_models::BankAccountId {
    fn convert_from(input: proto_models::BankAccountId) -> Self {
        Self {
            routing_number: input.routing_number,
            account_number: input.account_number,
        }
    }
}

impl FromWrapper<proto_models::Proposal> for xand_models::Proposal {
    fn convert_from(input: proto_models::Proposal) -> Self {
        xand_models::Proposal {
            id: input.id,
            votes: input.votes,
            proposer: input.proposer,
            expiration_block_id: input.expiration_block_id,
            proposed_action: input.proposed_action.convert_into(),
            status: input.status.convert_into(),
        }
    }
}

impl FromWrapper<proto_models::CorrelationId> for xand_models::CorrelationId {
    fn convert_from(input: proto_models::CorrelationId) -> Self {
        input
            .as_bytes()
            .to_vec()
            .try_into()
            .expect("correlation id should be compatible")
    }
}

impl FromWrapper<proto_models::AdministrativeTransaction>
    for xand_models::AdministrativeTransaction
{
    fn convert_from(input: proto_models::AdministrativeTransaction) -> Self {
        match input {
            proto_models::AdministrativeTransaction::RegisterAccountAsMember(x) => {
                xand_models::AdministrativeTransaction::RegisterAccountAsMember(x.convert_into())
            }
            proto_models::AdministrativeTransaction::SetTrust(x) => {
                xand_models::AdministrativeTransaction::SetTrust(x.convert_into())
            }
            proto_models::AdministrativeTransaction::AddAuthorityKey(x) => {
                xand_models::AdministrativeTransaction::AddAuthorityKey(x.convert_into())
            }
            proto_models::AdministrativeTransaction::RemoveAuthorityKey(x) => {
                xand_models::AdministrativeTransaction::RemoveAuthorityKey(x.convert_into())
            }
            proto_models::AdministrativeTransaction::RootAllowlistCidrBlock(x) => {
                xand_models::AdministrativeTransaction::RootAllowlistCidrBlock(x.convert_into())
            }
            proto_models::AdministrativeTransaction::RootRemoveAllowlistCidrBlock(x) => {
                xand_models::AdministrativeTransaction::RootRemoveAllowlistCidrBlock(
                    x.convert_into(),
                )
            }
            proto_models::AdministrativeTransaction::RuntimeUpgrade(_) => {
                unimplemented!()
            }
            proto_models::AdministrativeTransaction::RemoveMember(x) => {
                xand_models::AdministrativeTransaction::RemoveMember(x.convert_into())
            }
            proto_models::AdministrativeTransaction::SetLimitedAgent(x) => {
                xand_models::AdministrativeTransaction::SetLimitedAgent(x.convert_into())
            }
            proto_models::AdministrativeTransaction::SetValidatorEmissionRate(x) => {
                xand_models::AdministrativeTransaction::SetValidatorEmissionRate(x.convert_into())
            }
        }
    }
}

impl FromWrapper<proto_models::SubmitProposal> for xand_models::SubmitProposal {
    fn convert_from(input: proto_models::SubmitProposal) -> Self {
        Self {
            proposed_action: input.proposed_action.convert_into(),
        }
    }
}

impl FromWrapper<proto_models::VoteProposal> for xand_models::VoteProposal {
    fn convert_from(input: proto_models::VoteProposal) -> Self {
        Self {
            id: input.id,
            vote: input.vote,
        }
    }
}

impl FromWrapper<proto_models::SetPendingCreateRequestExpire>
    for xand_models::SetPendingCreateExpire
{
    fn convert_from(input: proto_models::SetPendingCreateRequestExpire) -> Self {
        Self {
            expire_in_milliseconds: input.expire_in_milliseconds,
        }
    }
}

impl FromWrapper<proto_models::RegisterAccountAsMember> for xand_models::RegisterAccountAsMember {
    fn convert_from(input: proto_models::RegisterAccountAsMember) -> Self {
        xand_models::RegisterAccountAsMember {
            address: input.address,
            encryption_key: input.encryption_key.convert_into(),
        }
    }
}

impl FromWrapper<proto_models::RegisterSessionKeys> for xand_models::RegisterSessionKeys {
    fn convert_from(input: proto_models::RegisterSessionKeys) -> Self {
        Self {
            block_production_pubkey: input.block_production_pubkey,
            block_finalization_pubkey: input.block_finalization_pubkey,
        }
    }
}

impl FromWrapper<proto_models::SetLimitedAgentId> for xand_models::SetLimitedAgentId {
    fn convert_from(input: proto_models::SetLimitedAgentId) -> Self {
        Self {
            address: input.address,
        }
    }
}

impl FromWrapper<proto_models::SetValidatorEmissionRate> for xand_models::SetValidatorEmissionRate {
    fn convert_from(input: proto_models::SetValidatorEmissionRate) -> Self {
        Self {
            block_quota: input.block_quota,
            minor_units_per_emission: input.minor_units_per_emission,
        }
    }
}

impl FromWrapper<proto_models::SetTrustNodeId> for xand_models::SetTrustNodeId {
    fn convert_from(input: proto_models::SetTrustNodeId) -> Self {
        xand_models::SetTrustNodeId {
            address: input.address,
            encryption_key: input.encryption_key.convert_into(),
        }
    }
}

impl FromWrapper<proto_models::SetMemberEncKey> for xand_models::SetMemberEncKey {
    fn convert_from(input: proto_models::SetMemberEncKey) -> Self {
        Self {
            key: input.key.convert_into(),
        }
    }
}

impl FromWrapper<proto_models::SetTrustEncKey> for xand_models::SetTrustEncKey {
    fn convert_from(input: proto_models::SetTrustEncKey) -> Self {
        Self {
            key: input.key.convert_into(),
        }
    }
}

impl FromWrapper<proto_models::AddAuthorityKey> for xand_models::AddAuthorityKey {
    fn convert_from(input: proto_models::AddAuthorityKey) -> Self {
        xand_models::AddAuthorityKey {
            account_id: input.account_id,
        }
    }
}

impl FromWrapper<proto_models::RemoveAuthorityKey> for xand_models::RemoveAuthorityKey {
    fn convert_from(input: proto_models::RemoveAuthorityKey) -> Self {
        xand_models::RemoveAuthorityKey {
            account_id: input.account_id,
        }
    }
}

impl FromWrapper<proto_models::AllowlistCidrBlock> for xand_models::AllowlistCidrBlock {
    fn convert_from(input: proto_models::AllowlistCidrBlock) -> Self {
        Self {
            cidr_block: input.cidr_block.convert_into(),
        }
    }
}

impl FromWrapper<proto_models::RootAllowlistCidrBlock> for xand_models::RootAllowlistCidrBlock {
    fn convert_from(input: proto_models::RootAllowlistCidrBlock) -> Self {
        Self {
            account: input.account,
            cidr_block: input.cidr_block.convert_into(),
        }
    }
}

impl FromWrapper<proto_models::RemoveAllowlistCidrBlock> for xand_models::RemoveAllowlistCidrBlock {
    fn convert_from(input: proto_models::RemoveAllowlistCidrBlock) -> Self {
        Self {
            cidr_block: input.cidr_block.convert_into(),
        }
    }
}

impl FromWrapper<proto_models::RootRemoveAllowlistCidrBlock>
    for xand_models::RootRemoveAllowlistCidrBlock
{
    fn convert_from(input: proto_models::RootRemoveAllowlistCidrBlock) -> Self {
        Self {
            account: input.account,
            cidr_block: input.cidr_block.convert_into(),
        }
    }
}

impl FromWrapper<proto_models::RemoveMember> for xand_models::RemoveMember {
    fn convert_from(input: proto_models::RemoveMember) -> Self {
        xand_models::RemoveMember {
            address: input.address,
        }
    }
}

impl FromWrapper<proto_models::ExitMember> for xand_models::ExitMember {
    fn convert_from(input: proto_models::ExitMember) -> Self {
        xand_models::ExitMember {
            address: input.address,
        }
    }
}

impl FromWrapper<proto_models::CidrBlock> for xand_models::CidrBlock {
    fn convert_from(input: proto_models::CidrBlock) -> Self {
        Self(input.0)
    }
}

impl FromWrapper<proto_models::PublicKey> for xand_models::X25519PublicKey {
    fn convert_from(input: proto_models::PublicKey) -> Self {
        (*input.as_bytes()).into()
    }
}

impl FromWrapper<proto_models::ProposalStage> for xand_models::ProposalStage {
    fn convert_from(input: proto_models::ProposalStage) -> Self {
        match input {
            proto_models::ProposalStage::Proposed => xand_models::ProposalStage::Proposed,
            proto_models::ProposalStage::Accepted => xand_models::ProposalStage::Accepted,
            proto_models::ProposalStage::Rejected => xand_models::ProposalStage::Rejected,
            proto_models::ProposalStage::Invalid => xand_models::ProposalStage::Invalid,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use xand_api_proto::proto_models;

    use proptest::prelude::*;

    proptest! {
        #[test]
        fn proto_transaction_id_roundtrips_through_xand_models_transaction_id(bytes: [u8; 32]) {
            let input = proto_models::TransactionId::from(bytes);
            let there_and_back = proto_models::TransactionId::convert_from(xand_models::TransactionId::convert_from(input.clone()));

            prop_assert_eq!(input, there_and_back);
        }
    }

    proptest! {
        #[test]
        fn xand_models_transaction_id_roundtrips_through_proto_models_transaction_id(bytes: [u8; 32]) {
            let input = xand_models::TransactionId::from(bytes);
            let there_and_back = xand_models::TransactionId::convert_from(proto_models::TransactionId::convert_from(input.clone()));

            prop_assert_eq!(input, there_and_back);
        }
    }
}
