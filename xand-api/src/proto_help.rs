use crate::errors::{BadAddress, Result, XandApiErrors};
use snafu::ResultExt;
use sp_core::{crypto::Ss58Codec, ed25519, sr25519};
use std::convert::{TryFrom, TryInto};
use std::{str::FromStr, sync::Arc};
use xand_api_proto::{
    self, administrative_transaction as admin_txn, user_txn as user_transaction, AddAuthorityKey,
    AllowlistCidrBlock, CashConfirmation, CreateCancellation, ExitMember, OptionalAddress,
    RedeemCancellation, RedeemFulfillment, RegisterAccountAsMember, RegisterSessionKeys,
    RemoveAllowlistCidrBlock, RemoveAuthorityKey, RemoveMember, RootAllowlistCidrBlock,
    SetLimitedAgent, SetPendingCreateRequestExpire, SetTrust, SetValidatorEmissionRate,
    UserTransaction,
};
use xand_ledger::{
    CreateCancellationTransaction, RedeemCancellationTransaction, RedeemFulfillmentTransaction,
};
use xand_models::CorrelationId;
use xand_runtime_models::{bytes_as_fiat_correlation_id, CidrBlock, FiatReqCorrelationId};
use xandstrate_client::{
    add_authority_key_extrinsic, allowlist_cidr_block, confidential_create_cancellation_extrinsic,
    confidential_create_extrinsic, confidential_redeem_cancellation,
    confidential_redeem_fulfillment, exit_member_extrinsic, register_member_extrinsic,
    register_session_keys_extrinsic, remove_allowlist_cidr_block, remove_authority_key_extrinsic,
    remove_member_extrinsic, root_allowlist_cidr_block, root_remove_allowlist_cidr_block,
    set_limited_agent_id_extrinsic, set_pending_create_expire_time_extrinsic,
    set_trust_node_id_extrinsic, set_validator_emission_rate_extrinsic, submit_proposal_extrinsic,
    vote_on_proposal_extrinsic, withdraw_from_network, Call,
};

/// Wraps the `Call` from the runtime (which defines all possible extrinsics) so that we can
/// provide conversions from the API protobuf types. It is done inside of this crate, because otherwise
/// we'd have to have the crate containing the protobuf types take an optional dependency on
/// the runtime.
// For some reason `Into` doesn't work because substrate is doing a weird blanket impl
// inside the runtime.
#[derive(derive_more::Deref, derive_more::From, Debug)]
pub(crate) struct CallWrapper(pub(crate) Call);

impl CallWrapper {
    fn into_proposal(self) -> Self {
        submit_proposal_extrinsic(self.0).into()
    }
}

pub(crate) fn from_create_to_confidential_create_fulfillment(
    c: CashConfirmation,
) -> Result<CallWrapper, XandApiErrors> {
    let chopped: FiatReqCorrelationId =
        bytes_as_fiat_correlation_id(c.correlation_id.parse::<CorrelationId>()?.as_bytes())?;

    Ok(
        confidential_create_extrinsic(xand_ledger::CashConfirmationTransaction {
            correlation_id: chopped,
        })
        .into(),
    )
}

pub(crate) fn from_create_cancellation_to_confidential_create_cancellation(
    c: CreateCancellation,
) -> Result<CallWrapper, XandApiErrors> {
    let correlation_id: FiatReqCorrelationId =
        bytes_as_fiat_correlation_id(c.correlation_id.parse::<CorrelationId>()?.as_bytes())?;
    let reason = c
        .reason
        .parse()
        .map_err(|source| XandApiErrors::InvalidCancellationReason {
            source: Arc::new(source),
        })?;
    Ok(
        confidential_create_cancellation_extrinsic(CreateCancellationTransaction {
            correlation_id,
            reason,
        })
        .into(),
    )
}

pub(crate) fn make_redeem_fulfillment_confidential(
    t: RedeemFulfillment,
) -> Result<CallWrapper, XandApiErrors> {
    let correlation_id =
        bytes_as_fiat_correlation_id(t.correlation_id.parse::<CorrelationId>()?.as_bytes())?;
    Ok(confidential_redeem_fulfillment(RedeemFulfillmentTransaction { correlation_id }).into())
}

pub(crate) fn make_redeem_cancellation_confidential(
    t: RedeemCancellation,
) -> Result<CallWrapper, XandApiErrors> {
    let correlation_id =
        bytes_as_fiat_correlation_id(t.correlation_id.parse::<CorrelationId>()?.as_bytes())?;
    let reason = t
        .reason
        .parse()
        .map_err(|source| XandApiErrors::InvalidCancellationReason {
            source: Arc::new(source),
        })?;
    Ok(
        confidential_redeem_cancellation(RedeemCancellationTransaction {
            correlation_id,
            reason,
        })
        .into(),
    )
}

impl TryFrom<AllowlistCidrBlock> for CallWrapper {
    type Error = XandApiErrors;
    fn try_from(c: AllowlistCidrBlock) -> Result<Self, Self::Error> {
        Ok(allowlist_cidr_block(
            c.cidr_block
                .parse()
                .map_err(|_| XandApiErrors::BadPayload {
                    reason: format!("{:?} is not a parsable cidr block", c),
                })?,
        )
        .into())
    }
}

impl TryFrom<RemoveAllowlistCidrBlock> for CallWrapper {
    type Error = XandApiErrors;
    fn try_from(c: RemoveAllowlistCidrBlock) -> Result<Self, Self::Error> {
        Ok(
            remove_allowlist_cidr_block(c.cidr_block.parse().map_err(|_| {
                XandApiErrors::BadPayload {
                    reason: format!("{:?} is not a parsable cidr block", c),
                }
            })?)
            .into(),
        )
    }
}

impl TryFrom<SetPendingCreateRequestExpire> for CallWrapper {
    type Error = XandApiErrors;
    fn try_from(c: SetPendingCreateRequestExpire) -> Result<Self, Self::Error> {
        Ok(set_pending_create_expire_time_extrinsic(c.expire_in_milliseconds).into())
    }
}

pub(crate) async fn transaction_req_to_call(
    transaction: UserTransaction,
) -> Result<CallWrapper, XandApiErrors> {
    match transaction.operation {
        Some(user_transaction::Operation::RegisterSessionKeys(v)) => Ok(v.try_into()?),
        Some(user_transaction::Operation::SetPendingCreateRequestExpire(v)) => Ok(v.try_into()?),
        Some(user_transaction::Operation::CashConfirmation(_))
        | Some(user_transaction::Operation::CreateRequest(_))
        | Some(user_transaction::Operation::CreateCancellation(_))
        | Some(user_transaction::Operation::RedeemFulfillment(_))
        | Some(user_transaction::Operation::RedeemCancellation(_))
        | Some(user_transaction::Operation::RedeemRequest(_))
        | Some(user_transaction::Operation::Send(_)) => Err(XandApiErrors::UnsupportedEndpoint {
            reason: "Non-confidential Xand transactions no longer supported".to_string(),
        }),
        Some(user_transaction::Operation::AllowlistCidrBlock(v)) => Ok(v.try_into()?),
        Some(user_transaction::Operation::RemoveCidrBlock(v)) => Ok(v.try_into()?),
        Some(user_transaction::Operation::WithdrawFromNetwork(_)) => {
            Ok(withdraw_from_network().into())
        }
        None => Err(XandApiErrors::BadPayload {
            reason: "`operation` field on UserTransaction must be specified!".to_string(),
        }),
    }
}

impl TryFrom<RegisterAccountAsMember> for CallWrapper {
    type Error = XandApiErrors;

    fn try_from(value: RegisterAccountAsMember) -> Result<Self, Self::Error> {
        let address = sr25519::Public::from_ss58check(&value.address).context(BadAddress {
            addr: value.address,
        })?;
        Ok(register_member_extrinsic(address.0.into(), value.encryption_key.try_into()?).into())
    }
}

impl TryFrom<RemoveMember> for CallWrapper {
    type Error = XandApiErrors;

    fn try_from(value: RemoveMember) -> Result<Self, Self::Error> {
        let address = sr25519::Public::from_ss58check(&value.address).context(BadAddress {
            addr: value.address,
        })?;
        Ok(remove_member_extrinsic(address.0.into()).into())
    }
}

impl TryFrom<ExitMember> for CallWrapper {
    type Error = XandApiErrors;

    fn try_from(value: ExitMember) -> Result<Self, Self::Error> {
        let address = sr25519::Public::from_ss58check(&value.address).context(BadAddress {
            addr: value.address,
        })?;
        Ok(exit_member_extrinsic(address.0.into()).into())
    }
}

impl TryFrom<SetTrust> for CallWrapper {
    type Error = XandApiErrors;

    fn try_from(value: SetTrust) -> Result<Self, Self::Error> {
        let address = sr25519::Public::from_ss58check(&value.address).context(BadAddress {
            addr: value.address,
        })?;
        Ok(set_trust_node_id_extrinsic(address.0.into(), value.encryption_key.try_into()?).into())
    }
}

impl TryFrom<SetLimitedAgent> for CallWrapper {
    type Error = XandApiErrors;

    fn try_from(value: SetLimitedAgent) -> Result<Self, Self::Error> {
        let address = value
            .address
            .map(|OptionalAddress { address_str }| {
                sr25519::Public::from_ss58check(&address_str)
                    .context(BadAddress { addr: address_str })
            })
            .transpose()?;

        Ok(set_limited_agent_id_extrinsic(address.map(|p| p.0.into())).into())
    }
}

impl TryFrom<SetValidatorEmissionRate> for CallWrapper {
    type Error = XandApiErrors;

    fn try_from(value: SetValidatorEmissionRate) -> Result<Self, Self::Error> {
        let SetValidatorEmissionRate {
            minor_units_per_emission,
            block_quota,
        } = value;
        Ok(set_validator_emission_rate_extrinsic(minor_units_per_emission, block_quota).into())
    }
}

impl TryFrom<AddAuthorityKey> for CallWrapper {
    type Error = XandApiErrors;

    fn try_from(value: AddAuthorityKey) -> Result<Self, Self::Error> {
        let address = sr25519::Public::from_ss58check(&value.address).context(BadAddress {
            addr: value.address,
        })?;
        Ok(add_authority_key_extrinsic(address.0.into()).into())
    }
}

impl TryFrom<RemoveAuthorityKey> for CallWrapper {
    type Error = XandApiErrors;

    fn try_from(value: RemoveAuthorityKey) -> Result<Self, Self::Error> {
        let address = sr25519::Public::from_ss58check(&value.address).context(BadAddress {
            addr: value.address,
        })?;
        Ok(remove_authority_key_extrinsic(address.0.into()).into())
    }
}

impl TryFrom<RegisterSessionKeys> for CallWrapper {
    type Error = XandApiErrors;

    fn try_from(value: RegisterSessionKeys) -> Result<Self, Self::Error> {
        let block_production_pubkey = sr25519::Public::from_ss58check(
            &value.block_production_pubkey,
        )
        .context(BadAddress {
            addr: value.block_production_pubkey,
        })?;
        let block_finalization_pubkey = ed25519::Public::from_ss58check(
            &value.block_finalization_pubkey,
        )
        .context(BadAddress {
            addr: value.block_finalization_pubkey,
        })?;
        Ok(
            register_session_keys_extrinsic(block_production_pubkey, block_finalization_pubkey)
                .into(),
        )
    }
}

impl TryFrom<RootAllowlistCidrBlock> for CallWrapper {
    type Error = XandApiErrors;

    fn try_from(value: xand_api_proto::RootAllowlistCidrBlock) -> Result<Self, Self::Error> {
        let account = sr25519::Public::from_ss58check(&value.address).context(BadAddress {
            addr: value.address,
        })?;
        let cidr_block =
            CidrBlock::from_str(&value.cidr_block).map_err(|e| XandApiErrors::BadPayload {
                reason: format!("Invalid CIDR block: {:?}", e),
            })?;
        Ok(root_allowlist_cidr_block(account.0.into(), cidr_block).into())
    }
}

impl TryFrom<xand_api_proto::RootRemoveAllowlistCidrBlock> for CallWrapper {
    type Error = XandApiErrors;

    fn try_from(value: xand_api_proto::RootRemoveAllowlistCidrBlock) -> Result<Self, Self::Error> {
        let account = sr25519::Public::from_ss58check(&value.address).context(BadAddress {
            addr: value.address,
        })?;
        let cidr_block =
            CidrBlock::from_str(&value.cidr_block).map_err(|e| XandApiErrors::BadPayload {
                reason: format!("Invalid CIDR block: {:?}", e),
            })?;
        Ok(root_remove_allowlist_cidr_block(account.0.into(), cidr_block).into())
    }
}

pub(crate) fn proposition_req_to_call(
    transaction: xand_api_proto::AdministrativeTransaction,
) -> Result<CallWrapper, XandApiErrors> {
    let action: CallWrapper = match transaction.operation {
        Some(admin_txn::Operation::RegisterAccountAsMember(v)) => v.try_into()?,
        Some(admin_txn::Operation::RemoveMember(r)) => r.try_into()?,
        Some(admin_txn::Operation::SetTrust(v)) => v.try_into()?,
        Some(admin_txn::Operation::SetLimitedAgent(v)) => v.try_into()?,
        Some(admin_txn::Operation::SetValidatorEmissionRate(v)) => v.try_into()?,
        Some(admin_txn::Operation::AddAuthorityKey(v)) => v.try_into()?,
        Some(admin_txn::Operation::RemoveAuthorityKey(v)) => v.try_into()?,
        Some(admin_txn::Operation::RootAllowlistCidrBlock(v)) => v.try_into()?,
        Some(admin_txn::Operation::RootRemoveAllowlistCidrBlock(v)) => v.try_into()?,
        Some(admin_txn::Operation::RuntimeUpgrade(_)) => unimplemented!(),
        None => {
            return Err(XandApiErrors::BadPayload {
                reason: "`operation` field on UserTransaction must be specified!".to_string(),
            });
        }
    };
    Ok(action.into_proposal())
}

impl TryFrom<xand_api_proto::VoteProposal> for CallWrapper {
    type Error = XandApiErrors;

    fn try_from(value: xand_api_proto::VoteProposal) -> Result<CallWrapper, Self::Error> {
        Ok(vote_on_proposal_extrinsic(value.id, value.vote).into())
    }
}

pub(crate) fn vote_req_to_call(
    transaction: xand_api_proto::VotingTransaction,
) -> Result<CallWrapper, XandApiErrors> {
    let call_wrapper = transaction
        .vote_proposal
        .ok_or_else(|| XandApiErrors::BadPayload {
            reason: "`vote_proposal` field on VotingTransaction must be specified!".to_string(),
        })?
        .try_into()?;
    Ok(call_wrapper)
}
