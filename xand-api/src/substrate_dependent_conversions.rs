use std::convert::From;
use xand_api_proto::error::XandApiProtoErrs;
pub use xand_models::{BankAccountConversionErr, BankAccountId, BankAccountInfo};
use xand_runtime_models::PubkeyWrongSizeError;

pub(crate) struct XandWireTypeError(pub(crate) XandApiProtoErrs);

/// Helper types and methods for xand-api-client and xand-api-proto which use Substrate dependencies
/// and Substrate implementation-specific types. Keeping this code outside of the client allows us
/// to keep the API surface area free of Substrate dependencies
impl From<PubkeyWrongSizeError> for XandWireTypeError {
    fn from(e: PubkeyWrongSizeError) -> Self {
        XandWireTypeError(XandApiProtoErrs::ConversionError {
            reason: format!(
                "Encryption key in proto message was the wrong size: {:?}",
                e
            ),
        })
    }
}
