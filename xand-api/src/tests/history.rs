use crate::{
    tests::{
        default_test_env, from_xand_txn, no_filters_txn_history, store_sends,
        transaction_list_from_proto, MetadataValue, BIGGIE, CHAIN_DATASOURCE_INFO_METADATA_KEY,
        OTHER, TUPAC,
    },
    xand_models_converter::{FromWrapper, IntoWrapper},
    AddressTransactionHistoryRequest, Request, TransactionDetailsRequest,
    TransactionHistoryRequest, XandApi,
};
use chrono::{TimeZone, Utc};
use petri::{HistoricalStore, LastBlockInfo};
use std::convert::TryFrom;
use xand_api_proto::proto_models;
use xand_models::{ToAddress, TransactionId, TransactionType};
use xandstrate_client::{Pair, Ss58Codec};

#[tokio::test]
async fn get_transaction_works() {
    let env = default_test_env().await;
    let fake_tx = proto_models::XandTransaction::Send(proto_models::Send {
        amount_in_minor_unit: 200,
        destination_account: TUPAC.public().to_address(),
    });
    let fake_id: proto_models::TransactionId = [42_u8; 32].into();
    let wrapper = from_xand_txn(
        fake_id.clone(),
        fake_tx,
        TUPAC.public().to_address(),
        proto_models::TransactionStatus::Committed,
    );
    env.service.history_store.store(wrapper.convert_into());
    let result = env
        .service
        .impl_get_tx_details(TransactionDetailsRequest {
            id: fake_id.to_string(),
        })
        .await
        .unwrap()
        .dat;
    assert!(result.is_some());
    assert_eq!(result.unwrap().id, fake_id.to_string());
}

#[tokio::test]
async fn transaction_notfound() {
    let env = default_test_env().await;
    let fake_id: TransactionId = [42_u8; 32].into();
    let result = env
        .service
        .impl_get_tx_details(TransactionDetailsRequest {
            id: fake_id.to_string(),
        })
        .await
        .unwrap()
        .dat;
    assert!(result.is_none());
}

#[tokio::test]
async fn get_address_history_works() {
    let env = default_test_env().await;
    // One outgoing and one incoming send, and one unrelated.
    let fake_id_1: proto_models::TransactionId = [42_u8; 32].into();
    let fake_id_2: proto_models::TransactionId = [43_u8; 32].into();
    let fake_id_3: proto_models::TransactionId = [44_u8; 32].into();
    store_sends(
        &env.service.history_store,
        fake_id_1.clone(),
        fake_id_2.clone(),
        fake_id_3,
    );
    let result = env
        .service
        .impl_get_addr_txns(AddressTransactionHistoryRequest {
            address: TUPAC.public().to_ss58check(),
            page_size: 0,
            page_number: 0,
        })
        .unwrap()
        .dat;
    assert_eq!(result.total, 2);
    assert_eq!(result.transactions.len(), 2);
    assert!(result
        .transactions
        .iter()
        .any(|t| t.id == fake_id_1.to_string()));
    assert!(result
        .transactions
        .iter()
        .any(|t| t.id == fake_id_2.to_string()));
    // Also test getting history for multiple addresses
    let result = env
        .service
        .impl_get_tx_hist(TransactionHistoryRequest {
            addresses: vec![TUPAC.public().to_ss58check(), OTHER.public().to_ss58check()],
            page_size: 0,
            page_number: 0,
            transaction_types: Vec::new(),
            start_time: None,
            end_time: None,
        })
        .await
        .unwrap()
        .dat;
    assert_eq!(result.total, 3);
    assert_eq!(result.transactions.len(), 3);
}

#[tokio::test]
async fn history_pagination_works() {
    let env = default_test_env().await;
    // Store many transactions -- 3 * 200 = 600
    for i in 1..=200 {
        let mut id1 = [0_u8; 32];
        id1[0] = i;
        let mut id2 = [0_u8; 32];
        id2[1] = i;
        let mut id3 = [0_u8; 32];
        id3[2] = i;
        store_sends(
            &env.service.history_store,
            id1.into(),
            id2.into(),
            id3.into(),
        );
    }

    // Test getting history for both addresses has the right total and page size chop works
    let addresses = vec![TUPAC.public().to_ss58check(), OTHER.public().to_ss58check()];
    let result = env
        .service
        .impl_get_tx_hist(TransactionHistoryRequest {
            addresses: addresses.clone(),
            page_size: 100,
            page_number: 0,
            transaction_types: Vec::new(),
            start_time: None,
            end_time: None,
        })
        .await
        .unwrap()
        .dat;
    assert_eq!(result.total, 600);
    assert_eq!(result.transactions.len(), 100);
    let result2 = env
        .service
        .impl_get_tx_hist(TransactionHistoryRequest {
            addresses: addresses.clone(),
            page_size: 50,
            page_number: 1,
            transaction_types: Vec::new(),
            start_time: None,
            end_time: None,
        })
        .await
        .unwrap()
        .dat;
    assert_eq!(result2.total, 600);
    assert_eq!(result2.transactions.len(), 50);
    // Page 1 of size 50 should == the second half of page 0 size 100
    assert_eq!(&result.transactions[50..], result2.transactions.as_slice());

    let remainder = env
        .service
        .impl_get_tx_hist(TransactionHistoryRequest {
            addresses,
            page_size: 599,
            page_number: 1,
            transaction_types: Vec::new(),
            start_time: None,
            end_time: None,
        })
        .await
        .unwrap()
        .dat;
    assert_eq!(remainder.transactions.len(), 1);
}

#[tokio::test]
async fn filtering_by_single_transaction_type_returns_only_transactions_of_that_type() {
    let mut env = default_test_env().await;
    let transactions = env.generate_transactions();
    env.store_transactions(&transactions);
    let expected_transactions: Vec<_> = transactions
        .into_iter()
        .rev()
        .filter(|t| matches!(t.txn, proto_models::XandTransaction::Send(_)))
        .collect();
    let result = env
        .service
        .impl_get_tx_hist(TransactionHistoryRequest {
            transaction_types: vec![xand_api_proto::TransactionType::from(
                proto_models::TransactionType::convert_from(TransactionType::Send),
            ) as i32],
            ..no_filters_txn_history()
        })
        .await
        .unwrap()
        .dat;
    let cast_result = usize::try_from(result.total).unwrap();

    assert_eq!(cast_result, expected_transactions.len());
    assert_eq!(
        transaction_list_from_proto(result.transactions),
        expected_transactions
    );
}

#[tokio::test]
async fn filtering_by_multiple_transaction_types_returns_transactions_of_either_type() {
    let mut env = default_test_env().await;
    let transactions = env.generate_transactions();
    env.store_transactions(&transactions);
    let expected_transactions: Vec<_> = transactions
        .into_iter()
        .rev()
        .filter(|t| {
            matches!(
                t.txn,
                proto_models::XandTransaction::Send(_)
                    | proto_models::XandTransaction::CreateRequest(_)
            )
        })
        .collect();
    let result = env
        .service
        .impl_get_tx_hist(TransactionHistoryRequest {
            transaction_types: vec![
                xand_api_proto::TransactionType::from(proto_models::TransactionType::convert_from(
                    TransactionType::Send,
                )) as i32,
                xand_api_proto::TransactionType::from(proto_models::TransactionType::convert_from(
                    TransactionType::CreatePendingCreate,
                )) as i32,
            ],
            ..no_filters_txn_history()
        })
        .await
        .unwrap()
        .dat;
    let cast_total = usize::try_from(result.total).unwrap();

    assert_eq!(cast_total, expected_transactions.len());
    assert_eq!(
        transaction_list_from_proto(result.transactions),
        expected_transactions
    );
}

#[tokio::test]
async fn filtering_by_start_time_returns_transactions_after() {
    let mut env = default_test_env().await;
    let transactions = env.generate_transactions();
    env.store_transactions(&transactions);
    let (start, expected_transactions) = {
        let mut list = transactions;
        let start = list[1].timestamp;
        list.reverse();
        let _ = list.pop();
        (start, list)
    };
    let result = env
        .service
        .impl_get_tx_hist(TransactionHistoryRequest {
            start_time: Some(xand_api_proto::Timestamp {
                unix_time_millis: start.timestamp_millis(),
            }),
            ..no_filters_txn_history()
        })
        .await
        .unwrap()
        .dat;
    let cast_total = usize::try_from(result.total).unwrap();

    assert_eq!(cast_total, expected_transactions.len());
    assert_eq!(
        transaction_list_from_proto(result.transactions),
        expected_transactions
    );
}

#[tokio::test]
async fn filtering_by_end_time_returns_transactions_before() {
    let mut env = default_test_env().await;
    let transactions = env.generate_transactions();
    env.store_transactions(&transactions);
    let (end, expected_transactions) = {
        let mut list = transactions;
        let _ = list.pop();
        list.reverse();
        let end = list[0].timestamp;
        (end, list)
    };
    let result = env
        .service
        .impl_get_tx_hist(TransactionHistoryRequest {
            end_time: Some(xand_api_proto::Timestamp {
                unix_time_millis: end.timestamp_millis(),
            }),
            ..no_filters_txn_history()
        })
        .await
        .unwrap()
        .dat;
    let cast_total = usize::try_from(result.total).unwrap();

    assert_eq!(cast_total, expected_transactions.len());
    assert_eq!(
        transaction_list_from_proto(result.transactions),
        expected_transactions
    );
}

#[tokio::test]
async fn filtering_by_time_range_returns_transactions_within() {
    let mut env = default_test_env().await;
    let transactions = env.generate_transactions();
    env.store_transactions(&transactions);
    let (start, end, expected_transactions) = {
        let mut list = transactions;
        let start = list[1].timestamp;
        let _ = list.pop();
        list.reverse();
        let _ = list.pop();
        let end = list[0].timestamp;
        (start, end, list)
    };
    let result = env
        .service
        .impl_get_tx_hist(TransactionHistoryRequest {
            start_time: Some(xand_api_proto::Timestamp {
                unix_time_millis: start.timestamp_millis(),
            }),
            end_time: Some(xand_api_proto::Timestamp {
                unix_time_millis: end.timestamp_millis(),
            }),
            ..no_filters_txn_history()
        })
        .await
        .unwrap()
        .dat;
    let cast_total = usize::try_from(result.total).unwrap();

    assert_eq!(cast_total, expected_transactions.len());
    assert_eq!(
        transaction_list_from_proto(result.transactions),
        expected_transactions
    );
}

#[tokio::test]
async fn filtering_by_transaction_issuer_returns_transaction() {
    let env = default_test_env().await;
    let transactions = vec![
        from_xand_txn(
            [1; 32].into(),
            proto_models::XandTransaction::Send(proto_models::Send {
                amount_in_minor_unit: 1,
                destination_account: BIGGIE.public().to_address(),
            }),
            TUPAC.public().to_address(),
            proto_models::TransactionStatus::Committed,
        ),
        from_xand_txn(
            [2; 32].into(),
            proto_models::XandTransaction::Send(proto_models::Send {
                amount_in_minor_unit: 10,
                destination_account: BIGGIE.public().to_address(),
            }),
            TUPAC.public().to_address(),
            proto_models::TransactionStatus::Committed,
        ),
    ];
    env.store_transactions(&transactions);
    let expected_transactions = {
        let mut list = transactions;
        list.reverse();
        list
    };
    let result = env
        .service
        .impl_get_tx_hist(TransactionHistoryRequest {
            addresses: vec![TUPAC.public().to_ss58check()],
            ..no_filters_txn_history()
        })
        .await
        .unwrap()
        .dat;
    let cast_total = usize::try_from(result.total).unwrap();

    assert_eq!(cast_total, expected_transactions.len());
    assert_eq!(
        transaction_list_from_proto(result.transactions),
        expected_transactions
    );
}

#[tokio::test]
async fn filtering_by_send_recipient_returns_send() {
    let env = default_test_env().await;
    let transactions = vec![from_xand_txn(
        [1; 32].into(),
        proto_models::XandTransaction::Send(proto_models::Send {
            amount_in_minor_unit: 1,
            destination_account: BIGGIE.public().to_address(),
        }),
        TUPAC.public().to_address(),
        proto_models::TransactionStatus::Committed,
    )];
    env.store_transactions(&transactions);
    let expected_transactions = {
        let mut list = transactions;
        list.reverse();
        list
    };
    let result = env
        .service
        .impl_get_tx_hist(TransactionHistoryRequest {
            addresses: vec![BIGGIE.public().to_ss58check()],
            ..no_filters_txn_history()
        })
        .await
        .unwrap()
        .dat;
    let cast_total = usize::try_from(result.total).unwrap();

    assert_eq!(cast_total, expected_transactions.len());
    assert_eq!(
        transaction_list_from_proto(result.transactions),
        expected_transactions
    );
}

#[tokio::test]
async fn filtering_by_issuer_and_recipient_returns_transaction_only_once() {
    let env = default_test_env().await;
    let transactions = [from_xand_txn(
        [1; 32].into(),
        proto_models::XandTransaction::Send(proto_models::Send {
            amount_in_minor_unit: 1,
            destination_account: BIGGIE.public().to_address(),
        }),
        TUPAC.public().to_address(),
        proto_models::TransactionStatus::Committed,
    )];
    env.store_transactions(&transactions);
    let expected_transactions = {
        let mut list = transactions;
        list.reverse();
        list
    };
    let result = env
        .service
        .impl_get_tx_hist(TransactionHistoryRequest {
            addresses: vec![
                TUPAC.public().to_ss58check(),
                BIGGIE.public().to_ss58check(),
            ],
            ..no_filters_txn_history()
        })
        .await
        .unwrap()
        .dat;
    let cast_total = usize::try_from(result.total).unwrap();

    assert_eq!(cast_total, expected_transactions.len());
    assert_eq!(
        transaction_list_from_proto(result.transactions),
        expected_transactions
    );
}

#[tokio::test]
async fn no_filtering_returns_all_cached_transactions() {
    let mut env = default_test_env().await;
    let transactions = env.generate_transactions();
    env.store_transactions(&transactions);
    let expected_transactions = {
        let mut list = transactions;
        list.reverse();
        list
    };
    let result = env
        .service
        .impl_get_tx_hist(no_filters_txn_history())
        .await
        .unwrap()
        .dat;
    let cast_total = usize::try_from(result.total).unwrap();

    assert_eq!(cast_total, expected_transactions.len());
    assert_eq!(
        transaction_list_from_proto(result.transactions),
        expected_transactions
    );
}

#[tokio::test]
async fn transactions_are_in_order_from_newer_to_older() {
    let mut env = default_test_env().await;
    let transactions = env.generate_transactions();
    env.store_transactions(&transactions);
    let expected_transactions = {
        let mut list = transactions;
        list.reverse();
        list
    };
    let result = env
        .service
        .impl_get_tx_hist(no_filters_txn_history())
        .await
        .unwrap()
        .dat;
    let cast_total = usize::try_from(result.total).unwrap();

    assert_eq!(cast_total, expected_transactions.len());
    assert_eq!(
        transaction_list_from_proto(result.transactions),
        expected_transactions
    );
}

#[tokio::test]
async fn last_block_info_is_attached_to_metadata_petri_src() {
    let env = default_test_env().await;
    env.service
        .history_store
        .set_last_processed_block_info(LastBlockInfo {
            number: 1337,
            timestamp: Utc.timestamp_millis(0),
        })
        .unwrap();
    let result = env
        .service
        .get_transaction_history(Request::new(no_filters_txn_history()))
        .await
        .unwrap();
    let cast_result = usize::try_from(result.get_ref().total).unwrap();
    assert_eq!(cast_result, 0);
    let met_value = result.metadata().get(CHAIN_DATASOURCE_INFO_METADATA_KEY);
    let expected = r#"{"source":"Petri","block_number":1337,"timestamp":"1970-01-01T00:00:00Z"}"#;
    assert_eq!(met_value, Some(&MetadataValue::from_str(expected).unwrap()));
}
