use crate::clear_txo_store::SubstrateClientClearTxOStore;
use crate::tests::{test_clear_redeem_builder, test_health_monitor};
use crate::{
    keys::test::create_encryption_key_resolver,
    tests::{test_create_builder, test_redeem_builder, test_send_builder},
    XandApiSvc,
};
use parity_codec::Encode;
use petri::{TransactionCache, TxoCache};
use pseudo::Mock;
use std::sync::Arc;
use xand_address::Address;
use xand_financial_client::test_helpers::managers::{
    generate_test_create_manager_simple, generate_test_redeem_manager_simple,
};
use xand_models::{dev_account_key, ToAddress};
use xandstrate_client::{
    mock::MockSubstrateClient, set_trust_node_id_extrinsic, test_helpers::create_key_signer, Pair,
    ProposalData, ProposalStage,
};

#[tokio::test]
async fn get_proposal_works() {
    let prop = set_trust_proposal_data();
    let mock_subc = MockSubstrateClient {
        mock_get_proposal: Mock::new(Ok(prop)),
        ..Default::default()
    };
    let testme = XandApiSvc::new(
        Arc::new(mock_subc),
        Arc::new(TransactionCache::default()),
        create_key_signer(),
        create_encryption_key_resolver(),
        generate_test_create_manager_simple(),
        generate_test_redeem_manager_simple(),
        TxoCache::default(),
        SubstrateClientClearTxOStore::<MockSubstrateClient>::default(),
        test_create_builder(),
        test_send_builder(),
        test_redeem_builder(),
        test_clear_redeem_builder(),
        test_health_monitor(),
    )
    .await;
    let result = testme.impl_get_proposal(8).await.unwrap().dat;
    assert_eq!(result.id, 8);
}

#[tokio::test]
async fn get_all_proposals_works() {
    let props = vec![set_trust_proposal_data()];
    let mock_subc = MockSubstrateClient {
        mock_get_all_proposals: Mock::new(Ok(props)),
        ..Default::default()
    };
    let testme = XandApiSvc::new(
        Arc::new(mock_subc),
        Arc::new(TransactionCache::default()),
        create_key_signer(),
        create_encryption_key_resolver(),
        generate_test_create_manager_simple(),
        generate_test_redeem_manager_simple(),
        TxoCache::default(),
        SubstrateClientClearTxOStore::<MockSubstrateClient>::default(),
        test_create_builder(),
        test_send_builder(),
        test_redeem_builder(),
        test_clear_redeem_builder(),
        test_health_monitor(),
    )
    .await;
    let res_props = testme.impl_get_all_proposals().await.unwrap().dat;
    assert_eq!(res_props[0].id, 8);
}

fn set_trust_proposal_data() -> ProposalData<Address, u32> {
    let proposer = dev_account_key("2Pac").public();
    let trust = dev_account_key("turst").public();
    let proposed_action = set_trust_node_id_extrinsic(trust.0.into(), Default::default()).encode();
    ProposalData {
        id: 8,
        votes: Vec::new(),
        proposer: proposer.to_address(),
        expiration_block_id: 2008,
        proposed_action,
        status: ProposalStage::Proposed,
    }
}
