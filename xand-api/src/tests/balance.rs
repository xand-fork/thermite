use crate::tests::{build_xand_api_for_member_balance, build_xand_api_for_validator_balance};
use crate::{
    tests::{build_xand_api, TestNetwork, TestNetworkParticipant},
    MaybeBalance,
};

#[tokio::test]
async fn balance_of_member_without_txos_is_zero_with_confidentiality() {
    let mut network = TestNetwork::new();
    let member = network.create_member_with_name(TestNetworkParticipant::Undefined);
    let xand_api = build_xand_api(&network, &member).await;
    let balance = xand_api
        .impl_get_balance(&member.address.to_string())
        .await
        .map(|r| r.dat.balance);
    assert!(
        matches!(balance, Ok(Some(MaybeBalance { amount: 0 }))),
        "Unexpected balance {:?}",
        balance,
    );
}

#[tokio::test]
async fn balance_of_member_with_xand_is_correct_with_confidentiality() {
    let mut network = TestNetwork::new();
    let member = network.create_member_with_name(TestNetworkParticipant::Undefined);
    let expected_balance = 15;
    let xand_api = build_xand_api_for_member_balance(&network, &member, expected_balance).await;
    let balance = xand_api
        .impl_get_balance(&member.address.to_string())
        .await
        .map(|r| r.dat.balance);
    assert!(
        matches!(balance, Ok(Some(MaybeBalance { amount: actual_balance })) if actual_balance == expected_balance),
        "Unexpected balance {:?}",
        balance,
    );
}

#[tokio::test]
async fn balance_of_other_member_is_unavailable_with_confidentiality() {
    let mut network = TestNetwork::new();
    let alice = network.create_member_with_name(TestNetworkParticipant::Alice);
    let bob = network.create_member_with_name(TestNetworkParticipant::Bob);
    let xand_api = build_xand_api(&network, &alice).await;
    let balance = xand_api
        .impl_get_balance(&bob.address.to_string())
        .await
        .map(|r| r.dat.balance);
    assert!(
        matches!(balance, Ok(None),),
        "Unexpected balance {:?}",
        balance,
    );
}

#[tokio::test]
async fn balance_of_validator_without_txos_is_zero_with_confidentiality() {
    let mut network = TestNetwork::new();
    let validator = network.create_validator_with_name(TestNetworkParticipant::Undefined);
    let xand_api = build_xand_api_for_validator_balance(&network, &validator, &[]).await;
    let balance = xand_api
        .impl_get_balance(&validator.address.to_string())
        .await
        .map(|r| r.dat.balance);
    assert!(
        matches!(balance, Ok(Some(MaybeBalance { amount: 0 }))),
        "Unexpected balance {:?}",
        balance,
    );
}

#[tokio::test]
async fn balance_of_validator_with_txos_is_correct_with_confidentiality() {
    let mut network = TestNetwork::new();
    let validator = network.create_validator_with_name(TestNetworkParticipant::Undefined);
    // Sums to 15
    let values = vec![1, 2, 3, 4, 5];
    let xand_api = build_xand_api_for_validator_balance(&network, &validator, &values).await;
    let balance = xand_api
        .impl_get_balance(&validator.address.to_string())
        .await
        .map(|r| r.dat.balance);
    assert!(
        matches!(balance, Ok(Some(MaybeBalance { amount: 15 }))),
        "Unexpected balance {:?}",
        balance,
    );
}
