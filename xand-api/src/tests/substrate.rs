use crate::clear_txo_store::SubstrateClientClearTxOStore;
use crate::tests::{test_clear_redeem_builder, test_health_monitor};
use crate::{
    keys::test::create_encryption_key_resolver,
    tests::{
        make_client_for_finalized_heads, setup_fake_block_header, test_create_builder,
        test_redeem_builder, test_send_builder, wait_for_block,
    },
    xand_models_converter::FromWrapper,
    UserTransaction, XandApiSvc,
};
use petri::{TransactionCache, TxoCache};
use pseudo::Mock;
use std::{convert::TryInto, sync::Arc};
use xand_api_proto::proto_models;
use xand_financial_client::test_helpers::managers::{
    generate_test_create_manager_simple, generate_test_redeem_manager_simple,
};
use xand_models::{dev_account_key, SendSchema, ToAddress, XandTransaction};
use xandstrate_client::{
    mock::MockSubstrateClient, test_helpers::create_key_signer, Pair, SubstrateClientErrors,
};

#[tokio::test]
async fn substrate_errors_propagate() {
    let tupac = dev_account_key("2Pac");
    let input_tx = XandTransaction::Send(SendSchema {
        amount_in_minor_unit: 0,
        destination_account: tupac.public().to_address(),
    });
    let proto_input_tx = proto_models::XandTransaction::convert_from(input_tx);
    let input = UserTransaction {
        issuer: "Someone".to_string(),
        operation: Some(proto_input_tx.try_into().unwrap()),
    };
    let mock_subc = MockSubstrateClient {
        mock_make_signable: Mock::new(Err(SubstrateClientErrors::RpcError {
            error: "Simulated substrate error".to_string(),
        })),
        ..Default::default()
    };

    let testme = XandApiSvc::new(
        Arc::new(mock_subc),
        Arc::new(TransactionCache::default()),
        create_key_signer(),
        create_encryption_key_resolver(),
        generate_test_create_manager_simple(),
        generate_test_redeem_manager_simple(),
        TxoCache::default(),
        SubstrateClientClearTxOStore::<MockSubstrateClient>::default(),
        test_create_builder(),
        test_send_builder(),
        test_redeem_builder(),
        test_clear_redeem_builder(),
        test_health_monitor(),
    )
    .await;
    let res = testme.impl_submit_tx(input).await;
    assert!(res.is_err());
}

#[tokio::test]
async fn server_resubscribes_to_finalized_heads_if_subscription_fails() {
    const BLOCK_COUNT: u32 = 3;
    const LAST_BLOCK_ID: u32 = BLOCK_COUNT - 1;
    let substrate = make_client_for_finalized_heads(vec![
        Err(SubstrateClientErrors::NoResponse),
        Ok((0..BLOCK_COUNT)
            .map(|n| Ok(setup_fake_block_header(n)))
            .collect()),
    ]);
    assert!(wait_for_block(substrate, LAST_BLOCK_ID).await.is_ok());
}

#[tokio::test]
async fn server_resubscribes_to_finalized_heads_if_subscription_ends() {
    const BLOCK_COUNT: u32 = 6;
    const LAST_BLOCK_ID: u32 = BLOCK_COUNT - 1;
    let mut first_batch = (0..BLOCK_COUNT)
        .map(|n| Ok(setup_fake_block_header(n)))
        .collect::<Vec<_>>();
    let second_batch = first_batch.split_off(3);
    let substrate = make_client_for_finalized_heads(vec![Ok(first_batch), Ok(second_batch)]);
    assert!(wait_for_block(substrate, LAST_BLOCK_ID).await.is_ok());
}
