use std::{collections::HashMap, convert::TryInto, sync::Arc, time::Duration};

use chrono::{DateTime, TimeZone, Utc};
use futures::lock::Mutex;
use futures::{future::ready, StreamExt};
use petri::{TransactionCache, TxoCache};
use pseudo::Mock;
use tokio_stream::wrappers::IntervalStream;
use tonic::metadata::MetadataValue;
use xand_address::{Address, AddressError};
use xand_models::{dev_account_key, substrate_test_helpers::call_to_ext, TimestampCall, ToAddress};
use xandstrate_client::{
    mock::MockSubstrateClient, test_helpers::create_key_signer, Block, Header, Pair, SignedBlock,
    SubstrateClientErrors,
};

use super::*;
use crate::clear_txo_store::SubstrateClientClearTxOStore;
use crate::errors::TextError;
use crate::health::HealthReport;
use crate::keys::test::TestNetworkParticipant;
use crate::keys::{
    encrypt_bank_account,
    test::{create_encryption_key_resolver, TestKeyResolver, TestNetwork, TestParticipant},
    WrappedNetworkKeyResolver,
};
use crate::xand_models_converter::IntoWrapper;
use petri::confidentiality_store::mock::MockTxoStore;
use rand::rngs::OsRng;
use std::fmt::{Debug, Formatter};
use std::str::FromStr;
use xand_api_proto::error::XandApiProtoErrs;
use xand_financial_client::clear_redeem_builder::{ClearRedeemBuilderDeps, ClearRedeemBuilderImpl};
use xand_financial_client::test_helpers::managers::FakeClearTxoRepo;
use xand_financial_client::test_helpers::{
    managers::{
        generate_test_create_manager_simple, generate_test_redeem_manager_simple, FakeKeyStore,
        FakeTxoRepo, TestCreateMgrConfig, TestEncryptionProvider, TestRedeemMgrConfig,
    },
    FakeIdentityTagSelector, FakeMemberContext,
};
use xand_financial_client::{
    create_builder::{CreateBuilderDeps, CreateBuilderImpl},
    CreateManagerImpl, RedeemBuilderDeps, RedeemBuilderImpl, RedeemManagerImpl, SendBuilderDeps,
    SendBuilderImpl,
};
use xand_ledger::esig_payload::{ESigPayload, UETA_TEXT};
use xand_ledger::ClearTransactionOutput;

mod balance;
mod create;
mod history;
mod last_block_info;
mod limited_agent;
mod proposal;
mod redeem;
mod send;
mod substrate;

fn index_to_id(index: u64) -> proto_models::TransactionId {
    let mut id = [0; 32];
    id[..std::mem::size_of_val(&index)].copy_from_slice(&index.to_ne_bytes());
    id.into()
}

fn no_filters_txn_history() -> TransactionHistoryRequest {
    TransactionHistoryRequest {
        addresses: Vec::new(),
        page_size: 0,
        page_number: 0,
        transaction_types: Vec::new(),
        start_time: None,
        end_time: None,
    }
}

fn no_filter_pending_creates() -> PendingCreateRequestsPagination {
    PendingCreateRequestsPagination {
        page_size: 0,
        page_number: 0,
    }
}

fn transaction_list_from_proto(
    transactions: Vec<xand_api_proto::FetchedTransaction>,
) -> Vec<proto_models::Transaction> {
    transactions
        .into_iter()
        .map(|t| proto_models::Transaction::try_from(t).unwrap())
        .collect()
}

fn fake_account() -> proto_models::BankAccountInfo {
    let id = proto_models::BankAccountId {
        account_number: "123".to_string(),
        routing_number: "456".to_string(),
    };
    id.into()
}

struct TestCreateBuilderDeps;

impl CreateBuilderDeps for TestCreateBuilderDeps {
    type IdentityTagSelector = FakeIdentityTagSelector;
    type KeyStore = FakeKeyStore;
    type NetworkKeyResolver = WrappedNetworkKeyResolver<TestKeyResolver>;
    type EncryptionProvider = TestEncryptionProvider;
    type MemberContext = FakeMemberContext;
    type Rng = OsRng;
}

struct TestSendBuilderDeps;

impl SendBuilderDeps for TestSendBuilderDeps {
    type IdentityTagSelector = FakeIdentityTagSelector;
    type KeyStore = FakeKeyStore;
    type NetworkKeyResolver = WrappedNetworkKeyResolver<TestKeyResolver>;
    type EncryptionProvider = TestEncryptionProvider;
    type Rng = OsRng;
    type TxoRepo = FakeTxoRepo;
    type MemberContext = FakeMemberContext;
}

struct TestRedeemBuilderDeps;

impl RedeemBuilderDeps for TestRedeemBuilderDeps {
    type TxoRepo = FakeTxoRepo;
    type IdentityTagSelector = FakeIdentityTagSelector;
    type KeyStore = FakeKeyStore;
    type MemberContext = FakeMemberContext;
    type EncryptionProvider = TestEncryptionProvider;
    type NetworkKeyResolver = WrappedNetworkKeyResolver<TestKeyResolver>;
    type Rng = OsRng;
}

type TestCreateBuilder = CreateBuilderImpl<TestCreateBuilderDeps>;

fn test_create_builder() -> TestCreateBuilder {
    let encryption_key_resolver = create_encryption_key_resolver().into();
    TestCreateBuilder {
        id_tag_selector: FakeIdentityTagSelector::default(),
        key_store: FakeKeyStore::new(vec![]),
        encryption_provider: TestEncryptionProvider::default(),
        network_key_resolver: encryption_key_resolver,
        member_context: FakeMemberContext::default(),
        rng: Mutex::new(OsRng::default()),
    }
}

type TestSendBuilder = SendBuilderImpl<TestSendBuilderDeps>;

fn test_send_builder() -> TestSendBuilder {
    let encryption_key_resolver = create_encryption_key_resolver().into();
    TestSendBuilder::new(
        FakeIdentityTagSelector::default(),
        FakeKeyStore::new(Vec::new()),
        FakeTxoRepo::default(),
        TestEncryptionProvider::default(),
        encryption_key_resolver,
        FakeMemberContext::default(),
        OsRng::default().into(),
    )
}

type TestRedeemBuilder = RedeemBuilderImpl<TestRedeemBuilderDeps>;

fn test_redeem_builder() -> TestRedeemBuilder {
    TestRedeemBuilder {
        txo_repo: FakeTxoRepo::default(),
        id_tag_selector: FakeIdentityTagSelector::default(),
        key_store: FakeKeyStore::default(),
        member_context: FakeMemberContext::default(),
        encryption_provider: TestEncryptionProvider::default(),
        network_key_resolver: create_encryption_key_resolver().into(),
        rng: OsRng::default().into(),
    }
}

struct TestClearRedeemBuilderDeps;

impl ClearRedeemBuilderDeps for TestClearRedeemBuilderDeps {
    type ClearTxoRepo = FakeClearTxoRepo;
    type EncryptionProvider = TestEncryptionProvider;
    type NetworkKeyResolver = WrappedNetworkKeyResolver<TestKeyResolver>;
    type Rng = OsRng;
}

type TestClearRedeemBuilder = ClearRedeemBuilderImpl<TestClearRedeemBuilderDeps>;

fn test_clear_redeem_builder() -> TestClearRedeemBuilder {
    TestClearRedeemBuilder {
        clear_txo_repo: FakeClearTxoRepo::default(),
        encryption_provider: TestEncryptionProvider::default(),
        network_key_resolver: create_encryption_key_resolver().into(),
        rng: OsRng::default().into(),
    }
}

fn test_health_monitor() -> TestHealthMonitor {
    TestHealthMonitor
}

type TestService = XandApiSvc<
    MockSubstrateClient,
    TransactionCache,
    TestKeyResolver,
    CreateManagerImpl<TestCreateMgrConfig>,
    RedeemManagerImpl<TestRedeemMgrConfig>,
    TxoCache,
    SubstrateClientClearTxOStore<MockSubstrateClient>,
    TestCreateBuilder,
    TestSendBuilder,
    TestRedeemBuilder,
    TestClearRedeemBuilder,
    TestHealthMonitor,
>;

pub struct TestHealthMonitor;

#[async_trait::async_trait]
impl HealthMonitor for TestHealthMonitor {
    async fn get_health(&self) -> Result<HealthReport, XandApiErrors> {
        todo!()
    }
}

struct TestEnv {
    service: TestService,
    transaction_index: u64,
    time: DateTime<Utc>,
}

impl TestEnv {
    fn store_transactions<'a, I: 'a>(&self, transactions: I)
    where
        I: IntoIterator<Item = &'a proto_models::Transaction>,
    {
        for transaction in transactions.into_iter().cloned() {
            self.service.history_store.store(transaction.convert_into());
        }
    }

    fn next_transaction_id(&mut self) -> proto_models::TransactionId {
        let id = index_to_id(self.transaction_index);
        self.transaction_index += 1;
        id
    }

    fn next_timestamp(&mut self) -> DateTime<Utc> {
        let t = self.time;
        self.time = self.time + chrono::Duration::minutes(1);
        t
    }

    fn generate_transactions(&mut self) -> Vec<proto_models::Transaction> {
        vec![
            proto_models::Transaction::from_xand_txn(
                self.next_transaction_id(),
                proto_models::XandTransaction::Send(proto_models::Send {
                    amount_in_minor_unit: 1,
                    destination_account: BIGGIE.public().to_address(),
                }),
                TUPAC.public().to_address(),
                proto_models::TransactionStatus::Committed,
                self.next_timestamp(),
            ),
            proto_models::Transaction::from_xand_txn(
                self.next_transaction_id(),
                proto_models::XandTransaction::CreateRequest(proto_models::PendingCreateRequest {
                    amount_in_minor_unit: 20,
                    correlation_id: proto_models::CorrelationId::gen_random(),
                    account: fake_account(),
                    completing_transaction: None,
                }),
                OTHER.public().to_address(),
                proto_models::TransactionStatus::Committed,
                self.next_timestamp(),
            ),
            proto_models::Transaction::from_xand_txn(
                self.next_transaction_id(),
                proto_models::XandTransaction::RedeemRequest(proto_models::PendingRedeemRequest {
                    amount_in_minor_unit: 30,
                    correlation_id: proto_models::CorrelationId::gen_random(),
                    account: fake_account(),
                    completing_transaction: None,
                }),
                OTHER.public().to_address(),
                proto_models::TransactionStatus::Committed,
                self.next_timestamp(),
            ),
        ]
    }
}

async fn default_test_env() -> TestEnv {
    TestEnv {
        service: XandApiSvc::new(
            Default::default(),
            Default::default(),
            create_key_signer(),
            create_encryption_key_resolver(),
            generate_test_create_manager_simple(),
            generate_test_redeem_manager_simple(),
            TxoCache::default(),
            SubstrateClientClearTxOStore::default(),
            test_create_builder(),
            test_send_builder(),
            test_redeem_builder(),
            test_clear_redeem_builder(),
            test_health_monitor(),
        )
        .await,
        transaction_index: 1,
        time: Utc.ymd(2020, 2, 5).and_hms(6, 0, 0),
    }
}

fn from_xand_txn(
    id: proto_models::TransactionId,
    txn: proto_models::XandTransaction,
    signer: Address,
    status: proto_models::TransactionStatus,
) -> proto_models::Transaction {
    proto_models::Transaction::from_xand_txn(
        id,
        txn,
        signer,
        status,
        Utc.ymd(2020, 2, 5).and_hms(6, 0, 0),
    )
}

// Making account keys is pricy and we call `store_sends` in a tight loop
lazy_static::lazy_static! {
    static ref TUPAC: sr25519::Pair = dev_account_key("2Pac");
    static ref BIGGIE: sr25519::Pair = dev_account_key("Biggie");
    static ref OTHER: sr25519::Pair = dev_account_key("Other");
}

/// Stores a send from 2Pac->Bigge, one in the other direction, and one admin txn from Other
fn store_sends(
    fake_cache: &TransactionCache,
    id1: proto_models::TransactionId,
    id2: proto_models::TransactionId,
    id3: proto_models::TransactionId,
) {
    let fake_tx = proto_models::XandTransaction::Send(proto_models::Send {
        amount_in_minor_unit: 200,
        destination_account: BIGGIE.public().to_address(),
    });
    let wrapper = from_xand_txn(
        id1,
        fake_tx,
        TUPAC.public().to_address(),
        proto_models::TransactionStatus::Committed,
    );
    fake_cache.store(wrapper.convert_into());

    let fake_tx = proto_models::XandTransaction::Send(proto_models::Send {
        amount_in_minor_unit: 200,
        destination_account: TUPAC.public().to_address(),
    });
    let wrapper = from_xand_txn(
        id2,
        fake_tx,
        TUPAC.public().to_address(),
        proto_models::TransactionStatus::Committed,
    );
    fake_cache.store(wrapper.convert_into());

    let fake_tx = proto_models::XandTransaction::SetTrust(proto_models::SetTrustNodeId {
        address: OTHER.public().to_address(),
        encryption_key: Default::default(),
    });
    let wrapper = from_xand_txn(
        id3,
        fake_tx,
        OTHER.public().to_address(),
        proto_models::TransactionStatus::Committed,
    );
    fake_cache.store(wrapper.convert_into());
}

fn setup_fake_block_header(number: u32) -> Header {
    Header {
        parent_hash: Default::default(),
        number,
        state_root: Default::default(),
        extrinsics_root: Default::default(),
        digest: Default::default(),
    }
}

fn setup_fake_block(header: Header) -> SignedBlock {
    let timestamp_ext = call_to_ext(
        xandstrate_client::Call::Timestamp(TimestampCall::set(100)),
        0,
        &TUPAC.public(),
    );
    SignedBlock {
        block: Block {
            extrinsics: vec![timestamp_ext],
            header,
        },
        justification: None,
    }
}

fn setup_fake_header_and_block(number: u32) -> (Header, SignedBlock) {
    let fake_block_header = setup_fake_block_header(number);
    let fake_block = setup_fake_block(fake_block_header.clone());
    (fake_block_header, fake_block)
}

fn make_create_request_transaction(
    correlation_id: proto_models::CorrelationId,
    bank_info: proto_models::BankAccountInfo,
) -> proto_models::XandTransaction {
    proto_models::XandTransaction::CreateRequest(make_create_request(correlation_id, bank_info))
}

fn make_redeem_request_transaction(
    correlation_id: proto_models::CorrelationId,
    bank_info: proto_models::BankAccountInfo,
) -> proto_models::XandTransaction {
    proto_models::XandTransaction::RedeemRequest(make_redeem_request(correlation_id, bank_info))
}

fn make_create_request(
    correlation_id: proto_models::CorrelationId,
    bank_info: proto_models::BankAccountInfo,
) -> proto_models::PendingCreateRequest {
    proto_models::PendingCreateRequest {
        amount_in_minor_unit: 2,
        correlation_id,
        account: bank_info,
        completing_transaction: None,
    }
}

fn make_redeem_request(
    correlation_id: proto_models::CorrelationId,
    bank_info: proto_models::BankAccountInfo,
) -> proto_models::PendingRedeemRequest {
    proto_models::PendingRedeemRequest {
        amount_in_minor_unit: 1,
        correlation_id,
        account: bank_info,
        completing_transaction: None,
    }
}

async fn get_transaction_details(
    service: &TestXandApiServer,
    id: &proto_models::TransactionId,
) -> proto_models::Transaction {
    service
        .impl_get_tx_details(TransactionDetailsRequest { id: id.to_string() })
        .await
        .unwrap()
        .dat
        .unwrap()
        .try_into()
        .unwrap()
}

struct BankAccountEncryptionEnv {
    network: TestNetwork,
    registered_txns: Vec<NamedTestTransaction<PreparedTransaction>>,
}

#[derive(Clone)]
struct RequestResults<T, F> {
    prepared: Vec<NamedTestTransaction<T>>,
    actual: Vec<NamedTestTransaction<F>>,
}

/// Transaction issued by the named `participant`, used for mocking transactions for API tests
#[derive(Clone)]
struct NamedTestTransaction<T> {
    participant: TestParticipant,
    txn: T,
}

/// Enables comparing NamedTestTransaction<PreparedTransaction> in tests.
impl Debug for NamedTestTransaction<PreparedTransaction> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match &self.txn.0.txn {
            proto_models::XandTransaction::RedeemRequest(txn) => write!(f, "PendingRedeemRequest {:?}", txn),
            proto_models::XandTransaction::CreateRequest(txn) => write!(f, "PendingCreateRequest {:?}", txn),

            _ => write!(
                f,
                "No such transaction registered, check your xand-api bank encryption test Debug impl"
            ),
        }
    }
}

impl PartialEq for NamedTestTransaction<PreparedTransaction> {
    fn eq(&self, rhs: &NamedTestTransaction<PreparedTransaction>) -> bool {
        test_participant_details_equal(&self.participant, &rhs.participant) && self.txn == rhs.txn
    }
}

impl PartialEq for NamedTestTransaction<proto_models::PendingCreateRequest> {
    fn eq(&self, rhs: &NamedTestTransaction<proto_models::PendingCreateRequest>) -> bool {
        test_participant_details_equal(&self.participant, &rhs.participant) && self.txn == rhs.txn
    }
}

impl PartialEq for NamedTestTransaction<proto_models::PendingRedeemRequest> {
    fn eq(&self, rhs: &NamedTestTransaction<proto_models::PendingRedeemRequest>) -> bool {
        test_participant_details_equal(&self.participant, &rhs.participant) && self.txn == rhs.txn
    }
}

impl PartialEq for PreparedTransaction {
    fn eq(&self, rhs: &PreparedTransaction) -> bool {
        self.0 == rhs.0
    }
}

// This comparison excludes the KeyManager, which does not impl Eq or PartialEq
fn test_participant_details_equal(lhs: &TestParticipant, rhs: &TestParticipant) -> bool {
    lhs.name == rhs.name && lhs.address == rhs.address && lhs.key == rhs.key
}

/// Transaction that will be used to make an API request, contains wire type of transaction
#[derive(Clone, Debug)]
struct PreparedTransaction(proto_models::Transaction);

impl From<PreparedTransaction> for proto_models::Transaction {
    fn from(txn: PreparedTransaction) -> Self {
        txn.0
    }
}

impl From<NamedTestTransaction<PreparedTransaction>> for proto_models::Transaction {
    fn from(txn: NamedTestTransaction<PreparedTransaction>) -> Self {
        match txn.txn {
            PreparedTransaction(txn) => txn,
        }
    }
}

impl From<PreparedTransaction> for proto_models::PendingCreateRequest {
    fn from(txn: PreparedTransaction) -> Self {
        match txn.0.txn {
            proto_models::XandTransaction::CreateRequest(t) => t,
            _ => panic!("Expected a CreateRequest txn"),
        }
    }
}

impl From<NamedTestTransaction<PreparedTransaction>> for proto_models::PendingCreateRequest {
    fn from(txn: NamedTestTransaction<PreparedTransaction>) -> Self {
        match txn.txn.0.txn {
            proto_models::XandTransaction::CreateRequest(txn) => txn,
            _ => panic!("Expected a CreateRequest txn"),
        }
    }
}

impl From<NamedTestTransaction<PreparedTransaction>> for proto_models::PendingRedeemRequest {
    fn from(txn: NamedTestTransaction<PreparedTransaction>) -> Self {
        match txn.txn.0.txn {
            proto_models::XandTransaction::RedeemRequest(txn) => txn,
            _ => panic!("Expected a RedeemRequest txn"),
        }
    }
}

#[derive(Debug)]
enum TestApiError {
    XandApi(XandApiErrors),
    AddressError(AddressError),
    XandApiProtoErrs(XandApiProtoErrs),
    OtherError,
}

impl From<XandApiErrors> for TestApiError {
    fn from(e: XandApiErrors) -> Self {
        Self::XandApi(e)
    }
}

impl From<AddressError> for TestApiError {
    fn from(e: AddressError) -> Self {
        Self::AddressError(e)
    }
}

impl From<XandApiProtoErrs> for TestApiError {
    fn from(e: XandApiProtoErrs) -> Self {
        Self::XandApiProtoErrs(e)
    }
}

// Nice to have at some point - pass txns by reference
// In the future, we may want to write multiple transactions per participant
fn get_first_txn_for_address<I, T>(txns: I, addr: &Address) -> NamedTestTransaction<T>
where
    I: IntoIterator<Item = NamedTestTransaction<T>>,
{
    let txn: NamedTestTransaction<T> = txns
        .into_iter()
        // Return the first item that matches
        .find(|NamedTestTransaction { participant, txn: _txn }| participant.address == *addr)
        .expect("Test data should contain txn for given address");

    txn
}

fn extract_bank_data(transaction: &proto_models::Transaction) -> proto_models::BankAccountInfo {
    match &transaction.txn {
        proto_models::XandTransaction::CreateRequest(create) => create.account.clone(),
        proto_models::XandTransaction::RedeemRequest(redeem) => redeem.account.clone(),
        x => unimplemented!("unhandled variant: {:?}", x),
    }
}

fn extract_transaction(
    named_txn: NamedTestTransaction<PreparedTransaction>,
) -> proto_models::Transaction {
    named_txn.txn.0
}

// Could also make another one of these helper functions for RequestResults<PreparedTransaction, T>
//where T is PendingCreate or PendingRedeem
fn assert_actual_bank_data_eq_expected(
    participant: &TestParticipant,
    results: &RequestResults<PreparedTransaction, PreparedTransaction>,
) {
    let actual = get_actual_bank_data_for_participant(participant, results.clone());
    let expected = get_expected_bank_data_for_participant(participant, results.clone());
    assert_eq!(actual, expected);
}

fn assert_actual_txn_eq_expected(
    participant: &TestParticipant,
    results: &RequestResults<PreparedTransaction, PreparedTransaction>,
) {
    let actual = extract_transaction(get_first_txn_for_address(
        results.clone().actual,
        &participant.address,
    ))
    .txn;
    let expected = extract_transaction(get_first_txn_for_address(
        results.clone().prepared,
        &participant.address,
    ))
    .txn;
    assert_eq!(actual, expected);
}

fn get_actual_bank_data_for_participant(
    participant: &TestParticipant,
    results: RequestResults<PreparedTransaction, PreparedTransaction>,
) -> proto_models::BankAccountInfo {
    let participant_address = participant.clone().address;
    extract_bank_data(
        &(proto_models::Transaction::from(get_first_txn_for_address(
            results.actual,
            &participant_address,
        ))),
    )
}

fn get_expected_bank_data_for_participant(
    participant: &TestParticipant,
    results: RequestResults<PreparedTransaction, PreparedTransaction>,
) -> proto_models::BankAccountInfo {
    let participant_address = participant.clone().address;
    extract_bank_data(
        &(proto_models::Transaction::from(get_first_txn_for_address(
            results.prepared,
            &participant_address,
        ))),
    )
}

// TODO refactor BankAccountEncryptionEnv and its calling tests out into a different file
// https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6964
/// This test environment allows for writing of mocked `NamedTestTransaction<PreparedTransaction<T>>`s
/// associated with a network participant, in order to compare them to expected return values from
/// the xand-api calls. Because transaction types returned from mock api calls
/// (e.g. `xand_models::XandTransaction::CreatePendingRedeem(PendingRedeem)`) have insufficient data
/// to be converted back to a`Transaction` type, helper methods exist on the mocked transactions
/// to enable test comparisons.
///
/// Returns a new `BankAccountEncryptionEnv` populated with registered members and trust
impl BankAccountEncryptionEnv {
    fn new() -> Self {
        // Get a registered trust for this env
        let mut network = TestNetwork::new();
        let _alice = network.create_member_with_name(TestNetworkParticipant::Alice);
        let _bob = network.create_member_with_name(TestNetworkParticipant::Bob);
        let _jane = network.create_member_with_name(TestNetworkParticipant::Jane);

        // TestNetworkParticipant::Undefined does not need to be initialized as it
        // is created on-demand in the TestNetwork

        let registered_txns: Vec<NamedTestTransaction<PreparedTransaction>> = vec![];
        Self {
            registered_txns,
            network,
        }
    }

    fn trust(&self) -> &TestParticipant {
        &self.network.trust
    }

    fn get_registered_txns(&self) -> Vec<proto_models::Transaction> {
        self.registered_txns
            .iter()
            .map(|NamedTestTransaction { txn, .. }| txn.clone().into())
            .collect()
    }

    fn get_participant_by_name(&self, name: &TestNetworkParticipant) -> TestParticipant {
        self.network
            .participants
            .get(name)
            .unwrap()
            .borrow()
            .clone()
    }

    fn get_participant_by_address(&self, addr: Address) -> TestParticipant {
        self.network
            .participants
            .iter()
            .find_map(|(_, participant)| {
                (participant.borrow().address == addr).then(|| participant.borrow().clone())
            })
            .expect("Participant should be registered in network, did you set them up correctly?")
    }

    fn named_transaction_list_from_proto(
        &self,
        transactions: Vec<xand_api_proto::FetchedTransaction>,
    ) -> Vec<NamedTestTransaction<PreparedTransaction>> {
        transactions
            .into_iter()
            .map(|fetched_txn| {
                let txn =
                    PreparedTransaction(proto_models::Transaction::try_from(fetched_txn).unwrap());
                let participant = self.get_participant_by_address(txn.clone().0.signer_address);
                NamedTestTransaction { participant, txn }
            })
            .collect()
    }

    async fn prepare_transaction<F>(
        &self,
        issuer: &TestParticipant,
        transaction_id: proto_models::TransactionId,
        make_xand_txn: F,
    ) -> PreparedTransaction
    where
        F: Fn(
            proto_models::CorrelationId,
            proto_models::BankAccountInfo,
        ) -> proto_models::XandTransaction,
    {
        let correlation_id: proto_models::CorrelationId = proto_models::CorrelationId::gen_random();
        let bank_account = proto_models::BankAccountId {
            routing_number: transaction_id.to_string(),
            account_number: transaction_id.to_string(),
        };
        let make_txn = |bank_info| {
            from_xand_txn(
                transaction_id.clone(),
                make_xand_txn(correlation_id.clone(), bank_info),
                issuer.address.clone(),
                proto_models::TransactionStatus::Committed,
            )
        };

        PreparedTransaction(make_txn(proto_models::BankAccountInfo::Unencrypted(
            bank_account,
        )))
    }

    /// Sets up transactions for mock network participants.
    /// If in the future we would like to write >1 transaction per participant, we might edit this
    /// function to take a strategy parameter to specify how to write the transaction(s).
    async fn network_participant_transactions<F>(
        &self,
        make_xand_txn: F,
    ) -> Vec<NamedTestTransaction<PreparedTransaction>>
    where
        F: Fn(
            proto_models::CorrelationId,
            proto_models::BankAccountInfo,
        ) -> proto_models::XandTransaction,
    {
        let mut txns = vec![];
        for (index, (_name, participant)) in self.network.participants.iter().enumerate() {
            let participant = participant.borrow().clone();
            txns.push(NamedTestTransaction {
                participant: participant.clone(),
                txn: (self
                    .prepare_transaction(&participant, [index as u8; 32].into(), &make_xand_txn)
                    .await),
            });
        }
        txns
    }
    async fn xand_service_with_transactions(
        &self,
        requester: &TestParticipant,
        transactions: Vec<proto_models::Transaction>,
    ) -> Result<
        XandApiSvc<
            MockSubstrateClient,
            TransactionCache,
            TestKeyResolver,
            CreateManagerImpl<TestCreateMgrConfig>,
            RedeemManagerImpl<TestRedeemMgrConfig>,
            TxoCache,
            SubstrateClientClearTxOStore<MockSubstrateClient>,
            TestCreateBuilder,
            TestSendBuilder,
            TestRedeemBuilder,
            TestClearRedeemBuilder,
            TestHealthMonitor,
        >,
        TestApiError,
    > {
        // Each of the `transactions` with any BankAccountInfo is of the `Unencrypted` variant.

        // Convert to crate xand_models and encrypt
        let mut txns_for_store: Vec<xand_models::Transaction> = vec![];
        for txn in transactions.into_iter() {
            txns_for_store.push(self.with_encrypted_bank_data(txn.convert_into()).await?);
        }
        let transactions = txns_for_store;

        // By this point all transactions with bank account info are properly encrypted.
        // let pending_creates = transactions
        //     .iter()
        //     .filter_map(|transaction| match &transaction.txn {
        //         xand_models::XandTransaction::CreatePendingCreate(create_request) => {
        //             Some((transaction.signer_address.clone(), create_request.clone()))
        //         }
        //         _ => None,
        //     })
        //     .collect();
        //
        // let pending_redeems = transactions
        //     .iter()
        //     .filter_map(|transaction| match &transaction.txn {
        //         xand_models::XandTransaction::CreatePendingRedeem(redeem_request) => {
        //             Some((transaction.signer_address.clone(), redeem_request.clone()))
        //         }
        //         _ => None,
        //     })
        //     .collect();
        // TODO: ADO 7820 - insert conf creates/redeems into Mock Client
        let substrate_client = Arc::new(MockSubstrateClient::default());
        let transaction_cache = Arc::new(TransactionCache::default());

        let clear_txo_store = SubstrateClientClearTxOStore::new(substrate_client.clone());

        let xand_service = XandApiSvc::new(
            substrate_client,
            transaction_cache.clone(),
            KeyMgrSigner(requester.key_manager.clone()),
            self.network.key_resolver(requester),
            generate_test_create_manager_simple(),
            generate_test_redeem_manager_simple(),
            TxoCache::default(),
            clear_txo_store,
            test_create_builder(),
            test_send_builder(),
            test_redeem_builder(),
            test_clear_redeem_builder(),
            test_health_monitor(),
        )
        .await;

        for txn in transactions {
            transaction_cache.store(txn);
        }
        Ok(xand_service)
    }

    async fn with_encrypted_bank_data(
        &self,
        txn: xand_models::Transaction,
    ) -> Result<xand_models::Transaction, TestApiError> {
        let bank_acct_id = |info: xand_models::BankAccountInfo| -> xand_models::BankAccountId {
            if let xand_models::BankAccountInfo::Unencrypted(id) = info {
                id
            } else {
                unreachable!("not expecting bank account info to be encrypted yet")
            }
        };

        let issuer = &self
            .network
            .participants
            .iter()
            .find_map(|(_, participant)| {
                (participant.borrow().address == txn.signer_address)
                    .then(|| participant.borrow().clone())
            })
            .ok_or_else(|| XandApiErrors::BadAddress {
                addr: txn.signer_address.to_string(),
                reason: TextError::from(String::from("Transaction signer address not recognized")),
            })?;
        match txn.txn {
            xand_models::XandTransaction::CreatePendingCreate(d) => {
                // Enables asserting on bank account encryption error
                let encrypted: tpfs_krypt::Encrypted<xand_models::BankAccountId> =
                    encrypt_bank_account(
                        &self.network.key_resolver(issuer),
                        &bank_acct_id(d.account).clone(),
                        &issuer.address,
                        &d.correlation_id.clone(),
                    )
                    .await
                    .map_err(TestApiError::from)?;

                Ok(xand_models::Transaction {
                    txn: xand_models::XandTransaction::CreatePendingCreate(
                        xand_models::PendingCreate {
                            account: xand_models::BankAccountInfo::Encrypted(encrypted),
                            ..d
                        },
                    ),
                    ..txn
                })
            }
            xand_models::XandTransaction::CreatePendingRedeem(d) => {
                // Enables asserting on bank account encryption error
                let encrypted: tpfs_krypt::Encrypted<xand_models::BankAccountId> =
                    encrypt_bank_account(
                        &self.network.key_resolver(issuer),
                        &bank_acct_id(d.account).clone(),
                        &issuer.address,
                        &d.correlation_id.clone(),
                    )
                    .await
                    .map_err(TestApiError::from)?;

                Ok(xand_models::Transaction {
                    txn: xand_models::XandTransaction::CreatePendingRedeem(
                        xand_models::PendingRedeem {
                            account: xand_models::BankAccountInfo::Encrypted(encrypted),
                            ..d
                        },
                    ),
                    ..txn
                })
            }
            _ => unreachable!(),
        }
    }

    fn register_transactions(&mut self, input_txns: &[NamedTestTransaction<PreparedTransaction>]) {
        for txn in input_txns {
            self.registered_txns.push(txn.clone());
        }
    }

    async fn submit_and_request_history<F>(
        &mut self,
        requester: &TestParticipant,
        make_xand_txn: F,
    ) -> Result<RequestResults<PreparedTransaction, PreparedTransaction>, TestApiError>
    where
        F: Fn(
            proto_models::CorrelationId,
            proto_models::BankAccountInfo,
        ) -> proto_models::XandTransaction,
    {
        let prepared = self.network_participant_transactions(make_xand_txn).await;
        self.register_transactions(&prepared);

        let results = self
            .xand_service_with_transactions(requester, self.get_registered_txns())
            .await?
            .impl_get_tx_hist(no_filters_txn_history())
            .await?
            .dat;
        let actual = self.named_transaction_list_from_proto(results.transactions);
        Ok(RequestResults { prepared, actual })
    }

    async fn submit_and_request_transaction_details<F>(
        &mut self,
        requester: &TestParticipant,
        make_xand_txn: F,
    ) -> Result<RequestResults<PreparedTransaction, PreparedTransaction>, TestApiError>
    where
        F: Fn(
            proto_models::CorrelationId,
            proto_models::BankAccountInfo,
        ) -> proto_models::XandTransaction,
    {
        let prepared = self.network_participant_transactions(make_xand_txn).await;
        self.register_transactions(&prepared);
        let transactions = self.get_registered_txns();
        let service = self
            .xand_service_with_transactions(requester, transactions)
            .await?;

        let mut actual = vec![];
        for named_txn in &prepared {
            actual.push(NamedTestTransaction {
                participant: named_txn.clone().participant,
                txn: PreparedTransaction(
                    get_transaction_details(
                        &service,
                        &proto_models::Transaction::from(named_txn.clone().txn).transaction_id,
                    )
                    .await,
                ),
            });
        }
        Ok(RequestResults { prepared, actual })
    }

    async fn submit_and_get_pending_creates(
        &mut self,
        requester: &TestParticipant,
    ) -> Result<RequestResults<PreparedTransaction, proto_models::PendingCreateRequest>, TestApiError>
    {
        let prepared: Vec<NamedTestTransaction<PreparedTransaction>> = self
            .network_participant_transactions(make_create_request_transaction)
            .await;
        self.register_transactions(&prepared);
        let mut transactions = vec![];
        for pc in &prepared {
            transactions.push(pc.clone().into());
        }

        let xand_service = self
            .xand_service_with_transactions(requester, transactions)
            .await?;
        let pre_result = xand_service
            .impl_get_creates(no_filter_pending_creates())
            .await?;
        let pending_creates: Vec<xand_api_proto::PendingCreateRequest> =
            pre_result.dat.pending_create_requests;
        let mut actual = vec![];
        for pm in pending_creates {
            let issuer: String = match pm.clone().issuer {
                Some(issuer) => issuer,
                None => {
                    return Err(TestApiError::OtherError);
                }
            }
            .issuer;
            let participant: TestParticipant =
                self.get_participant_by_address(match Address::try_from(issuer) {
                    Ok(addr) => addr,
                    Err(e) => {
                        return Err(TestApiError::from(e));
                    }
                });
            let txn: xand_api_proto::proto_models::PendingCreateRequest =
                match proto_models::PendingCreateRequest::try_from(pm) {
                    Ok(txn) => txn,
                    Err(e) => {
                        return Err(TestApiError::from(e));
                    }
                };
            actual.push(
                NamedTestTransaction::<xand_api_proto::proto_models::PendingCreateRequest> {
                    participant,
                    txn,
                },
            );
        }

        Ok(RequestResults { prepared, actual })
    }
}

type TestXandApiServer = XandApiSvc<
    MockSubstrateClient,
    TransactionCache,
    TestKeyResolver,
    CreateManagerImpl<TestCreateMgrConfig>,
    RedeemManagerImpl<TestRedeemMgrConfig>,
    TxoCache,
    SubstrateClientClearTxOStore<MockSubstrateClient>,
    TestCreateBuilder,
    TestSendBuilder,
    TestRedeemBuilder,
    TestClearRedeemBuilder,
    TestHealthMonitor,
>;

type TestXandApiServerMockTxoStore = XandApiSvc<
    MockSubstrateClient,
    TransactionCache,
    TestKeyResolver,
    CreateManagerImpl<TestCreateMgrConfig>,
    RedeemManagerImpl<TestRedeemMgrConfig>,
    MockTxoStore,
    SubstrateClientClearTxOStore<MockSubstrateClient>,
    TestCreateBuilder,
    TestSendBuilder,
    TestRedeemBuilder,
    TestClearRedeemBuilder,
    TestHealthMonitor,
>;

fn make_client_for_finalized_heads<I>(header_groups: I) -> MockSubstrateClient
where
    I: IntoIterator<
        Item = Result<Vec<Result<Header, SubstrateClientErrors>>, SubstrateClientErrors>,
    >,
{
    let header_groups = header_groups.into_iter().collect::<Vec<_>>();
    let blocks = header_groups
        .iter()
        .flatten() // Remove the outer Result layer
        .flatten() // Flatten the Vec
        .flatten() // Remove the inner Result layer
        .cloned()
        .map(|header| (header.hash(), setup_fake_block(header)))
        .collect::<HashMap<_, _>>();
    let header_groups = std::sync::Mutex::new(header_groups.into_iter());
    MockSubstrateClient {
        mock_finalized_heads: {
            let m = Mock::new(Ok(Vec::new()));
            m.use_closure(Box::new(move |_| {
                header_groups
                    .lock()
                    .unwrap()
                    .next()
                    .unwrap_or_else(|| Ok(Vec::new()))
            }));
            m
        },
        mock_get_block: {
            let m = Mock::new(Err(SubstrateClientErrors::NoResponse));
            m.use_closure(Box::new(move |block_hash| {
                blocks
                    .get(&block_hash)
                    .cloned()
                    .ok_or(SubstrateClientErrors::NoResponse)
            }));
            m
        },
        ..Default::default()
    }
}

async fn wait_for_block(
    substrate: MockSubstrateClient,
    block_id: u32,
) -> Result<(), tokio::time::error::Elapsed> {
    let last_block_info =
        crate::finalized_head_listener::spawn_data_info_listener(substrate.into());
    let find_block = IntervalStream::new(tokio::time::interval(Duration::from_millis(500)))
        .take_while(|_| async {
            last_block_info
                .get_value()
                .await
                .map_or(true, |b| b.block_number != block_id)
        })
        .for_each(|_| ready(()));
    tokio::time::timeout(Duration::from_secs(10), find_block).await
}

async fn build_xand_api(network: &TestNetwork, participant: &TestParticipant) -> TestXandApiServer {
    let encryption_key_resolver = network.key_resolver(participant);
    let substrate = Arc::new(MockSubstrateClient::default());
    XandApiSvc::new(
        substrate.clone(),
        Arc::new(TransactionCache::default()),
        create_key_signer(),
        encryption_key_resolver,
        generate_test_create_manager_simple(),
        generate_test_redeem_manager_simple(),
        TxoCache::default(),
        SubstrateClientClearTxOStore::new(substrate),
        test_create_builder(),
        test_send_builder(),
        test_redeem_builder(),
        test_clear_redeem_builder(),
        test_health_monitor(),
    )
    .await
}

async fn build_xand_api_with_mock_substrate_client(
    network: &TestNetwork,
    participant: &TestParticipant,
    mock: MockSubstrateClient,
) -> TestXandApiServer {
    let encryption_key_resolver = network.key_resolver(participant);
    let substrate = Arc::new(mock);
    XandApiSvc::new(
        substrate.clone(),
        Arc::new(TransactionCache::default()),
        create_key_signer(),
        encryption_key_resolver,
        generate_test_create_manager_simple(),
        generate_test_redeem_manager_simple(),
        TxoCache::default(),
        SubstrateClientClearTxOStore::new(substrate),
        test_create_builder(),
        test_send_builder(),
        test_redeem_builder(),
        test_clear_redeem_builder(),
        test_health_monitor(),
    )
    .await
}

async fn build_xand_api_for_member_balance(
    network: &TestNetwork,
    participant: &TestParticipant,
    balance: u64,
) -> TestXandApiServerMockTxoStore {
    let encryption_key_resolver = network.key_resolver(participant);
    let substrate = Arc::new(MockSubstrateClient::default());
    let mut txo_store = MockTxoStore::default();
    txo_store.insert_member_with_balance(participant.address.clone(), balance);
    XandApiSvc::new(
        substrate.clone(),
        Arc::new(TransactionCache::default()),
        create_key_signer(),
        encryption_key_resolver,
        generate_test_create_manager_simple(),
        generate_test_redeem_manager_simple(),
        txo_store,
        SubstrateClientClearTxOStore::new(substrate),
        test_create_builder(),
        test_send_builder(),
        test_redeem_builder(),
        test_clear_redeem_builder(),
        test_health_monitor(),
    )
    .await
}

async fn build_xand_api_for_validator_balance(
    network: &TestNetwork,
    participant: &TestParticipant,
    values: &[u32],
) -> TestXandApiServer {
    let validators = vec![participant.address.to_owned()];
    let encryption_key_resolver = network.key_resolver(participant);

    let public_key = address_to_public_key(&participant.address).unwrap();
    let mut clear_utxos = Vec::new();
    for value in values {
        let clear_utxo = ClearTransactionOutput::new(
            public_key,
            (*value).try_into().unwrap(),
            420,
            ESigPayload::from_str(UETA_TEXT).unwrap(),
        );
        clear_utxos.push(clear_utxo);
    }

    let substrate = Arc::new(MockSubstrateClient {
        mock_get_authority_keys: Mock::new(Ok(validators)),
        mock_clear_utxos: Mock::new(Ok(clear_utxos)),
        ..Default::default()
    });
    XandApiSvc::new(
        substrate.clone(),
        Arc::new(TransactionCache::default()),
        create_key_signer(),
        encryption_key_resolver,
        generate_test_create_manager_simple(),
        generate_test_redeem_manager_simple(),
        TxoCache::default(),
        SubstrateClientClearTxOStore::new(substrate),
        test_create_builder(),
        test_send_builder(),
        test_redeem_builder(),
        test_clear_redeem_builder(),
        test_health_monitor(),
    )
    .await
}
