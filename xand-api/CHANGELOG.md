<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Changelog](#changelog)
  - [Unreleased](#unreleased)
  - [[4.0.1] - 2021-02-12](#401---2021-02-12)
    - [Removed](#removed)
  - [[4.0.0] - 2021-1-29](#400---2021-1-29)
    - [Added](#added)
    - [Changed](#changed)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## Unreleased
<!-- When a new release is made, update tags and the URL hyperlinking the diff, see end of document -->
<!-- You may also wish to list updates that are made via MRs but not yet cut as part of a release -->

<!-- When a new release is made, add it here
e.g. ## [0.8.2] - 2020-11-09 -->

## [4.0.1] - 2021-02-12

### Removed 

Crate publishing to TPFS Alexandrie removed.

## [4.0.0] - 2021-1-29

### Added
- Converters between Xand API Proto's `xand_api_proto::proto_models` and this crate's dependency `xand-models` in `xand_models_converters.rs`
- Encrypted bank account error handling
- Substrate dependent conversions (ported from `xand-api-proto`)
- Xandstrate adapters for proto types (ported from `xand-api-proto`)

### Changed
- `xand-api-client` and `xand-api-proto` no longer depend on `xand-models`
