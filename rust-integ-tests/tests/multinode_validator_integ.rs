mod validator_integ;
mod validator_redemption_integ;

fn init_logger() {
    static LOG_INIT: std::sync::Once = std::sync::Once::new();
    LOG_INIT.call_once(|| {
        // Init simple env logger
        tpfs_logger_log4rs_adapter::init_with_default_config().unwrap();
    });
}
