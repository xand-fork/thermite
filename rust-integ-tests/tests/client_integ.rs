//! Tests for the xand-api client itself and its interactions with the backing service
use chrono::Utc;
use futures::FutureExt;
use petri::{IdTagCache, TransactionCache, TxoCache};
use rand_core::OsRng;
use std::{sync::Arc, time::Duration};
use tokio::runtime::Handle;
use tpfs_krypt::KeyManagement;
use xand_address::Address;
use xand_api::clear_txo_store::SubstrateClientClearTxOStore;
use xand_api::container::start_server_test;
use xand_api::health::{HealthMonitorImpl, StartupState};
use xand_api::{
    finalized_head_listener,
    keys::{NetworkKeyResolver, WrappedNetworkKeyResolver},
    listen_for_initial_block_information, XandApiErrors, XandApiSvc, XandNetworkKeyResolver,
};
use xand_api_client::{ReconnectingXandApiClient, XandApiClientTrait};
use xand_financial_client::clear_redeem_builder::{ClearRedeemBuilderDeps, ClearRedeemBuilderImpl};
use xand_financial_client::{
    create_builder::{CreateBuilderDeps, CreateBuilderImpl},
    RedeemBuilderDeps, RedeemBuilderImpl, SendBuilderDeps, SendBuilderImpl,
};
use xand_financial_client_adapter::{
    krypt_adapter::KryptKeyStore, member_context::SubstrateClientMemberContext,
};
use xand_test_helpers::init_logger;
use xandstrate_client::{
    mock::MockSubstrateClient, test_helpers::create_key_manager, test_helpers::get_initial_keys,
    KeyMgrSigner,
};

struct IntegTestCreateBuilderDeps;

impl CreateBuilderDeps for IntegTestCreateBuilderDeps {
    type IdentityTagSelector = IdTagCache<OsRng>;
    type KeyStore = KryptKeyStore;
    type NetworkKeyResolver =
        WrappedNetworkKeyResolver<XandNetworkKeyResolver<MockSubstrateClient>>;
    type EncryptionProvider = KryptKeyStore;
    type MemberContext = SubstrateClientMemberContext<MockSubstrateClient>;
    type Rng = OsRng;
}

const XAND_API_TEST_PORT: u16 = 53033;

struct IntegTestSendBuilderDeps;

impl SendBuilderDeps for IntegTestSendBuilderDeps {
    type KeyStore = KryptKeyStore;
    type TxoRepo = TxoCache;
    type Rng = OsRng;
    type NetworkKeyResolver =
        WrappedNetworkKeyResolver<XandNetworkKeyResolver<MockSubstrateClient>>;
    type EncryptionProvider = KryptKeyStore;
    type IdentityTagSelector = IdTagCache<OsRng>;
    type MemberContext = SubstrateClientMemberContext<MockSubstrateClient>;
}

struct IntegTestRedeemBuilderDeps;

impl RedeemBuilderDeps for IntegTestRedeemBuilderDeps {
    type TxoRepo = TxoCache;
    type IdentityTagSelector = IdTagCache<OsRng>;
    type KeyStore = KryptKeyStore;
    type MemberContext = SubstrateClientMemberContext<MockSubstrateClient>;
    type EncryptionProvider = KryptKeyStore;
    type NetworkKeyResolver =
        WrappedNetworkKeyResolver<XandNetworkKeyResolver<MockSubstrateClient>>;
    type Rng = OsRng;
}

struct IntegTestClearRedeemBuilderDeps;

impl ClearRedeemBuilderDeps for IntegTestClearRedeemBuilderDeps {
    type ClearTxoRepo = SubstrateClientClearTxOStore<MockSubstrateClient>;
    type EncryptionProvider = KryptKeyStore;
    type NetworkKeyResolver =
        WrappedNetworkKeyResolver<XandNetworkKeyResolver<MockSubstrateClient>>;
    type Rng = OsRng;
}

async fn wait_for_server_to_start() {
    // Server needs a beat to get started
    tokio::time::sleep(Duration::from_millis(300)).await;
}

struct BankEncryptionManager {}

#[async_trait::async_trait]
impl NetworkKeyResolver for BankEncryptionManager {
    fn get_key_manager(&self) -> Arc<dyn KeyManagement> {
        unimplemented!()
    }

    async fn get_encryption_key(
        &self,
        _: &Address,
    ) -> Result<xandstrate_client::xand_models::EncryptionKey, XandApiErrors> {
        unimplemented!()
    }

    async fn get_trust_encryption_key(
        &self,
    ) -> Result<xandstrate_client::xand_models::EncryptionKey, XandApiErrors> {
        unimplemented!()
    }

    async fn get_trust_address(&self) -> Result<Address, XandApiErrors> {
        unimplemented!()
    }
}

#[tokio::test]
async fn test_client_reconnecting() {
    init_logger();

    let mockclient = Arc::new(MockSubstrateClient::default());
    let arc_key_mgr = Arc::from(create_key_manager(get_initial_keys()));
    let key_store = KryptKeyStore::from(Arc::clone(&arc_key_mgr));
    let encryption_key_resolver: WrappedNetworkKeyResolver<
        XandNetworkKeyResolver<MockSubstrateClient>,
    > = XandNetworkKeyResolver::new(Arc::clone(&mockclient), Arc::clone(&arc_key_mgr)).into();
    let txo_cache = TxoCache::default();
    let id_tag_cache = IdTagCache::new(OsRng::default());
    let create_builder = CreateBuilderImpl::<IntegTestCreateBuilderDeps> {
        id_tag_selector: IdTagCache::new(OsRng::default()),
        key_store: key_store.clone(),
        encryption_provider: key_store.clone(),
        network_key_resolver: encryption_key_resolver.clone(),
        member_context: mockclient.clone().into(),
        rng: futures::lock::Mutex::new(OsRng::default()),
    };
    let send_builder = SendBuilderImpl::<IntegTestSendBuilderDeps>::new(
        id_tag_cache.clone(),
        key_store.clone(),
        txo_cache.clone(),
        key_store.clone(),
        encryption_key_resolver.clone(),
        mockclient.clone().into(),
        OsRng::default().into(),
    );
    let redeem_builder = RedeemBuilderImpl::<IntegTestRedeemBuilderDeps> {
        txo_repo: txo_cache,
        id_tag_selector: id_tag_cache,
        key_store: key_store.clone(),
        member_context: mockclient.clone().into(),
        encryption_provider: key_store.clone(),
        network_key_resolver: encryption_key_resolver.clone(),
        rng: OsRng::default().into(),
    };
    let clear_txo_repo = SubstrateClientClearTxOStore::new(mockclient.clone());

    let clear_redeem_builder = ClearRedeemBuilderImpl::<IntegTestClearRedeemBuilderDeps> {
        clear_txo_repo,
        encryption_provider: key_store,
        network_key_resolver: encryption_key_resolver,
        rng: OsRng::default().into(),
    };

    let clear_txo_store = SubstrateClientClearTxOStore::new(mockclient.clone());
    let (initial_block_sender, initial_block_receiver) =
        tokio::sync::watch::channel::<Option<u32>>(None);
    let last_block_info = finalized_head_listener::spawn_data_info_listener(mockclient.clone());

    tokio::spawn(listen_for_initial_block_information(
        last_block_info.clone(),
        initial_block_sender,
    ));

    let startup_state = StartupState {
        block_number: Some(initial_block_receiver),
        startup_timestamp: Utc::now(),
    };

    let last_block_info = finalized_head_listener::spawn_data_info_listener(mockclient.clone());
    let transaction_cache = Arc::new(TransactionCache::default());
    let health_monitor =
        HealthMonitorImpl::new(transaction_cache.clone(), startup_state, last_block_info);

    let svc = XandApiSvc::new(
        mockclient.clone(),
        transaction_cache,
        KeyMgrSigner::new(arc_key_mgr.clone()),
        BankEncryptionManager {},
        xand_financial_client_adapter::new_create_mgr(arc_key_mgr.clone(), mockclient.clone()),
        xand_financial_client_adapter::new_redeem_mgr(arc_key_mgr, mockclient),
        TxoCache::default(),
        clear_txo_store,
        create_builder,
        send_builder,
        redeem_builder,
        clear_redeem_builder,
        health_monitor,
    )
    .await;
    let (shutdown_send, signal) = futures::channel::oneshot::channel();
    let signal = signal.map(|r| r.unwrap());

    // Create a client before the server is up
    let reconnecting = ReconnectingXandApiClient::stub(
        format!("http://127.0.0.1:{}", XAND_API_TEST_PORT)
            .parse()
            .unwrap(),
    )
    .await;
    // Should connect properly at this point
    assert!(reconnecting
        .get_pending_create_requests(None)
        .await
        .is_err());

    let server_fut = start_server_test(XAND_API_TEST_PORT, svc, signal, None);
    let rt = Handle::current();
    let srv_handle = rt.spawn(server_fut.map(|_| ()));
    wait_for_server_to_start().await;
    // Now the client should work
    reconnecting
        .get_pending_create_requests(None)
        .await
        .unwrap();

    // If we got to here the request worked
    shutdown_send.send(()).unwrap();
    srv_handle.await.unwrap();
}
