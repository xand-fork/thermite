//! Tests which are validate setting and getting metadata from xand-api
use xand_address::Address;
use xand_api_client::{
    CorrelationId, HealthStatus, PendingCreateRequest, RegisterSessionKeys, TransactionStatus,
    XandApiClientTrait, XandTransaction,
};
use xand_api_proto::proto_models::SetPendingCreateRequestExpire;
use xand_test_helpers::{NetworkBuilder, XandKeyRing};
use xandstrate::chain_spec::DEFAULT_TESTNET_PENDING_CREATE_EXPIRE_TIME_MILLISECS;

#[tokio::test]
async fn a_list_of_members_can_be_retrieved() {
    let members = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .run(|network| async move {
            network
                .api_services
                .get(&XandKeyRing::Tupac)
                .unwrap()
                .get_members()
                .await
                .unwrap()
        })
        .await;
    assert!(members.contains(&XandKeyRing::Tupac.into()));
    assert!(members.contains(&XandKeyRing::Biggie.into()));
}

#[tokio::test]
async fn a_list_of_authority_keys_can_be_retrieved() {
    let validators = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .run(|network| async move {
            network
                .api_services
                .get(&XandKeyRing::Tupac)
                .unwrap()
                .get_authority_keys()
                .await
                .unwrap()
        })
        .await;
    assert!(!validators.is_empty());
}

#[tokio::test]
async fn session_keys_can_be_registered() {
    let status = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .run(|network| async move {
            network
                .api_services
                .get(&XandKeyRing::Tupac)
                .unwrap()
                .submit_transaction_wait_final(
                    XandKeyRing::Tupac.into(),
                    XandTransaction::RegisterSessionKeys(RegisterSessionKeys {
                        block_production_pubkey: XandKeyRing::Tupac.into(),
                        block_finalization_pubkey: XandKeyRing::Tupac.into(),
                    }),
                )
                .await
                .unwrap()
                .status
        })
        .await;
    assert_eq!(status, TransactionStatus::Finalized);
}

#[tokio::test]
async fn trustee_can_be_retrieved() {
    let trustee = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .run(|network| async move {
            network
                .api_services
                .get(&XandKeyRing::Tupac)
                .unwrap()
                .get_trustee()
                .await
                .unwrap()
        })
        .await;
    assert_eq!(trustee, XandKeyRing::Trust.into());
}

#[tokio::test]
async fn pending_create_expire_time_can_be_retrieved() {
    let actual_expire_time = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .run(|network| async move {
            network
                .api_services
                .get(&XandKeyRing::Tupac)
                .unwrap()
                .get_pending_create_request_expire_time()
                .await
                .unwrap()
        })
        .await;
    assert_eq!(
        actual_expire_time,
        DEFAULT_TESTNET_PENDING_CREATE_EXPIRE_TIME_MILLISECS
    );
}

#[tokio::test]
async fn pending_create_expire_time_can_be_set() {
    let expected_expire_time = 1_000_000;
    let txn = XandTransaction::SetPendingCreateRequestExpire(SetPendingCreateRequestExpire {
        expire_in_milliseconds: expected_expire_time,
    });

    let actual_expire_time = NetworkBuilder::default()
        .add_service(XandKeyRing::Trust)
        .run(|network| async move {
            let client = network.api_services.get(&XandKeyRing::Trust).unwrap();
            client
                .submit_transaction_wait_final(XandKeyRing::Trust.into(), txn)
                .await
                .unwrap();
            client
                .get_pending_create_request_expire_time()
                .await
                .unwrap()
        })
        .await;
    assert_eq!(actual_expire_time, expected_expire_time);
}

#[tokio::test]
async fn cidr_block_allowlist_can_be_retrieved() {
    let _ = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .run(|network| async move {
            network
                .api_services
                .get(&XandKeyRing::Tupac)
                .unwrap()
                .get_allowlist()
                .await
                .unwrap()
        })
        .await;
}

#[tokio::test]
async fn health_check_can_be_retrieved_from_new_network() {
    let health_response = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .run(|network| async move {
            // Given a newly created xand api and chain
            let client = network.api_services.get(&XandKeyRing::Tupac).unwrap();

            // Submit a create to wait some time for the initial chain connection to be established
            let account = xand_api_proto::proto_models::BankAccountInfo::Unencrypted(
                xand_api_proto::proto_models::BankAccountId {
                    routing_number: "111111111".to_string(),
                    account_number: "222222222".to_string(),
                },
            );
            let correlation_id = CorrelationId::gen_random();
            let transaction = XandTransaction::CreateRequest(PendingCreateRequest {
                amount_in_minor_unit: 1,
                correlation_id: correlation_id.clone(),
                account: account.clone(),
                completing_transaction: None,
            });
            client
                .submit_transaction_wait_final(
                    Address::from(XandKeyRing::Tupac),
                    transaction.clone(),
                )
                .await
                .unwrap();

            // When | The health is checked
            client.check_health().await.unwrap()
        })
        .await;

    // Then the response is healthy and the chain is synced
    assert_eq!(health_response.status, HealthStatus::Healthy);
}
