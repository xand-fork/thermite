//! This module contains integration tests between the client and the runtime. Don't expect to
//! write tests in here that make use of the peripheral services (Trust, Member API, etc). If you
//! need to write such tests, they should go in the repo of the peripheral service they target, or
//! if they involve more than one such service, consider writing an acceptance test instead.

use super::init_logger;
use futures::{executor::block_on, StreamExt, TryStreamExt};
use rust_integ_tests::utils::new_network;
use secrecy::Secret;
use std::path::PathBuf;
use std::{collections::HashMap, fs, panic, sync::Arc, time::Duration};
use tempfile::TempDir;
use tpfs_krypt::KeyType;
use xand_ledger::{CorrelationId, RedeemCancellationReason, RedeemCancellationTransaction};
use xand_models::{dev_account_key, hash_substrate_encodeable};
use xand_test_helpers::xandstrate_client::test_helpers::get_initial_keys;
use xand_test_helpers::{get_connected_client, SubsClientExt, TestClientsManager};
use xandstrate::multinode_util::SnapshotOverride;
use xandstrate::{
    chain_spec::{dev_authority_keypair, dev_root_key, SHORT_EPOCH_ENV_FLAG},
    multinode_util::{
        run_network_for_test, run_with_network, start_and_wait_test_network,
        start_one_multinode_node, MultinodeParams, NetworkParams,
    },
};
use xandstrate_client::test_helpers::create_key_manager;
use xandstrate_client::{
    add_authority_key_extrinsic, register_member_extrinsic, register_session_keys_extrinsic,
    remove_authority_key_extrinsic, test_helpers::create_key_signer, CallExt, KeyMgrSigner, Pair,
    SubstrateClientErrors, SubstrateClientInterface, SubstrateEventProvider, DEV_PHRASE,
};
use xandstrate_runtime::extrinsic_builder::{
    confidential_redeem_cancellation, set_validator_emission_rate_extrinsic,
};
use xandstrate_runtime::{
    DispatchError, Event, Runtime, SystemEvent, SHORT_TEST_EPOCH_DURATION_IN_BLOCKS,
};

#[test]
fn can_tell_if_tx_failed_when_committed() {
    init_logger();
    run_with_network(|| {
        block_on(async {
            let client = get_connected_client().await;

            let trust = dev_account_key("Trust").public();
            let correlation_id: CorrelationId = [69; 16];
            let redeem_cancellation_txn = RedeemCancellationTransaction {
                correlation_id,
                reason: RedeemCancellationReason::InvalidData,
            };
            let call = confidential_redeem_cancellation(redeem_cancellation_txn);
            let key_manager = create_key_signer();
            let ext = client.make_signable(&trust, call).await.unwrap();
            let res = client
                .submit_and_watch_extrinsic(&ext.sign(&key_manager).unwrap())
                .await
                .unwrap()
                .wait_until_committed()
                .await
                .unwrap();

            let is_success = client.is_extrinsic_success(res).await.unwrap();
            assert!(!is_success);

            let detailed_events = client.get_events_for(res).await.unwrap();
            assert!(detailed_events.iter().any(|er| match er {
                Event::system(SystemEvent::<Runtime>::ExtrinsicFailed(
                    DispatchError::Module {
                        message: Some(msg), ..
                    },
                    _,
                )) => {
                    msg == &"NoMatchingRedeemCorrelationIdToCancel"
                }
                _ => false,
            }));
        })
    })
}

#[test]
fn can_tell_if_tx_success_when_committed() {
    init_logger();
    run_with_network(|| {
        block_on(async {
            let client = get_connected_client().await;

            let root = dev_root_key().public();
            let update_emission_extrinsic =
                set_validator_emission_rate_extrinsic(100u64, 1u32).into_sudo();
            let key_manager = create_key_signer();
            let extrinsic = client
                .make_signable(&root, update_emission_extrinsic)
                .await
                .unwrap();

            // When
            let res = client
                .submit_and_watch_extrinsic(&extrinsic.sign(&key_manager).unwrap())
                .await
                .unwrap()
                .wait_until_committed()
                .await
                .unwrap();

            let is_success = client.is_extrinsic_success(res).await.unwrap();
            assert!(is_success);
        })
    })
}

#[test]
fn test_local_ext_hash_matches_substrate_ext_hash() {
    init_logger();
    run_with_network(|| {
        block_on(async {
            let client = get_connected_client().await;
            let root = dev_root_key().public();

            // Build extrinsic
            let update_emission_extrinsic =
                set_validator_emission_rate_extrinsic(100u64, 1u32).into_sudo();
            let res = client.make_signable(&root, update_emission_extrinsic).await;

            let res = res.unwrap();
            let key_manager = create_key_signer();
            let signed = res.sign(&key_manager).unwrap();
            let local_hash = hash_substrate_encodeable(&signed);
            let res = client.submit_extrinsic(&signed).await.unwrap();
            assert_eq!(res, local_hash);
        })
    })
}

#[test]
fn get_next_account_nonce_correctly_increments_from_existing_account_nonce() {
    init_logger();
    run_with_network(|| {
        block_on(async {
            let client = get_connected_client().await;

            let root = dev_root_key().public();

            // Submit 1 TX from Tupac
            let update_emission_extrinsic =
                set_validator_emission_rate_extrinsic(100u64, 1u32).into_sudo();
            let key_manager = create_key_signer();
            let extrinsic = client
                .make_signable(&root, update_emission_extrinsic)
                .await
                .unwrap();
            client
                .sub_ext_and_watch_until_success(&extrinsic.sign(&key_manager).unwrap())
                .await
                .unwrap();

            // Fetching next account nonce should start at correct index
            let new_nonce = client
                .query_next_account_nonce(&root.0.into())
                .await
                .unwrap();

            assert_eq!(new_nonce, 1)
        })
    })
}

#[test]
fn can_listen_to_block_production() {
    init_logger();
    run_with_network(|| {
        block_on(async {
            let client = get_connected_client().await;

            let headstream = client.subscribe_finalized_heads().await.unwrap();
            let res: Vec<_> = headstream.take(3).try_collect().await.unwrap();
            assert_eq!(res.len(), 3);
            assert_eq!(res[2].number, 2);
        })
    })
}

#[test]
fn member_registration() {
    init_logger();
    run_with_network(|| {
        block_on(async {
            let client = get_connected_client().await;

            let xand = dev_root_key().public();
            let pac = dev_account_key("2Pac").public();
            let trust = dev_account_key("Trust").public();
            let key_manager = create_key_signer();

            // Verify 2pac can't register trust as a member
            let reg_call =
                register_member_extrinsic(trust.0.into(), Default::default()).into_sudo();
            let ext = client.make_signable(&pac, reg_call).await.unwrap();
            let res = client
                .sub_ext_and_watch_until_success(&ext.sign(&key_manager).unwrap())
                .await;
            let as_client_err = res.unwrap_err();
            assert_that!(
                &as_client_err,
                is_variant!(SubstrateClientErrors::ExtrinsicCommittedButFailed)
            );
            // Verify Xand *can*
            let reg_call =
                register_member_extrinsic(trust.0.into(), Default::default()).into_sudo();
            let ext = client.make_signable(&xand, reg_call).await.unwrap();
            let res = client
                .sub_ext_and_watch_until_success(&ext.sign(&key_manager).unwrap())
                .await;
            assert!(res.is_ok())
        })
    })
}

/// This test takes a while because two sessions need to elapse for validator set changes
/// to take effect
#[test]
fn can_add_validator_at_runtime_2_to_3() {
    init_logger();
    run_network_for_test(
        NetworkParams {
            num_nodes: 2,
            use_short_epochs: true,
            ..NetworkParams::default()
        },
        || {
            // Needed b/c of tokio timer usage
            let rt = tokio::runtime::Runtime::new().unwrap();
            rt.block_on(async {
                let client = get_connected_client().await;

                let new_validator_path = "ImANewBoi";
                let mut key_manager = create_key_manager(get_initial_keys());
                key_manager
                    .import_keypair(
                        Secret::new(format!("{}//{}", DEV_PHRASE, new_validator_path)),
                        KeyType::SubstrateSr25519,
                    )
                    .unwrap();
                let new_validator = dev_account_key(new_validator_path).public();
                // Wait for at least the first block to be finalized
                client.wait_until_final(1, Duration::from_secs(30)).await;

                // Note this is only the pubkey (must be Xand3 to match key passed to new validator
                // when it's started)
                let new_validator_auth_path = "Xand3";
                let new_validator_auth = dev_authority_keypair(new_validator_auth_path).public();
                key_manager
                    .import_keypair(
                        Secret::new(format!("{}//{}", DEV_PHRASE, new_validator_auth_path)),
                        KeyType::SubstrateEd25519,
                    )
                    .unwrap();
                let key_manager = KeyMgrSigner(Arc::from(key_manager));

                // Add the new validator to the network
                let root = dev_root_key().public();
                let add_key_call = add_authority_key_extrinsic(new_validator.0.into()).into_sudo();

                info!("TEST: Begin add authority key transaction.");
                let ext = client.make_signable(&root, add_key_call).await.unwrap();
                client
                    .sub_ext_and_watch_until_success(&ext.sign(&key_manager).unwrap())
                    .await
                    .unwrap();

                // Start the new validator
                let new_validator_handle = start_one_multinode_node(
                    2,
                    MultinodeParams {
                        env_overrides: {
                            let mut hm = HashMap::new();
                            hm.insert(SHORT_EPOCH_ENV_FLAG.to_string(), "true".to_string());
                            hm
                        },
                        ..MultinodeParams::default()
                    },
                    2,
                )
                .unwrap();

                // Register the new validator's session keys
                let reg_sess_call =
                    register_session_keys_extrinsic(new_validator, new_validator_auth);
                info!("TEST: Begin submit register session keys transactions");

                let ext = client
                    .make_signable(&new_validator, reg_sess_call)
                    .await
                    .unwrap();
                client
                    .sub_ext_and_watch_until_success(&ext.sign(&key_manager).unwrap())
                    .await
                    .unwrap();

                // Wait two sessions
                info!("TEST: Begin waiting for up to three minutes.");
                client
                    .wait_until_final(
                        SHORT_TEST_EPOCH_DURATION_IN_BLOCKS * 2 + 1,
                        Duration::from_secs(60 * 3),
                    )
                    .await;

                // Submit another transaction
                let root = dev_root_key().public();
                let update_emission_extrinsic =
                    set_validator_emission_rate_extrinsic(100u64, 1u32).into_sudo();

                info!("TEST: Begin submit set validator emission rate transaction.");
                let ext = client
                    .make_signable(&root, update_emission_extrinsic)
                    .await
                    .unwrap();
                client
                    .sub_ext_and_watch_until_success(&ext.sign(&key_manager).unwrap())
                    .await
                    .unwrap();

                info!("TEST: Begin waiting for up to three minutes.");
                client
                    .wait_until_final(
                        SHORT_TEST_EPOCH_DURATION_IN_BLOCKS * 2 + 4,
                        Duration::from_secs(60),
                    )
                    .await;

                drop(new_validator_handle);
            })
        },
    );
}

/// This test takes a while because two sessions need to elapse for validator set changes
/// to take effect
#[tokio::test]
async fn can_remove_validator_at_runtime_3_to_2() {
    init_logger();
    // Network needs to be manually initialized in this test so we can kill the third validator
    let mut subprocs = start_and_wait_test_network(NetworkParams {
        use_short_epochs: true,
        num_nodes: 3,
        ..NetworkParams::default()
    })
    .await;

    {
        let client = get_connected_client().await;

        let existing_validator_kp = dev_account_key("Xand3");

        // Wait for at least the first block to be finalized
        client.wait_until_final(1, Duration::from_secs(30)).await;

        // Remove validator from the network
        let root = dev_root_key().public();
        let remove_key_call =
            remove_authority_key_extrinsic(existing_validator_kp.public().0.into()).into_sudo();
        let key_manager = create_key_signer();
        // is sometimes invalid for totally unclear reasons (but the transaction still processes
        // and actually is valid, rpc just says it isn't.)
        let ext = client.make_signable(&root, remove_key_call).await.unwrap();
        client
            .sub_ext_and_watch_until_success(&ext.sign(&key_manager).unwrap())
            .await
            .unwrap();

        // Shut down the validator. Can't do this until everything's taken effect, which is
        // a full two sessions!
        client
            .wait_until_final(
                SHORT_TEST_EPOCH_DURATION_IN_BLOCKS * 2 + 1,
                Duration::from_secs(60 * 3),
            )
            .await;
        subprocs.children[2].kill();
        dbg!("Killed third validator");

        // Submit another transaction, and make sure it gets finalized
        let root = dev_root_key().public();
        let update_emission_extrinsic =
            set_validator_emission_rate_extrinsic(100u64, 1u32).into_sudo();
        let ext = client
            .make_signable(&root, update_emission_extrinsic)
            .await
            .unwrap();
        client
            .sub_ext_and_watch_until_success(&ext.sign(&key_manager).unwrap())
            .await
            .unwrap();

        client
            .wait_until_final(
                SHORT_TEST_EPOCH_DURATION_IN_BLOCKS * 2 + 4,
                Duration::from_secs(30),
            )
            .await;
    }

    drop(subprocs);
}

#[tokio::test]
async fn can_reboot_validators() {
    // Given
    let mut _network = new_network().await;
    let mgr = TestClientsManager::new().await;

    // When
    mgr.verify_can_submit_any_transaction_successfully().await;

    drop(_network);
    _network = start_and_wait_test_network(NetworkParams {
        num_nodes: 3,
        start_fresh: false,
        ..NetworkParams::default()
    })
    .await;

    mgr.verify_can_submit_any_transaction_successfully().await;
}

#[tokio::test]
async fn snapshot_file_saved() {
    init_logger();
    let snapshot_dir = TempDir::new().unwrap();

    let every_block = 0;
    let snapshot_override = Some(SnapshotOverride {
        enable_snapshotter: true,
        snapshot_dir: Some(PathBuf::from(snapshot_dir.path())),
        snapshot_cool_down_secs: Some(every_block),
    });

    let mut _subprocs = start_and_wait_test_network(NetworkParams {
        use_short_epochs: true,
        num_nodes: 1,
        snapshot_override,
        ..NetworkParams::default()
    })
    .await;

    {
        let client = get_connected_client().await;
        let finalized_block_count = 3;
        client
            .wait_until_final(finalized_block_count, Duration::from_secs(45))
            .await;
    }

    let files = fs::read_dir(snapshot_dir.path()).unwrap();
    let mut expected_index_file = snapshot_dir.into_path();
    expected_index_file.push("INDEX_FILE");

    let snapshot_files = files.map(|d| d.unwrap().path()).collect::<Vec<_>>();

    assert_eq!(snapshot_files.len(), 3);
    assert!(snapshot_files.contains(&expected_index_file));
}
