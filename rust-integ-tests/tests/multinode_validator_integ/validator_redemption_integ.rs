use super::init_logger;
use futures::executor::block_on;
use futures::{StreamExt, TryStreamExt};
use sp_core::{crypto::Ss58Codec, sr25519::Public};
use xand_test_helpers::get_connected_client;
use xandstrate::{chain_spec::dev_root_key, multinode_util::run_with_network};
use xandstrate_client::{
    set_validator_emission_rate_extrinsic, test_helpers::create_key_signer, CallExt, Pair,
    SubstrateClientInterface,
};
use xandstrate_runtime::validator_emissions::models::{
    block_quota::BlockQuota, minor_unit_emission_rate::MinorUnitEmissionRate,
    validator_emission_rate::ValidatorEmissionRate,
};

#[test]
fn validator_emission_progress_returns_successfully_after_modification() {
    // Given
    init_logger();
    run_with_network(|| {
        block_on(async {
            let client = get_connected_client().await;
            let root = dev_root_key().public();
            let update_emission_extrinsic =
                set_validator_emission_rate_extrinsic(100u64, 1u32).into_sudo();
            let key_manager = create_key_signer();
            let extrinsic = client
                .make_signable(&root, update_emission_extrinsic)
                .await
                .unwrap();
            let first_validator_address = client
                .get_authority_keys()
                .await
                .unwrap()
                .into_iter()
                .next()
                .unwrap();

            // When
            client
                .sub_ext_and_watch_until_success(&extrinsic.sign(&key_manager).unwrap())
                .await
                .unwrap();
            let headstream = client.subscribe_finalized_heads().await.unwrap();
            let _block_headers: Vec<_> = headstream.take(5).try_collect().await.unwrap();

            let progress = client
                .get_validator_emission_progress(
                    &Public::from_ss58check(&first_validator_address.to_string()).unwrap(),
                )
                .await
                .unwrap();

            // Then
            assert_eq!(
                progress.effective_emission_rate,
                ValidatorEmissionRate {
                    minor_units_per_emission: MinorUnitEmissionRate::from(100u32),
                    block_quota: BlockQuota::try_from(1u32).unwrap(),
                }
            );
        })
    })
}

#[test]
fn total_issuance_is_nonzero_after_rewards() {
    // Given | a confidential network with a 100-minor-unit emission rate rewarded each block and 0 issuance at start
    init_logger();
    run_with_network(|| {
        let rt = tokio::runtime::Runtime::new().unwrap();
        rt.block_on(async {
            let client = get_connected_client().await;
            let initial_issuance = client.get_total_issuance().await.unwrap();
            let root = dev_root_key().public();
            let update_emission_extrinsic =
                set_validator_emission_rate_extrinsic(100u64, 1u32).into_sudo();
            let key_manager = create_key_signer();
            let extrinsic = client
                .make_signable(&root, update_emission_extrinsic)
                .await
                .unwrap();
            client
                .sub_ext_and_watch_until_success(&extrinsic.sign(&key_manager).unwrap())
                .await
                .unwrap();

            // When | the chain is run for an arbitrary number of blocks
            let blocks_to_run = 20u64;
            let headstream = client.subscribe_finalized_heads().await.unwrap();
            let _block_headers: Vec<_> = headstream
                .take(blocks_to_run as usize)
                .try_collect()
                .await
                .unwrap();

            // Then | total issuance on the network will be increased
            let new_issuance = client.get_total_issuance().await.unwrap();
            assert_eq!(initial_issuance, 0);
            assert!(new_issuance.gt(&0));
        })
    })
}
