//! Tests for non-confidential transactions
use xand_api_client::XandApiClientTrait;
use xand_test_helpers::{NetworkBuilder, XandKeyRing};

#[tokio::test]
async fn member_can_withdraw_self_from_network() {
    NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .run(|network| async move {
            let service = network.api_services.get(&XandKeyRing::Tupac).unwrap();
            let original_member_count = service.get_members().await.unwrap().len();
            service
                .submit_transaction_wait_final(
                    XandKeyRing::Tupac.into(),
                    xand_api_client::XandTransaction::WithdrawFromNetwork(
                        xand_api_client::WithdrawFromNetwork {},
                    ),
                )
                .await
                .unwrap();
            let after_member_count = service.get_members().await.unwrap().len();
            dbg!(&original_member_count);
            dbg!(&after_member_count);
            assert_eq!(after_member_count, original_member_count - 1);
        })
        .await;
}

#[tokio::test]
async fn validator_can_withdraw_self_from_network() {
    NetworkBuilder::default()
        .add_service(XandKeyRing::Xand1)
        .run(|network| async move {
            let service = network.api_services.get(&XandKeyRing::Xand1).unwrap();
            let original_validator_count = service.get_authority_keys().await.unwrap().len();
            service
                .submit_transaction_wait_final(
                    XandKeyRing::Xand1.into(),
                    xand_api_client::XandTransaction::WithdrawFromNetwork(
                        xand_api_client::WithdrawFromNetwork {},
                    ),
                )
                .await
                .unwrap();
            let after_validator_count = service.get_authority_keys().await.unwrap().len();
            assert_eq!(after_validator_count, original_validator_count - 1);
        })
        .await;
}
