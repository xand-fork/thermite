#![forbid(unsafe_code)]

#[macro_use]
extern crate log;

pub mod utils;

pub use utils::get_xand_api_client;
