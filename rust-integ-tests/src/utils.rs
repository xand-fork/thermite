use xand_api_client::{ReconnectingXandApiClient, XandApiClientTrait};
use xandstrate::multinode_util::{start_and_wait_test_network, MultinodeHandle, NetworkParams};

pub async fn new_network() -> MultinodeHandle {
    info!("Starting network");
    start_and_wait_test_network(NetworkParams {
        num_nodes: 3,
        ..NetworkParams::default()
    })
    .await
}

pub async fn get_xand_api_client() -> impl XandApiClientTrait {
    let client = ReconnectingXandApiClient::stub(
        format!(
            "http://127.0.0.1:{}",
            xandstrate::SUBSTRATE_STARTING_XAND_API_PORT
        )
        .parse()
        .unwrap(),
    );
    client.await
}
