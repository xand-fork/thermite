<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Machine Setup](#machine-setup)
  - [Install packages we depend on](#install-packages-we-depend-on)
  - [Setup an ssh key for Gitlab](#setup-an-ssh-key-for-gitlab)
  - [Clone the repo](#clone-the-repo)
  - [Development Dependencies](#development-dependencies)
    - [Artifactory Access and NPM Dependencies](#artifactory-access-and-npm-dependencies)
    - [Rustup](#rustup)
    - [VPN](#vpn)
    - [TPFS Crates](#tpfs-crates)
      - [GitLab crates index SSH access](#gitlab-crates-index-ssh-access)
      - [Setting up tpfs crates globally](#setting-up-tpfs-crates-globally)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Machine Setup

## Install packages we depend on
Run `deps.sh` from the repo root

## Setup an ssh key for Gitlab

```bash
cd ~/.ssh
ssh-keygen -t rsa -C "<email address>@tpfs.io"

```
Copy the contents of `id rsa.pub` and paste them into a new ssh key in GitLab.

## Clone the repo

```bash
cd <your src directory>
git clone git@gitlab.com:TransparentIncDevelopment/thermite.git
```

## Development Dependencies

### Artifactory Access and NPM Dependencies

NPM dependencies are managed by a jfrog repository. Follow [these instructions](https://transparentinc.atlassian.net/wiki/spaces/PD/pages/89030660/Artifactory+Instructions) to configure and authenticate your machine for local access to yarn.

### Rustup

We use [rustup](https://rustup.rs) to manage the installed version of rust.

### VPN

We use [Pritunl](https://pritunl.com) for our VPN services. If you are offsite, you will need it to
pull from our internal crates repository (`cargo build`) and access apps running in GCP.

When enbaled, the client will route specific outbound traffic through the VPN server. Right now,
the VPN is configured to route requests to the IP addresses associated with the `tpfs-apps` and `development` clusters
through the VPN. All other traffic will remain the same.

>You will need an OTP client (one time password) for 2FA.
>[Google Authenticator](https://apps.apple.com/us/app/google-authenticator/id388497605)
>and [Authy](https://apps.apple.com/us/app/authy/id494168017) are 2 popular phone apps for OTP.
>[1Password](https://support.1password.com/one-time-passwords/) can also serve as an OTP client.
>You can also try out this rust `otpgen` [CLI](https://gitlab.com/TransparentIncDevelopment/r-d/otpgen)
>Stephane put together to generate one time passwords!

Setup steps:

1. Install the [Pritunl client](https://client.pritunl.com/#install)
1. Visit: https://vpn.xand.tools and sign in with your tpfs.io account
1. Enter a pin, and click "Change Pin". You will use this PIN to sign in on the client.
1. Configure your OTP client with the Two-Step Authentication Key
1. Click "Show More", and then "Download Profile" (singular "Profile". It's a small blue button, easy to miss). You will have downloaded a `.ovpn` file.
1. Open your Pritunl client, and click "Import Profile" to import the `.ovpn` file you just downloaded
1. Click "Connect" to enter your PIN and OTP (from your configured OTP client of choice)

Voilà! You should now be able to visit protected sites like https://crates.xand.tools.

### TPFS Crates

We have a private crates to store version Rust packages. Setting up your environment to access it requires
 - GitLab ssh access
 - Running the ssh agent in the background
 - Generating and setting a token to authenticate against the tpfs crates API

#### GitLab crates index SSH access

If on linux, use an ssh-agent for auth :
```shell
# Run the ssh-agent in the background
eval `ssh-agent`

# Now that the ssh agent is running, add your gitlab private key path to the agent.
ssh-add {PATH_TO_YOUR_PRIVATE_KEY_REGISTERED_WITH_GITLAB}
```

Add those commands to `.bashrc` so it's automated.

#### Setting up tpfs crates globally

Create a `~/.cargo/config` file with the contents:
```toml
[registries.tpfs]
index = "ssh://git@gitlab.com/TransparentIncDevelopment/tpfs-crates-index"
```
This globally defines the `tpfs` registry, such that you can reference it from any rust project.
You can also set these per project, so only that specific project binds `tpfs` to the specified registry and index.
You do this by placing the same file at `../project_root/.cargo/config`.
