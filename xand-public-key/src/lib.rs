//! Public keys contain the native cryptographic primitives we need for various operations on the
//! Xand network. They can be used to deterministically derive Addresses, which are user-facing,
//! stringly-typed primitives that identify entities participating on a Substrate blockchain network.
//!
//! Note: This crate takes Substrate-specific cryptographic dependencies.
//!
//! Public keys are verified to be compatible with Substrate's
//! SS58 External Address format:
//! https://github.com/paritytech/substrate/wiki/External-Address-Format-(SS58)

#[cfg(test)]
mod test;

use curve25519_dalek::ristretto::{CompressedRistretto, RistrettoPoint};
use sp_core::{crypto::Ss58Codec, sr25519::Public};
use xand_address::Address;
use xand_ledger::PublicKey;

pub fn address_to_public_key(address: &Address) -> Option<PublicKey> {
    let substrate_public_key = Public::from_ss58check(&address.to_string()).ok()?;
    // TODO - add manual impl for deserialization of Address, remove serde autoderive impl - https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6757
    let ristretto_point =
        CompressedRistretto::from_slice(substrate_public_key.as_ref()).decompress()?;
    Some(ristretto_point.into())
}

pub fn public_key_to_address(key: &PublicKey) -> Address {
    ristretto_point_to_address(&curve25519_dalek::ristretto::RistrettoPoint::from(*key))
}

pub fn ristretto_point_to_address(point: &RistrettoPoint) -> Address {
    Public::from_raw(point.compress().0)
        .to_ss58check()
        .parse::<Address>()
        .expect(
            "Conversion failure impossible in practice: RistrettoPoint cannot be invalid; \
        deserialization should not fail from a valid Public keypair",
        )
}
