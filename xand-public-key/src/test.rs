#![allow(non_snake_case)]
use super::{address_to_public_key, public_key_to_address, ristretto_point_to_address};
use curve25519_dalek::ristretto::RistrettoPoint;
use proptest::proptest;
use rand::thread_rng;
use sp_core::crypto::Ss58Codec;
use xand_address::Address;

#[test]
fn public_key_to_address__roundtrip_succeeds() {
    // Given
    let ristretto_point = &RistrettoPoint::random(&mut thread_rng())
        .compress()
        .decompress()
        .unwrap();
    // When
    let expected_address: Address = ristretto_point_to_address(ristretto_point);
    // Then
    assert_eq!(
        public_key_to_address(&address_to_public_key(&expected_address).unwrap()),
        expected_address,
    );
}
// TODO add proptest - https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6750

#[test]
fn address_to_public_key__valid_address_from_invalid_compressed_ristretto_point_fails() {
    // Given
    let public_key = sp_core::sr25519::Public([255; 32]);
    // When
    let address = public_key.to_ss58check().parse().unwrap();
    // Then
    let res = address_to_public_key(&address);
    assert!(res.is_none());
}

fn join_byte_arrays(bytes0: [u8; 32], bytes1: [u8; 32]) -> [u8; 64] {
    let mut output = [0u8; 64];
    output[0..32].copy_from_slice(&bytes0);
    output[32..64].copy_from_slice(&bytes1);
    output
}

proptest! {
    #[test]
    fn proptest__public_key_to_address__roundtrip_succeeds(bytes0:[u8;32], bytes1:[u8;32]){

        let input  = join_byte_arrays(bytes0,bytes1);

        let ristretto_point = RistrettoPoint::from_uniform_bytes(&input);
    // When
        let expected_address: Address = ristretto_point_to_address(&ristretto_point);
        // Then
        assert_eq!(
            public_key_to_address(&address_to_public_key(&expected_address).unwrap()),
            expected_address,
        );
    }
}
