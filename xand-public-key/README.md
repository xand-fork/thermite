# xand-public-key

Public keys contain the native cryptographic primitives we need for various operations on the Xand network. 

They can be used to deterministically derive Addresses, which are user-facing, stringly-typed primitives that 
identify entities participating on a Substrate-based network. 

> Note: this implementation depends on Substrate, as the Address format must adhere to the SS58 address format used by our chain.
> [Read more](https://substrate.dev/docs/en/knowledgebase/advanced/ss58-address-format)

