Welcome to Thermite!
[![pipeline status](https://gitlab.com/TransparentIncDevelopment/thermite/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/TransparentIncDevelopment/thermite/commits/master)

Working in this Repo
==============

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Getting Started](#getting-started)
  - [What is in this repo?](#what-is-in-this-repo)
  - [Understanding more about xandstrate/substrate the validator](#understanding-more-about-xandstratesubstrate-the-validator)
  - [Example of transaction flow](#example-of-transaction-flow)
    - [The transaction payload(s)](#the-transaction-payloads)
  - [How to Build the Software](#how-to-build-the-software)
  - [Running the software locally](#running-the-software-locally)
  - [Live debugging Validator instances with CLion / GDB](#live-debugging-validator-instances-with-clion--gdb)
  - [Controlling logging output](#controlling-logging-output)
  - [Build details](#build-details)
    - [Rust specifics](#rust-specifics)
- [Publishing Artifacts](#publishing-artifacts)
  - [Docker Images](#docker-images)
    - [For Beta Images](#for-beta-images)
    - [For Release Images](#for-release-images)
    - [Steps](#steps)
- [Testing in Thermite](#testing-in-thermite)
  - [Overview](#overview)
  - [Standard Unit Tests](#standard-unit-tests)
    - [Examples](#examples)
    - [Limitations](#limitations)
  - [Runtime Unit Tests](#runtime-unit-tests)
    - [Examples](#examples-1)
    - [Limitations](#limitations-1)
  - [`NetworkBuilder` Integ Tests](#networkbuilder-integ-tests)
    - [Examples](#examples-2)
    - [Limitations](#limitations-2)
  - [**(Deprecated)** "Multinode" Integ Tests](#deprecated-multinode-integ-tests)
    - [Examples](#examples-3)
    - [Limitations](#limitations-3)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Getting Started

If you have not already configured your machine for development in this project,
follow [these instructions](MACHINE-SETUP.md).

## What is in this repo?

The thermite repository contains the code for running a validator on the xand
network. Broadly, the important components of the repo are:

* `validator/` - The actual validator built on top of substrate.
    * `validator/client/` - A client for communicating directly with substrate
    * `validator/runtime/` - This is the "core" logic of our blockchain
* `xand-api/` - Our abstraction of what substrate/the blockchain is doing.
    Sits on top of a blockchain client and exposes a gRPC api for our other
    services (and customers) to talk to.
* `xand-models/` - Models of the transactions / structures involved in
    the runtime and our network and server-side operations. Free of large dependencies.
* `xand-runtime-models/` - `no-std` compatible models that the runtime uses,
    and are also useful outside of it.

## Understanding more about xandstrate/substrate the validator

If you are doing any work inside the `validator` directory (or below) you will
need to understand a bit about substrate. We call our validator and runtime
`xandstrate`. Please see the readme [here](validator/README.md) for more.

## Example of transaction flow
All transactions ( ~= extrinsics, in substrate lingo), regardless of what type, flow through the system
in largely the same way. The below diagram serves to illustrate how that happens, and serves as a
useful tour of the components in the repo. We start with xand-api, which would have already received
some request in business terms (EX: Transfer $400 to XYZ, these are all defined, readably, in the
[protobuf definitions](xand-api/proto/xand-api.proto)) - the diagram illustrates what happens
after that. Notes expanding on numbered steps are below. Dashed lines represent library usage,
there is no wire communication associated with the dashed lines.

```mermaid
sequenceDiagram
  participant OS as Other System
  participant XA as Xand Api
  participant VC as Validator (xandstrate) Client
  participant V as Validator Node
  participant R as Runtime

  OS ->> XA: Transfer $100 to XYZ (a)
  XA -->> XA: Construct Transaction (1)
  XA -->> VC: Make this signable for me
  VC ->> V: Fetch info for signing (2,b)
  V ->> VC:
  VC -->> XA:
  XA -->> XA: Encode/Serialize Transaction (3)
  XA -->> XA: Sign Transaction (4)
  XA -->> VC: Submit this transaction for me
  VC ->> V: Submit transaction (5,c)
  V -->> R: Process this transaction (6)
  R -->> R: Validate / Execute Transaction Logic (7)
```

1. Transaction construction here means translating from our domain-level representations of
   transactions (`xand-models`) which `xand-api` uses, to the structs defined by the runtime, which
   substrate calls [Calls](https://substrate.dev/docs/en/knowledgebase/runtime/macros#decl_module)
   and are defined inside our runtime modules.
   Primarily [here](validator/runtime/src/xandstrate/mod.rs). The linked documentation goes into
   detail, but it's important to understand that the `decl_module` macro ends up generating structs
   that represent the calls (transactions).
1. Some additional information is required in order to sign and encode the transaction properly.
   Namely, the hash of the genesis block and the appropriate account nonce must be attached. An
   explanation of the nonce is provided [here](https://substrate.dev/docs/en/knowledgebase/learn-substrate/tx-pool#transaction-dependencies).
   Note that this is not the same "nonce" that is used for correlating creates/redeems with bank transfers.
1. Now the `Call`, the additional information we fetched, must be bundled into a
   [SignedPayload](https://substrate.dev/rustdocs/v2.0.0-rc5/sp_runtime/generic/struct.SignedPayload.html)
   and encoded, so that the encoded payload may be signed and placed into an
   [UncheckedExtrinsic](https://substrate.dev/rustdocs/v2.0.0-rc5/sp_runtime/generic/struct.UncheckedExtrinsic.html).
   The extrinsic is substrate's representation of a "transaction issued  externally". Extrinsics are
   distinguished from calls that might be generated from the intrinsic logic of the chain (ex: the
   timestamp in every block). The signed payload (and, everything else that is encoded, unless
   otherwise mentioned) is encoded via the
   [SCALE](https://substrate.dev/docs/en/knowledgebase/advanced/codec) encoding scheme.
1. Signing the transaction is accomplished by using the issuer's keypair, which is an instance of
   substrate's [sr25519](https://substrate.dev/docs/en/knowledgebase/advanced/cryptography#sr25519)
   type. This is abstracted away by our [tpfs_krypt](https://gitlab.com/TransparentIncDevelopment/product/libs/tpfs_krypt/)
   library. The signed transaction is an `UncheckedExtrinsic` - which will again be serialized
   with `SCALE`.
1. Transaction submission is performed via the substrate RPC APIs. Either over the wire (current
   implementation) or an in-memory transport. Unfortunately substrate very poorly documents this.
   See the validator client [readme](validator/client/README.md) for more info. Essentially, the
   format is JSON, but the fields are frequently hex-encoded representations of SCALE payloads.
   These APIs consist of two layers - an API in the runtime, and (typically) a JSON RPC endpoint
   which exposes that runtime API. This concept is not well documented in substrate. Runtime
   APIs are built with [decl_runtime_apis!](https://substrate.dev/rustdocs/v2.0.0-rc5/sp_api/macro.decl_runtime_apis.html)
   and it's hooked up to RPC [here](validator/src/rpc/mod.rs).
1. The validator receives the transaction and puts it in the transaction pool. We have not modified
   or authored any of this code - see [here](https://substrate.dev/docs/en/knowledgebase/learn-substrate/tx-pool).
1. Once transactions are executed, our runtime logic that we have authored will determine their
   validity and how they affect state. This is our code in `validator/runtime`. See the big
   [overview](https://substrate.dev/docs/en/knowledgebase/runtime/) for more.


### The transaction payload(s)

There are a handful of payloads involved in the above flow. Here is a worked example with a transfer
from `Alice` to `Bob`. Each bullet point represents a solid line of communication in the above
diagram.

* **A**: The payload submitted to xand-api is encoded binary as defined by gRPC/protobuf. See
   [the proto file](xand-api/proto/xand-api.proto). A JSON-y version of the payload submitted to the
   api would look like the following. The issuer/to are [SS58](https://substrate.dev/docs/en/knowledgebase/advanced/ss58-address-format) addresses.

   ```json
   {
     "issuer": "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ",
     "payment": {
       "to": "5EgqcJ1sbFkUvySAmxc3bThRePvdmw3zCrXMjqYPaSSSzbkQ",
       "amount": 5000
     }
   }
   ```

* **B**: Fetching the info needed for signing involves two requests and would look something like:
   ```json
   // Request
   {
       "method": "chain_getblockhash",
       "params": [0],
       "jsonrpc": "2.0",
       "id": 0
   }
   // Response
   {
       "jsonrpc":"2.0",
       "result":"0x91b171bb158e2d3848fa23a9f1c25182fb8e20313b2c1eb49219da7a70ce90c3",
       "id": 0
   }
   // Request
   {
       "method": "account_nextIndex",
       "params": ["5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"],
       "jsonrpc": "2.0",
       "id": 1
   }
   // Response
   {
       "jsonrpc":"2.0",
       "result":"1337",  // This acccount has submitted 1336 transactions already
       "id": 1
   }
   ```

   For more detail, please see [the validator client readme](validator/client/README.md).

* **C**: Submitting the transaction to the validator is almost entirely inscrutable. It looks like this,
   where everything you'd actually care about has been SCALE-then-hex encoded into the payload. The
   payload is an [UncheckedExtrinsic](https://substrate.dev/rustdocs/v2.0.0-rc5/sp_runtime/generic/struct.UncheckedExtrinsic.html)

   ```json
   {
       "method": "author_submitAndWatchExtrinsic",
       "params": ["0xdeadbeef..deadbeef"],
       "jsonrpc": "2.0",
       "id": 0
   }
   ```

## How to Build the Software

We use [cargo-make](https://github.com/sagiegurari/cargo-make) for basic build orchestration. You
can use it to install most of the dependencies you need, and build everything in the repo. You will
need to install it first like so:

    cargo install cargo-make

Now, try building everything in the repo with

    cargo make build

If you run into problems building, ask someone, and update this README with the fix! (Or, better,
make the error impossible by improving our automation)

Prior to checking in code there's some checks that you'd want to run on your code. Like compile,
linting, unit testing, and formatting checks. We've created a helper utility to keep these together for you.
This way you can get feedback about your commit sooner.

    cargo make preflight-check

## Running the software locally

To run the validators, with your local changes from
this repository, you can run `cargo make multinode -- --release`. By default it will start
one validator. You can start multiple validators using `-n`, e.g.

`cargo make multinode -- --release -n 3`

For running other components with published versions of the validator software, see the acceptance tests [repo](https://gitlab.com/TransparentIncDevelopment/product/testing/xand-acceptance-tests)
which includes a docker-compose setup for running all components together. Thermite's CI publishes a 
chain spec template and docker image per pipeline, using the commit sha for the version. You can use this version
to generate a network and run the acceptance tests in that repo.

## Live debugging Validator instances with CLion / GDB

You can setup a validator network for debugging by running `cargo make multinode -- --release --dry-run`

The `--dry-run` option will run through most of the multinode setup process but not actually start each node.
Instead, `--dry-run` will print out the exact startup command needed to launch each validator in the network. 
These outputs can be used to configure Rust based debugging in IDE's like CLion.

## Controlling logging output

Our nodes use the `tpfs-logging` crate for log outputs, which accepts an environment variable
specifying the log granularity for each module in the application. For example, the following
enables the most detailed log level for `xandstrate_client`:

```
export XAND_TARGET_LOG_LEVEL=xandstrate_client=DEBUG
```

Of particular note are the optional debug logs in `petri`, which will report notable events included
in each block, including extrinsic applications or failures:

```
export XAND_TARGET_LOG_LEVEL=petri=debug
```

## Build details

### Rust specifics

Cargo commands are used to manage the Rust components. All of the rust code (with the exception of
the substrate WASM runtime) is part of a cargo workspace. You should be able to rely on `cargo make`
for all your building and testing needs, but you can use cargo directly if desired.

```bash
cargo build                       # Builds all of the Rust code.
cargo test                        # Runs all of the Rust tests.
cargo lint                        # Runs clippy on all of the Rust code.
cargo fix                         # Attempt to automatically fix any lint issues using rustfix.
cargo make multinode -- --release # Spin up a local validator cluster
```

# Publishing Artifacts

## Docker Images

Thermite uses the `Tag-To-Publish` flow for publishing docker images with semver tags.
 This avoids the need to bump versions on every MR to master.
 SemVer tag publishing will only occur if the pushed tag has a basic semver format like `0.0.1`.
 It also needs to have a common ancestor commit with the mainline branch. 
 Beta publishing for docker images is possible without requiring a common ancestor by
 appending `-beta*` to the version (e.g. `0.0.1-beta.45`).

For inter-release integration testing, each MR to master will publish a non-semver docker image using the commit sha as the tag.
 
For listing out git tags, you can use either of the following commands:

```bash
git fetch --tags
git log --tags --simplify-by-decoration --pretty="format:%ai %d"

2020-08-04 21:57:35 +0000  (tag: 1.0.11, origin/master, origin/HEAD, master)
2020-07-30 18:49:29 -0700  (origin/update-rust-1.45.1)
2020-07-30 09:44:27 -0700  (tag: 0.0.1-beta.5, origin/convert-publishing-to-python)
```

Or more simply:

```bash
git fetch --tags
git tag -l --sort=-creatordate

1.0.11
1.0.10
```

### For Beta Images

While on your feature branch, look at the list of tags.

```bash
git fetch --tags
git tag -l --sort=-creatordate

1.0.11
1.0.10
```

Next, choose a new tag version that increments on the most recent tag.

Next, tag your commit with the intended version + beta tag.

```bash
git tag -m "some message" 1.0.12-beta-1 HEAD
```

Finally, push your tags.

```bash
git push origin 1.0.12-beta-1
```

### For Release Images

After your MR has been merged to `master`:

```bash
git fetch --tags
git tag -l --sort=-creatordate

1.0.11
1.0.10
```

Next, choose a new tag version that increments on the most recent tag.

Next, tag your commit with the version you've selected.

```bash
git tag -m "some message" 1.0.12 HEAD
```

Finally, push your tags.

```bash
git push origin 1.0.12
```

### Steps

After your MR has been merged to `master`:

```bash
git fetch --tags
git tag -l --sort=-creatordate

1.0.11
1.0.10
```

Next, choose a new tag version that increments on the most recent tag.

Next, tag your commit with an incremented version.

```bash
git tag -m "some message" 1.0.12 HEAD
```

Finally, push your tags.

```bash
git push origin 1.0.12
```

> If the docker images have not been modified, they will not be updated. 


# Testing in Thermite

## Overview

Thermite is big project space and it can be difficult to know where to begin writing tests for your use case.  Below are the different testing paradigms we use in Thermite (in order of testing scope) with suggestions on when to use which.

## Standard Unit Tests 

Write tests for small isolated units of functionality. Use test doubles to isolate from dependencies. *Prefer testing functionality using unit tests where possible*. 

### Examples

- [Xand Financial Client unit tests](xand-financial-client/src/create_builder.rs#L100)
- [Xand Models Converter prop tests](xand-api/src/xand_models_converter.rs#L1038)

### Limitations

These tests don’t actually hit other components or exercise a running system. They don’t verify that pieces of the system work well with each other. Therefore wider scoped tests are frequently necessary to verify functionality. 

## Runtime Unit Tests 

Runtime unit tests are tests that are used to assert behavior of a pallet in isolation from the rest of the running network (xandstrate, any other running pallets, petri, etc).  The surface of the test is the defined pallet behavior where the macros are used to generate expected events, transactions, storage changes, etc.  [Validator Emissions](validator/runtime/src/validator_emissions.rs) is new and does a good job of showing the expected surface that would require runtime unit tests. 

Substrate tools allow runtime unit tests to change chain state and behavior during test runs, allowing for very flexible testing setups per-test.

### Examples

Older code used a singular Xandstrate test runtime to create different scenarios for isolated testing of pallet behavior: 

- [Xandstrate test runtime](validator/runtime/src/xandstrate/xandstrate_test_runtime/test_runtime.rs)

However, as we create new code that lives in separated pallets instead of Xandstrate, the new practice is to define a test runtime scoped to the level being tested: 

- [Validator Emissions test runtime definition](validator/runtime/src/validator_emissions/validator_emissions_test_runtime.rs)

- [Validator Emissions tests using a test runtime](validator/runtime/src/validator_emissions/tests)

### Limitations

Using a test runtime means that the full behavior of a running network is not asserted.  However, the scope being tested is greater than standard unit tests and relations between components in-scope can be tested.

Fakes & Mocks take overhead to plan, implement, maintain for the testruntime and any external dependencies.  If engineers are not careful, a trusim or some other error could be built into the test runtime that would render tests run insufficient.  Always make sure to get engineering review for mock implementations.

## `NetworkBuilder` Integ Tests  

Allows interacting with a live, local blockchain and includes a XandApi for indexing additional data. Includes a builder pattern for specifying config values and participants in the local chain. *This is the intended testing paradigm for whole-network integration tests.*

### Examples

- [Confidentiality integ tests](rust-integ-tests/tests/confidential_integ.rs#L419)

### Limitations

These tests will take a long time to compile and run.  They should only be written when integration tests are required to assert whole-network behaviors and relations between network components.  There may be tests which require extensions of the NetworkBuilder infrastructure to allow for different chain states at runtime, but the builder pattern which NetworkBuilder follows should make it simple to extend.

## **(Deprecated)** "Multinode" Integ Tests  

*We do not recommend writing new Multinode tests. Integ tests should be written using network builder tests.* 

Allows interacting with a live, local blockchain. Provides functioning substrate client, with all its methods, e.g., `get_balance`,  `get_members`,  all defined [here](validator/client/src/lib.rs#L85).

Allows the construction, signing, and submission of extrinsics to a functional, local blockchain. 

### Examples

- [All multinode tests](rust-integ-tests/tests/multinode_validator_integ)

### Limitations

Does not provide a XandApi, so any tests that need to test XandApi endpoints, must use the `NetworkBuilder` approach. The testing framework itself is tightly coupled to substrate, so prefer other tests where possible.  Only the actual chain state is available in these tests, not other network components like petri or UTXO balances.