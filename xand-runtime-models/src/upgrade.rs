use alloc::vec::Vec;
use serde_scale_wrap::Wrap;
use wasm_hash_verifier::{WasmBlob, WasmHashVerifier, XandHash, XandWasmHasher};

extern crate alloc;

pub type UpgradeWasmBlob = Wrap<WasmBlob<Vec<u8>>>;
pub type UpgradeXandHash = Wrap<XandHash>;

pub fn verify_upgrade_wasm_blob_and_xand_hash(
    wasm_blob: &UpgradeWasmBlob,
    xand_hash: &UpgradeXandHash,
) -> bool {
    XandWasmHasher::verify(&xand_hash.0, &wasm_blob.0)
}
