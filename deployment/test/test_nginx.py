import unittest
import os

class NginxTests(unittest.TestCase):
    def test_deny_all(self):
        path = os.path.join(os.path.dirname(__file__), './../docker/allowlist-nginx/nginx_proxy.conf.template')
        config = open(path).read()
        deny_all_in_config = "deny all;" in config
        self.assertTrue(deny_all_in_config, "No \"deny all;\" in config template")

if __name__ == '__main__':
    unittest.main()
