#!/usr/bin/env bash
set -e

die() { echo "$*" 1>&2 ; exit 1; }

# These environment variables must be set
[ -n "$CONSENSUS_NODE" ] || die "CONSENSUS_NODE must be set"
[ "$BOOTNODE_URL_VEC" ] || die "BOOTNODE_URL_VEC must be set"
[ "$NODE_NAME" ] || die "NODE_NAME must be set"
[ "$TELEMETRY_URL" ] || die "TELEMETRY_URL must be set"

TX_CACHE_LIMIT=${TX_CACHE_SIZE:=125000}

LOWERCASE_CONSENSUS_NODE=$(echo $CONSENSUS_NODE | tr '[:upper:]' '[:lower:]')
if [[ "$LOWERCASE_CONSENSUS_NODE" = "true" ]] ; then
    [ "$NODE_KEY" ] || die "NODE_KEY must be set"

    # Run the "firstrun" binary to setup the keystore properly
    ./firstrun /xand/keypairs /xand/keystore
    CONSENSUS_BASED_FLAGS="--keystore-path /xand/keystore --node-key-type ed25519 --node-key $NODE_KEY --validator"
else
    CONSENSUS_BASED_FLAGS="--pruning archive"
fi

cp /xand/chainspec/chainSpecModified.json /xand/chainSpec.json

echo "Args: --chain /xand/chainSpec.json \
            --bootnodes $BOOTNODE_URL_VEC --telemetry-url \"$TELEMETRY_URL\" --name $NODE_NAME \
            --transaction-limit $TX_CACHE_LIMIT \
            $CONSENSUS_BASED_FLAGS $@"

# Start the validator, passing any arguments through to it.
exec ./xandstrate --chain /xand/chainSpec.json \
                  --bootnodes $BOOTNODE_URL_VEC --telemetry-url "$TELEMETRY_URL" --name $NODE_NAME \
                  --transaction-limit $TX_CACHE_LIMIT --wasm-execution compiled \
                  $CONSENSUS_BASED_FLAGS "$@"
