# maintainer: Sage Mitchell <sage.mitchell@tpfs.io>
#
# Usage
# =====
#
# Run the test suite:
#
#     $ nix-shell --run "cargo make test"
#
# Enter an interactive shell with the dependencies readily available:
#
#     $ nix-shell
#
# Assumptions
# ===========
#
# 1. machine is running Linux (maybe macOS?) and has nix installed
# 2. SSH key authorized with TPFS GitLab is at $HOME/.ssh/id_ed25519
#    (may be overridden by passing an explicit `ssh-private-key-path`
#    argument to the nix expression)
# 3. connected to TPFS VPN
# 4. yarn is authorized to fetch from TPFS' Artifactory
#
# Future Improvements
# ===================
#
# - manually verify assumptions in a fresh environment
# - dynamically verify assumptions in the `shellHook`
# - document why/how to bump pinned inputs
#
{ nixpkgs-url ? "https://github.com/NixOS/nixpkgs/archive/3590f02e7d5760e52072c1a729ee2250b5560746.tar.gz"
, nixpkgs-mozilla-url ? "https://github.com/mozilla/nixpkgs-mozilla/archive/efda5b357451dbb0431f983cca679ae3cd9b9829.tar.gz"
, ssh-private-key-path ? "$HOME/.ssh/id_ed25519"
}:

let
  nixpkgs-mozilla = import (fetchTarball nixpkgs-mozilla-url);

  pkgs = import (fetchTarball nixpkgs-url) {
    overlays = [ nixpkgs-mozilla ];
  };

  with-overrides = rust: rust.override {
    targets = [
      "x86_64-unknown-linux-gnu"
      "wasm32-unknown-unknown"
    ];
  };

  # currently unused; rust toolchains are managed by rustup
  rust = {
    stable = with-overrides pkgs.latest.rustChannels.stable.rust;
    nightly = with-overrides pkgs.latest.rustChannels.nightly.rust;
    dated-nightly = date: with-overrides (pkgs.rustChannelOf { inherit date; channel = "nightly"; }).rust;
    # from-toolchain-file = with-overrides (pkgs.rustChannelOf { rustToolchain = ./rust-toolchain; }).rust;
  };
in

pkgs.mkShell {
  shellHook = ''
    eval $(ssh-agent)
    ssh-add ${ssh-private-key-path}
  '';

  PROTOC = "${pkgs.protobuf}/bin/protoc";
  PROTOC_INCLUDE = "${pkgs.protobuf}/include";
  LIBCLANG_PATH = "${pkgs.llvmPackages.libclang}/lib";
  LD_LIBRARY_PATH = "${pkgs.zlib}/lib";

  buildInputs = with pkgs; [
    cacert
    cargo-make
    clang
    cmake
    docker
    docker-compose
    git
    jq
    jre
    libressl
    llvmPackages.libclang
    moreutils
    nodejs
    openssh
    pkg-config
    protobuf
    rustfmt
    rustup
    yarn
  ];
}
