#![allow(clippy::result_large_err)]
use crate::{
    confidentiality_store::{
        TransactionOutputRecord, TxoHash, TxoSpentStatus, TxoStore, TxoStoreError,
    },
    errors::LOCK_ERROR_TEXT,
    logging_events::LoggingEvent,
};
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use rand::seq::SliceRandom;
use rand::thread_rng;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use std::collections::HashMap;
use std::sync::{Arc, RwLock};
use xand_address::Address;
use xand_financial_client::txo_selection::{random_improve, MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION};
use xand_financial_client::{
    errors::FinancialClientError, models::TransactionOutputData, TxoRepository,
};
use xand_ledger::{OpenedTransactionOutput, PublicKey, TransactionOutput};
use xand_models::CorrelationId;
use xand_public_key::public_key_to_address;

#[cfg(test)]
pub mod serialization_tests;

#[derive(Clone, Default, Debug)]
pub struct TxoCache(Arc<RwLock<TxoCacheInner>>);

impl PartialEq for TxoCache {
    fn eq(&self, other: &Self) -> bool {
        let inner = (*self.0).read().unwrap();
        let other_inner = (*other.0).read().unwrap();
        inner.eq(&other_inner)
    }
}

impl TxoCache {
    fn get_spendable_txo_candidates(
        &self,
        owner: PublicKey,
    ) -> Result<Vec<OpenedTransactionOutput>, FinancialClientError> {
        let cache = self.0.read().expect(LOCK_ERROR_TEXT);
        let records = cache
            .owned_txos_by_pubkey(owner)
            .filter(|record| record.status == TxoSpentStatus::UnSpent);
        Ok(private_outputs(records).cloned().collect())
    }
}

#[derive(Serialize, Deserialize)]
struct SerializableTxoCache {
    txos: Vec<(TxoHash, TransactionOutputRecord)>,
    txos_by_correlation_id: Vec<(CorrelationId, Vec<TxoHash>)>,
    owned_txos: Vec<(Address, Vec<TxoHash>)>,
}

impl From<&TxoCache> for SerializableTxoCache {
    fn from(cache: &TxoCache) -> Self {
        let inner = (*cache.0).read().expect(
            "If this is poisoned, the cache is useless \
                           and the process probably has already panicked",
        );
        SerializableTxoCache {
            txos: inner.txos.clone().into_iter().collect(),
            txos_by_correlation_id: inner.txos_by_correlation_id.clone().into_iter().collect(),
            owned_txos: inner.owned_txos.clone().into_iter().collect(),
        }
    }
}

impl From<SerializableTxoCache> for TxoCache {
    fn from(serializable: SerializableTxoCache) -> Self {
        let inner = TxoCacheInner {
            txos: serializable.txos.clone().into_iter().collect(),
            txos_by_correlation_id: serializable
                .txos_by_correlation_id
                .clone()
                .into_iter()
                .collect(),
            owned_txos: serializable.owned_txos.into_iter().collect(),
        };
        TxoCache(Arc::new(RwLock::new(inner)))
    }
}

impl Serialize for TxoCache {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        let serializable: SerializableTxoCache = self.into();
        serializable.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for TxoCache {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        let deserialized: SerializableTxoCache = Deserialize::deserialize(deserializer)?;
        Ok(deserialized.into())
    }
}

#[derive(Default, Debug, Eq, PartialEq)]
pub struct TxoCacheInner {
    txos: HashMap<TxoHash, TransactionOutputRecord>,
    txos_by_correlation_id: HashMap<CorrelationId, Vec<TxoHash>>,
    owned_txos: HashMap<Address, Vec<TxoHash>>,
}

impl TxoCacheInner {
    fn owned_txos_by_pubkey(
        &self,
        owner: PublicKey,
    ) -> impl Iterator<Item = &TransactionOutputRecord> {
        self.owned_txos(&public_key_to_address(&owner))
    }

    fn owned_txos(&self, owner: &Address) -> impl Iterator<Item = &TransactionOutputRecord> {
        self.owned_txos
            .get(owner)
            .into_iter()
            .flatten()
            .filter_map(move |hash| self.txos.get(hash))
    }
}

fn private_outputs<'a, I>(records: I) -> impl Iterator<Item = &'a OpenedTransactionOutput>
where
    I: IntoIterator<Item = &'a TransactionOutputRecord>,
{
    records.into_iter().filter_map(|record| match &record.txo {
        TransactionOutputData::Private(txo, ..) => Some(txo),
        TransactionOutputData::Public(..) => None,
    })
}

// Petri using in-memory cache presents the biggest scalability bottleneck
// and deserves more immediate optimization than get_decoys() -
// https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6594
#[async_trait]
impl TxoRepository for TxoCache {
    async fn get_spendable_txos(
        &self,
        owner: PublicKey,
        amount: u64,
    ) -> Result<Vec<OpenedTransactionOutput>, FinancialClientError> {
        let spendable_txos: Vec<OpenedTransactionOutput> =
            self.get_spendable_txo_candidates(owner)?;
        Ok(random_improve(
            amount,
            spendable_txos.iter().collect(),
            MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION,
        )?
        .into_iter()
        .cloned()
        .collect())
    }

    async fn get_all_spendable_txos(
        &self,
        owner: PublicKey,
        until: chrono::DateTime<chrono::Utc>,
    ) -> Result<Vec<OpenedTransactionOutput>, FinancialClientError> {
        let cache = self.0.read().expect(LOCK_ERROR_TEXT);
        let records = cache
            .owned_txos_by_pubkey(owner)
            .filter(|record| record.status == TxoSpentStatus::UnSpent && record.timestamp <= until);
        Ok(private_outputs(records).cloned().collect())
    }

    async fn get_decoys(
        &self,
        count: usize,
        timestamp: Option<DateTime<Utc>>,
    ) -> Result<Vec<TransactionOutput>, FinancialClientError> {
        let cache = self.0.read().expect(LOCK_ERROR_TEXT);

        // Get all spendable txos
        let mut decoys: Vec<&TransactionOutputRecord> = cache
            .txos
            .values()
            .filter(|txo| txo.status != TxoSpentStatus::Pending)
            .filter(|txo| {
                if let Some(ts) = timestamp {
                    txo.timestamp <= ts
                } else {
                    true
                }
            })
            .collect();
        if decoys.len() < count {
            return Err(FinancialClientError::RequestedTooManyDecoys);
        }
        // Randomize decoys
        decoys.shuffle(&mut thread_rng());
        let shuffled_decoys = decoys
            .into_iter()
            .take(count)
            .cloned() // see ADO item re: memory usage improvements https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6594
            .map(TransactionOutput::from)
            .collect();

        Ok(shuffled_decoys)
    }
}

impl TxoStore for TxoCache {
    fn get_txo(&self, id: &[u8; 32]) -> Option<TransactionOutputRecord> {
        let inner = self.0.read().expect(LOCK_ERROR_TEXT);
        inner.txos.get(id).cloned()
    }

    fn insert(&mut self, txo: TransactionOutputRecord) {
        let mut inner = self
            .0
            .write()
            .expect("Unable to acquire write lock; read-write lock is poisoned");
        let txo_id = txo.to_hash();

        // ensure primary record is in place
        inner.txos.insert(txo_id, txo.clone());

        // associate with a pending correlation id if applicable
        if let Some(correlation_id) = txo.correlation_id {
            inner
                .txos_by_correlation_id
                .entry(correlation_id)
                .or_default()
                .push(txo_id);
        }

        // associate with known addresses
        if let TransactionOutputData::Private(_, key) = &txo.txo {
            inner
                .owned_txos
                .entry(public_key_to_address(key))
                .or_default()
                .push(txo_id);
        }
    }

    fn get_txos_by_correlation_id(
        &self,
        correlation_id: &CorrelationId,
    ) -> Vec<TransactionOutputRecord> {
        let inner = self.0.read().expect(LOCK_ERROR_TEXT);
        inner
            .txos_by_correlation_id
            .get(correlation_id)
            .map(|txos| {
                txos.iter()
                    .filter_map(|id| inner.txos.get(id).cloned())
                    .collect()
            })
            .unwrap_or_default()
    }

    fn update_txo_status_by_correlation_id(
        &mut self,
        correlation_id: &CorrelationId,
        new_status: TxoSpentStatus,
    ) {
        let mut inner = self
            .0
            .write()
            .expect("Unable to acquire write lock; read-write lock is poisoned");
        let inner = &mut *inner;

        // Remove correlation id reference if the status is being updated to anything other than
        // pending
        if new_status != TxoSpentStatus::Pending {
            if let Some(records) = inner.txos_by_correlation_id.remove(correlation_id) {
                for txo_id in records.iter() {
                    let to_update = inner.txos.get_mut(txo_id);
                    if let Some(mut txo_record) = to_update {
                        txo_record.status = new_status;
                    }
                }
            } else {
                warn!(LoggingEvent::NoTxoFoundForCorrelationId(
                    correlation_id.clone()
                ));
            }
        }
    }

    fn update_txo_status_by_hash(&mut self, txo_hash: TxoHash, status: TxoSpentStatus) {
        let mut inner = self.0.write().expect(LOCK_ERROR_TEXT);
        if let Some(txo) = inner.txos.get_mut(&txo_hash) {
            txo.status = status;
        } else {
            warn!(LoggingEvent::TxoNotFound(txo_hash));
        }
    }

    fn remove_txos_by_correlation_id(&mut self, correlation_id: &CorrelationId) {
        let mut inner = self
            .0
            .write()
            .expect("Unable to acquire write lock; read-write lock is poisoned");
        let inner = &mut *inner;
        if let Some(records) = inner.txos_by_correlation_id.remove(correlation_id) {
            for txo_id in records.iter() {
                inner.txos.remove(txo_id);
            }
        } else {
            warn!(LoggingEvent::NoTxoFoundForCorrelationId(
                correlation_id.clone()
            ));
        }
        // this does not clean up the by_address index but should be harmless since the underlying
        // txo is removed and won't cause any errors. Also cancellations should be rare events.
    }

    fn balance(&self, address: &Address) -> Result<u64, TxoStoreError> {
        let inner = self.0.read().expect(LOCK_ERROR_TEXT);
        if !inner.owned_txos.contains_key(address) {
            return Err(TxoStoreError::BalanceNotFound);
        }
        let records = inner
            .owned_txos(address)
            .filter(|txo| txo.status == TxoSpentStatus::UnSpent);
        let balance = private_outputs(records)
            .map(|output| output.value)
            .try_fold(0u64, |acc, amount| {
                acc.checked_add(amount)
                    .ok_or(TxoStoreError::BalanceOverflow)
            });
        balance
    }
}

#[cfg(test)]
pub(crate) mod tests {
    use crate::confidentiality_store::{TransactionOutputRecord, TxoSpentStatus, TxoStore};
    use crate::errors::LOCK_ERROR_TEXT;
    use crate::test_helpers::arbitrary_address;
    use crate::txo_cache::TxoCache;
    use chrono::{DateTime, NaiveDateTime, TimeZone, Utc, MIN_DATETIME};
    use curve25519_dalek::scalar::Scalar;
    use futures::executor::block_on;
    use proptest::test_runner::Config;
    use proptest::{
        collection::vec, prelude::prop::test_runner::TestRng, prelude::*, test_runner::RngAlgorithm,
    };
    use rand::rngs::OsRng;
    use rand::{CryptoRng, Error};
    use std::{
        cmp::min,
        collections::HashMap,
        time::{Duration, Instant},
    };
    use xand_address::Address;
    use xand_financial_client::errors::FinancialClientError;
    use xand_financial_client::test_helpers::NetworkParticipant;
    use xand_financial_client::{models::TransactionOutputData, TxoRepository};
    use xand_ledger::{OneTimeKey, OpenedTransactionOutput, TransactionOutput};
    use xand_models::CorrelationId;
    use xand_public_key::address_to_public_key;

    #[test]
    fn can_fetch_txos_by_correlation_id() {
        let mut csprng: OsRng = OsRng::default();
        let id = CorrelationId::gen_random();
        let record = TransactionOutputRecord {
            txo: TransactionOutputData::Public(TransactionOutput::random(&mut csprng)),
            status: TxoSpentStatus::Pending,
            correlation_id: Some(id.clone()),
            timestamp: MIN_DATETIME,
        };

        let mut sut = TxoCache::default();

        sut.insert(record.clone());
        let actual = sut.get_txos_by_correlation_id(&id);
        assert_eq!(actual, vec![record]);
    }

    #[test]
    fn fetching_by_a_correlation_id_with_no_related_txos_returns_nothing() {
        let mut csprng: OsRng = OsRng::default();
        let id = CorrelationId::gen_random();
        let record = TransactionOutputRecord {
            txo: TransactionOutputData::Public(TransactionOutput::random(&mut csprng)),
            status: TxoSpentStatus::Pending,
            correlation_id: Some(id),
            timestamp: MIN_DATETIME,
        };

        let mut sut = TxoCache::default();

        sut.insert(record);

        let other_correlation_id = CorrelationId::gen_random();
        let actual = sut.get_txos_by_correlation_id(&other_correlation_id);
        assert_eq!(actual, vec![]);
    }

    #[test]
    fn updating_by_correlation_id_with_no_related_txos_does_nothing() {
        let mut csprng: OsRng = OsRng::default();
        let id = CorrelationId::gen_random();
        let other_correlation_id = CorrelationId::gen_random();
        let expected_status = TxoSpentStatus::Pending;
        let record = TransactionOutputRecord {
            txo: TransactionOutputData::Public(TransactionOutput::random(&mut csprng)),
            status: expected_status,
            correlation_id: Some(id.clone()),
            timestamp: MIN_DATETIME,
        };

        let mut sut = TxoCache::default();

        sut.insert(record);
        sut.update_txo_status_by_correlation_id(&other_correlation_id, TxoSpentStatus::UnSpent);

        let actual = sut.get_txos_by_correlation_id(&id);
        let actual_status = actual.get(0).expect("There should be 1 txn").status;
        assert_eq!(actual_status, expected_status)
    }

    #[test]
    fn updating_by_correlation_id_with_related_txos_updates_status() {
        let mut csprng: OsRng = OsRng::default();
        let correlation_id = CorrelationId::gen_random();
        let initial_status = TxoSpentStatus::Pending;
        let updated_status = TxoSpentStatus::UnSpent;
        let record = TransactionOutputRecord {
            txo: TransactionOutputData::Public(TransactionOutput::random(&mut csprng)),
            status: initial_status,
            correlation_id: Some(correlation_id.clone()),
            timestamp: MIN_DATETIME,
        };

        let mut sut = TxoCache::default();

        sut.insert(record.clone());
        sut.update_txo_status_by_correlation_id(&correlation_id, updated_status);

        let actual = sut.get_txo(&record.to_hash());
        let actual_status = actual.expect("There should be 1 txo").status;
        assert_eq!(actual_status, updated_status)
    }

    #[test]
    fn removing_by_correlation_id_with_no_related_txos_does_nothing() {
        let mut csprng: OsRng = OsRng::default();
        let id = CorrelationId::gen_random();
        let other_correlation_id = CorrelationId::gen_random();
        let record = TransactionOutputRecord {
            txo: TransactionOutputData::Public(TransactionOutput::random(&mut csprng)),
            status: TxoSpentStatus::Pending,
            correlation_id: Some(id.clone()),
            timestamp: MIN_DATETIME,
        };

        let mut sut = TxoCache::default();

        sut.insert(record);
        sut.remove_txos_by_correlation_id(&other_correlation_id);

        let actual = sut.get_txos_by_correlation_id(&id);
        assert_eq!(actual.len(), 1)
    }

    #[test]
    fn removing_by_correlation_id_with_related_txos_removes_from_cache() {
        let mut csprng: OsRng = OsRng::default();
        let id = CorrelationId::gen_random();
        let record = TransactionOutputRecord {
            txo: TransactionOutputData::Public(TransactionOutput::random(&mut csprng)),
            status: TxoSpentStatus::Pending,
            correlation_id: Some(id.clone()),
            timestamp: MIN_DATETIME,
        };

        let mut sut = TxoCache::default();

        sut.insert(record);
        sut.remove_txos_by_correlation_id(&id);

        let actual = sut.get_txos_by_correlation_id(&id);
        assert!(actual.is_empty())
    }

    proptest! {
        #![proptest_config(Config::with_cases(20))]
        #[test]
        fn get_spendable_txos_returns_unspent_txos_owned_by_caller(
            records in vec(arbitrary_txo_record(), 1..100usize),
            txn_amt in 1..5u64,
            txo_value in 6..100u64,
            bytes: [u8; 32]
        ) {
            // Given
            let mut csprng: FakeCsprng = FakeCsprng::from_seed(bytes);

            let alice = NetworkParticipant::random(&mut csprng);
            let alice_opened_txo = OpenedTransactionOutput {
                transaction_output: TransactionOutput::random(&mut csprng),
                blinding_factor: Default::default(),
                value: txo_value,
            };
            let alice_txn_amount = txn_amt;

            let alice_record = TransactionOutputRecord {
                // Private because we are testing from the perspective of alice
                txo: TransactionOutputData::Private(alice_opened_txo.clone(), address_to_public_key(&alice.address()).unwrap()),
                status: TxoSpentStatus::UnSpent,
                correlation_id: Some(CorrelationId::gen_random()),
                timestamp: MIN_DATETIME
            };

            // feed txos into the cache
            let mut cache = TxoCache::default();
            cache.insert(alice_record); // we must have at least one verified record for the caller
            for record in records {
                cache.insert(record);
            }

            // check that all owned txos indexed by alice's address in the cache also embed alice's address
            let inner_cache = cache
                .0
                .read()
                .expect(LOCK_ERROR_TEXT);
            debug_assert!(inner_cache.owned_txos[&alice.address()]
                .iter()
                .all(|&txo_hash| {
                    match &inner_cache.txos[&txo_hash].txo {
                        TransactionOutputData::Private(_, address) => *address == address_to_public_key(&alice.address()).unwrap(),
                        _ => false,
                    }
                }));

            // When
            let alice_set =
                block_on(cache.get_spendable_txos(alice.public_key(), alice_txn_amount)).unwrap();

            // Then
            assert_eq!(alice_set, vec![alice_opened_txo]);
        }
    }
    proptest! {
        #![proptest_config(Config::with_cases(20))]
        #[test]
        fn get_spendable_txos_fails_when_insufficient_unspent_owned_txos_belonging_to_calling_member(
            ( // Given
                spendable_txo_amount,
                alice_owned_spendable_txos,
                bob_owned_unspendable_txos,
                bob_owned_spendable_txos,
                arbitrary_public_txos,
                bob_network_participant
            ) in (
                10..100usize,
                0..200usize,
                arbitrary_network_participant(), // Alice
                arbitrary_network_participant(), // Bob
            )
            .prop_filter("Alice and Bob addresses cannot be the same in this test", |(_, _, alice, bob)|
                alice.address().to_string() != bob.address().to_string()
            )
            .prop_flat_map(|(txn_amt, public_amt, alice, bob)|
                (
                    Just(txn_amt),
                    // Each txo has a value of `1`
                    vec(arbitrary_owned_txo_record_spendable_value_of_1(alice.address()), txn_amt), // Alice's spendable txos
                    vec(arbitrary_owned_txo_record_unspendable_value_of_1(bob.address()), txn_amt), // Bob's unspendable txos
                    vec(arbitrary_owned_txo_record_spendable_value_of_1(bob.address()), txn_amt-1), // Bob's spendable txos
                    vec(arbitrary_public_txo_record(), public_amt), // arbitrary public (unopened) records
                    Just(bob)
                )
            ))
        {
            debug_assert!(bob_owned_spendable_txos.len() < spendable_txo_amount, "Bob's spendable txo balance too high for test");
            // feed txos into the cache
            let mut cache = TxoCache::default();
            for alice_txo in alice_owned_spendable_txos {
                cache.insert(alice_txo)
            }
            for bob_unspendable_txo in bob_owned_unspendable_txos {
                cache.insert(bob_unspendable_txo)
            }
            for bob_spendable_txo in bob_owned_spendable_txos {
                cache.insert(bob_spendable_txo)
            }
            for unopened_txo in arbitrary_public_txos {
                cache.insert(unopened_txo)
            }

            // When
            let result = block_on(cache.get_spendable_txos(bob_network_participant.public_key(), spendable_txo_amount as u64));

            // Then
            assert!(matches!(
                result,
                Err(FinancialClientError::InsufficientClaims)
            ))
        }
    }

    #[test]
    fn get_spendable_txos_returns_insufficient_claims_error_when_owners_txos_are_all_pending() {
        // Given
        let mut csprng: OsRng = OsRng::default();

        let alice = NetworkParticipant::random(&mut csprng);
        let alice_opened_txo = OpenedTransactionOutput {
            transaction_output: TransactionOutput::random(&mut csprng),
            blinding_factor: Default::default(),
            value: 42u64,
        };
        let alice_txn_amount = 1u64;

        let alice_record = TransactionOutputRecord {
            // Private because we are testing from the perspective of alice
            txo: TransactionOutputData::Private(
                alice_opened_txo,
                address_to_public_key(&alice.address()).unwrap(),
            ),
            status: TxoSpentStatus::Pending,
            correlation_id: Some(CorrelationId::gen_random()),
            timestamp: MIN_DATETIME,
        };

        // feed txos into the cache
        let mut cache = TxoCache::default();
        cache.insert(alice_record);

        // check that all owned txos indexed by alice's address in the cache also embed alice's address
        let inner_cache = cache.0.read().expect(LOCK_ERROR_TEXT);
        debug_assert!(inner_cache.owned_txos[&alice.address()]
            .iter()
            .all(|&txo_hash| {
                match &inner_cache.txos[&txo_hash].txo {
                    TransactionOutputData::Private(_, key) => {
                        *key == address_to_public_key(&alice.address()).unwrap()
                    }
                    _ => false,
                }
            }));

        // When
        let result = block_on(cache.get_spendable_txos(alice.public_key(), alice_txn_amount));

        // Then
        assert!(matches!(
            result,
            core::result::Result::Err(FinancialClientError::InsufficientClaims)
        ))
    }

    #[test]
    fn get_spendable_txos_returns_only_the_set_that_belongs_to_caller_when_other_decipherable_txos_present(
    ) {
        // Given
        let mut csprng: OsRng = OsRng::default();
        let alice = NetworkParticipant::random(&mut csprng);
        let bob = NetworkParticipant::random(&mut csprng);

        let bob_txn_id = CorrelationId::gen_random();
        let bob_opened_txo = OpenedTransactionOutput {
            transaction_output: TransactionOutput::random(&mut csprng),
            blinding_factor: Default::default(),
            value: 42u64,
        };

        let alice_txn_amount = 1u64;

        // isolate the test case so that we can be sure to be asserting on the inequality between members
        debug_assert!(
            alice_txn_amount < bob_opened_txo.value,
            "Alice transaction amount too large"
        );

        let bob_record = TransactionOutputRecord {
            txo: TransactionOutputData::Private(
                bob_opened_txo,
                address_to_public_key(&bob.address()).unwrap(),
            ),
            status: TxoSpentStatus::UnSpent,
            correlation_id: Some(bob_txn_id),
            timestamp: MIN_DATETIME,
        };

        // feed txos into the cache
        let mut cache = TxoCache::default();
        cache.insert(bob_record);

        // When
        let result = block_on(cache.get_spendable_txos(alice.public_key(), alice_txn_amount));

        // Then
        assert!(matches!(
            result,
            core::result::Result::Err(FinancialClientError::InsufficientClaims)
        ))
    }

    #[test]
    fn get_spendable_txos_errors_appropriately_when_not_enough_candidate_txo_value_exists() {
        // Given
        let mut csprng: OsRng = OsRng::default();

        let alice = NetworkParticipant::random(&mut csprng);
        let alice_opened_txo = OpenedTransactionOutput {
            transaction_output: TransactionOutput::random(&mut csprng),
            blinding_factor: Default::default(),
            value: 12u64,
        };
        let alice_txn_amount = 42u64;
        debug_assert!(
            alice_txn_amount > alice_opened_txo.value,
            "Transaction amount too small"
        );

        let alice_record = TransactionOutputRecord {
            // Private because we are testing from the perspective of alice
            txo: TransactionOutputData::Private(
                alice_opened_txo,
                address_to_public_key(&alice.address()).unwrap(),
            ),
            status: TxoSpentStatus::UnSpent,
            correlation_id: Some(CorrelationId::gen_random()),
            timestamp: MIN_DATETIME,
        };

        // feed txos into the cache
        let mut cache = TxoCache::default();
        cache.insert(alice_record);

        // When
        let result = block_on(cache.get_spendable_txos(alice.public_key(), alice_txn_amount));

        // Then
        assert!(matches!(
            result,
            core::result::Result::Err(FinancialClientError::InsufficientClaims)
        ));
    }

    // This impl of FakeCsprng is borrowed from xand_ledger - there is an opportunity to
    // factor it out into a separate crate
    // https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6595

    /// The Dalek types require the `CryptoRng` trait for random number generators. While this may
    /// be important for cryptographic safety in some cases, this prevents deterministic RNG
    /// needed for proptesting. We are using a new-type which impl's the trait
    /// to trick Dalek that the RNG is cryptographically secure.
    #[derive(Debug)]
    pub struct FakeCsprng(TestRng);

    impl CryptoRng for FakeCsprng {}

    impl RngCore for FakeCsprng {
        fn next_u32(&mut self) -> u32 {
            self.0.next_u32()
        }

        fn next_u64(&mut self) -> u64 {
            self.0.next_u64()
        }

        fn fill_bytes(&mut self, dest: &mut [u8]) {
            self.0.fill_bytes(dest)
        }

        fn try_fill_bytes(&mut self, dest: &mut [u8]) -> Result<(), Error> {
            self.0.try_fill_bytes(dest)
        }
    }

    impl FakeCsprng {
        pub fn from_seed(seed: [u8; 32]) -> Self {
            FakeCsprng(TestRng::from_seed(RngAlgorithm::ChaCha, &seed))
        }
    }
    prop_compose! {
        fn arbitrary_onetime_key()(bytes: [u8; 32])-> OneTimeKey {
            let mut csprng: FakeCsprng = FakeCsprng::from_seed(bytes);
            OneTimeKey::random(&mut csprng)
        }
    }

    prop_compose! {
        fn arbitrary_transaction_output()(bytes: [u8; 32]) -> TransactionOutput {
            let mut csprng: FakeCsprng = FakeCsprng::from_seed(bytes);
            TransactionOutput::random(&mut csprng)
        }
    }

    prop_compose! {
        fn arbitrary_scalar()(n: u128) -> Scalar {
            Scalar::from(n)
        }
    }

    prop_compose! {
        fn arbitrary_opened_txo()(
            txo in arbitrary_transaction_output(),
            scalar in arbitrary_scalar(),
            value: u64,
        ) -> OpenedTransactionOutput {
            OpenedTransactionOutput {
                transaction_output: txo,
                blinding_factor: scalar,
                value,
            }
        }
    }

    prop_compose! {
        fn arbitrary_opened_txo_value_of_1()(
            txo in arbitrary_transaction_output(),
            scalar in arbitrary_scalar(),
        ) -> OpenedTransactionOutput {
            OpenedTransactionOutput {
                transaction_output: txo,
                blinding_factor: scalar,
                value: 1,
            }
        }
    }

    prop_compose! {
        fn arbitrary_network_participant()(bytes: [u8; 32]) -> NetworkParticipant {
            let mut csprng: FakeCsprng = FakeCsprng::from_seed(bytes);
            NetworkParticipant::random(&mut csprng)
        }
    }

    prop_compose! {
        fn arbitrary_transaction_output_data_private()(
            opened_txo in arbitrary_opened_txo(),
            address in arbitrary_address(),
        ) -> TransactionOutputData {
            TransactionOutputData::Private(opened_txo, address_to_public_key(&address).unwrap())
        }
    }

    prop_compose! {
        fn arbitrary_transaction_output_data_private_value_of_1(address: Address)(
            opened_txo in arbitrary_opened_txo_value_of_1(),
        ) -> TransactionOutputData {
            TransactionOutputData::Private(opened_txo, address_to_public_key(&address).unwrap())
        }
    }

    prop_compose! {
        fn arbitrary_transaction_output_data_public()(
            txo in arbitrary_transaction_output(),
        ) -> TransactionOutputData {
            TransactionOutputData::Public(txo)
        }
    }

    fn arbitrary_transaction_output_data() -> impl Strategy<Value = TransactionOutputData> {
        prop_oneof![
            arbitrary_transaction_output_data_public(),
            arbitrary_transaction_output_data_private(),
        ]
    }

    fn arbitrary_txo_spent_status() -> impl Strategy<Value = TxoSpentStatus> {
        prop_oneof![Just(TxoSpentStatus::UnSpent), Just(TxoSpentStatus::Spent),]
    }

    fn arbitrary_txo_spent_status_output_unspendable() -> impl Strategy<Value = TxoSpentStatus> {
        prop_oneof![Just(TxoSpentStatus::Pending), Just(TxoSpentStatus::Spent),]
    }

    prop_compose! {
        pub fn arbitrary_correlation_id()(bytes: [u8; 16]) -> CorrelationId {
            CorrelationId::from(bytes)
        }
    }

    fn timestamp_from_seconds(seconds: u32) -> DateTime<Utc> {
        Utc.timestamp(seconds.into(), 0)
    }

    prop_compose! {
        pub fn arbitrary_txo_record()(
            status in arbitrary_txo_spent_status(),
            correlation_id in proptest::option::of(arbitrary_correlation_id()),
            txo_data in arbitrary_transaction_output_data(),
        ) -> TransactionOutputRecord {
            TransactionOutputRecord {
                txo: txo_data,
                status,
                correlation_id,
                timestamp: MIN_DATETIME
            }
        }
    }

    prop_compose! {
        fn arbitrary_txo_record_with_up_to_timestamp(cutoff: u32)(
            status in arbitrary_txo_spent_status(),
            correlation_id in proptest::option::of(arbitrary_correlation_id()),
            txo_data in arbitrary_transaction_output_data(),
            seconds in 0..=cutoff
        ) -> TransactionOutputRecord {
            TransactionOutputRecord {
                txo: txo_data,
                status,
                correlation_id,
                timestamp: timestamp_from_seconds(seconds)
            }
        }
    }

    prop_compose! {
        fn arbitrary_txo_record_with_after_timestamp(cutoff: u32)(
            status in arbitrary_txo_spent_status(),
            correlation_id in proptest::option::of(arbitrary_correlation_id()),
            txo_data in arbitrary_transaction_output_data(),
            seconds in (cutoff + 1)..,
        ) -> TransactionOutputRecord {
            TransactionOutputRecord {
                txo: txo_data,
                status,
                correlation_id,
                timestamp: timestamp_from_seconds(seconds)
            }
        }
    }

    prop_compose! {
        fn arbitrary_public_txo_record()(
            status in arbitrary_txo_spent_status(),
            correlation_id in proptest::option::of(arbitrary_correlation_id()),
            txo_data in arbitrary_transaction_output_data_public(),
        ) -> TransactionOutputRecord {
            TransactionOutputRecord {
                txo: txo_data,
                status,
                correlation_id,
                timestamp: MIN_DATETIME
            }
        }
    }

    prop_compose! {
        fn arbitrary_owned_txo_record()(
            status in arbitrary_txo_spent_status(),
            correlation_id in proptest::option::of(arbitrary_correlation_id()),
            txo_data in arbitrary_transaction_output_data_private(), // Owned TxO must be Private
        ) -> TransactionOutputRecord {
            TransactionOutputRecord {
                txo: txo_data,
                status,
                correlation_id,
                timestamp: MIN_DATETIME
            }
        }
    }

    prop_compose! {
        fn arbitrary_owned_txo_record_unspendable_value_of_1(address: Address)(
            status in arbitrary_txo_spent_status_output_unspendable(),
            correlation_id in proptest::option::of(arbitrary_correlation_id()),
            txo_data in arbitrary_transaction_output_data_private_value_of_1(address), // Owned TxO must be Private
        ) -> TransactionOutputRecord {
            TransactionOutputRecord {
                txo: txo_data,
                status,
                correlation_id,
                timestamp: MIN_DATETIME
            }
        }
    }

    prop_compose! {
        fn arbitrary_owned_txo_record_spendable_value_of_1(address: Address)(
            correlation_id in proptest::option::of(arbitrary_correlation_id()),
            txo_data in arbitrary_transaction_output_data_private_value_of_1(address), // Owned TxO must be Private
        ) -> TransactionOutputRecord {
            TransactionOutputRecord {
                txo: txo_data,
                status: TxoSpentStatus::UnSpent,
                correlation_id,
                timestamp: MIN_DATETIME
            }
        }
    }

    prop_compose! {
        fn arbitrary_valid_and_invalid_decoys_with_timestamp()(
            cutoff in 0..u32::MAX
        )(
            cutoff in Just(cutoff),
            (valid_txo_records, invalid_txo_records) in (
                0..50usize, // count up to timestamp
                0..50usize // count after timestamp
            )
                .prop_flat_map(move |(n, m)| (
                    vec(arbitrary_txo_record_with_up_to_timestamp(cutoff), n),
                    vec(arbitrary_txo_record_with_after_timestamp(cutoff), m))
                )) -> (u32, Vec<TransactionOutputRecord>, Vec<TransactionOutputRecord>) {
                (cutoff, valid_txo_records, invalid_txo_records)
                }
    }

    proptest! {
        #[test]
        fn get_decoys_from_txo_cache(
            count in 0..100usize,
            (owned_txo_records, txo_records) in (
                1..100usize, // We must have at least one owned TxO
                0..100usize // We can have any number of TxOs with arbitrary ownership
            )
                .prop_flat_map(|(n, m)| (
                    vec(arbitrary_owned_txo_record(), n),
                    vec(arbitrary_txo_record(), m))
                ),
        ) {
            // Given
            let max_valid_decoy_count = owned_txo_records.len(); // Can't have a larger input set than owned txos
            let expected_decoy_count = min(max_valid_decoy_count, count);

            let mut txo_cache = TxoCache::default();
            for owned_txo in owned_txo_records.iter() {
                txo_cache.insert(owned_txo.clone()); // add owned txos to the cache
            }
            for r in txo_records.iter() {
                txo_cache.insert(r.clone()); // add txos with arbitrary ownership to the cache
            }

            // When
            let result = block_on(txo_cache.get_decoys(expected_decoy_count, None)).unwrap();

            // Decoy count matches request
            assert_eq!(result.len(), expected_decoy_count);

            // Decoys have no duplicates
            let mut deduped_decoys = result.clone();
            deduped_decoys.sort_by_key(|txo| txo.to_hash());
            deduped_decoys.dedup_by_key(|txo| txo.to_hash());
            assert_eq!(deduped_decoys.len(), result.len());
        }
    }

    fn is_too_new(
        invalid_txo_records: &[TransactionOutputRecord],
        txo: &TransactionOutput,
    ) -> bool {
        invalid_txo_records
            .iter()
            .map(|record| &record.txo)
            .any(|data| match data {
                TransactionOutputData::Private(openned, _) => &openned.transaction_output == txo,

                TransactionOutputData::Public(r_txo) => r_txo == txo,
            })
    }

    proptest! {
        #[test]
        fn get_decoys_from_txo_cache_with_timestamp(
            (cutoff, valid_txo_records, invalid_txo_records) in arbitrary_valid_and_invalid_decoys_with_timestamp()
        ) {
            // Given
            let cutoff_ts = timestamp_from_seconds(cutoff);
            let valid_count = valid_txo_records.len();

            let mut txo_cache = TxoCache::default();
            for owned_txo in invalid_txo_records.iter() {
                txo_cache.insert(owned_txo.clone()); // add owned txos to the cache
            }
            for r in valid_txo_records.iter() {
                txo_cache.insert(r.clone()); // add txos with arbitrary ownership to the cache
            }

            // When
            let decoys = block_on(txo_cache.get_decoys(valid_count, Some(cutoff_ts))).unwrap();

            let a_txo_is_too_new = decoys.iter().any(|txo| is_too_new(&invalid_txo_records, txo));

            assert!(!a_txo_is_too_new)
        }
    }

    proptest! {
        #[test]
        fn get_too_many_decoys_from_txo_cache_with_timestamp_errors(
            extra in 1..10usize,
            (cutoff, valid_txo_records, invalid_txo_records) in arbitrary_valid_and_invalid_decoys_with_timestamp()
        ) {
            // Given
            let cutoff_ts = timestamp_from_seconds(cutoff);
            let valid_count = valid_txo_records.len();

            let mut txo_cache = TxoCache::default();
            for owned_txo in invalid_txo_records.iter() {
                txo_cache.insert(owned_txo.clone()); // add owned txos to the cache
            }
            for r in valid_txo_records.iter() {
                txo_cache.insert(r.clone()); // add txos with arbitrary ownership to the cache
            }

            // When
            let result = block_on(txo_cache.get_decoys(valid_count + extra, Some(cutoff_ts)));

            assert!(matches!(result, Err(FinancialClientError::RequestedTooManyDecoys)));
        }
    }

    const DECOY_COUNT: usize = 100;
    proptest! {
        #[test]
        fn get_decoys_when_fewer_total_txos_than_request_count_fails_with_error(
            count in (DECOY_COUNT + 1)..200usize, // Pretty arbitrary, it just needs to be greater than total potential decoy txos
            owned_txo_record in arbitrary_owned_txo_record(), // We must have at least one owned TxO, and we only need one for this test
            txo_records in vec(arbitrary_txo_record(), 0..DECOY_COUNT)  // This is the set of total potential decoy txos we care about
        ) {
            // Test precondition
            debug_assert!(count > txo_records.len() + 1);

            // Given
            let mut txo_cache = TxoCache::default();
            txo_cache.insert(owned_txo_record); // add owned txo to the cache
            for r in txo_records.iter() {
                txo_cache.insert(r.clone()); // add txos with arbitrary ownership to the cache
            }

            // When
            let result = block_on(txo_cache.get_decoys(count, None));

            //Then
            // Returned decoy count matches total eligible decoys
            assert!(matches!(result, Err(FinancialClientError::RequestedTooManyDecoys)));
        }
    }

    // If this test is un-ignored, it should be executed in cargo's release mode
    proptest! {
        #![proptest_config(Config::with_cases(1))]
        #[ignore = "Use for benchmarking, not to be run with other unit tests"]
        #[test]
        fn get_decoys_from_txo_cache_1million_benchmark(
            count in Just(1_000_000usize),
            (_, owned_txo_records, txo_records) in (
                Just(1), // We must have at least one owned TxO
                Just(1_000_000usize) // We must have the same amount of ambiguous txo_records as the count
            )
                .prop_flat_map(|(n, m)| (
                    (0usize..n), // range max is exclusive, aligning with our zero-based indexing
                    vec(arbitrary_owned_txo_record(), n),
                    vec(arbitrary_txo_record(), m))
                ),
        ) {
            // Given
            let max_seconds = Duration::from_secs(3);
            let mut txo_cache = TxoCache::default();
            for owned_txo in owned_txo_records.iter() {
                txo_cache.insert(owned_txo.clone()); // add owned txos to the cache
            }
            for r in txo_records.iter() {
                txo_cache.insert(r.clone()); // add txos with arbitrary ownership to the cache
            }

            // When
            let start_time = Instant::now();
            let _result = block_on(txo_cache.get_decoys(count, None)).unwrap();
            let stop_time = Instant::now();
            let elapsed_time = stop_time-start_time;

            // Then
            assert!(elapsed_time <= max_seconds);
        }
    }

    #[tokio::test]
    async fn get_decoys_doesnt_select_pending_txos() {
        // add one pending txo to the cache
        let mut csprng = OsRng::default();
        let mut txo_cache = TxoCache::default();
        txo_cache.insert(TransactionOutputRecord {
            txo: TransactionOutputData::Public(TransactionOutput::random(&mut csprng)),
            status: TxoSpentStatus::Pending,
            correlation_id: None,
            timestamp: MIN_DATETIME,
        });
        // attempt to fetch 1 decoy
        let decoy_result = txo_cache.get_decoys(1, None).await;
        // verify we weren't able to select any decoys
        assert!(matches!(
            decoy_result,
            Err(FinancialClientError::RequestedTooManyDecoys)
        ));
    }

    #[tokio::test]
    async fn get_decoys_can_select_spent_txos() {
        // insert one spent decoy into the cache
        let mut csprng = OsRng::default();
        let mut txo_cache = TxoCache::default();
        txo_cache.insert(TransactionOutputRecord {
            txo: TransactionOutputData::Public(TransactionOutput::random(&mut csprng)),
            status: TxoSpentStatus::Spent,
            correlation_id: None,
            timestamp: MIN_DATETIME,
        });
        // select one decoy
        let decoys = txo_cache.get_decoys(1, None).await.unwrap();
        // verify we successfully retrieved a single decoy
        assert_eq!(decoys.len(), 1);
    }

    #[tokio::test]
    async fn get_timestamp_for_txo() {
        // insert one spent decoy into the cache
        let mut csprng = OsRng::default();
        let mut txo_cache = TxoCache::default();
        let expected = DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(123, 456), Utc);
        let txo = TransactionOutputRecord {
            txo: TransactionOutputData::Public(TransactionOutput::random(&mut csprng)),
            status: TxoSpentStatus::Spent,
            correlation_id: None,
            timestamp: expected,
        };
        let hash = txo.to_hash();
        txo_cache.insert(txo);
        // get timestamp
        let actual = txo_cache.get_timestamp_for_txo(&hash).unwrap();
        // verify we retrieved matching timestamp
        assert_eq!(actual, expected);
    }

    #[tokio::test]
    async fn txos_older_than_specific_time_can_be_retrieved() {
        let mut rng = OsRng::default();
        let alice = NetworkParticipant::random(&mut rng);
        let mut cache = TxoCache::default();
        let txos = std::iter::repeat_with(|| OpenedTransactionOutput {
            transaction_output: TransactionOutput::random(&mut rng),
            blinding_factor: Default::default(),
            value: 10,
        })
        .take(5)
        .collect::<Vec<_>>();
        let txo_records = txos
            .iter()
            .enumerate()
            .map(|(day, txo)| TransactionOutputRecord {
                txo: TransactionOutputData::Private(txo.clone(), alice.public_key()),
                status: TxoSpentStatus::UnSpent,
                correlation_id: Some(CorrelationId::gen_random()),
                timestamp: Utc.ymd(2021, 10, day as u32 + 1).and_hms(0, 0, 0),
            })
            .collect::<Vec<_>>();
        for txo in txo_records {
            cache.insert(txo);
        }
        let expected_txos = txos
            .iter()
            .take(4)
            .map(|txo| (txo.transaction_output.to_hash(), txo.clone()))
            .collect::<HashMap<_, _>>();
        let actual_txos = cache
            .get_all_spendable_txos(alice.public_key(), Utc.ymd(2021, 10, 4).and_hms(0, 0, 0))
            .await
            .unwrap()
            .into_iter()
            .map(|txo| (txo.transaction_output.to_hash(), txo))
            .collect::<HashMap<_, _>>();
        assert_eq!(actual_txos, expected_txos);
    }
}
