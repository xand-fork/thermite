use super::*;
use chrono::{DateTime, NaiveDateTime, TimeZone, Utc};
use proptest::prelude::*;
use std::str::FromStr;
use xand_models::PendingCreate;

pub fn assert_relevant_data_is_equal(cache_one: &TransactionCache, cache_two: &TransactionCache) {
    transactions_by_correlation_id_match(cache_one, cache_two);
    transactions_by_address_match(cache_one, cache_two);
    transactions_by_id_match(cache_one, cache_two);
    transactions_queue_match(cache_one, cache_two);
    transactions_cap_match(cache_one, cache_two);
    last_processed_block_info_match(cache_one, cache_two);
}

fn transactions_by_correlation_id_match(
    cache_one: &TransactionCache,
    cache_two: &TransactionCache,
) {
    fn inner(cache_one: &TransactionCache, cache_two: &TransactionCache) -> bool {
        cache_one
            .transactions_by_correlation_id
            .clone()
            .into_iter()
            .all(|i| {
                cache_two
                    .transactions_by_correlation_id
                    .clone()
                    .into_iter()
                    .any(|e| i == e)
            })
    }
    assert!(
        inner(cache_one, cache_two) && inner(cache_two, cache_one),
        "one = {:?}, two = {:?}",
        &cache_one.transactions_by_correlation_id,
        &cache_two.transactions_by_correlation_id
    );
}

fn transactions_by_address_match(cache_one: &TransactionCache, cache_two: &TransactionCache) {
    fn inner(cache_one: &TransactionCache, cache_two: &TransactionCache) -> bool {
        cache_one
            .transactions_by_address
            .clone()
            .into_iter()
            .all(|i| {
                cache_two
                    .transactions_by_address
                    .clone()
                    .into_iter()
                    .any(|e| i == e)
            })
    }
    assert!(
        inner(cache_one, cache_two) && inner(cache_two, cache_one),
        "one = {:?}, two = {:?}",
        &cache_one.transactions_by_address,
        &cache_two.transactions_by_address
    );
}

fn transactions_by_id_match(cache_one: &TransactionCache, cache_two: &TransactionCache) {
    fn inner(cache_one: &TransactionCache, cache_two: &TransactionCache) -> bool {
        cache_one.transactions_by_id.clone().into_iter().all(|i| {
            cache_two
                .transactions_by_id
                .clone()
                .into_iter()
                .any(|e| i == e)
        })
    }
    assert!(
        inner(cache_one, cache_two) && inner(cache_two, cache_one),
        "one = {:?}, two = {:?}",
        &cache_one.transactions_by_id,
        &cache_two.transactions_by_id
    );
}

fn transactions_queue_match(cache_one: &TransactionCache, cache_two: &TransactionCache) {
    fn inner(cache_one: &TransactionCache, cache_two: &TransactionCache) -> bool {
        cache_one
            .transaction_queue
            .lock()
            .unwrap()
            .clone()
            .into_iter()
            .all(|i| {
                cache_two
                    .transaction_queue
                    .lock()
                    .unwrap()
                    .clone()
                    .into_iter()
                    .any(|e| i == e)
            })
    }
    assert!(
        inner(cache_one, cache_two) && inner(cache_two, cache_one),
        "one = {:?}, two = {:?}",
        &cache_one.transaction_queue,
        &cache_two.transaction_queue
    );
}

fn transactions_cap_match(cache_one: &TransactionCache, cache_two: &TransactionCache) {
    assert_eq!(cache_one.transaction_limit(), cache_two.transaction_limit());
}

fn last_processed_block_info_match(cache_one: &TransactionCache, cache_two: &TransactionCache) {
    assert_eq!(
        cache_one.last_processed_block_info(),
        cache_two.last_processed_block_info()
    );
}

prop_compose! {
    pub fn arbitrary_set_of_transactions()(count in 0usize..100) -> Vec<Transaction> {
        std::iter::repeat_with(some_transaction).take(count).collect()
    }
}

prop_compose! {
    pub fn arbitrary_timestamp()(timestamp in 0i64..1_500_000_000) -> DateTime<Utc> {
        let naive = NaiveDateTime::from_timestamp(timestamp, 0);
        DateTime::from_utc(naive, Utc)
    }
}

fn example_addr() -> Address {
    Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap()
}

fn from_xand_txn(
    id: TransactionId,
    txn: XandTransaction,
    signer: Address,
    status: TransactionStatus,
) -> Transaction {
    let timestamp = Utc.ymd(2020, 2, 5).and_hms(6, 0, 0);
    Transaction::from_xand_txn(id, txn, signer, status, timestamp)
}

fn some_transaction() -> Transaction {
    from_xand_txn(
        TransactionId::default(),
        XandTransaction::CreatePendingCreate(PendingCreate {
            account: Default::default(),
            amount_in_minor_unit: 42,
            correlation_id: CorrelationId::gen_random(),
            completing_transaction: None,
        }),
        example_addr(),
        TransactionStatus::Pending,
    )
}

fn transactions_correlation_id_pair(tx: &Transaction) -> (CorrelationId, Vec<TransactionId>) {
    let correlation_id = tx.get_correlation_id().unwrap();
    let id = tx.transaction_id.clone();
    (correlation_id.clone(), vec![id])
}

fn transactions_address_pair(tx: &Transaction) -> (Address, Vec<TransactionId>) {
    let address = tx.signer_address.clone();
    let id = tx.transaction_id.clone();
    (address, vec![id])
}

fn transactions_id_pair(tx: &Transaction) -> (TransactionId, Transaction) {
    let id = tx.transaction_id.clone();
    (id, tx.clone())
}

prop_compose! {
    pub fn arbitrary_transaction_cache()(
        transactions in arbitrary_set_of_transactions(),
        transaction_limit: usize,
        number: u32,
        timestamp in arbitrary_timestamp(),
    ) -> TransactionCache {
        let transactions_by_correlation_id = transactions.iter().map(transactions_correlation_id_pair).collect();
        let transactions_by_address = transactions.iter().map(transactions_address_pair).collect();
        let transactions_by_id = transactions.iter().map(transactions_id_pair).collect();
        let transaction_queue = Mutex::new(transactions.iter().map(|tx| tx.transaction_id.clone()).collect());
        let transaction_limit = RwLock::new(transaction_limit);
        TransactionCache {
           transactions_by_correlation_id,
           transactions_by_address,
           transactions_by_id,
           transaction_queue,
           transaction_limit,
           last_processed_block_info: RwLock::new(LastBlockInfo {
                number,
                timestamp,
           }),
           cache_observers: Mutex::new(Vec::new())
        }
    }
}

proptest! {
    #[test]
    fn serialize_deserialize_round_trip(
        expected in arbitrary_transaction_cache()
    ) {
        let serialized = bincode::serialize(&expected).unwrap();
        let deserialized: TransactionCache = bincode::deserialize(&serialized).unwrap();
        assert_relevant_data_is_equal(&expected, &deserialized);
    }
}
