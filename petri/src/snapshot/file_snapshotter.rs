use crate::snapshot::Snapshotter;
use crate::snapshot::{Error as SnapshotError, NUMBER_OF_PERSISTED_SNAPSHOTS};

use async_trait::async_trait;
use crc32fast::hash;
use error::{Error, Result};
pub use file_manager_impl::FileManagerImpl;
pub use file_reader_impl::FileReaderImpl;
pub use file_writer_impl::FileWriterImpl;
use logging_events::LoggingEvent;
pub use scheduler_impl::SchedulerImpl;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::path::{Path, PathBuf};
use std::time::Duration;

pub mod error;

#[cfg(test)]
pub mod tests;

#[derive(Default, Debug)]
pub struct FileSnapshotter<W: FileWriter, R: FileReader, M: FileManager, S: Scheduler> {
    file_writer: W,
    file_reader: R,
    file_manager: M,
    scheduler: S,
}

mod file_manager_impl;
mod file_reader_impl;
mod file_writer_impl;
mod logging_events;
mod scheduler_impl;

#[async_trait]
pub trait FileWriter: Send + Sync + std::fmt::Debug {
    async fn write<P: AsRef<Path> + Send + Sync>(
        &mut self,
        path: P,
        snapshot: Snapshot,
    ) -> Result<()>;

    async fn delete<P: AsRef<Path> + Send + Sync>(&mut self, path: P) -> Result<()>;
}

pub trait FileReader: Sync + Send + std::fmt::Debug {
    fn read<P: AsRef<Path> + Send + Sync>(&self, path: P) -> Result<Snapshot>;
}

pub trait FileManager: Sync + Send + std::fmt::Debug {
    fn new_file_path(&mut self) -> Result<PathBuf>;
    fn track_file(&mut self, path: PathBuf) -> Result<()>;
    fn prune_tracked_files_to_x_and_return_old(&mut self, x: usize) -> Result<Vec<PathBuf>>;
    fn files_youngest_to_oldest(&self) -> Result<Vec<PathBuf>>;
}

#[async_trait]
pub trait Scheduler: Sync + Send + std::fmt::Debug {
    async fn should_snapshot(&self) -> bool;
    async fn snapshot_complete(&mut self) -> Result<()>;
}

impl From<Error> for SnapshotError {
    fn from(fs_error: Error) -> Self {
        SnapshotError::SnapshotError(Box::new(fs_error))
    }
}

#[derive(Debug, Default, PartialEq, Clone, Copy, Serialize, Deserialize)]
pub(crate) struct Checksum(u32);

impl Checksum {
    pub fn new(data: &[u8]) -> Self {
        Self(hash(data))
    }
}

#[derive(Debug, Default, PartialEq, Clone, Serialize, Deserialize)]
pub struct Snapshot {
    data: Vec<u8>,
    checksum: Checksum,
}

impl Snapshot {
    #[cfg(test)]
    pub(crate) fn set_data(&mut self, data: Vec<u8>) {
        self.data = data;
    }

    #[cfg(test)]
    pub(crate) fn set_checksum(&mut self, checksum: Checksum) {
        self.checksum = checksum;
    }

    pub fn from_serializable<T: Serialize>(stuff: &T) -> Result<Self> {
        let data = bincode::serialize(stuff).map_err(|e| Error::Serialization(Box::new(e)))?;
        let checksum = Checksum::new(&data);

        Ok(Self { data, checksum })
    }

    pub fn to_deserializable<'a, T: Deserialize<'a>>(&'a self) -> Result<Option<T>> {
        if !self.is_valid() {
            error!(LoggingEvent::BadSnapshotChecksum);
            return Ok(None);
        }

        let stuff: T =
            bincode::deserialize(&self.data).map_err(|e| Error::Serialization(Box::new(e)))?;
        Ok(Some(stuff))
    }

    fn is_valid(&self) -> bool {
        self.checksum == Checksum::new(&self.data)
    }
}

#[async_trait]
impl<T: std::fmt::Debug + Clone, W, R, M, S> Snapshotter<T> for FileSnapshotter<W, R, M, S>
where
    T: Serialize + Send + Sync + DeserializeOwned,
    W: FileWriter,
    R: FileReader,
    M: FileManager,
    S: Scheduler,
{
    async fn snapshot(&mut self, data: &T) -> crate::snapshot::Result<()> {
        if self.scheduler.should_snapshot().await {
            info!(LoggingEvent::SnapshotBegin);

            let path = self.file_manager.new_file_path()?;
            let snapshot = Snapshot::from_serializable(&data)?;

            info!(LoggingEvent::SavingSnapshotToFile(format!("{:?}", path)));

            self.file_writer.write(path.clone(), snapshot).await?;
            self.file_manager.track_file(path)?;
            let to_be_pruned = self
                .file_manager
                .prune_tracked_files_to_x_and_return_old(NUMBER_OF_PERSISTED_SNAPSHOTS)?;
            for path in to_be_pruned {
                if let Err(error) = self.file_writer.delete(&path).await {
                    error!(LoggingEvent::PruningSnapshotFileError(format!(
                        "Error deleting snapshot file: {:?}; Error: {:?}",
                        path, error
                    )))
                }
            }
            self.scheduler.snapshot_complete().await?;
            info!(LoggingEvent::SnapshotComplete);
        }
        Ok(())
    }

    fn get_snapshot(&self) -> Option<T> {
        let maybe_data = self
            .file_manager
            .files_youngest_to_oldest()
            .map_err(|e| {
                error!(LoggingEvent::ListOfSnapshotFilesError(format!(
                    "Error getting snapshot:{:?}",
                    e
                )))
            })
            .unwrap_or_default()
            .iter()
            .filter_map(|snapshot_path| {
                info!(LoggingEvent::AttemptingToReadSnapshotFile(format!(
                    "{:?}",
                    &snapshot_path
                )));
                self.file_reader
                    .read(snapshot_path)
                    .map_err(log_snapshot_read_error(snapshot_path))
                    .ok()
                    .and_then(try_deserialize_snapshot(snapshot_path))
                    .map(|x| {
                        info!(LoggingEvent::RetrievedSnapshot(format!(
                            "{:?}",
                            &snapshot_path
                        )));
                        x
                    })
            })
            .next();
        maybe_data
    }
}

fn try_deserialize_snapshot<T: DeserializeOwned>(
    snapshot_path: &'_ Path,
) -> impl Fn(Snapshot) -> Option<T> + '_ {
    move |s| {
        Snapshot::to_deserializable::<T>(&s)
            .map_err(log_snapshot_read_error(snapshot_path))
            .ok()
            .flatten()
    }
}

fn log_snapshot_read_error<T: std::fmt::Debug>(snapshot_path: &'_ Path) -> impl Fn(T) -> T + '_ {
    move |e| {
        error!(LoggingEvent::SnapshotReadError(format!(
            "Unable to read snapshot file: {:?} error: {:?}",
            snapshot_path, e
        )));
        e
    }
}

impl<W, R, M, S> FileSnapshotter<W, R, M, S>
where
    W: FileWriter,
    R: FileReader,
    M: FileManager,
    S: Scheduler,
{
    pub fn new(file_writer: W, file_reader: R, file_manager: M, scheduler: S) -> Self {
        Self {
            file_writer,
            file_reader,
            file_manager,
            scheduler,
        }
    }
}

impl FileSnapshotter<FileWriterImpl, FileReaderImpl, FileManagerImpl, SchedulerImpl> {
    pub fn new_standard(snapshot_dir: PathBuf, wait_time: Duration) -> Self {
        Self {
            file_writer: FileWriterImpl {},
            file_reader: FileReaderImpl {},
            file_manager: FileManagerImpl::new(snapshot_dir),
            scheduler: SchedulerImpl::new(wait_time),
        }
    }
}
