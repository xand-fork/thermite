use crate::StdResult;
use thiserror::Error;

pub type Result<T, E = Error> = StdResult<T, E>;

#[derive(Debug, Error)]
#[error(transparent)]
pub enum Error {
    #[error(transparent)]
    SnapshotError(#[from] Box<dyn std::error::Error + Send + Sync>),
}
