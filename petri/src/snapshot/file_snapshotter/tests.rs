#![allow(non_snake_case)]

use std::path::PathBuf;

use test_data::SimpleData;

use crate::snapshot::error::Error as SnapshotError;
use error::TestError;

use super::*;
use crate::snapshot::file_snapshotter::tests::snapshotter_builder::{
    test_data_snapshot, FileSnapshotterBuilder,
};
use crate::snapshot::file_snapshotter::tests::test_data::UnserializableData;
use crate::snapshot::file_snapshotter::{error::Error, Checksum, Snapshot};
use std::sync::atomic::Ordering::Relaxed;

use std::io::{Error as IoError, ErrorKind};

mod error;
pub mod test_data;

mod snapshotter_builder;

#[tokio::test]
async fn get_snapshot__given_one_valid_snapshot_then_returns_it() {
    let latest_file_path: PathBuf = PathBuf::from(r"latest/path");
    let latest_data = SimpleData::new(vec!["latest_data".to_string()]);

    let snapshotter = FileSnapshotterBuilder::new()
        .with_next_snapshot_result(
            latest_file_path,
            Ok(Snapshot::from_serializable(&latest_data).unwrap()),
        )
        .build();

    let actual_result: SimpleData = snapshotter.get_snapshot().unwrap();

    assert_eq!(actual_result, latest_data);
}

#[tokio::test]
async fn get_snapshot__given_two_valid_snapshots_then_returns_latest() {
    let old_file_path: PathBuf = PathBuf::from(r"old/path");
    let latest_file_path: PathBuf = PathBuf::from(r"latest/path");

    let old_data = SimpleData::new(vec!["old_data".to_string()]);
    let latest_data = SimpleData::new(vec!["latest_data".to_string()]);

    let snapshotter = FileSnapshotterBuilder::new()
        .with_next_snapshot_result(
            old_file_path,
            Ok(Snapshot::from_serializable(&old_data).unwrap()),
        )
        .with_next_snapshot_result(
            latest_file_path,
            Ok(Snapshot::from_serializable(&latest_data).unwrap()),
        )
        .build();

    let actual_result: SimpleData = snapshotter.get_snapshot().unwrap();

    assert_eq!(actual_result, latest_data);
}

#[tokio::test]
async fn get_snapshot__when_no_files_then_return_none() {
    let snapshotter = FileSnapshotterBuilder::new().build();

    let maybe_data: Option<SimpleData> = snapshotter.get_snapshot();

    assert!(maybe_data.is_none());
}

#[tokio::test]
async fn get_snapshot__given_two_snapshots_and_latest_has_bad_checksum_then_returns_older() {
    let older_file_path: PathBuf = PathBuf::from(r"old/path");
    let older_data = SimpleData::new(vec!["old_data".to_string()]);

    let mut corrupted_snapshot = test_data_snapshot();

    // corrupt data
    corrupted_snapshot.set_checksum(Checksum::new(&[0u8]));

    let snapshotter = FileSnapshotterBuilder::new()
        .with_next_snapshot_result(
            older_file_path,
            Ok(Snapshot::from_serializable(&older_data).unwrap()),
        )
        .with_next_snapshot_result(PathBuf::from(r"some/path"), Ok(corrupted_snapshot))
        .build();

    let actual: SimpleData = snapshotter.get_snapshot().unwrap();

    assert_eq!(actual, older_data);
}

#[tokio::test]
async fn get_snapshot__given_two_snapshots_with_bad_checksums_then_returns_none() {
    let mut corrupted_snapshot = test_data_snapshot();

    // corrupt data
    corrupted_snapshot.set_checksum(Checksum::new(&[0u8]));

    let snapshotter = FileSnapshotterBuilder::new()
        .with_next_snapshot_result(
            PathBuf::from(r"some/path/a"),
            Ok(corrupted_snapshot.clone()),
        )
        .with_next_snapshot_result(PathBuf::from(r"some/path/b"), Ok(corrupted_snapshot))
        .build();

    let maybe_data: Option<SimpleData> = snapshotter.get_snapshot();

    assert!(maybe_data.is_none());
}

#[tokio::test]
async fn get_snapshot__given_two_snapshots_and_latest_file_is_missing_then_returns_older() {
    let older_file_path: PathBuf = PathBuf::from(r"old/path");
    let older_data = SimpleData::new(vec!["old_data".to_string()]);

    let snapshotter = FileSnapshotterBuilder::new()
        .with_next_snapshot_result(
            older_file_path,
            Ok(Snapshot::from_serializable(&older_data).unwrap()),
        )
        .with_next_snapshot_result(
            PathBuf::from(r"some/path"),
            Err(Error::Io(IoError::new(
                ErrorKind::NotFound,
                "Not Found".to_string(),
            ))),
        )
        .build();

    let actual: SimpleData = snapshotter.get_snapshot().unwrap();

    assert_eq!(actual, older_data);
}

#[tokio::test]
async fn get_snapshot__given_two_missing_snapshots_then_returns_none() {
    let snapshotter = FileSnapshotterBuilder::new()
        .with_next_snapshot_result(
            PathBuf::from(r"some/path/a"),
            Err(Error::Io(IoError::new(
                ErrorKind::NotFound,
                "Not Found".to_string(),
            ))),
        )
        .with_next_snapshot_result(
            PathBuf::from(r"some/path/b"),
            Err(Error::Io(IoError::new(
                ErrorKind::NotFound,
                "Not Found".to_string(),
            ))),
        )
        .build();

    let maybe_data: Option<SimpleData> = snapshotter.get_snapshot();

    assert!(maybe_data.is_none());
}

#[tokio::test]
async fn get_snapshot__given_two_snapshots_and_latest_is_not_deserializable_then_returns_older() {
    let older_file_path: PathBuf = PathBuf::from(r"old/path");
    let older_data = SimpleData::new(vec!["old_data".to_string()]);

    let mut corrupted_snapshot = test_data_snapshot();

    // corrupt data
    corrupted_snapshot.set_data(vec![]);

    let snapshotter = FileSnapshotterBuilder::new()
        .with_next_snapshot_result(
            older_file_path,
            Ok(Snapshot::from_serializable(&older_data).unwrap()),
        )
        .with_next_snapshot_result(PathBuf::from(r"some/path"), Ok(corrupted_snapshot))
        .build();

    let actual: SimpleData = snapshotter.get_snapshot().unwrap();

    assert_eq!(actual, older_data);
}

#[tokio::test]
async fn get_snapshot__given_two_snapshots_that_both_are_not_deserializable_then_returns_none() {
    let mut corrupted_snapshot = test_data_snapshot();

    // corrupt data
    corrupted_snapshot.set_data(vec![]);

    let snapshotter = FileSnapshotterBuilder::new()
        .with_next_snapshot_result(
            PathBuf::from(r"some/path/a"),
            Ok(corrupted_snapshot.clone()),
        )
        .with_next_snapshot_result(PathBuf::from(r"some/path/b"), Ok(corrupted_snapshot))
        .build();

    let maybe_data: Option<SimpleData> = snapshotter.get_snapshot();

    assert!(maybe_data.is_none());
}

#[tokio::test]
async fn snapshot__0_to_1_files() {
    let new_file_path: PathBuf = PathBuf::from(r"some/path");

    let (mut snapshotter, files) = FileSnapshotterBuilder::new()
        .with_new_file_path(new_file_path.clone())
        .build_with_files_handle();

    let new_data = SimpleData::new(vec!["new_data".to_string()]);

    snapshotter.snapshot(&new_data).await.unwrap();

    assert_eq!(
        files
            .lock()
            .unwrap()
            .get(&new_file_path)
            .unwrap()
            .as_ref()
            .unwrap(),
        &Snapshot::from_serializable(&new_data).unwrap()
    );
}

#[tokio::test]
async fn snapshot__1_to_2_files() {
    let old_file_path: PathBuf = PathBuf::from(r"old/path");
    let new_file_path: PathBuf = PathBuf::from(r"new/path");

    let old_data = SimpleData::new(vec!["old_data".to_string()]);

    let (mut snapshotter, files) = FileSnapshotterBuilder::new()
        .with_next_snapshot_result(
            old_file_path.clone(),
            Ok(Snapshot::from_serializable(&old_data).unwrap()),
        )
        .with_new_file_path(new_file_path.clone())
        .build_with_files_handle();

    let new_data = SimpleData::default();

    snapshotter.snapshot(&new_data).await.unwrap();

    assert_eq!(
        files
            .lock()
            .unwrap()
            .get(&old_file_path)
            .unwrap()
            .as_ref()
            .unwrap(),
        &Snapshot::from_serializable(&old_data).unwrap()
    );
    assert_eq!(
        files
            .lock()
            .unwrap()
            .get(&new_file_path)
            .unwrap()
            .as_ref()
            .unwrap(),
        &Snapshot::from_serializable(&new_data).unwrap()
    );
}

#[tokio::test]
async fn snapshot__2_to_2_files() {
    let oldest_file_path: PathBuf = PathBuf::from(r"oldest/path");
    let old_file_path: PathBuf = PathBuf::from(r"old/path");
    let new_file_path: PathBuf = PathBuf::from(r"new/path");

    let oldest_data = SimpleData::new(vec!["oldest_data".to_string()]);
    let old_data = SimpleData::new(vec!["old_data".to_string()]);

    let (mut snapshotter, files) = FileSnapshotterBuilder::new()
        .with_next_snapshot_result(
            oldest_file_path.clone(),
            Ok(Snapshot::from_serializable(&oldest_data).unwrap()),
        )
        .with_next_snapshot_result(
            old_file_path.clone(),
            Ok(Snapshot::from_serializable(&old_data).unwrap()),
        )
        .with_new_file_path(new_file_path.clone())
        .build_with_files_handle();

    let new_data = SimpleData::default();

    snapshotter.snapshot(&new_data).await.unwrap();

    let unlocked_files = files.lock().unwrap();

    assert!(!unlocked_files.contains_key(&oldest_file_path));
    assert_eq!(
        unlocked_files
            .get(&old_file_path)
            .unwrap()
            .as_ref()
            .unwrap(),
        &Snapshot::from_serializable(&old_data).unwrap()
    );
    assert_eq!(
        unlocked_files
            .get(&new_file_path)
            .unwrap()
            .as_ref()
            .unwrap(),
        &Snapshot::from_serializable(&new_data).unwrap()
    );
}

#[tokio::test]
async fn snapshot__when_snapshot_is_scheduled_then_snapshot_is_written() {
    let (mut snapshotter, files) = FileSnapshotterBuilder::new()
        .with_scheduled(true)
        .build_with_files_handle();

    let data = SimpleData::default();

    snapshotter.snapshot(&data).await.unwrap();

    assert_eq!(files.lock().unwrap().len(), 1);
}

#[tokio::test]
async fn snapshot__when_snapshot_is_not_scheduled_then_snapshot_is_not_written() {
    let (mut snapshotter, files) = FileSnapshotterBuilder::new()
        .with_scheduled(false)
        .build_with_files_handle();

    let data = SimpleData::default();

    snapshotter.snapshot(&data).await.unwrap();

    assert_eq!(files.lock().unwrap().len(), 0);
}

#[tokio::test]
pub async fn file_snapshot_writer_serialization_failure_returns_expected_error() {
    let data = UnserializableData::new();
    let mut snapshotter = FileSnapshotterBuilder::new().build();

    let result = snapshotter.snapshot(&data).await;

    assert!(matches!(result, Err(SnapshotError::SnapshotError(_))));
}

#[tokio::test]
pub async fn file_snapshot_writer_fails_because_file_writer_error() {
    let mut snapshotter = FileSnapshotterBuilder::new()
        .with_file_writer_error(Error::Serialization(Box::new(TestError::Test)))
        .build();

    // When a snapshot occurs
    let maybe_snapshot = snapshotter.snapshot(&SimpleData::default()).await;

    // Then it is an error
    assert!(matches!(
        maybe_snapshot,
        Err(SnapshotError::SnapshotError(_))
    ));
}

#[tokio::test]
async fn snapshot__when_snapshot_is_scheduled_then_snapshot_is_marked_complete() {
    let (mut snapshotter, snapshot_completed_handle) = FileSnapshotterBuilder::new()
        .with_scheduled(true)
        .build_with_snapshot_completed_handle();

    let data = SimpleData::default();

    snapshotter.snapshot(&data).await.unwrap();

    assert!(snapshot_completed_handle.load(Relaxed));
}

#[tokio::test]
async fn snapshot__when_snapshot_is_not_scheduled_then_snapshot_is_not_marked_complete() {
    let (mut snapshotter, snapshot_completed_handle) = FileSnapshotterBuilder::new()
        .with_scheduled(false)
        .build_with_snapshot_completed_handle();

    let data = SimpleData::default();

    snapshotter.snapshot(&data).await.unwrap();

    assert!(!snapshot_completed_handle.load(Relaxed));
}
