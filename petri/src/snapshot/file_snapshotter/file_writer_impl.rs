use std::fs;
use std::fs::{remove_file, File};
use std::io::Write;
use std::path::Path;

use async_trait::async_trait;

use crate::snapshot::file_snapshotter::{
    error::{Error, Result},
    FileWriter, Snapshot,
};

use std::path::PathBuf;

#[derive(Debug)]
pub struct FileWriterImpl;

#[async_trait]
impl FileWriter for FileWriterImpl {
    async fn write<P: AsRef<Path> + Send + Sync>(
        &mut self,
        path: P,
        snapshot: Snapshot,
    ) -> Result<()> {
        if let Some(parent_dir) = path.as_ref().parent() {
            fs::create_dir_all(parent_dir)
                .map_err(|e| Error::FilePath(PathBuf::from(path.as_ref()), e))?;
        }
        let mut file =
            File::create(&path).map_err(|e| Error::FilePath(PathBuf::from(path.as_ref()), e))?;
        let bytes = bincode::serialize(&snapshot).map_err(|e| Error::Serialization(Box::new(e)))?;
        file.write_all(&bytes)?;
        Ok(())
    }

    async fn delete<P: AsRef<Path> + Send + Sync>(&mut self, path: P) -> Result<()> {
        remove_file(&path).map_err(|e| Error::FilePath(PathBuf::from(path.as_ref()), e))?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    #![allow(non_snake_case)]

    use std::io::Read;
    use std::path::PathBuf;

    use serde::de::DeserializeOwned;
    use tempfile::NamedTempFile;

    use super::*;
    use crate::snapshot::file_snapshotter::tests::test_data::SimpleData;

    fn new_temp_path() -> PathBuf {
        NamedTempFile::new().unwrap().into_temp_path().to_path_buf()
    }

    fn read_snapshot_file<T: DeserializeOwned>(path: PathBuf) -> T {
        let mut file = File::open(path).unwrap();
        let mut bytes = Vec::new();
        file.read_to_end(&mut bytes).unwrap();
        let snapshot: Snapshot = bincode::deserialize(&bytes).unwrap();
        snapshot.to_deserializable().unwrap().unwrap()
    }

    #[tokio::test]
    async fn write__can_write_to_file() {
        let path = new_temp_path();
        let mut writer = FileWriterImpl;
        let expected = SimpleData::default();
        let snapshot = Snapshot::from_serializable(&expected).unwrap();

        writer.write(path.clone(), snapshot).await.unwrap();

        let actual: SimpleData = read_snapshot_file(path.clone());
        assert_eq!(&expected, &actual);
    }

    #[tokio::test]
    async fn write__when_snapshot_dir_does_not_exist_then_create_dir_without_error() {
        let mut path = new_temp_path();

        path.push("some");
        path.push("new");
        path.push("dir");

        let mut writer = FileWriterImpl;

        let expected = SimpleData::default();

        let snapshot = Snapshot::from_serializable(&expected).unwrap();

        writer.write(path.clone(), snapshot).await.unwrap();

        let actual: SimpleData = read_snapshot_file(path.clone());

        assert_eq!(expected, actual);
    }

    #[tokio::test]
    async fn delete__can_delete_specified_file() {
        let path = new_temp_path();

        let bytes = b"123";
        let mut file = File::create(path.clone()).unwrap();
        file.write_all(bytes).unwrap();

        let mut writer = FileWriterImpl;

        writer.delete(path.clone()).await.unwrap();

        assert!(!path.exists())
    }

    #[tokio::test]
    async fn delete__returns_error_if_fails_to_delete() {
        let path = new_temp_path();

        let mut writer = FileWriterImpl;

        let result = writer.delete(path.clone()).await;

        assert!(matches!(result, Err(_)))
    }
}
