use crate::snapshot::file_snapshotter::error::Result;
use crate::snapshot::file_snapshotter::Scheduler;
use async_trait::async_trait;
use std::time::Duration;
use tokio::time::Instant;

pub struct SchedulerTimerImpl {
    wait_time: Duration,
    last_snap_time: Instant,
}

#[cfg(test)]
impl SchedulerTimerImpl {
    pub fn new(wait_time: Duration) -> Self {
        let last_snap_time = Instant::now();
        SchedulerTimerImpl {
            wait_time,
            last_snap_time,
        }
    }
}

#[async_trait]
impl Scheduler for SchedulerTimerImpl {
    async fn should_snapshot(&self) -> bool {
        let now = Instant::now();
        let deadline = self.last_snap_time + self.wait_time;
        now >= deadline
    }

    async fn snapshot_complete(&mut self) -> Result<()> {
        self.last_snap_time = Instant::now();
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    #[allow(non_snake_case)]
    async fn should_snapshot__when_wait_time_100_ms_then_true_10_times_in_1_sec() {
        let wait_time = Duration::from_millis(100);
        let mut scheduler = SchedulerTimerImpl::new(wait_time);

        let mut count = 0;
        let start = Instant::now();

        // Wait just over 1 second
        while Instant::now() - start < Duration::from_millis(1050) {
            tokio::time::sleep(Duration::from_millis(1)).await;
            if scheduler.should_snapshot().await {
                count += 1;
                scheduler.snapshot_complete().await.unwrap()
            }
        }

        let expected = 10;
        assert_eq!(expected, count);
    }
}
