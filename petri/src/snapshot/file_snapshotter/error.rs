use std::io::Error as IoError;
use std::path::PathBuf;
use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Serialization failed.")]
    Serialization(Box<dyn std::error::Error + Send + Sync>),
    #[error("IO failed.")]
    Io(#[from] IoError),
    #[error("Couldn't access path.")]
    FilePath(PathBuf, IoError),
}
