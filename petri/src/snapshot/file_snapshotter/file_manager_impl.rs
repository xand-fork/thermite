use crate::snapshot::file_snapshotter::{
    error::{Error, Result},
    FileManager, LoggingEvent,
};
use serde::Deserialize;
use std::collections::VecDeque;
use std::fs;
use std::fs::File;
use std::io::{Read, Write};
use std::path::{Path, PathBuf};
use uuid::Uuid;

const INDEX_FILE_NAME: &str = "INDEX_FILE";

#[derive(Debug)]
pub struct FileManagerImpl {
    snapshot_dir: PathBuf,
}

#[derive(Serialize, Deserialize, Default, Debug)]
struct FileIndex {
    existing_files: VecDeque<PathBuf>,
}

impl FileIndex {
    pub fn add_latest(&mut self, path: PathBuf) {
        self.existing_files.push_front(path);
    }
}

impl FileManager for FileManagerImpl {
    fn new_file_path(&mut self) -> Result<PathBuf> {
        let mut path = self.snapshot_dir.clone();
        let uuid = Uuid::new_v4();
        path.push(format!("{}", uuid));
        Ok(path)
    }

    fn track_file(&mut self, path: PathBuf) -> Result<()> {
        let mut index = self.get_index()?.unwrap_or_default();
        index.add_latest(path);
        self.write_index(index)?;
        Ok(())
    }

    fn prune_tracked_files_to_x_and_return_old(&mut self, x: usize) -> Result<Vec<PathBuf>> {
        let mut existing_files = self.get_index()?.unwrap_or_default().existing_files;
        let mut to_be_pruned = Vec::new();
        if existing_files.len() > x {
            to_be_pruned = existing_files.split_off(x).into();
        }
        let index = FileIndex { existing_files };
        self.write_index(index)?;
        Ok(to_be_pruned)
    }

    fn files_youngest_to_oldest(&self) -> Result<Vec<PathBuf>> {
        let paths = self.get_index()?.unwrap_or_default().existing_files.into();
        Ok(paths)
    }
}

impl FileManagerImpl {
    fn get_index(&self) -> Result<Option<FileIndex>> {
        let mut index_path = self.snapshot_dir.clone();
        index_path.push(INDEX_FILE_NAME);

        if !index_path.exists() {
            return Ok(None);
        }

        match Self::read_serialized_file(&index_path) {
            Ok(file_index) => Ok(Some(file_index)),
            Err(error) => {
                error!(LoggingEvent::DeserializeSnapshotIndexFileError(
                    format!(
                        "Unable to deserialize snapshot index file: {:?}; Error: {:?}; Using clean slate.",
                        index_path,
                        error
                    )
                ));
                Ok(None)
            }
        }
    }

    fn read_serialized_file(path: &Path) -> Result<FileIndex> {
        let mut file = File::open(path).map_err(|e| Error::FilePath(path.to_path_buf(), e))?;
        let mut bytes = Vec::new();
        file.read_to_end(&mut bytes)?;
        let data = bincode::deserialize(&bytes).map_err(|e| Error::Serialization(Box::new(e)))?;

        Ok(data)
    }

    fn write_index(&self, index: FileIndex) -> Result<()> {
        let bytes = bincode::serialize(&index).map_err(|e| Error::Serialization(Box::new(e)))?;

        let mut index_path = self.snapshot_dir.clone();
        fs::create_dir_all(&index_path)?;
        index_path.push(INDEX_FILE_NAME);

        let mut index_file = File::create(index_path)?;

        index_file.write_all(&bytes)?;
        Ok(())
    }
}

impl FileManagerImpl {
    pub fn new(snapshot_dir: PathBuf) -> Self {
        Self { snapshot_dir }
    }
}

#[cfg(test)]
mod tests {
    #![allow(non_snake_case)]

    use super::*;
    use serde::de::DeserializeOwned;
    use std::collections::HashSet;
    use std::io::Read;
    use std::iter::repeat;
    use tempfile::tempdir;

    fn temp_dir() -> PathBuf {
        PathBuf::from(tempdir().unwrap().as_ref())
    }

    #[tokio::test]
    pub async fn new_file_path__path_is_in_snapshot_dir() {
        let mut file_manager = FileManagerImpl::new(temp_dir());
        let actual_file_path = file_manager.new_file_path().unwrap();

        assert_eq!(
            actual_file_path.parent().unwrap(),
            &file_manager.snapshot_dir
        )
    }

    #[tokio::test]
    pub async fn new_file_path__is_unique() {
        let mut file_manager = FileManagerImpl::new(temp_dir());

        const EXPECTED_COUNT: usize = 100;

        let uniques_file_paths: HashSet<_> = repeat(())
            .take(EXPECTED_COUNT)
            .map(|_| file_manager.new_file_path().unwrap())
            .collect();

        assert_eq!(uniques_file_paths.len(), EXPECTED_COUNT)
    }

    #[tokio::test]
    pub async fn track_file__when_no_index_exists_then_new_index_created() {
        let temp_dir = temp_dir();
        let mut file_manager = FileManagerImpl::new(temp_dir.clone());

        let file_name = "some_file";

        let new_file = PathBuf::from(&file_name);

        let mut index_path = temp_dir;
        index_path.push(INDEX_FILE_NAME);

        file_manager.track_file(new_file).unwrap();

        assert!(index_path.exists());
    }

    #[tokio::test]
    pub async fn track_file__when_snapshot_dir_does_not_exist_then_create_dir_without_error() {
        let mut temp_dir = temp_dir();
        temp_dir.push("some");
        temp_dir.push("new");
        temp_dir.push("dir");
        let mut file_manager = FileManagerImpl::new(temp_dir.clone());

        let file_name = "some_file";

        let new_file = PathBuf::from(&file_name);

        let mut index_path = temp_dir;
        index_path.push(INDEX_FILE_NAME);

        file_manager.track_file(new_file).unwrap();

        assert!(index_path.exists());
    }

    fn read_serialized_file<T: DeserializeOwned>(path: PathBuf) -> T {
        let mut file = File::open(path).unwrap();
        let mut bytes = Vec::new();
        file.read_to_end(&mut bytes).unwrap();
        bincode::deserialize(&bytes).unwrap()
    }

    #[tokio::test]
    pub async fn track_file__index_includes_tracked_file() {
        let temp_dir = temp_dir();
        let mut file_manager = FileManagerImpl::new(temp_dir.clone());

        let file_name = "some_file";

        let new_file = PathBuf::from(&file_name);

        let mut index_path = temp_dir;
        index_path.push(INDEX_FILE_NAME);

        file_manager.track_file(new_file.clone()).unwrap();

        let index: FileIndex = read_serialized_file(index_path);

        assert!(index.existing_files.contains(&new_file));
    }

    #[tokio::test]
    pub async fn track_file__when_index_file_is_invalid_then_continues_as_if_no_index() {
        let temp_dir = temp_dir();
        let mut index_path = temp_dir.clone();
        index_path.push(INDEX_FILE_NAME);

        fs::create_dir_all(&temp_dir).unwrap();
        let mut existing_index_file = File::create(index_path).unwrap();
        existing_index_file.write_all(b"not a valid index").unwrap();

        let mut file_manager = FileManagerImpl::new(temp_dir.clone());

        let new_file_name = "some_new_file";
        let new_file_name = PathBuf::from(&new_file_name);

        let mut index_path = temp_dir;
        index_path.push(INDEX_FILE_NAME);

        file_manager.track_file(new_file_name.clone()).unwrap();

        let index: FileIndex = read_serialized_file(index_path);

        assert!(index.existing_files.contains(&new_file_name));
    }

    #[tokio::test]
    pub async fn track_file__when_index_exists_then_tracked_file_added() {
        let temp_dir = temp_dir();
        let mut index_path = temp_dir.clone();
        index_path.push(INDEX_FILE_NAME);

        let mut existing_file_path = temp_dir.clone();
        existing_file_path.push("existing_file");

        let mut existing_paths = VecDeque::new();
        existing_paths.push_front(existing_file_path.clone());

        let existing_file_index = FileIndex {
            existing_files: existing_paths,
        };

        fs::create_dir_all(&temp_dir).unwrap();
        let mut existing_index_file = File::create(index_path).unwrap();
        existing_index_file
            .write_all(&bincode::serialize(&existing_file_index).unwrap())
            .unwrap();

        let mut file_manager = FileManagerImpl::new(temp_dir.clone());

        let new_file_name = "some_new_file";
        let new_file_name = PathBuf::from(&new_file_name);

        let mut index_path = temp_dir;
        index_path.push(INDEX_FILE_NAME);

        file_manager.track_file(new_file_name.clone()).unwrap();

        let index: FileIndex = read_serialized_file(index_path);

        assert!(index.existing_files.contains(&new_file_name));
        assert!(index.existing_files.contains(&existing_file_path));
    }

    #[tokio::test]
    pub async fn prune_tracked_files_to_x_and_return_old__when_over_x_return_extras() {
        let temp_dir = temp_dir();
        let mut file_manager = FileManagerImpl::new(temp_dir);

        let one = PathBuf::from("one");
        let two = PathBuf::from("two");
        let three = PathBuf::from("three");

        file_manager.track_file(one.clone()).unwrap();
        file_manager.track_file(two).unwrap();
        file_manager.track_file(three).unwrap();

        let x = 2;

        let expected = vec![one];
        let actual = file_manager
            .prune_tracked_files_to_x_and_return_old(x)
            .unwrap();

        assert_eq!(expected, actual);
    }

    #[tokio::test]
    pub async fn prune_tracked_files_to_x_and_return_old__when_eq_2_return_empty() {
        let temp_dir = temp_dir();
        let mut file_manager = FileManagerImpl::new(temp_dir);

        let one = PathBuf::from("one");
        let two = PathBuf::from("two");

        file_manager.track_file(one).unwrap();
        file_manager.track_file(two).unwrap();

        let x = 2;

        let expected: Vec<PathBuf> = vec![];
        let actual = file_manager
            .prune_tracked_files_to_x_and_return_old(x)
            .unwrap();

        assert_eq!(expected, actual);
    }

    #[tokio::test]
    pub async fn prune_tracked_files_to_x_and_return_old__when_less_than_x_return_empty() {
        let temp_dir = temp_dir();
        let mut file_manager = FileManagerImpl::new(temp_dir);

        let one = PathBuf::from("one");

        file_manager.track_file(one).unwrap();

        let x = 2;

        let expected: Vec<PathBuf> = vec![];
        let actual = file_manager
            .prune_tracked_files_to_x_and_return_old(x)
            .unwrap();

        assert_eq!(expected, actual);
    }

    #[tokio::test]
    pub async fn prune_tracked_files_to_x_and_return_old__when_over_x_then_index_file_updated() {
        let temp_dir = temp_dir();
        let mut file_manager = FileManagerImpl::new(temp_dir.clone());

        let one = PathBuf::from("one");
        let two = PathBuf::from("two");
        let three = PathBuf::from("three");

        file_manager.track_file(one).unwrap();
        file_manager.track_file(two).unwrap();
        file_manager.track_file(three).unwrap();

        let x = 2;

        file_manager
            .prune_tracked_files_to_x_and_return_old(x)
            .unwrap();

        let mut index_path = temp_dir;
        index_path.push(INDEX_FILE_NAME);
        let index: FileIndex = read_serialized_file(index_path);

        assert_eq!(index.existing_files.len(), x);
    }

    #[tokio::test]
    pub async fn prune_tracked_files_to_x_and_return_old__when_eq_to_2_then_index_file_unchanged() {
        let temp_dir = temp_dir();
        let mut file_manager = FileManagerImpl::new(temp_dir.clone());

        let one = PathBuf::from("one");
        let two = PathBuf::from("two");

        file_manager.track_file(one).unwrap();
        file_manager.track_file(two).unwrap();

        let x = 2;

        file_manager
            .prune_tracked_files_to_x_and_return_old(x)
            .unwrap();

        let mut index_path = temp_dir;
        index_path.push(INDEX_FILE_NAME);
        let index: FileIndex = read_serialized_file(index_path);

        assert_eq!(index.existing_files.len(), x);
    }

    #[tokio::test]
    pub async fn prune_tracked_files_to_x_and_return_old__when_less_than_2_then_index_file_unchanged(
    ) {
        let temp_dir = temp_dir();
        let mut file_manager = FileManagerImpl::new(temp_dir.clone());

        let one = PathBuf::from("one");

        file_manager.track_file(one).unwrap();

        let x = 2;

        file_manager
            .prune_tracked_files_to_x_and_return_old(x)
            .unwrap();

        let mut index_path = temp_dir;
        index_path.push(INDEX_FILE_NAME);
        let index: FileIndex = read_serialized_file(index_path);

        assert_eq!(index.existing_files.len(), 1);
    }

    #[tokio::test]
    pub async fn files_youngest_to_oldest__nothing_returns_empty() {
        let temp_dir = temp_dir();
        let file_manager = FileManagerImpl::new(temp_dir);

        let expected = Vec::<PathBuf>::new();
        let actual = file_manager.files_youngest_to_oldest().unwrap();

        assert_eq!(expected, actual);
    }

    #[tokio::test]
    pub async fn files_youngest_to_oldest__single_returns_just_the_one() {
        let temp_dir = temp_dir();
        let mut file_manager = FileManagerImpl::new(temp_dir);

        let one = PathBuf::from("one");

        file_manager.track_file(one.clone()).unwrap();

        let expected = vec![one];
        let actual = file_manager.files_youngest_to_oldest().unwrap();

        assert_eq!(expected, actual);
    }

    #[tokio::test]
    pub async fn files_youngest_to_oldest__two_retuns_in_correct_order() {
        let temp_dir = temp_dir();
        let mut file_manager = FileManagerImpl::new(temp_dir);

        let one = PathBuf::from("one");
        let two = PathBuf::from("two");

        file_manager.track_file(one.clone()).unwrap();
        file_manager.track_file(two.clone()).unwrap();

        let expected = vec![two, one];
        let actual = file_manager.files_youngest_to_oldest().unwrap();

        assert_eq!(expected, actual);
    }
}
