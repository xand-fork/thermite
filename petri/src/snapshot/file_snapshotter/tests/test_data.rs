use serde::{Serialize, Serializer};

#[derive(PartialEq, Eq, Clone, Debug, Serialize, serde_derive::Deserialize)]
pub struct SimpleData {
    some_strings: Vec<String>,
}

impl SimpleData {
    pub fn new(some_strings: Vec<String>) -> Self {
        Self { some_strings }
    }
}

impl Default for SimpleData {
    fn default() -> Self {
        SimpleData::new(vec![
            "fred".to_string(),
            "jane".to_string(),
            "cats".to_string(),
        ])
    }
}

#[derive(PartialEq, Eq, Clone, Debug, serde_derive::Deserialize)]
pub struct UnserializableData {}

impl UnserializableData {
    pub fn new() -> Self {
        Self {}
    }
}

impl Default for UnserializableData {
    fn default() -> Self {
        UnserializableData::new()
    }
}

impl Serialize for UnserializableData {
    fn serialize<S>(
        &self,
        _serializer: S,
    ) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        Err(serde::ser::Error::custom("Unserializable test data"))
    }
}
