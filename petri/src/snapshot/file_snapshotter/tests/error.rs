use thiserror::Error;

#[derive(Debug, Error)]
pub enum TestError {
    #[error("Test error.")]
    Test,
}
