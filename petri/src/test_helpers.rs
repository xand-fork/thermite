use crate::snapshot::Result;
use crate::subs_client_wrapper::BlockNumber;
use crate::{
    confidentiality_store::{IdTagRecord, IdTagStatus},
    petri_persisted_state::PetriPersistedState,
    subs_client_wrapper::{ConcreteBlock, MockSubstrateBlockchainClient},
    ExtrinsicDecoderImpl, Petri, Snapshotter,
};
use async_trait::async_trait;
use proptest::{
    prelude::*,
    test_runner::{RngAlgorithm, TestRng},
};
use rand::rngs::OsRng;
use rand::SeedableRng;
use sp_core::{sr25519::Public, Encode};
use sp_runtime::{
    generic::{BlockId, SignedBlock},
    traits::Zero,
    DispatchError,
};
use std::collections::HashSet;
use std::sync::atomic::AtomicBool;
use std::sync::Mutex;
use std::{collections::HashMap, sync::Arc};
use xand_address::Address;
use xand_financial_client::test_helpers::managers::{
    generate_test_redeem_manager_with_keys, TestRedeemMgrConfig,
};
pub(crate) use xand_financial_client::test_helpers::{
    managers::{generate_test_create_manager_with_keys, FakeSendManagerDeps, TestCreateMgrConfig},
    private_key_to_address,
};
use xand_financial_client::{
    models::{EncryptionKey, TransactionOutputData},
    CreateManagerImpl, RedeemManagerImpl, SendManagerImpl,
};
use xand_ledger::curve25519_dalek::ristretto::RistrettoPoint;
use xand_ledger::{PrivateKey, PublicKey, TransactionOutput};
use xand_models::{
    substrate_test_helpers::call_to_opaque_ext, CorrelationId, ExtrinsicDecoderConfig, ToAddress,
};
use xand_public_key::ristretto_point_to_address;
use xandstrate_runtime::network_voting::PropIndex;
use xandstrate_runtime::{
    system_events_storage_key, Call, EventRecord, Phase, Runtime, SystemEvent, TimestampCall,
    XandCall,
};

// Helpers for local testing

pub struct TestExtrinsicDecoderConfig;

impl ExtrinsicDecoderConfig for TestExtrinsicDecoderConfig {
    type CreateManager = CreateManagerImpl<TestCreateMgrConfig>;
    type SendManager = SendManagerImpl<FakeSendManagerDeps>;
    type RedeemManager = RedeemManagerImpl<TestRedeemMgrConfig>;
}

pub(crate) fn to_txo(data: &TransactionOutputData) -> TransactionOutput {
    match data {
        TransactionOutputData::Private(otxo, _) => otxo.transaction_output,
        TransactionOutputData::Public(txo) => *txo,
    }
}

// Stolen and adapted from xand-ledger's `test::fakes` module. If we find a need to copy it or other
// test helpers to more crates, we should consider creating internal test utils crate(s).
//
// Beware, the API proptest's `TestRng` includes `rand_core::errors::Error`, but does not re-export
// the type, so attempts to upgrade proptest to "1.0.0" didn't go so well. Welcome to version hell.
#[derive(Clone, Debug)]
pub struct FakeCsprng(TestRng);

impl Default for FakeCsprng {
    fn default() -> Self {
        Self(TestRng::deterministic_rng(RngAlgorithm::ChaCha))
    }
}

impl rand::CryptoRng for FakeCsprng {}

impl rand::RngCore for FakeCsprng {
    fn next_u32(&mut self) -> u32 {
        self.0.next_u32()
    }

    fn next_u64(&mut self) -> u64 {
        self.0.next_u64()
    }

    fn fill_bytes(&mut self, dest: &mut [u8]) {
        self.0.fill_bytes(dest)
    }

    fn try_fill_bytes(&mut self, dest: &mut [u8]) -> Result<(), rand::Error> {
        self.0.try_fill_bytes(dest)
    }
}

impl rand::SeedableRng for FakeCsprng {
    type Seed = [u8; 32];

    fn from_seed(seed: Self::Seed) -> Self {
        FakeCsprng(TestRng::from_seed(RngAlgorithm::ChaCha, &seed))
    }
}

prop_compose! {
    pub fn arbitrary_address()(seed: [u8; 32]) -> Address {
        private_key_to_address(&PrivateKey::random(&mut FakeCsprng::from_seed(seed)))
    }
}

prop_compose! {
    pub fn arbitrary_public_key()(seed: [u8; 32]) -> PublicKey {
        PrivateKey::random(&mut FakeCsprng::from_seed(seed)).into()
    }
}

prop_compose! {
    pub fn arbitrary_identity_tag()(seed: [u8; 32]) -> xand_ledger::IdentityTag {
        xand_ledger::IdentityTag::random(&mut FakeCsprng::from_seed(seed))
    }
}

pub fn arbitrary_id_tag_status() -> impl Strategy<Value = IdTagStatus> {
    prop_oneof![Just(IdTagStatus::Valid), Just(IdTagStatus::Pending),]
}

prop_compose! {
    pub fn arbitrary_correlation_id()(bytes: [u8; 16]) -> CorrelationId {
        CorrelationId::from(bytes)
    }
}

prop_compose! {
    pub fn arbitrary_id_tag_record()(
        tag in arbitrary_identity_tag(),
        owner in proptest::option::of(arbitrary_public_key()),
        status in arbitrary_id_tag_status(),
        correlation_id in proptest::option::of(arbitrary_correlation_id()),
    ) -> IdTagRecord {
        IdTagRecord {
            tag,
            owner,
            status,
            correlation_id,
        }
    }
}
pub struct TestPetriBuilder {
    members: HashMap<Address, EncryptionKey>,
    transactions: Vec<(Public, Call, bool)>,
    timestamp_interval: u64,
    private_key: Option<PrivateKey>,
    trust_encryption_key: Option<EncryptionKey>,
    trust_address: Option<Address>,
    events: HashMap<usize, Vec<EventRecord>>,
    proposals: HashMap<PropIndex, xandstrate_runtime::Call>,
    validators: HashSet<PublicKey>,
    shutdown_flag: Arc<AtomicBool>,
    snapshotter: Option<FakeSnapshotter>,
    starting_block: BlockNumber,
}

#[derive(Clone, Default, Debug)]
pub struct FakeSnapshotter {
    snapshot: Arc<Mutex<Option<PetriPersistedState<FakeCsprng>>>>,
    snapshot_count: Arc<Mutex<usize>>,
}

impl FakeSnapshotter {
    pub fn get_last_snapshot(&self) -> Option<PetriPersistedState<FakeCsprng>> {
        self.snapshot.lock().unwrap().clone()
    }

    pub fn get_snapshot_count(&self) -> usize {
        *self.snapshot_count.lock().unwrap()
    }
}

#[async_trait]
impl Snapshotter<PetriPersistedState<FakeCsprng>> for FakeSnapshotter {
    async fn snapshot(&mut self, data: &PetriPersistedState<FakeCsprng>) -> Result<()> {
        *self.snapshot.lock().unwrap() = Some(data.clone());
        *self.snapshot_count.lock().unwrap() += 1;
        Ok(())
    }

    fn get_snapshot(&self) -> Option<PetriPersistedState<FakeCsprng>> {
        unimplemented!()
    }
}

impl TestPetriBuilder {
    pub fn member(self, member: Address) -> Self {
        self.member_with_encryption_key(
            member,
            EncryptionKey::from(rand::thread_rng().gen::<[u8; 32]>()),
        )
    }

    pub fn member_with_encryption_key(
        mut self,
        member: Address,
        encryption_key: EncryptionKey,
    ) -> Self {
        self.members.insert(member, encryption_key);
        self
    }

    pub fn transactions(mut self, transactions: Vec<(Public, Call)>) -> Self {
        self.transactions.extend(
            transactions
                .into_iter()
                .map(|(key, call)| (key, call, true)),
        );
        self
    }

    pub fn transactions_with_outcomes(mut self, transactions: Vec<(Public, Call, bool)>) -> Self {
        self.transactions.extend(transactions);
        self
    }

    pub fn timestamp_interval(self, inc: u64) -> Self {
        Self {
            timestamp_interval: inc,
            ..self
        }
    }

    pub fn private_key(self, key: PrivateKey) -> Self {
        Self {
            private_key: Some(key),
            ..self
        }
    }

    pub fn trust_encryption_key(self, key: EncryptionKey) -> Self {
        Self {
            trust_encryption_key: Some(key),
            ..self
        }
    }

    pub fn trust_address(self, address: Address) -> Self {
        Self {
            trust_address: Some(address),
            ..self
        }
    }

    pub fn events(mut self, events: HashMap<usize, Vec<EventRecord>>) -> Self {
        self.events.extend(events);
        self
    }

    pub fn proposals(mut self, proposals: HashMap<PropIndex, xandstrate_runtime::Call>) -> Self {
        self.proposals.extend(proposals);
        self
    }

    pub fn validators(mut self, validators: Vec<PublicKey>) -> Self {
        self.validators.extend(validators);
        self
    }

    pub fn with_snapshotter(mut self, snapshotter: FakeSnapshotter) -> Self {
        self.snapshotter = Some(snapshotter);
        self
    }

    pub fn with_shutdown_flag(mut self, shutdown_flag: Arc<AtomicBool>) -> Self {
        self.shutdown_flag = shutdown_flag;
        self
    }

    pub fn with_starting_block(mut self, starting_block: BlockNumber) -> Self {
        self.starting_block = starting_block;
        self
    }

    pub fn build(self) -> Petri<TestExtrinsicDecoderConfig, FakeCsprng, FakeSnapshotter> {
        let members_by_block = if self.transactions.is_empty() {
            vec![self.members.into_iter().collect::<Vec<_>>()]
        } else {
            self.transactions
                .iter()
                .scan(self.members, |members, (_, transaction, valid)| {
                    if *valid {
                        match transaction {
                            Call::XandStrate(XandCall::register_account_as_member(
                                account,
                                pub_key,
                            )) => {
                                members.insert(account.to_address(), (*pub_key.as_bytes()).into());
                            }
                            Call::XandStrate(XandCall::remove_member(account)) => {
                                members.remove(&account.to_address());
                            }
                            Call::XandStrate(XandCall::exit_member(account)) => {
                                members.remove(&account.to_address());
                            }
                            _ => {}
                        }
                    }
                    Some(members.iter().map(|(k, v)| (k.clone(), *v)).collect())
                })
                .collect()
        };
        let mut mock_client = MockSubstrateBlockchainClient::new();
        mock_client.expect_members().returning({
            let starting_block = self.starting_block as usize;
            let members = members_by_block
                .iter()
                .map(|members| members.iter().map(|(a, _)| a.clone()).collect::<Vec<_>>())
                .collect::<Vec<_>>();
            move |block_id| Ok(members[block_id_to_index(block_id) - starting_block].clone())
        });
        let txn_outcomes = self
            .transactions
            .iter()
            .map(|&(_, _, success)| success)
            .collect::<Vec<_>>();
        let blocks = {
            let mut account_indices = HashMap::new();
            let timestamp_interval = self.timestamp_interval;
            let starting_block = self.starting_block;
            self.transactions
                .into_iter()
                .enumerate()
                .map(|(block_number, (issuer, transaction, _))| {
                    let block_number = block_number as u32 + starting_block;
                    let account_index = account_indices.entry(issuer).or_default();
                    let mut next_index = || {
                        let next = *account_index + 1;
                        std::mem::replace(account_index, next)
                    };
                    let timestamp_index = next_index();
                    let transaction_index = next_index();
                    let timestamp_ext = call_to_opaque_ext(
                        xandstrate_runtime::Call::Timestamp(TimestampCall::set(
                            timestamp_interval * u64::from(block_number),
                        )),
                        timestamp_index,
                        &issuer,
                    );
                    let transaction_ext =
                        call_to_opaque_ext(transaction, transaction_index, &issuer);
                    SignedBlock {
                        justification: None,
                        block: ConcreteBlock {
                            extrinsics: vec![timestamp_ext, transaction_ext],
                            header: xandstrate_runtime::opaque::Header {
                                number: block_number,
                                parent_hash: Default::default(),
                                state_root: Default::default(),
                                extrinsics_root: Default::default(),
                                digest: Default::default(),
                            },
                        },
                    }
                })
                .collect::<Vec<_>>()
        };
        let block_count = blocks.len() as u32 + self.starting_block;
        let starting_block = self.starting_block as usize;
        // Block index is equal to the block number minus the starting block :/
        mock_client
            .expect_block()
            .returning(move |b| Ok(blocks.get(block_id_to_index(b) - starting_block).cloned()));
        mock_client
            .expect_chain_info()
            .returning(move || sp_blockchain::Info {
                best_hash: Default::default(),
                best_number: block_count.saturating_sub(1),
                genesis_hash: Default::default(),
                finalized_hash: Default::default(),
                finalized_number: block_count.saturating_sub(1),
                number_leaves: 0,
            });
        let extra_events = self.events;
        mock_client
            .expect_storage()
            .returning(move |block_id, key| {
                let mut block_events = vec![mk_ext_pass(0)];
                if let Some(succ) = txn_outcomes.get(block_id_to_index(block_id)) {
                    let txn_event = if *succ {
                        mk_ext_pass(1)
                    } else {
                        mk_ext_fail(1)
                    };
                    block_events.push(txn_event);
                };
                if key == &system_events_storage_key() {
                    if let Some(extend_with) = extra_events.get(&block_id_to_index(block_id)) {
                        block_events.extend_from_slice(extend_with);
                    }
                }
                Ok(Some(sp_storage::StorageData(block_events.encode())))
            });
        let proposals = self.proposals;
        mock_client
            .expect_get_proposal_action()
            .returning(move |_, index| Ok(proposals.get(index).cloned()));
        let private_key = self.private_key.unwrap_or_default();
        let trust_encryption_key = self
            .trust_encryption_key
            .unwrap_or_else(|| EncryptionKey::from(rand::thread_rng().gen::<[u8; 32]>()));
        let trust_address = self.trust_address.unwrap_or_else(|| {
            let ristretto_point = RistrettoPoint::random(&mut OsRng::default());
            ristretto_point_to_address(&ristretto_point)
        });
        let all_members_ever = members_by_block
            .iter()
            .flatten()
            .map(|(k, v)| (k.clone(), *v))
            .collect::<HashMap<_, _>>()
            .into_iter()
            .collect::<Vec<_>>();
        let create_manager = generate_test_create_manager_with_keys(
            private_key,
            false,
            trust_encryption_key,
            trust_address.clone(),
            all_members_ever.clone(),
        );
        let send_manager = SendManagerImpl::<FakeSendManagerDeps> {
            key_store: create_manager.key_store.clone(),
            encryption_provider: create_manager.encryption_provider.clone(),
            key_resolver: create_manager.network_key_lookup.clone(),
        };
        let redeem_manager = generate_test_redeem_manager_with_keys(
            private_key,
            false,
            trust_encryption_key,
            trust_address,
            all_members_ever,
        );

        let mut petri = Petri::new(
            Arc::new(mock_client),
            ExtrinsicDecoderImpl::new(create_manager, send_manager, redeem_manager),
            self.shutdown_flag,
            self.snapshotter,
            PetriPersistedState::default(),
        );
        petri.state.next_block_to_process = self.starting_block;
        petri.cache_member_id_tags();
        petri
    }
}

impl Default for TestPetriBuilder {
    fn default() -> Self {
        Self {
            members: Default::default(),
            transactions: Vec::new(),
            timestamp_interval: 1,
            private_key: None,
            trust_encryption_key: None,
            trust_address: None,
            events: HashMap::new(),
            proposals: HashMap::new(),
            validators: HashSet::new(),
            shutdown_flag: Arc::new(AtomicBool::new(false)),
            snapshotter: Default::default(),
            starting_block: Zero::zero(),
        }
    }
}

pub(crate) fn block_id_to_index(b: &BlockId<ConcreteBlock>) -> usize {
    match b {
        BlockId::Number(n) => *n as usize,
        BlockId::Hash(_) => unimplemented!("Petri tests don't use that for now"),
    }
}

pub(crate) fn mk_ext_pass(ext_ix: u32) -> EventRecord {
    EventRecord {
        phase: Phase::ApplyExtrinsic(ext_ix),
        event: xandstrate_runtime::Event::system(SystemEvent::<Runtime>::ExtrinsicSuccess(
            Default::default(),
        )),
        topics: vec![],
    }
}

pub(crate) fn mk_ext_fail(ext_ix: u32) -> EventRecord {
    EventRecord {
        phase: Phase::ApplyExtrinsic(ext_ix),
        event: xandstrate_runtime::Event::system(SystemEvent::<Runtime>::ExtrinsicFailed(
            DispatchError::Other("I'm busted yo"),
            Default::default(),
        )),
        topics: vec![],
    }
}
