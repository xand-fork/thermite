use async_trait::async_trait;
pub use error::{Error, Result};
#[cfg(test)]
use mockall::*;
use serde::de::DeserializeOwned;
use serde::Serialize;

mod error;
pub mod file_snapshotter;

const NUMBER_OF_PERSISTED_SNAPSHOTS: usize = 2;

#[cfg_attr(test, automock)]
#[async_trait]
pub trait Snapshotter<S: Serialize + DeserializeOwned + Send + Sync>:
    Send + Sync + std::fmt::Debug
{
    async fn snapshot(&mut self, data: &S) -> Result<()>;
    fn get_snapshot(&self) -> Option<S>;
}
