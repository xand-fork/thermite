use lazy_static::lazy_static;
#[cfg(not(test))]
use pipesrv_emitter::spin_up_emitter;
use std::string::ToString;
use xand_logging_metrics::LoggingMetricsBag;
use xand_metrics::{
    DimensionedCtrDef, DimensionedHistoDef, GaugeDef, MetricOpts, Metrics, MetricsBag,
};
use xand_metrics_prometheus::PromMetrics;
use xand_models::{Transaction, TransactionStatus};

lazy_static! {
    static ref VALIDATOR_METRICS: ValidatorMetricsBag = {
        let mut validator_metrics_bag = ValidatorMetricsBag::default();
        let metrics = PromMetrics::default();
        metrics.initialize(&mut validator_metrics_bag).unwrap();
        LoggingMetricsBag::init(&metrics).unwrap();
        #[cfg(not(test))]
        spin_up_emitter(5, Box::new(metrics));
        validator_metrics_bag
    };
}

#[derive(MetricsBag)]
pub struct ValidatorMetricsBag {
    pub txn_count: DimensionedCtrDef,
    pub txn_amount: DimensionedHistoDef,
    /// Number of transactions that can be added to the cache before it is full
    pub remaining_transaction_cache: GaugeDef,
    /// How full the transaction cache is in percentage of its maximum capacity (`[0, 1]`)
    pub percentage_transaction_cache: GaugeDef,
}

impl Default for ValidatorMetricsBag {
    fn default() -> Self {
        Self {
            txn_count: DimensionedCtrDef::new(
                vec!["type", "status"],
                MetricOpts::new(
                    "transaction_counter".to_string(),
                    "Track sends, creates, and redeems and their status of committed or invalid".to_string(),
                ),
            ),
            txn_amount: DimensionedHistoDef::new(
                vec!["type", "status"],
                MetricOpts::new(
                    "transaction_amount_histogram".to_string(),
                    "Track the amount moved in sends, creates, and redeems".to_string(),
                ),
            ),
            remaining_transaction_cache: GaugeDef::new(
                "remaining_transaction_cache",
                "Track the number of transactions that can be added to the cache before it is full",
            ),
            percentage_transaction_cache: GaugeDef::new(
                "percentage_transaction_cache",
                "Track how full the transaction cache is in percentage of its maximum capacity ([0, 1])",
            ),
        }
    }
}

/// Function for sorting txns based on xand_models::XandTransaction
pub fn meter_txn(transaction: &Transaction) -> Option<()> {
    let txn_status = match transaction.status {
        TransactionStatus::Invalid(_) => Some("invalid"),
        TransactionStatus::Committed => Some("committed"),
        _ => None,
    }?;
    let txn_type = transaction.txn.to_string();
    let dims = vec![txn_type.as_ref(), txn_status];
    VALIDATOR_METRICS
        .txn_count
        .with_dims(dims.clone())
        .unwrap()
        .inc();

    let txn_amount = transaction.get_amount()?;
    VALIDATOR_METRICS
        .txn_amount
        .with_dims(dims)
        .unwrap()
        .observe(txn_amount as f64);
    Some(())
}

/// Record metrics for how full the cache is.
pub fn meter_cache_utilization(current: usize, max: usize) {
    VALIDATOR_METRICS
        .remaining_transaction_cache
        .set((max.saturating_sub(current)) as f64);
    VALIDATOR_METRICS
        .percentage_transaction_cache
        .set((current as f64 / max as f64).min(1.0));
}
