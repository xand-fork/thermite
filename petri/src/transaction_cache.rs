use crate::{historical_store::HistoricalStore, LastBlockInfo};
use chashmap::CHashMap;
use chrono::MIN_DATETIME;
use futures::{channel::oneshot, future::BoxFuture, FutureExt};
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use std::{
    borrow::Borrow,
    collections::VecDeque,
    ops::DerefMut,
    sync::{Mutex, RwLock},
    vec::IntoIter,
};
use xand_address::Address;
use xand_models::{
    CorrelationId, CreateCompletion, RedeemCompletion, Transaction, TransactionId,
    TransactionStatus, XandTransaction,
};

#[cfg(test)]
pub mod serialization_tests;

#[derive(Debug)]
pub struct TransactionCache {
    transactions_by_correlation_id: CHashMap<CorrelationId, Vec<TransactionId>>,
    transactions_by_address: CHashMap<Address, Vec<TransactionId>>,
    transactions_by_id: CHashMap<TransactionId, Transaction>,
    /// Transactions in create order from older to newer
    transaction_queue: Mutex<VecDeque<TransactionId>>,
    transaction_limit: RwLock<usize>,
    /// Information about the last block we fully processed
    last_processed_block_info: RwLock<LastBlockInfo>,
    /// Senders to notify when a transaction is cached.
    ///
    /// This allows synchronizing the delivery of a finalization update to Xand API clients with
    /// the caching of the transaction, so that clients can rely on the transaction being cached
    /// when receiving the finalization update.
    cache_observers: Mutex<Vec<(TransactionId, oneshot::Sender<()>)>>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
struct SerializableTransactionCache {
    transactions_by_correlation_id: Vec<(CorrelationId, Vec<TransactionId>)>,
    transactions_by_address: Vec<(Address, Vec<TransactionId>)>,
    transactions_by_id: Vec<(TransactionId, Transaction)>,
    transaction_queue: Vec<TransactionId>,
    // TODO: This is interesting since it is a param normally. We might want a way to override this
    //   when deserializing?
    //   Address in https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/7422
    transaction_cap: usize,
    last_processed_block_info: LastBlockInfo,
}

impl Serialize for TransactionCache {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        let serializable: SerializableTransactionCache = self.into();
        serializable.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for TransactionCache {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        let deserialized: SerializableTransactionCache = Deserialize::deserialize(deserializer)?;
        Ok(deserialized.into())
    }
}

impl From<&TransactionCache> for SerializableTransactionCache {
    fn from(tx_cache: &TransactionCache) -> Self {
        SerializableTransactionCache {
            transactions_by_correlation_id: tx_cache
                .transactions_by_correlation_id
                .clone()
                .into_iter()
                .collect(),
            transactions_by_address: tx_cache
                .transactions_by_address
                .clone()
                .into_iter()
                .collect(),
            transactions_by_id: tx_cache.transactions_by_id.clone().into_iter().collect(),
            transaction_queue: tx_cache
                .transaction_queue
                .lock()
                .expect(
                    "If this is poisoned, the cache is useless \
                           and the process probably has already panicked",
                )
                .clone()
                .into_iter()
                .collect(),
            transaction_cap: tx_cache.transaction_limit(),
            last_processed_block_info: tx_cache.last_processed_block_info(),
        }
    }
}

impl From<SerializableTransactionCache> for TransactionCache {
    fn from(serializable: SerializableTransactionCache) -> Self {
        TransactionCache {
            transactions_by_correlation_id: serializable
                .transactions_by_correlation_id
                .into_iter()
                .collect(),
            transactions_by_address: serializable.transactions_by_address.into_iter().collect(),
            transactions_by_id: serializable.transactions_by_id.into_iter().collect(),
            transaction_queue: Mutex::new(serializable.transaction_queue.into_iter().collect()),
            transaction_limit: RwLock::new(serializable.transaction_cap),
            last_processed_block_info: RwLock::new(serializable.last_processed_block_info),
            cache_observers: Default::default(),
        }
    }
}

impl Default for TransactionCache {
    fn default() -> Self {
        Self::with_transaction_limit(Self::DEFAULT_TRANSACTION_LIMIT)
    }
}

impl HistoricalStore for TransactionCache {
    type TransactionIter = IntoIter<Transaction>;

    fn store(&self, transaction: Transaction) {
        if self
            .transactions_by_id
            .contains_key(&transaction.transaction_id)
        {
            return;
        }
        info!(TransactionCacheLogging::CachingTransaction {
            value: transaction.transaction_id.to_string(),
            status: transaction.status.clone(),
        });

        let key = &transaction.signer_address;
        // Recording by ID must be done first as other maps assume theirs IDs are keys in the
        // by-id map.
        self.record_transaction_by_id(&transaction);
        self.record_transaction_by_address(key, &transaction);
        self.record_transaction_by_correlation_id(&transaction);
        self.record_related_transactions(&transaction);
        self.notify_observers(&transaction.transaction_id);
    }

    #[allow(clippy::needless_collect)]
    fn all_transactions(&self) -> Self::TransactionIter {
        // Collect IDs first so that the lock is not held later while the `CHashMap` is accessed
        // which has locks of its own. The goal is to avoid deadlocks now and in the future as this
        // code evolves.
        let ids = self
            .transaction_queue
            .lock()
            .unwrap()
            .iter()
            .rev()
            .cloned()
            .collect::<Vec<_>>();
        ids.into_iter()
            .filter_map(|id| self.existing_by_id(&id))
            .collect::<Vec<_>>()
            .into_iter()
    }

    fn by_address(&self, address: &Address) -> Self::TransactionIter {
        self.transactions_by_address
            .get(address)
            .map(|transactions| {
                transactions
                    .iter()
                    .rev()
                    .filter_map(|id| self.existing_by_id(id))
                    .collect::<Vec<_>>()
            })
            .unwrap_or_default()
            .into_iter()
    }

    fn by_id(&self, transaction_id: &TransactionId) -> Option<Transaction> {
        Some((*self.transactions_by_id.get(transaction_id)?).clone())
    }

    fn last_processed_block_info(&self) -> LastBlockInfo {
        // The result is unwrapped here because if the thread which writes to this lock panics
        // (which is the only condition that would cause a panic here), then the whole thread
        // with xand-api in it will be restarted. The trait will need to evolve when we use a real
        // DB to account for error handling, so adding a `Result` to function signature(s) is left
        // for then.
        *self
            .last_processed_block_info
            .read()
            .expect("Must be able to get read lock for last processed block info")
    }

    fn wait_for_transaction(&self, transaction_id: TransactionId) -> BoxFuture<'static, ()> {
        let mut observers = self
            .cache_observers
            .lock()
            .unwrap_or_else(|e| e.into_inner());
        if self.transactions_by_id.contains_key(&transaction_id) {
            return futures::future::ready(()).boxed();
        }
        let (sender, receiver) = futures::channel::oneshot::channel();
        observers.push((transaction_id, sender));
        receiver.map(|_| ()).boxed()
    }
}

fn log_cache_missing_id_bug(id: &TransactionId) {
    error!(TransactionCacheLogging::NoTransactionForIdBug { id: id.clone() });
    #[cfg(debug_assertions)]
    panic!(
        "The cache was expected to contain a transaction with ID {}",
        id,
    );
}

impl TransactionCache {
    pub const DEFAULT_TRANSACTION_LIMIT: usize = 125_000;

    pub fn with_transaction_limit(limit: usize) -> Self {
        // Beware that `TransactionCache::default` calls `with_transaction_limit`, so do no try
        // to shorten the following using functional update syntax and calling
        // `TransactionCache::default`. rustc won't hold your hand and keep you out of harm's way
        // if you attempt to do so.
        TransactionCache {
            transactions_by_correlation_id: Default::default(),
            transactions_by_address: Default::default(),
            transactions_by_id: Default::default(),
            transaction_queue: Default::default(),
            transaction_limit: RwLock::new(limit),
            last_processed_block_info: RwLock::new(LastBlockInfo {
                number: 0,
                timestamp: MIN_DATETIME,
            }),
            cache_observers: Default::default(),
        }
    }

    pub fn set_last_processed_block_info(&self, info: LastBlockInfo) -> Result<(), PoisonedRwLock> {
        if let Ok(mut v) = self.last_processed_block_info.write() {
            *v = info;
            Ok(())
        } else {
            Err(PoisonedRwLock)
        }
    }

    pub fn set_transaction_limit(&self, transaction_limit: usize) -> Result<(), PoisonedRwLock> {
        if let Ok(mut limit) = self.transaction_limit.write() {
            *limit = transaction_limit;
            Ok(())
        } else {
            Err(PoisonedRwLock)
        }
    }

    fn existing_by_id(&self, id: &TransactionId) -> Option<Transaction> {
        let transaction = self.by_id(id);
        if transaction.is_none() {
            log_cache_missing_id_bug(id);
        }
        transaction
    }

    pub fn get_by_correlation_id(
        &self,
        correlation_id: &CorrelationId,
    ) -> Result<Vec<Transaction>, CorrelationIdNotFoundError> {
        self.transactions_by_correlation_id
            .get(correlation_id)
            .map(|transactions| {
                transactions
                    .iter()
                    .rev()
                    .filter_map(|id| self.existing_by_id(id))
                    .collect()
            })
            .ok_or_else(|| CorrelationIdNotFoundError {
                _value: correlation_id.clone(),
            })
    }

    fn record_related_transactions(&self, transaction: &Transaction) {
        // Don't record correlating transactions if invalid
        if let TransactionStatus::Invalid(_) = transaction.status {
            return;
        }

        match &transaction.txn {
            XandTransaction::FulfillCreate(data) => {
                let matching = self.update_matching_create_transaction(
                    &data.correlation_id,
                    CreateCompletion::Confirmation(transaction.transaction_id.clone()),
                );
                self.link_related_transaction(matching, "create", transaction, "fulfillment");
            }
            XandTransaction::FulfillRedeem(data) => {
                let matching = self.update_matching_redeem_transaction(
                    &data.correlation_id,
                    RedeemCompletion::Confirmation(transaction.transaction_id.clone()),
                );
                self.link_related_transaction(matching, "redeem", transaction, "fulfillment");
            }
            XandTransaction::Send(data) => {
                self.record_transaction_by_address(&data.destination_account, transaction);
            }
            XandTransaction::CancelCreate(data) => {
                let matching = self.update_matching_create_transaction(
                    &data.correlation_id,
                    CreateCompletion::Cancellation(transaction.transaction_id.clone()),
                );
                self.link_related_transaction(matching, "create", transaction, "cancellation");
            }
            XandTransaction::CancelRedeem(data) => {
                let matching = self.update_matching_redeem_transaction(
                    &data.correlation_id,
                    RedeemCompletion::Cancellation(transaction.transaction_id.clone()),
                );
                self.link_related_transaction(matching, "redeem", transaction, "cancellation");
            }
            _ => {
                // do nothing
            }
        }
    }

    fn link_related_transaction(
        &self,
        original: Option<Transaction>,
        original_type: &str,
        related: &Transaction,
        related_type: &str,
    ) {
        if let Some(original) = original {
            self.record_transaction_by_address(&original.signer_address, related);
        } else {
            let msg = format!(
                "Could not find matching {} for {} id {:?}",
                original_type, related_type, related.transaction_id,
            );
            warn!(TransactionCacheLogging::MatchingTransactionNotFound { msg });
        }
    }

    fn update_matching_create_transaction(
        &self,
        correlation_id: &CorrelationId,
        related_transaction: CreateCompletion,
    ) -> Option<Transaction> {
        for mut t in resolved_transactions_mut(
            &*self.transactions_by_correlation_id.get(correlation_id)?,
            &self.transactions_by_id,
        ) {
            if let XandTransaction::CreatePendingCreate(create) = &mut t.txn {
                create.completing_transaction = Some(related_transaction);
                return Some((*t).clone());
            }
        }
        None
    }

    fn update_matching_redeem_transaction(
        &self,
        correlation_id: &CorrelationId,
        related_transaction: RedeemCompletion,
    ) -> Option<Transaction> {
        for mut t in resolved_transactions_mut(
            &*self.transactions_by_correlation_id.get(correlation_id)?,
            &self.transactions_by_id,
        ) {
            if let XandTransaction::CreatePendingRedeem(redeem) = &mut t.txn {
                redeem.completing_transaction = Some(related_transaction);
                return Some((*t).clone());
            }
        }
        None
    }

    fn record_transaction_by_correlation_id(&self, transaction: &Transaction) {
        let correlation_id = match transaction.get_correlation_id() {
            Some(correlation_id) => correlation_id.clone(),
            None => return,
        };
        let id = &transaction.transaction_id;
        self.transactions_by_correlation_id
            .alter(correlation_id, |ids| {
                let mut ids = ids.unwrap_or_default();
                if ids.iter().all(|txn_id| txn_id != id) {
                    ids.push(id.clone());
                }
                Some(ids)
            });
    }

    fn record_transaction_by_id(&self, transaction: &Transaction) {
        let full = self.transactions_by_id.len() >= self.transaction_limit();
        // The new transaction needs to be inserted before the oldest is removed to prevent a
        // concurrent transaction from stealing the free spot.
        self.transactions_by_id
            .insert(transaction.transaction_id.clone(), transaction.clone());
        self.transaction_queue
            .lock()
            .unwrap()
            .push_back(transaction.transaction_id.clone());
        if full {
            self.forget_oldest_transaction();
        }
    }

    fn record_transaction_by_address(&self, key: &Address, transaction: &Transaction) {
        let id = &transaction.transaction_id;
        self.transactions_by_address.alter(key.clone(), |ids| {
            let mut ids = ids.unwrap_or_default();
            if ids.iter().all(|txn_id| txn_id != id) {
                ids.push(id.clone());
            }
            Some(ids)
        });
    }

    fn forget_oldest_transaction(&self) {
        let oldest = self.transaction_queue.lock().unwrap().pop_front();
        let oldest = match oldest {
            Some(oldest) => oldest,
            None => return,
        };
        warn!(TransactionCacheLogging::CacheFullDroppingTransaction { id: oldest.clone() });
        let (correlation_id, address) = match self.transactions_by_id.get(&oldest) {
            Some(t) => (t.get_correlation_id().cloned(), t.signer_address.clone()),
            None => {
                log_cache_missing_id_bug(&oldest);
                return;
            }
        };
        if let Some(mut ids) = correlation_id
            .as_ref()
            .and_then(|correlation_id| self.transactions_by_correlation_id.get_mut(correlation_id))
        {
            ids.retain(|id| *id != oldest);
        }
        if let Some(mut ids) = self.transactions_by_address.get_mut(&address) {
            ids.retain(|id| *id != oldest);
        }
        // This needs to be done last as other maps assume theirs IDs are keys into the by-id map.
        self.transactions_by_id.remove(&oldest);
    }

    pub fn transaction_count(&self) -> usize {
        self.transactions_by_id.len()
    }

    pub fn transaction_limit(&self) -> usize {
        *self
            .transaction_limit
            .read()
            .expect("This should never be poisoned")
    }

    fn notify_observers(&self, new_transaction_id: &TransactionId) {
        let observers = {
            let mut all_observers = self
                .cache_observers
                .lock()
                .unwrap_or_else(|e| e.into_inner());
            let (ready_observers, pending_observers): (Vec<_>, _) = all_observers
                .drain(..)
                .partition(|(id, sender)| id == new_transaction_id || sender.is_canceled());
            *all_observers = pending_observers;
            ready_observers
        };
        for (_, observer) in observers {
            let _ = observer.send(());
        }
    }
}

fn resolved_transactions_mut<I, T>(
    it: I,
    transactions_by_id: &CHashMap<TransactionId, Transaction>,
) -> impl Iterator<Item = impl DerefMut<Target = Transaction> + '_>
where
    I: IntoIterator<Item = T>,
    T: Borrow<TransactionId>,
{
    it.into_iter().filter_map(move |id| {
        let id = id.borrow();
        let t = transactions_by_id.get_mut(id);
        if t.is_none() {
            log_cache_missing_id_bug(id);
        }
        t
    })
}

#[derive(Clone, Debug)]
pub struct CorrelationIdNotFoundError {
    _value: CorrelationId,
}

#[derive(Clone, Debug)]
pub struct PoisonedRwLock;

#[logging_event]
pub enum TransactionCacheLogging {
    CachingTransaction {
        value: String,
        status: TransactionStatus,
    },
    MatchingTransactionNotFound {
        msg: String,
    },
    /// This indicates a broken invariant in the code; this is a bug that must be fixed.
    NoTransactionForIdBug {
        id: TransactionId,
    },
    CacheFullDroppingTransaction {
        id: TransactionId,
    },
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use chrono::{TimeZone, Utc};
    use futures::{
        executor::block_on,
        future::{
            select,
            Either::{Left, Right},
        },
    };
    use futures_timer::Delay;
    use sp_core::{crypto::Ss58Codec, sr25519, Pair};
    use std::{convert::TryInto, sync::atomic::AtomicU64, time::Duration};
    use xand_address::Address;
    use xand_ledger::{CreateCancellationReason, RedeemCancellationReason};
    use xand_models::{
        dev_account_key, BankAccountInfo, CancelPendingCreate, CancelPendingRedeem, CorrelationId,
        FulfillPendingCreate, FulfillPendingRedeem, PendingCreate, PendingRedeem, SendSchema,
        ToAddress, Transaction, TransactionId, TransactionStatus, XandTransaction,
    };

    fn test_addr(name: &str) -> Address {
        sr25519::Pair::from_string(name, None)
            .unwrap()
            .public()
            .to_ss58check()
            .try_into()
            .unwrap()
    }

    fn tupac() -> Address {
        test_addr("//2Pac")
    }

    fn biggie() -> Address {
        test_addr("//Biggie")
    }

    fn next_transaction_id() -> TransactionId {
        static COUNTER: AtomicU64 = AtomicU64::new(0);
        let mut id = [0; 32];
        let counter = COUNTER.fetch_add(1, std::sync::atomic::Ordering::SeqCst);
        id[..std::mem::size_of_val(&counter)].copy_from_slice(&counter.to_ne_bytes());
        id.into()
    }

    fn make_transaction(
        signer_address: Address,
        status: TransactionStatus,
        txn: XandTransaction,
    ) -> Transaction {
        let timestamp = Utc.ymd(2020, 2, 5).and_hms(6, 0, 0);
        Transaction::from_xand_txn(
            next_transaction_id(),
            txn,
            signer_address,
            status,
            timestamp,
        )
    }

    fn make_pending_transaction(signer: Address, txn: XandTransaction) -> Transaction {
        make_transaction(signer, TransactionStatus::Pending, txn)
    }

    fn make_committed_transaction(signer: Address, txn: XandTransaction) -> Transaction {
        make_transaction(signer, TransactionStatus::Committed, txn)
    }

    const AMOUNT_IN_MINOR_UNIT: u64 = 42;

    fn make_pending_create(correlation_id: CorrelationId) -> XandTransaction {
        XandTransaction::CreatePendingCreate(PendingCreate {
            account: BankAccountInfo::Unencrypted(Default::default()),
            amount_in_minor_unit: AMOUNT_IN_MINOR_UNIT,
            correlation_id,
            completing_transaction: None,
        })
    }

    fn make_pending_redeem(correlation_id: CorrelationId) -> XandTransaction {
        XandTransaction::CreatePendingRedeem(PendingRedeem {
            account: BankAccountInfo::Unencrypted(Default::default()),
            amount_in_minor_unit: AMOUNT_IN_MINOR_UNIT,
            correlation_id,
            completing_transaction: None,
        })
    }

    fn make_send() -> SendSchema {
        SendSchema {
            amount_in_minor_unit: AMOUNT_IN_MINOR_UNIT,
            destination_account: biggie(),
        }
    }

    fn update_related_transaction(initial: &mut Transaction, consequence: &Transaction) {
        match (&mut initial.txn, &consequence.txn) {
            (XandTransaction::CreatePendingCreate(create), XandTransaction::FulfillCreate(_)) => {
                create.completing_transaction = Some(CreateCompletion::Confirmation(
                    consequence.transaction_id.clone(),
                ));
            }
            (XandTransaction::CreatePendingRedeem(redeem), XandTransaction::FulfillRedeem(_)) => {
                redeem.completing_transaction = Some(RedeemCompletion::Confirmation(
                    consequence.transaction_id.clone(),
                ));
            }
            (XandTransaction::CreatePendingCreate(create), XandTransaction::CancelCreate(_)) => {
                create.completing_transaction = Some(CreateCompletion::Cancellation(
                    consequence.transaction_id.clone(),
                ));
            }
            (XandTransaction::CreatePendingRedeem(redeem), XandTransaction::CancelRedeem(_)) => {
                redeem.completing_transaction = Some(RedeemCompletion::Cancellation(
                    consequence.transaction_id.clone(),
                ));
            }
            _ => {}
        }
    }

    #[test]
    fn create_request_get_by_address() {
        // Given
        let cache = TransactionCache::default();
        let create_request =
            make_pending_transaction(tupac(), make_pending_create(CorrelationId::gen_random()));

        // When
        cache.store(create_request.clone());

        // Then
        let transactions = cache.by_address(&create_request.signer_address);
        assert_eq!(transactions.as_slice()[0], create_request);
    }

    #[test]
    fn cache_payment() {
        // Given
        let cache = TransactionCache::default();
        let send = make_send();
        let payment = make_pending_transaction(tupac(), XandTransaction::Send(send.clone()));

        // When
        cache.store(payment.clone());

        // Then
        let signer_transactions = cache.by_address(&payment.signer_address);
        let destination_transactions = cache.by_address(&send.destination_account);
        assert_eq!(signer_transactions.as_slice()[0], payment);
        assert_eq!(destination_transactions.as_slice()[0], payment);
    }

    #[test]
    fn update_payment() {
        // Given
        let cache = TransactionCache::default();
        let send = make_send();
        let payment = make_pending_transaction(tupac(), XandTransaction::Send(send.clone()));

        let mut new = payment.clone();
        new.status = TransactionStatus::Pending;

        // When
        cache.store(payment.clone());
        cache.store(new.clone());

        // Then
        let signer_transactions = cache.by_address(&payment.signer_address);
        let destination_transactions = cache.by_address(&send.destination_account);
        assert_eq!(signer_transactions.as_slice()[0], new);
        assert_eq!(destination_transactions.as_slice()[0], new);
    }

    #[test]
    fn most_recent_transactions_are_first() {
        // Given
        let cache = TransactionCache::default();
        let send = make_send();
        let payment = make_committed_transaction(tupac(), XandTransaction::Send(send.clone()));

        let mut new = payment.clone();
        new.status = TransactionStatus::Pending;
        new.transaction_id = next_transaction_id();

        // When
        cache.store(payment.clone());
        cache.store(new.clone());

        // Then
        let signer_transactions = cache.by_address(&payment.signer_address);
        let destination_transactions = cache.by_address(&send.destination_account);
        assert_eq!(signer_transactions.as_slice()[0], new);
        assert_eq!(destination_transactions.as_slice()[0], new);
    }

    #[test]
    fn get_transaction_by_id() {
        // Given
        let cache = TransactionCache::default();
        let send = make_send();
        let payment = make_pending_transaction(tupac(), XandTransaction::Send(send));

        // When
        cache.store(payment.clone());

        // Then
        let result = cache.by_id(&payment.transaction_id);
        assert_eq!(result, Some(payment));
    }

    #[test]
    fn get_by_correlation_id_create_request_and_create() {
        // Given
        let cache = TransactionCache::default();
        let correlation_id = CorrelationId::gen_random();
        let mut create_request =
            make_committed_transaction(tupac(), make_pending_create(correlation_id.clone()));
        let create = make_committed_transaction(
            biggie(),
            XandTransaction::FulfillCreate(FulfillPendingCreate {
                correlation_id: correlation_id.clone(),
            }),
        );

        // When
        cache.store(create_request.clone());
        cache.store(create.clone());
        update_related_transaction(&mut create_request, &create);

        // Then
        let transactions = cache.get_by_correlation_id(&correlation_id).unwrap();
        assert_eq!(transactions, [create, create_request]);
    }

    #[test]
    fn get_by_correlation_id_create_request_and_cancellation() {
        // Given
        let cache = TransactionCache::default();
        let correlation_id = CorrelationId::gen_random();
        let mut create_request =
            make_committed_transaction(tupac(), make_pending_create(correlation_id.clone()));
        let cancellation = make_committed_transaction(
            biggie(),
            XandTransaction::CancelCreate(CancelPendingCreate {
                correlation_id: correlation_id.clone(),
                reason: CreateCancellationReason::Expired,
            }),
        );

        // When
        cache.store(create_request.clone());
        cache.store(cancellation.clone());
        update_related_transaction(&mut create_request, &cancellation);

        // Then
        let transactions = cache.get_by_correlation_id(&correlation_id).unwrap();
        assert_eq!(transactions, [cancellation, create_request]);
    }

    #[test]
    fn get_by_correlation_id_create_request_and_cancellation_from_event() {
        // Given
        let cache = TransactionCache::default();
        let correlation_id = CorrelationId::gen_random();
        let create_request =
            make_committed_transaction(tupac(), make_pending_create(correlation_id.clone()));
        let completion = CreateCompletion::Expiration(create_request.transaction_id.clone());

        // When
        cache.store(create_request);
        cache.update_matching_create_transaction(&correlation_id, completion.clone());

        // Then
        let transactions = cache.get_by_correlation_id(&correlation_id).unwrap();
        assert_eq!(transactions.len(), 1);
        assert!(matches!(&transactions[0], Transaction {
            txn: XandTransaction::CreatePendingCreate(PendingCreate {
                    completing_transaction: Some(c),
                    ..
                }),
            ..
        } if *c == completion));
    }

    #[test]
    fn create_is_present_in_address_of_initiator() {
        // Given
        let cache = TransactionCache::default();
        let correlation_id = CorrelationId::gen_random();
        let mut create_request =
            make_committed_transaction(tupac(), make_pending_create(correlation_id.clone()));
        let create = make_committed_transaction(
            biggie(),
            XandTransaction::FulfillCreate(FulfillPendingCreate { correlation_id }),
        );

        // When
        cache.store(create_request.clone());
        cache.store(create.clone());
        update_related_transaction(&mut create_request, &create);

        // Then
        let transactions = cache.by_address(&tupac());
        assert_eq!(transactions.as_slice(), [create, create_request]);
    }

    #[test]
    fn get_by_correlation_id_redeem_request_and_redeem() {
        // Given
        let cache = TransactionCache::default();
        let correlation_id = CorrelationId::gen_random();
        let mut redeem_request =
            make_committed_transaction(tupac(), make_pending_redeem(correlation_id.clone()));
        let redeem = make_committed_transaction(
            biggie(),
            XandTransaction::FulfillRedeem(FulfillPendingRedeem {
                correlation_id: correlation_id.clone(),
            }),
        );

        // When
        cache.store(redeem_request.clone());
        cache.store(redeem.clone());
        update_related_transaction(&mut redeem_request, &redeem);

        // Then
        let transactions = cache.get_by_correlation_id(&correlation_id).unwrap();
        assert_eq!(transactions, [redeem, redeem_request]);
    }

    #[test]
    fn get_by_correlation_id_redeem_request_and_cancellation() {
        // Given
        let cache = TransactionCache::default();
        let correlation_id = CorrelationId::gen_random();
        let mut redeem_request =
            make_committed_transaction(tupac(), make_pending_redeem(correlation_id.clone()));
        let cancellation = make_committed_transaction(
            biggie(),
            XandTransaction::CancelRedeem(CancelPendingRedeem {
                correlation_id: correlation_id.clone(),
                reason: RedeemCancellationReason::InvalidData,
            }),
        );

        // When
        cache.store(redeem_request.clone());
        cache.store(cancellation.clone());
        update_related_transaction(&mut redeem_request, &cancellation);

        // Then
        let transactions = cache.get_by_correlation_id(&correlation_id).unwrap();
        assert_eq!(transactions, [cancellation, redeem_request]);
    }

    #[test]
    fn redeem_is_present_in_address_of_initiator() {
        // Given
        let cache = TransactionCache::default();
        let correlation_id = CorrelationId::gen_random();
        let mut redeem_request =
            make_committed_transaction(tupac(), make_pending_redeem(correlation_id.clone()));
        let redeem = make_committed_transaction(
            biggie(),
            XandTransaction::FulfillRedeem(FulfillPendingRedeem { correlation_id }),
        );

        // When
        cache.store(redeem_request.clone());
        cache.store(redeem.clone());
        update_related_transaction(&mut redeem_request, &redeem);

        // Then
        let transactions = cache.by_address(&tupac());
        assert_eq!(transactions.as_slice(), [redeem, redeem_request]);
    }

    #[test]
    fn cache_retrieves_member_marked_for_removal_txn_for_member_address() {
        // Given
        let cache = TransactionCache::default();
        let member = dev_account_key("2Pac");
        let member2 = dev_account_key("Biggie");
        let member_addr = member.public().to_address();
        let member2_addr = member2.public().to_address();
        let remove_member = make_committed_transaction(
            member_addr.clone(),
            XandTransaction::RemoveMember(xand_models::RemoveMember {
                address: member_addr.clone(),
            }),
        );

        // When
        cache.store(remove_member.clone());

        // Then
        let member_transactions = cache.by_address(&member_addr);
        assert!(member_transactions.as_slice().contains(&remove_member));
        let member2_transactions = cache.by_address(&member2_addr);
        assert!(!member2_transactions.as_slice().contains(&remove_member));
    }

    #[test]
    fn cache_retrieves_member_final_exit_txn_for_member_address() {
        // Given
        let cache = TransactionCache::default();
        let member = dev_account_key("2Pac");
        let member2 = dev_account_key("Biggie");
        let member_addr = member.public().to_address();
        let member2_addr = member2.public().to_address();
        let remove_member = make_committed_transaction(
            member_addr.clone(),
            XandTransaction::RemoveMember(xand_models::RemoveMember {
                address: member_addr.clone(),
            }),
        );

        let exit_member = make_committed_transaction(
            member_addr.clone(),
            XandTransaction::ExitMember(xand_models::ExitMember {
                address: member_addr.clone(),
            }),
        );

        // When
        cache.store(remove_member);
        cache.store(exit_member.clone());

        // Then
        let member_transactions = cache.by_address(&member_addr);
        assert!(member_transactions.as_slice().contains(&exit_member));
        let member2_transactions = cache.by_address(&member2_addr);
        assert!(!member2_transactions.as_slice().contains(&exit_member));
    }

    #[test]
    fn oldest_transaction_is_dropped_when_limit_is_reached() {
        let cache = TransactionCache::with_transaction_limit(1);
        let send = make_pending_transaction(tupac(), XandTransaction::Send(make_send()));
        let create =
            make_pending_transaction(tupac(), make_pending_create(CorrelationId::gen_random()));
        cache.store(send);
        cache.store(create.clone());
        assert_eq!(cache.by_address(&tupac()).as_slice(), [create]);
    }

    #[test]
    fn caching_transaction_does_not_replace_cached_transaction_with_same_id() {
        let cache = TransactionCache::default();
        let create = PendingCreate {
            account: BankAccountInfo::Unencrypted(Default::default()),
            amount_in_minor_unit: AMOUNT_IN_MINOR_UNIT,
            correlation_id: CorrelationId::gen_random(),
            completing_transaction: Some(xand_models::CreateCompletion::Confirmation(
                TransactionId::default(),
            )),
        };
        let transaction = make_committed_transaction(
            tupac(),
            XandTransaction::CreatePendingCreate(create.clone()),
        );
        cache.store(transaction.clone());
        let create_without_confirmation = PendingCreate {
            completing_transaction: None,
            ..create
        };
        let transaction_without_confirmation = {
            let mut transaction = transaction.clone();
            transaction.txn = XandTransaction::CreatePendingCreate(create_without_confirmation);
            transaction
        };
        cache.store(transaction_without_confirmation);
        let cached_transaction = cache.by_id(&transaction.transaction_id);
        assert_eq!(cached_transaction, Some(transaction));
    }

    #[test]
    fn waiting_for_already_cached_transaction_completes_immediately() {
        let cache = TransactionCache::default();
        let transaction = XandTransaction::Send(make_send());
        let transaction = make_committed_transaction(tupac(), transaction);
        let id = transaction.transaction_id.clone();
        cache.store(transaction);
        block_on(cache.wait_for_transaction(id));
    }

    #[test]
    fn waiting_for_transaction_not_in_cache_completes_when_transaction_is_cached() {
        let cache = TransactionCache::default();
        let transaction = XandTransaction::Send(make_send());
        let transaction = make_committed_transaction(tupac(), transaction);
        let id = transaction.transaction_id.clone();
        let wait = block_on(select(
            cache.wait_for_transaction(id),
            Delay::new(Duration::from_millis(100)),
        ));
        let txn_wait = match wait {
            Right(((), txn_wait)) => txn_wait,
            Left(_) => panic!("Transaction should not be cached yet"),
        };
        cache.store(transaction);
        block_on(txn_wait);
    }
}
