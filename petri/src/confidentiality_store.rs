pub mod mock;

use chrono::{DateTime, Utc};
use serde::Deserialize;
use thiserror::Error;
use xand_address::Address;
use xand_financial_client::models::TransactionOutputData;
use xand_ledger::{IdentityTag, PublicKey, TransactionOutput};
use xand_models::CorrelationId;

pub trait TxoStore {
    fn get_txo(&self, id: &TxoHash) -> Option<TransactionOutputRecord>;
    fn insert(&mut self, txo: TransactionOutputRecord);
    fn get_txos_by_correlation_id(
        &self,
        correlation_id: &CorrelationId,
    ) -> Vec<TransactionOutputRecord>;
    fn update_txo_status_by_correlation_id(
        &mut self,
        correlation_id: &CorrelationId,
        new_status: TxoSpentStatus,
    );
    fn update_txo_status_by_hash(&mut self, txo_hash: TxoHash, status: TxoSpentStatus);
    fn remove_txos_by_correlation_id(&mut self, correlation_id: &CorrelationId);

    /// Returns balance for this address.
    ///
    /// If no txo exists for this address, [TxoStoreError::BalanceNotFound] is returned.
    fn balance(&self, address: &Address) -> Result<u64, TxoStoreError>;

    fn get_timestamp_for_txo(&self, id: &TxoHash) -> Option<DateTime<Utc>> {
        self.get_txo(id).map(|txo| txo.timestamp)
    }
}

pub trait IdTagStore {
    /// Retrieve ID tag by hash
    fn get_id_tag(&self, id_hash: &IdTagHash) -> Option<IdTagRecord>;

    /// Retrieve ID tags that are owned by the given public key
    fn get_known_tags(&self, account: &PublicKey) -> Vec<IdTagRecord>;

    /// Record an ID tag that may or may not have a known owner. Inserting two different ID tags
    /// with the same correlation id will cause an error as they are a 1:1 relationship.
    fn insert(&mut self, tag: IdTagRecord) -> Result<(), IdTagStoreError>;

    /// Update the status of an ID Tag by correlation id
    fn update_id_tag_by_correlation_id(
        &mut self,
        correlation_id: &CorrelationId,
        status: IdTagStatus,
    );

    /// Remove pending id tag by correlation id
    fn remove_id_tag_by_correlation_id(&mut self, correlation_id: &CorrelationId);

    /// Remove specific id tag
    fn remove(&mut self, tag: &IdentityTag);
}

#[derive(Debug, Error)]
pub enum TxoStoreError {
    #[error("Account balance was not found")]
    BalanceNotFound,
    #[error("Account balance overflowed")]
    BalanceOverflow,
}

#[derive(Debug, Error)]
pub enum IdTagStoreError {
    #[error("The correlation id is already mapped to different ID tag.")]
    CorrelationIdCollision,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum TxoSpentStatus {
    Spent,
    UnSpent,
    Pending,
}

pub type TxoHash = [u8; 32];

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct TransactionOutputRecord {
    pub txo: TransactionOutputData,
    pub status: TxoSpentStatus,
    /// The pending correlation ID associated with the outputs
    pub correlation_id: Option<CorrelationId>,
    pub timestamp: DateTime<Utc>,
}

impl TransactionOutputRecord {
    pub fn to_hash(&self) -> TxoHash {
        match &self.txo {
            TransactionOutputData::Private(txo, _) => txo.transaction_output.to_hash(),
            TransactionOutputData::Public(txo) => txo.to_hash(),
        }
    }
}

impl From<TransactionOutputRecord> for TransactionOutput {
    fn from(record: TransactionOutputRecord) -> Self {
        match record.txo {
            TransactionOutputData::Private(txo, _key) => txo.transaction_output,
            TransactionOutputData::Public(txo) => txo,
        }
    }
}

pub type IdTagHash = [u8; 32];

#[derive(Copy, Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum IdTagStatus {
    Valid,
    Pending,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct IdTagRecord {
    pub tag: IdentityTag,
    pub owner: Option<PublicKey>,
    pub status: IdTagStatus,
    pub correlation_id: Option<CorrelationId>,
}

impl IdTagRecord {
    pub fn to_hash(&self) -> IdTagHash {
        self.tag.to_hash()
    }
}
