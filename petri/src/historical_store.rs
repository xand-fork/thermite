use chrono::{DateTime, Utc};
use futures::future::BoxFuture;
use serde_derive::Deserialize;
use xand_address::Address;
use xand_models::{Transaction, TransactionId};
use xandstrate_runtime::BlockNumber;

/// Implementors of this trait provide access to a historical view of the blockchain.
/// Essentially, a cache. Current state of the chain is readily available at any
/// validator/client, but looking up past transactions is not necessarily easy or performant,
/// hence the need for such a cache.
///
/// TODO: Eventually it makes sense for this trait to be async, since it will be I/O bound.
///   -- while there are still remnants of old petri that conversion is a bit onerous, so
///   conversion is left until needed.
#[auto_impl::auto_impl(&, Box, Arc, Rc)]
pub trait HistoricalStore: Send + Sync {
    /// Because the returned transaction histories may be very large, the store implementation
    /// must return some iterator (later, stream) over them. It specifies what type.
    type TransactionIter: Iterator<Item = Transaction>;

    /// Store a given transaction
    ///
    /// If a transaction with the same ID has already been stored, it is not replaced.
    fn store(&self, transaction: Transaction);

    /// Returns all transactions in the cache from newer to older.
    fn all_transactions(&self) -> Self::TransactionIter;

    /// Given an address, return all transactions which were issued by it or transactions that
    /// affected it in any way.
    ///
    /// List of "affecting" transactions:
    /// * Create fulfills to the address
    /// * Redeem fulfills from the address
    /// * Send to the address
    fn by_address(&self, address: &Address) -> Self::TransactionIter;

    /// Returns the transaction with the given id, if it exists
    fn by_id(&self, transaction_id: &TransactionId) -> Option<Transaction>;

    /// Returns information about the last block that this store processed. This can be used to
    /// determine whether or not information in the store is stale.
    fn last_processed_block_info(&self) -> LastBlockInfo;

    /// Returns a future that resolves when the transaction is cached. If it is already cached, the
    /// future resolves immediately.
    fn wait_for_transaction(&self, transaction_id: TransactionId) -> BoxFuture<'static, ()>;
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq)]
pub struct LastBlockInfo {
    pub number: BlockNumber,
    pub timestamp: DateTime<Utc>,
}
