use crate::{
    confidentiality_store::IdTagHash, id_tag_cache::IdTagCacheInner,
    test_helpers::arbitrary_id_tag_record, IdTagCache, IdTagRecord,
};
use proptest::collection::vec;
use proptest::prelude::*;
use std::sync::{Arc, Mutex, RwLock};
use xand_ledger::PublicKey;
use xand_models::CorrelationId;

pub fn assert_relevant_data_is_equal<R>(cache_one: &IdTagCache<R>, cache_two: &IdTagCache<R>) {
    id_tags_match(cache_one, cache_two);
    known_id_tags_match(cache_one, cache_two);
    tags_by_correlation_id_match(cache_one, cache_two);
}

fn id_tags_match<R>(cache_one: &IdTagCache<R>, cache_two: &IdTagCache<R>) {
    let inner_one = (*cache_one.0).read().unwrap();
    let inner_two = (*cache_two.0).read().unwrap();
    assert_eq!(inner_one.id_tags(), inner_two.id_tags())
}

fn known_id_tags_match<R>(cache_one: &IdTagCache<R>, cache_two: &IdTagCache<R>) {
    let inner_one = (*cache_one.0).read().unwrap();
    let inner_two = (*cache_two.0).read().unwrap();
    assert_eq!(inner_one.known_id_tags(), inner_two.known_id_tags())
}

fn tags_by_correlation_id_match<R>(cache_one: &IdTagCache<R>, cache_two: &IdTagCache<R>) {
    let inner_one = (*cache_one.0).read().unwrap();
    let inner_two = (*cache_two.0).read().unwrap();
    assert_eq!(
        inner_one.tags_by_correlation_id(),
        inner_two.tags_by_correlation_id()
    )
}

fn id_tag_pair(tag: &IdTagRecord) -> (IdTagHash, IdTagRecord) {
    let id = tag.to_hash();
    (id, tag.clone())
}

fn known_id_tags_pair(tag: &IdTagRecord) -> Option<(PublicKey, Vec<IdTagHash>)> {
    tag.owner.map(|pubkey| (pubkey, vec![tag.to_hash()]))
}

fn tags_by_correlation_id_pair(tag: &IdTagRecord) -> Option<(CorrelationId, IdTagHash)> {
    tag.correlation_id
        .as_ref()
        .map(|correlation_id| (correlation_id.clone(), tag.to_hash()))
}

prop_compose! {
    pub fn arbitrary_id_tag_cache()(
        recs in vec(arbitrary_id_tag_record(), 0..100)
    ) -> IdTagCache<()> {
         let id_tags = recs.iter().map(id_tag_pair).collect();
         let known_id_tags = recs.iter().filter_map(known_id_tags_pair).collect();
         let tags_by_correlation_id = recs.iter().filter_map(tags_by_correlation_id_pair).collect();
         let inner = IdTagCacheInner {
             id_tags,
             known_id_tags,
             tags_by_correlation_id,
             csprng: Mutex::new(()),
         };
         #[allow(clippy::init_numbered_fields)]
         IdTagCache{ 0: Arc::new(RwLock::new(inner))}
    }
}

proptest! {
    #[test]
    fn serialize_deserialize_round_trip(
        expected in arbitrary_id_tag_cache()
    ) {
        let serialized = bincode::serialize(&expected).unwrap();
        let deserialized: IdTagCache<()> = bincode::deserialize(&serialized).unwrap();
        assert_relevant_data_is_equal(&expected, &deserialized);
    }
}
