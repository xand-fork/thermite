//! Contains logic for extracting and interpreting events emitted from the susbtrate runtime

use super::*;
use xandstrate_runtime::{network_voting, ConfidentialityEvent, ValidatorEmissionsEvent};

#[derive(Default, Debug)]
pub(crate) struct InterestingEvents<'a> {
    pub extrinsic_events: HashMap<u32, Vec<&'a Event>>,
    pub confidentiality_events: Vec<&'a ConfidentialityEvent<Runtime>>,
    pub _validator_emissions_events: Vec<&'a ValidatorEmissionsEvent<Runtime>>,
    pub network_voting_events: Vec<&'a network_voting::Event<Runtime>>,
}

pub(crate) fn extract_interesting_events(
    events_in_block: &'_ [EventRecord],
) -> InterestingEvents<'_> {
    let mut extrinsic_events = HashMap::new();
    let mut confidentiality_events = vec![];
    let mut _validator_emissions_events = vec![];
    let mut network_voting_events = vec![];
    for eventrec in events_in_block {
        match &eventrec.event {
            Event::xandstrate(_) => (),
            Event::confidentiality(e) => confidentiality_events.push(e),
            Event::validator_emissions(e) => _validator_emissions_events.push(e),
            Event::network_voting(e) => network_voting_events.push(e),
            Event::system(_) => (),
            Event::pallet_grandpa(_) => (),
            Event::pallet_sudo(_) => (),
            Event::xandvalidators(_) => (),
            Event::pallet_session(_) => (),
            #[cfg(feature = "test-upgrade-pallet")]
            Event::test_upgrade(_) => (),
        }

        // Events associated with extrinsics
        if let Phase::ApplyExtrinsic(ext_ix) = eventrec.phase {
            let new_event = &eventrec.event;
            extrinsic_events
                .entry(ext_ix)
                .and_modify(|evlist: &mut Vec<&Event>| evlist.push(new_event))
                .or_insert_with(|| vec![new_event]);
        }
    }
    InterestingEvents {
        extrinsic_events,
        confidentiality_events,
        _validator_emissions_events,
        network_voting_events,
    }
}

/// This function expects to be passed all events that are associated with a particular
/// extrinsic -- it searches all of them to determine the status.
pub(crate) fn extract_txn_status_from_events(events: &[&Event]) -> TransactionStatus {
    events
        .iter()
        .rev()
        .find_map(|event| match event {
            Event::system(SystemEvent::<Runtime>::ExtrinsicSuccess(_)) => {
                // This code is specific to petri, petri only looks for finalized txns, therefore
                // this txn is finalized.
                Some(TransactionStatus::Finalized)
            }
            Event::system(SystemEvent::<Runtime>::ExtrinsicFailed(dispatch_err, _)) => {
                let msg = match dispatch_err {
                    DispatchError::Module {
                        message: Some(message),
                        ..
                    } => (*message).to_string(),
                    _ => format!("{:?}", dispatch_err),
                };
                Some(TransactionStatus::Invalid(msg))
            }
            _ => None,
        })
        .unwrap_or(TransactionStatus::Unknown)
}
