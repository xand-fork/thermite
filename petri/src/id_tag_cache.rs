use crate::confidentiality_store::{
    IdTagHash, IdTagRecord, IdTagStatus, IdTagStore, IdTagStoreError,
};
use async_trait::async_trait;
use rand::{seq::SliceRandom, Rng};
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use std::{
    collections::HashMap,
    sync::{Arc, Mutex, RwLock},
};
use xand_financial_client::{errors::FinancialClientError, IdentityTagSelector};
use xand_ledger::{IdentityTag, PublicKey};
use xand_models::CorrelationId;

#[cfg(test)]
pub mod serialization_tests;

#[derive(Clone, Default, Debug)]
pub struct IdTagCache<R>(Arc<RwLock<IdTagCacheInner<R>>>);

impl<R> Serialize for IdTagCache<R> {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        let serializable: SerializableIdTagCache = self.into();
        serializable.serialize(serializer)
    }
}

impl<'de, R: Default> Deserialize<'de> for IdTagCache<R> {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        let deserialized: SerializableIdTagCache = Deserialize::deserialize(deserializer)?;
        Ok(deserialized.into())
    }
}

#[derive(Debug)]
struct IdTagCacheInner<R> {
    /// Every encountered ID tag, whether its owner is known or not
    id_tags: HashMap<IdTagHash, IdTagRecord>,
    /// ID tags associated with a known owner
    known_id_tags: HashMap<PublicKey, Vec<IdTagHash>>,
    /// Tag associated with a particular correlation id
    tags_by_correlation_id: HashMap<CorrelationId, IdTagHash>,
    // Cryptographically-secure random number generator for ID tag selection
    csprng: Mutex<R>,
}

#[derive(Serialize, Deserialize)]
struct SerializableIdTagCache {
    pub id_tags: Vec<(IdTagHash, IdTagRecord)>,
    pub known_id_tags: Vec<(PublicKey, Vec<IdTagHash>)>,
    pub tags_by_correlation_id: Vec<(CorrelationId, IdTagHash)>,
}

impl<R> From<&IdTagCache<R>> for SerializableIdTagCache {
    fn from(cache: &IdTagCache<R>) -> Self {
        let inner = (*cache.0).read().expect(
            "If this is poisoned, the cache is useless \
                           and the process probably has already panicked",
        );
        SerializableIdTagCache {
            id_tags: inner.id_tags().clone().into_iter().collect(),
            known_id_tags: inner.known_id_tags().clone().into_iter().collect(),
            tags_by_correlation_id: inner.tags_by_correlation_id().clone().into_iter().collect(),
        }
    }
}

impl<R: Default> From<SerializableIdTagCache> for IdTagCache<R> {
    fn from(serializable: SerializableIdTagCache) -> Self {
        let inner = IdTagCacheInner {
            id_tags: serializable.id_tags.clone().into_iter().collect(),
            known_id_tags: serializable.known_id_tags.clone().into_iter().collect(),
            tags_by_correlation_id: serializable.tags_by_correlation_id.into_iter().collect(),
            csprng: Mutex::new(R::default()),
        };
        IdTagCache(Arc::new(RwLock::new(inner)))
    }
}

impl<R: Default> Default for IdTagCacheInner<R> {
    fn default() -> Self {
        Self::new(R::default())
    }
}

impl<R> IdTagCacheInner<R> {
    pub fn new(csprng: R) -> Self {
        Self {
            id_tags: HashMap::new(),
            known_id_tags: HashMap::new(),
            tags_by_correlation_id: HashMap::new(),
            csprng: Mutex::new(csprng),
        }
    }

    fn remove_tag_by_hash(&mut self, id_tag_hash: IdTagHash) {
        self.id_tags.remove(&id_tag_hash);
        self.known_id_tags.retain(|_, tag_hashes| {
            tag_hashes.retain(|h| *h != id_tag_hash);
            !tag_hashes.is_empty()
        });
        self.tags_by_correlation_id.retain(|_, h| *h != id_tag_hash);
    }

    pub fn id_tags(&self) -> &HashMap<IdTagHash, IdTagRecord> {
        &self.id_tags
    }
    pub fn known_id_tags(&self) -> &HashMap<PublicKey, Vec<IdTagHash>> {
        &self.known_id_tags
    }
    pub fn tags_by_correlation_id(&self) -> &HashMap<CorrelationId, IdTagHash> {
        &self.tags_by_correlation_id
    }
}

impl<R> IdTagCache<R> {
    pub fn new(csprng: R) -> Self {
        Self(Arc::new(RwLock::new(IdTagCacheInner::new(csprng))))
    }
}

impl<R> IdTagStore for IdTagCache<R> {
    fn get_id_tag(&self, id_hash: &IdTagHash) -> Option<IdTagRecord> {
        let inner = self.0.read().unwrap();
        inner.id_tags.get(id_hash).cloned()
    }

    fn get_known_tags(&self, account: &PublicKey) -> Vec<IdTagRecord> {
        let inner = self.0.read().unwrap();
        inner
            .known_id_tags
            .get(account)
            .map(|tags| {
                tags.iter()
                    .filter_map(|id| inner.id_tags.get(id).cloned())
                    .collect()
            })
            .unwrap_or_default()
    }

    fn insert(&mut self, tag: IdTagRecord) -> Result<(), IdTagStoreError> {
        let mut inner = self.0.write().unwrap();

        let id = tag.to_hash();

        // check for correlation id collision
        if let Some(correlation_id) = &tag.correlation_id {
            if let Some(other_txo_id) = inner.tags_by_correlation_id.get(correlation_id) {
                // if id tag hashes don't match, return an error
                if id != *other_txo_id {
                    return Err(IdTagStoreError::CorrelationIdCollision);
                }
            }
        }

        // insert primary record
        inner.id_tags.insert(id, tag.clone());

        // insert link to owner if any
        if let Some(owner) = &tag.owner {
            inner.known_id_tags.entry(*owner).or_default().push(id);
        }

        // insert link by correlation id if present
        if let Some(correlation_id) = tag.correlation_id {
            inner.tags_by_correlation_id.insert(correlation_id, id);
        }
        Ok(())
    }

    fn update_id_tag_by_correlation_id(
        &mut self,
        correlation_id: &CorrelationId,
        status: IdTagStatus,
    ) {
        let mut inner = self.0.write().unwrap();

        // cleanup correlation id index if status is valid
        if status == IdTagStatus::Valid {
            if let Some(tag_id) = inner.tags_by_correlation_id.remove(correlation_id) {
                if let Some(mut tag) = inner.id_tags.get_mut(&tag_id) {
                    tag.status = status;
                }
            }
        }
    }

    fn remove_id_tag_by_correlation_id(&mut self, correlation_id: &CorrelationId) {
        let mut inner = self.0.write().unwrap();
        if let Some(id_tag_id) = inner.tags_by_correlation_id.remove(correlation_id) {
            inner.remove_tag_by_hash(id_tag_id);
        }
    }

    fn remove(&mut self, tag: &IdentityTag) {
        let mut inner = self.0.write().unwrap();
        inner.remove_tag_by_hash(tag.to_hash());
    }
}

pub trait SelectionRng: rand::RngCore + rand::CryptoRng + Send + Sync {}

#[cfg(test)]
impl SelectionRng for crate::test_helpers::FakeCsprng {}

impl SelectionRng for rand::rngs::OsRng {}

#[async_trait]
impl<R> IdentityTagSelector for IdTagCache<R>
where
    R: SelectionRng,
{
    async fn get_owned_id_tag(&self, key: &PublicKey) -> Result<IdentityTag, FinancialClientError> {
        let mut tags = self.get_known_tags(key);
        tags.retain(|r| r.status == IdTagStatus::Valid);

        if tags.is_empty() {
            Err(FinancialClientError::NotFound)
        } else {
            let inner = self.0.read().unwrap();
            let mut csprng = inner.csprng.lock().unwrap();
            let rand_index: usize = csprng.gen_range(0, tags.len());

            Ok(tags
                .get(rand_index)
                .expect("random index expected to be in range")
                // TODO remove panic - https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6785
                .tag)
        }
    }

    async fn get_decoy_id_tags(
        &self,
        true_tag: &IdentityTag,
        num_decoys: u8,
    ) -> Result<Vec<IdentityTag>, FinancialClientError> {
        let inner = self.0.read().unwrap();

        let mut candidate_tags = inner
            .id_tags
            .values()
            .filter_map(|r| {
                if r.tag == *true_tag || r.status != IdTagStatus::Valid {
                    None
                } else {
                    Some(r.tag)
                }
            })
            .collect::<Vec<_>>();

        let mut csprng = inner.csprng.lock().unwrap();
        candidate_tags.shuffle(&mut *csprng);

        Ok(candidate_tags.into_iter().take(num_decoys.into()).collect())
    }
}

#[cfg(test)]
mod tests {
    use super::IdTagCache;
    use crate::confidentiality_store::{IdTagRecord, IdTagStatus, IdTagStore, IdTagStoreError};
    use crate::test_helpers::{arbitrary_id_tag_record, arbitrary_public_key, FakeCsprng};
    use proptest::prelude::*;
    use rand::rngs::OsRng;
    use xand_financial_client::{errors::FinancialClientError, IdentityTagSelector};
    use xand_ledger::{IdentityTag, PrivateKey, PublicKey};
    use xand_models::CorrelationId;

    proptest! {
        #[test]
        fn when_a_tag_is_inserted_without_an_associated_public_key_nothing_is_returned_for_a_given_public_key(
            id_tag_record in arbitrary_id_tag_record(),
            my_public_key in arbitrary_public_key()
        ) {
            let mut sut = IdTagCache::<FakeCsprng>::default();
            let item_to_insert = IdTagRecord {
                owner: None,
                ..id_tag_record
            };

            sut.insert(item_to_insert).unwrap();

            assert!(sut.get_known_tags(&my_public_key).is_empty());
        }
    }

    proptest! {
        #[test]
        fn when_a_tag_is_inserted_for_my_public_key_then_it_can_be_retrieved(
            id_tag_record in arbitrary_id_tag_record(),
            my_public_key in arbitrary_public_key()
        ) {
            let mut sut = IdTagCache::<FakeCsprng>::default();
            let item_to_insert = IdTagRecord {
                owner: Some(my_public_key),
                ..id_tag_record
            };

            sut.insert(item_to_insert.clone()).unwrap();

            assert_eq!(sut.get_known_tags(&my_public_key), vec![item_to_insert]);
        }
    }

    #[tokio::test]
    async fn when_a_tag_is_inserted_for_my_public_key_then_it_can_be_randomly_selected() {
        let mut csprng: OsRng = OsRng::default();
        let expected_tag = IdentityTag::random(&mut csprng);
        let my_public_key = generate_random_public_key();
        let item_to_insert = IdTagRecord {
            tag: expected_tag,
            owner: Some(my_public_key),
            status: IdTagStatus::Valid,
            correlation_id: None,
        };

        let mut sut = IdTagCache::<FakeCsprng>::default();

        sut.insert(item_to_insert.clone()).unwrap();

        let one_of_my_tags = sut.get_owned_id_tag(&my_public_key).await.unwrap();
        assert_eq!(one_of_my_tags, expected_tag)
    }

    proptest! {
        #[test]
        fn when_a_tag_is_inserted_for_another_public_key_then_it_cant_be_retrieved(
            id_tag_record in arbitrary_id_tag_record(),
            my_public_key in arbitrary_public_key(),
            another_public_key in arbitrary_public_key(),
        ) {
            prop_assume!(my_public_key != another_public_key);

            let mut sut = IdTagCache::<FakeCsprng>::default();
            let item_to_insert = IdTagRecord {
                owner: Some(another_public_key),
                ..id_tag_record
            };

            sut.insert(item_to_insert).unwrap();

            assert!(sut.get_known_tags(&my_public_key).is_empty())
        }
    }

    #[tokio::test]
    async fn when_a_tag_is_inserted_for_another_public_key_then_it_cant_be_randomly_selected_as_owned(
    ) {
        let mut csprng: OsRng = OsRng::default();
        let inner_tag = IdentityTag::random(&mut csprng);
        let other_public_key = generate_random_public_key();
        let item_to_insert = IdTagRecord {
            tag: inner_tag,
            owner: Some(other_public_key),
            status: IdTagStatus::Valid,
            correlation_id: None,
        };

        let mut sut = IdTagCache::<OsRng>::default();

        sut.insert(item_to_insert).unwrap();

        let my_public_key = generate_random_public_key();
        let result = sut.get_owned_id_tag(&my_public_key).await;

        assert!(matches!(result, Err(FinancialClientError::NotFound)));
    }

    #[tokio::test]
    async fn when_a_pending_tag_is_inserted_then_it_cant_be_randomly_selected_as_owned() {
        let mut csprng: OsRng = OsRng::default();
        let inner_tag = IdentityTag::random(&mut csprng);
        let my_public_key = generate_random_public_key();
        let item_to_insert = IdTagRecord {
            tag: inner_tag,
            owner: Some(my_public_key),
            status: IdTagStatus::Pending,
            correlation_id: None,
        };

        let mut sut = IdTagCache::<OsRng>::default();

        sut.insert(item_to_insert).unwrap();

        let result = sut.get_owned_id_tag(&my_public_key).await;

        assert!(matches!(result, Err(FinancialClientError::NotFound)));
    }

    #[tokio::test]
    async fn when_a_tag_is_inserted_for_an_unknown_tag_then_it_can_be_randomly_selected_as_decoy() {
        let mut csprng: OsRng = OsRng::default();
        let expected_tag = IdentityTag::random(&mut csprng);
        let item_to_insert = IdTagRecord {
            tag: expected_tag,
            owner: None,
            status: IdTagStatus::Valid,
            correlation_id: None,
        };

        let mut sut = IdTagCache::<OsRng>::default();

        sut.insert(item_to_insert).unwrap();

        let arbitrary_true_id_tag = IdentityTag::random(&mut csprng);
        let decoy_samples = sut
            .get_decoy_id_tags(&arbitrary_true_id_tag, 1u8)
            .await
            .unwrap();

        assert_eq!(decoy_samples, vec![expected_tag]);
    }

    #[tokio::test]
    async fn when_a_tag_is_inserted_for_a_known_tag_then_it_can_be_randomly_selected_as_decoy() {
        let mut csprng: OsRng = OsRng::default();
        let expected_tag = IdentityTag::random(&mut csprng);
        let known_owner = generate_random_public_key();
        let item_to_insert = IdTagRecord {
            tag: expected_tag,
            owner: Some(known_owner),
            status: IdTagStatus::Valid,
            correlation_id: None,
        };

        let mut sut = IdTagCache::<OsRng>::default();

        sut.insert(item_to_insert).unwrap();

        let my_true_id_tag = IdentityTag::random(&mut csprng);
        let decoy_samples = sut.get_decoy_id_tags(&my_true_id_tag, 1u8).await.unwrap();

        assert_eq!(decoy_samples, vec![expected_tag]);
    }

    #[tokio::test]
    async fn get_decoy_id_tags_cannot_return_specified_true_id_tag() {
        // It would be nice to make this deterministic and implement it as a proptest, but it's
        // sufficient to benefit from the non-deterministic coverage for now. If ~50% of its
        // runs fail and indicates a bug, the rest of the time it passes by chance as a false
        // positive.
        //
        // Proptest doesn't work well with async tests yet (see
        // https://github.com/AltSysrq/proptest/issues/179).

        let mut csprng: OsRng = OsRng::default();
        let expected_tag = IdentityTag::random(&mut csprng);
        let my_true_id_tag = IdentityTag::random(&mut csprng);

        let record1 = IdTagRecord {
            tag: expected_tag,
            owner: None,
            status: IdTagStatus::Valid,
            correlation_id: None,
        };
        let record2 = IdTagRecord {
            tag: my_true_id_tag,
            owner: Some(generate_random_public_key()),
            status: IdTagStatus::Valid,
            correlation_id: None,
        };

        let mut sut = IdTagCache::<OsRng>::default();

        sut.insert(record1).unwrap();
        sut.insert(record2).unwrap();

        let decoy_samples = sut.get_decoy_id_tags(&my_true_id_tag, 1u8).await.unwrap();

        assert_eq!(decoy_samples, vec![expected_tag]);
    }

    #[tokio::test]
    async fn get_decoy_tags_returns_all_tags_when_more_than_are_available_are_requested() {
        let mut csprng: OsRng = OsRng::default();
        let expected_tag = IdentityTag::random(&mut csprng);
        let my_true_id_tag = IdentityTag::random(&mut csprng);

        let record = IdTagRecord {
            tag: expected_tag,
            owner: None,
            status: IdTagStatus::Valid,
            correlation_id: None,
        };

        let mut sut = IdTagCache::<OsRng>::default();

        sut.insert(record).unwrap();

        let decoy_samples = sut
            .get_decoy_id_tags(&my_true_id_tag, u8::MAX)
            .await
            .unwrap();

        assert_eq!(decoy_samples, vec![expected_tag]);
    }

    #[tokio::test]
    async fn get_decoy_tags_does_not_return_any_tags_that_are_not_valid() {
        let mut csprng: OsRng = OsRng::default();
        let tag = IdentityTag::random(&mut csprng);
        let my_true_id_tag = IdentityTag::random(&mut csprng);

        let record = IdTagRecord {
            tag,
            owner: None,
            status: IdTagStatus::Pending,
            correlation_id: None,
        };

        let mut sut = IdTagCache::<OsRng>::default();

        sut.insert(record).unwrap();

        let decoy_samples = sut
            .get_decoy_id_tags(&my_true_id_tag, u8::MAX)
            .await
            .unwrap();

        assert!(decoy_samples.is_empty());
    }

    #[test]
    fn updating_id_tag_status_by_correlation_id_changes_status() {
        let mut csprng: OsRng = OsRng::default();
        let inner_tag = IdentityTag::random(&mut csprng);
        let corr_id = CorrelationId::gen_random();
        let owner = generate_random_public_key();
        let item_to_insert = IdTagRecord {
            tag: inner_tag,
            owner: Some(owner),
            status: IdTagStatus::Pending,
            correlation_id: Some(corr_id.clone()),
        };

        let mut sut = IdTagCache::<FakeCsprng>::default();

        sut.insert(item_to_insert).unwrap();

        sut.update_id_tag_by_correlation_id(&corr_id, IdTagStatus::Valid);

        let my_tag_status: Vec<IdTagStatus> = sut
            .get_known_tags(&owner)
            .iter()
            .map(|tag| tag.status)
            .collect();

        assert_eq!(my_tag_status, vec![IdTagStatus::Valid])
    }

    #[test]
    fn updating_id_tag_status_for_a_different_correlation_id_does_not_change_status_for_my_correlation_id(
    ) {
        let mut csprng: OsRng = OsRng::default();
        let inner_tag = IdentityTag::random(&mut csprng);
        let my_corr_id = CorrelationId::gen_random();
        let my_public_key = generate_random_public_key();
        let my_item_to_insert = IdTagRecord {
            tag: inner_tag,
            owner: Some(my_public_key),
            status: IdTagStatus::Pending,
            correlation_id: Some(my_corr_id),
        };

        let inner_tag = IdentityTag::random(&mut csprng);
        let other_corr_id = CorrelationId::gen_random();
        let other_public_key = generate_random_public_key();
        let other_item_to_insert = IdTagRecord {
            tag: inner_tag,
            owner: Some(other_public_key),
            status: IdTagStatus::Pending,
            correlation_id: Some(other_corr_id.clone()),
        };

        let mut sut = IdTagCache::<FakeCsprng>::default();

        sut.insert(my_item_to_insert).unwrap();
        sut.insert(other_item_to_insert).unwrap();

        sut.update_id_tag_by_correlation_id(&other_corr_id, IdTagStatus::Valid);

        let my_tag_status: Vec<IdTagStatus> = sut
            .get_known_tags(&my_public_key)
            .iter()
            .map(|tag| tag.status)
            .collect();

        assert_eq!(my_tag_status, vec![IdTagStatus::Pending])
    }

    #[test]
    fn removing_tag_by_correlation_id_removes_my_id_tag() {
        let mut csprng: OsRng = OsRng::default();
        let expected_tag = IdentityTag::random(&mut csprng);
        let correlation_id = CorrelationId::gen_random();
        let item_to_insert = IdTagRecord {
            tag: expected_tag,
            owner: None,
            status: IdTagStatus::Valid,
            correlation_id: Some(correlation_id.clone()),
        };

        let mut sut = IdTagCache::<FakeCsprng>::default();

        sut.insert(item_to_insert).unwrap();
        // expect inserted tag to be present
        sut.get_id_tag(&expected_tag.to_hash())
            .expect("missing expected id tag");

        sut.remove_id_tag_by_correlation_id(&correlation_id);
        let removed_tag = sut.get_id_tag(&expected_tag.to_hash());
        assert_eq!(removed_tag, None)
    }

    #[test]
    fn removing_other_tag_by_different_correlation_id_does_not_delete_my_tag() {
        let mut csprng: OsRng = OsRng::default();
        let inner_tag = IdentityTag::random(&mut csprng);
        let my_corr_id = CorrelationId::gen_random();
        let my_public_key = generate_random_public_key();
        let my_item_to_insert = IdTagRecord {
            tag: inner_tag,
            owner: Some(my_public_key),
            status: IdTagStatus::Pending,
            correlation_id: Some(my_corr_id),
        };

        let inner_tag = IdentityTag::random(&mut csprng);
        let other_corr_id = CorrelationId::gen_random();
        let other_public_key = generate_random_public_key();
        let other_item_to_insert = IdTagRecord {
            tag: inner_tag,
            owner: Some(other_public_key),
            status: IdTagStatus::Pending,
            correlation_id: Some(other_corr_id.clone()),
        };

        let mut sut = IdTagCache::<FakeCsprng>::default();

        sut.insert(my_item_to_insert.clone()).unwrap();
        sut.insert(other_item_to_insert.clone()).unwrap();

        // ensure records are inserted before removing
        assert!(sut.get_id_tag(&my_item_to_insert.to_hash()).is_some());
        assert!(sut.get_id_tag(&other_item_to_insert.to_hash()).is_some());

        // remove other tag by correlation id
        sut.remove_id_tag_by_correlation_id(&other_corr_id);

        // ensure my record is still intact while the other is removed
        assert!(sut.get_id_tag(&my_item_to_insert.to_hash()).is_some());
        assert!(sut.get_id_tag(&other_item_to_insert.to_hash()).is_none());
    }

    #[test]
    fn inserting_two_different_id_tags_with_same_correlation_id_returns_a_collision_error() {
        let mut csprng: OsRng = OsRng::default();
        let shared_corr_id = CorrelationId::gen_random();
        let inner_tag = IdentityTag::random(&mut csprng);
        let my_public_key = generate_random_public_key();
        let my_item_to_insert = IdTagRecord {
            tag: inner_tag,
            owner: Some(my_public_key),
            status: IdTagStatus::Pending,
            correlation_id: Some(shared_corr_id.clone()),
        };

        let inner_tag = IdentityTag::random(&mut csprng);
        let other_public_key = generate_random_public_key();
        let other_item_to_insert = IdTagRecord {
            tag: inner_tag,
            owner: Some(other_public_key),
            status: IdTagStatus::Pending,
            correlation_id: Some(shared_corr_id),
        };

        let mut sut = IdTagCache::<FakeCsprng>::default();

        sut.insert(my_item_to_insert).unwrap();
        let other_insert_result = sut.insert(other_item_to_insert);
        assert!(matches!(
            other_insert_result,
            Err(IdTagStoreError::CorrelationIdCollision)
        ))
    }

    fn generate_random_public_key() -> PublicKey {
        let mut csprng: OsRng = OsRng::default();
        PrivateKey::random(&mut csprng).into()
    }

    #[test]
    fn removing_id_tag_removes_it_from_all_internal_collections() {
        let correlation_id = CorrelationId::gen_random();
        let public_key = generate_random_public_key();
        let id_tag = IdentityTag::from_key_with_generator_base(public_key);
        let mut id_tag_cache = IdTagCache::<FakeCsprng>::default();
        id_tag_cache
            .insert(IdTagRecord {
                tag: id_tag,
                owner: Some(public_key),
                status: IdTagStatus::Valid,
                correlation_id: Some(correlation_id),
            })
            .unwrap();
        id_tag_cache.remove(&id_tag);
        let cache = id_tag_cache.0.read().unwrap();
        let id_tag_hash = id_tag.to_hash();
        assert!(!cache.id_tags.contains_key(&id_tag_hash));
        assert!(cache
            .known_id_tags
            .values()
            .flatten()
            .all(|h| *h != id_tag_hash));
        assert!(cache
            .tags_by_correlation_id
            .values()
            .all(|h| *h != id_tag_hash));
    }
}
