use crate::confidentiality_store::TxoHash;
use xand_models::CorrelationId;
use xandstrate_runtime::{Hash, UncheckedExtrinsic};

#[logging_event]
pub(crate) enum LoggingEvent {
    PetriError(crate::errors::PetriError),
    PetriBackfillingBlock(String),
    PetriProcessingBlock(String),
    TransientBlockProcessingError { block_number: u32 },
    MaxRetriesForTransientBlockProcessingError { retries: u8 },
    BlockNotFound { block_number: u32 },
    CouldNotDecodeExtrinsic(String),
    InvalidExtrinsic(crate::errors::PetriError),
    NoEventsForExtrinsic(String),
    IrrelevantTransaction(String),
    SawFailedTransaction(Hash, UncheckedExtrinsic),
    BlockMissingTimestamp { block_number: u32 },
    Info(String),
    IdTagStoreError(String),
    TxoNotFound(TxoHash),
    NoTxoFoundForCorrelationId(CorrelationId),
    SnapshotError(String),
}
