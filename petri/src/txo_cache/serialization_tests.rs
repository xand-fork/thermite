use crate::{
    confidentiality_store::{TransactionOutputRecord, TxoHash},
    txo_cache::tests::arbitrary_txo_record,
    txo_cache::TxoCacheInner,
    TxoCache,
};
use proptest::{collection::vec, prelude::*};
use std::sync::{Arc, RwLock};
use xand_address::Address;
use xand_financial_client::models::TransactionOutputData;
use xand_models::CorrelationId;
use xand_public_key::public_key_to_address;

fn txo_pair(rec: &TransactionOutputRecord) -> (TxoHash, TransactionOutputRecord) {
    (rec.to_hash(), rec.clone())
}

fn txo_by_correlation_id_pair(
    rec: &TransactionOutputRecord,
) -> Option<(CorrelationId, Vec<TxoHash>)> {
    rec.correlation_id
        .as_ref()
        .map(|correlation_id| (correlation_id.clone(), vec![rec.to_hash()]))
}

fn own_txo_pair(rec: &TransactionOutputRecord) -> Option<(Address, Vec<TxoHash>)> {
    match rec.txo {
        TransactionOutputData::Private(_, key) => {
            let address = public_key_to_address(&key);
            Some((address, vec![rec.to_hash()]))
        }
        TransactionOutputData::Public(_) => None,
    }
}

prop_compose! {
    pub fn arbitrary_txo_cache()(
        recs in vec(arbitrary_txo_record(), 0..20)
    ) -> TxoCache {
        let txos = recs.iter().map(txo_pair).collect();
        let txos_by_correlation_id = recs.iter().filter_map(txo_by_correlation_id_pair).collect();
        let owned_txos = recs.iter().filter_map(own_txo_pair).collect();

        let inner = TxoCacheInner {
            txos,
            txos_by_correlation_id,
            owned_txos,
        };
        #[allow(clippy::init_numbered_fields)]
        TxoCache{ 0: Arc::new(RwLock::new(inner))}
    }
}

proptest! {
    #[test]
    fn serialize_deserialize_round_trip(
        expected in arbitrary_txo_cache()
    ) {
        let serialized = bincode::serialize(&expected).unwrap();
        let deserialized: TxoCache = bincode::deserialize(&serialized).unwrap();
        assert_eq!(expected, deserialized);
    }
}
