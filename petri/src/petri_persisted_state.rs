use crate::subs_client_wrapper::BlockNumber;
use crate::{IdTagCache, TransactionCache, TxoCache};
use serde::de::{MapAccess, SeqAccess, Visitor};
use serde::ser::SerializeStruct;
use serde::{de, Deserialize, Deserializer, Serialize, Serializer};
use sp_runtime::traits::Zero;
use std::fmt;
use std::marker::PhantomData;
use std::sync::Arc;

// Be careful adding new fields to this considering we have a manual implementation for `Serialize`
// and `Deserialize`
#[derive(Clone, Debug, Default)]
pub struct PetriPersistedState<R> {
    pub next_block_to_process: BlockNumber,
    pub transaction_cache: Arc<TransactionCache>,
    pub utxo_store: TxoCache,
    pub identity_tag_store: IdTagCache<R>,
}

impl<R> PetriPersistedState<R> {
    pub fn new(
        transaction_cache: Arc<TransactionCache>,
        utxo_store: TxoCache,
        identity_tag_store: IdTagCache<R>,
    ) -> Self {
        Self {
            next_block_to_process: Zero::zero(),
            transaction_cache,
            utxo_store,
            identity_tag_store,
        }
    }
}

const PETRI_PERSISTED_STATE_STRUCT_NAME: &str = "PetriPersistedState";
const NEXT_BLOCK_TO_PROCESS_FIELD: &str = "next_block_to_process";
const TRANSACTION_CACHE_FIELD: &str = "transaction_cache";
const UTXO_STORE_FIELD: &str = "utxo_store";
const IDENTITY_TAG_STORE_FIELD: &str = "identity_tag_store";

impl<R> Serialize for PetriPersistedState<R> {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        let mut state = serializer.serialize_struct(PETRI_PERSISTED_STATE_STRUCT_NAME, 5)?;
        state.serialize_field(NEXT_BLOCK_TO_PROCESS_FIELD, &self.next_block_to_process)?;
        state.serialize_field(TRANSACTION_CACHE_FIELD, &(*self.transaction_cache))?;
        state.serialize_field(UTXO_STORE_FIELD, &self.utxo_store)?;
        state.serialize_field(IDENTITY_TAG_STORE_FIELD, &self.identity_tag_store)?;
        state.end()
    }
}

impl<'de, R: Default> Deserialize<'de> for PetriPersistedState<R> {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        #[derive(Debug)]
        enum Field {
            NextBlockToProcess,
            TransactionCache,
            UTxOStore,
            IdentityTagStore,
        }

        impl<'de> Deserialize<'de> for Field {
            fn deserialize<D>(deserializer: D) -> Result<Field, D::Error>
            where
                D: Deserializer<'de>,
            {
                struct FieldVisitor;

                impl<'de> Visitor<'de> for FieldVisitor {
                    type Value = Field;

                    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                        let expecting_string = format!(
                            "`{:?}`, `{:?}`, `{:?}`, or `{:?}`",
                            NEXT_BLOCK_TO_PROCESS_FIELD,
                            TRANSACTION_CACHE_FIELD,
                            UTXO_STORE_FIELD,
                            IDENTITY_TAG_STORE_FIELD
                        );
                        formatter.write_str(&expecting_string)
                    }

                    fn visit_str<E>(self, value: &str) -> Result<Field, E>
                    where
                        E: de::Error,
                    {
                        match value {
                            NEXT_BLOCK_TO_PROCESS_FIELD => Ok(Field::NextBlockToProcess),
                            TRANSACTION_CACHE_FIELD => Ok(Field::TransactionCache),
                            UTXO_STORE_FIELD => Ok(Field::UTxOStore),
                            IDENTITY_TAG_STORE_FIELD => Ok(Field::IdentityTagStore),
                            _ => Err(de::Error::unknown_field(value, FIELDS)),
                        }
                    }
                }

                deserializer.deserialize_identifier(FieldVisitor)
            }
        }

        #[derive(Default)]
        struct PetriPersistedStateVisitor<R> {
            _phantom_data: PhantomData<R>,
        }

        impl<'de, R: Default> Visitor<'de> for PetriPersistedStateVisitor<R> {
            type Value = PetriPersistedState<R>;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                let expecting_string = format!("struct {:?}", PETRI_PERSISTED_STATE_STRUCT_NAME);
                formatter.write_str(&expecting_string)
            }

            fn visit_seq<V>(self, mut seq: V) -> Result<PetriPersistedState<R>, V::Error>
            where
                V: SeqAccess<'de>,
            {
                let next_block_to_process = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(0, &self))?;
                let transaction_cache = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(1, &self))?;
                let transaction_cache = Arc::new(transaction_cache);
                let utxo_store = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(2, &self))?;
                let identity_tag_store = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(4, &self))?;
                let state = PetriPersistedState {
                    next_block_to_process,
                    transaction_cache,
                    utxo_store,
                    identity_tag_store,
                };
                Ok(state)
            }

            fn visit_map<V>(self, mut map: V) -> Result<PetriPersistedState<R>, V::Error>
            where
                V: MapAccess<'de>,
            {
                let mut next_block_to_process: Option<BlockNumber> = None;
                let mut transaction_cache: Option<TransactionCache> = None;
                let mut utxo_store: Option<TxoCache> = None;
                let mut identity_tag_store: Option<IdTagCache<R>> = None;
                while let Some(key) = map.next_key::<Field>()? {
                    match key {
                        Field::NextBlockToProcess => {
                            if next_block_to_process.is_some() {
                                return Err(de::Error::duplicate_field(
                                    NEXT_BLOCK_TO_PROCESS_FIELD,
                                ));
                            }
                            next_block_to_process = Some(map.next_value()?);
                        }
                        Field::TransactionCache => {
                            if transaction_cache.is_some() {
                                return Err(de::Error::duplicate_field(TRANSACTION_CACHE_FIELD));
                            }
                            transaction_cache = Some(map.next_value()?);
                        }
                        Field::UTxOStore => {
                            if utxo_store.is_some() {
                                return Err(de::Error::duplicate_field(UTXO_STORE_FIELD));
                            }
                            utxo_store = Some(map.next_value()?);
                        }
                        Field::IdentityTagStore => {
                            if identity_tag_store.is_some() {
                                return Err(de::Error::duplicate_field(IDENTITY_TAG_STORE_FIELD));
                            }
                            identity_tag_store = Some(map.next_value()?);
                        }
                    }
                }
                let next_block_to_process = next_block_to_process
                    .ok_or_else(|| de::Error::missing_field(NEXT_BLOCK_TO_PROCESS_FIELD))?;
                let transaction_cache = transaction_cache
                    .ok_or_else(|| de::Error::missing_field(TRANSACTION_CACHE_FIELD))?;
                let transaction_cache = Arc::new(transaction_cache);
                let utxo_store =
                    utxo_store.ok_or_else(|| de::Error::missing_field(UTXO_STORE_FIELD))?;
                let identity_tag_store = identity_tag_store
                    .ok_or_else(|| de::Error::missing_field(IDENTITY_TAG_STORE_FIELD))?;
                let state = PetriPersistedState {
                    next_block_to_process,
                    transaction_cache,
                    utxo_store,
                    identity_tag_store,
                };
                Ok(state)
            }
        }

        const FIELDS: &[&str] = &[
            NEXT_BLOCK_TO_PROCESS_FIELD,
            TRANSACTION_CACHE_FIELD,
            UTXO_STORE_FIELD,
            IDENTITY_TAG_STORE_FIELD,
        ];
        deserializer.deserialize_struct(
            PETRI_PERSISTED_STATE_STRUCT_NAME,
            FIELDS,
            PetriPersistedStateVisitor::default(),
        )
    }
}

#[cfg(test)]
mod serde_tests {
    use super::*;
    use crate::{
        id_tag_cache::serialization_tests::arbitrary_id_tag_cache,
        test::assert_relevent_data_matches,
        transaction_cache::serialization_tests::arbitrary_transaction_cache,
        txo_cache::serialization_tests::arbitrary_txo_cache,
    };
    use proptest::prelude::*;

    prop_compose! {
    pub fn arbitrary_petri_state()(
        next_block_to_process: u32,
        transaction_cache in arbitrary_transaction_cache(),
        utxo_store in arbitrary_txo_cache(),
        identity_tag_store in arbitrary_id_tag_cache(),
    ) -> PetriPersistedState<()> {
        let transaction_cache = Arc::new(transaction_cache);
        PetriPersistedState {
            next_block_to_process,
            transaction_cache,
            utxo_store,
            identity_tag_store
        }
        }
    }

    proptest! {
        #[test]
        fn serialize_deserialize_round_trip(
            expected in arbitrary_petri_state()
        ) {
            let serialized = bincode::serialize(&expected).unwrap();
            let deserialized: PetriPersistedState<()> = bincode::deserialize(&serialized).unwrap();
            assert_relevent_data_matches(&expected, &deserialized);
        }
    }
}
