// Generated mock code triggers this lint.
#![allow(clippy::unused_unit, clippy::upper_case_acronyms)]

use crate::errors::*;
use sc_client_api::{BlockBackend, CallExecutor, StorageProvider, UsageProvider};
use sc_service::client::Client;
use snafu::ResultExt;
use sp_api::ProvideRuntimeApi;
use sp_blockchain::Info;
use sp_core::storage::{StorageData, StorageKey};
use sp_runtime::{
    generic::{BlockId as RawBlockId, SignedBlock},
    traits::{Block, Header},
};
use xand_address::Address;
use xand_models::ToAddress;
use xandstrate_runtime::network_voting::PropIndex;
use xandstrate_runtime::{
    opaque,
    xandstrate::xandstrate_api::{MemberInfoApi, ProposalsApi},
    AccountId, ProposalData, RuntimeApi,
};

pub(crate) type ConcreteBlock = opaque::Block;
pub(crate) type ConcreteClient<E> =
    Client<sc_client_db::Backend<ConcreteBlock>, E, ConcreteBlock, RuntimeApi>;
pub(crate) type BlockId = RawBlockId<ConcreteBlock>;
pub(crate) type BlockNumber = <<ConcreteBlock as Block>::Header as Header>::Number;

use xandstrate_client::parity_codec::Decode;

/// A trait that wraps substrate chain clients (not rpc clients) so we can test / swap them.
/// A `MockSubstrateBlockchainClient` is provided by `mockall` during testing.
///
///
/// Hopefully, substrate will one day provide such a trait.
/// See: https://github.com/paritytech/substrate/issues/5867
#[cfg_attr(test, mockall::automock)]
pub trait SubstrateBlockchainClient: Send + Sync {
    /// Returns info about the blockchain (current head, etc)
    fn chain_info(&self) -> Info<ConcreteBlock>;

    /// Fetches details of a specific block
    fn block(&self, block_id: &BlockId) -> Result<Option<SignedBlock<ConcreteBlock>>, PetriError>;

    /// Fetches the storage value under the provided key as it was at the provided block
    fn storage(
        &self,
        block_id: &BlockId,
        storage_key: &StorageKey,
    ) -> Result<Option<StorageData>, PetriError>;

    fn members(&self, block_id: &BlockId) -> Result<Vec<Address>, PetriError>;

    fn get_proposal_action(
        &self,
        block_id: &BlockId,
        prop_id: &PropIndex,
    ) -> Result<Option<xandstrate_runtime::Call>, PetriError>;
}

impl<E> SubstrateBlockchainClient for ConcreteClient<E>
where
    Self: ProvideRuntimeApi<ConcreteBlock>,
    E: CallExecutor<ConcreteBlock, Backend = sc_client_db::Backend<ConcreteBlock>> + Send + Sync,
    <Self as ProvideRuntimeApi<ConcreteBlock>>::Api: MemberInfoApi<ConcreteBlock, AccountId>,
    <Self as ProvideRuntimeApi<ConcreteBlock>>::Api:
        ProposalsApi<ConcreteBlock, AccountId, BlockNumber>,
{
    fn chain_info(&self) -> Info<ConcreteBlock> {
        self.usage_info().chain
    }

    fn block(&self, block_id: &BlockId) -> Result<Option<SignedBlock<ConcreteBlock>>, PetriError> {
        BlockBackend::block(self, block_id).context(SubstrateClientError {
            msg: format!("Fetching block {} failed", block_id),
        })
    }

    fn storage(
        &self,
        block_id: &BlockId,
        storage_key: &StorageKey,
    ) -> Result<Option<StorageData>, PetriError> {
        StorageProvider::storage(self, block_id, storage_key).context(SubstrateClientError {
            msg: format!(
                "Fetching storage @ {:?} in block {} failed",
                storage_key, block_id
            ),
        })
    }

    fn members(&self, block_id: &BlockId) -> Result<Vec<Address>, PetriError> {
        let api = self.runtime_api();
        let members = api
            .get_members(block_id)
            .map_err(|e| PetriError::FailedToGetMembers {
                source_str: format!("{:?}", e),
            })?;
        Ok(members.into_iter().map(|m| m.to_address()).collect())
    }

    fn get_proposal_action(
        &self,
        block_id: &BlockId,
        prop_id: &PropIndex,
    ) -> Result<Option<xandstrate_runtime::Call>, PetriError> {
        let api = self.runtime_api();

        let maybe_data =
            api.get_proposal(block_id, *prop_id)
                .map_err(|e| PetriError::FailedToGetProposal {
                    source_str: format!("{:?}", e),
                })?;

        if let Some(data) = maybe_data {
            let call = decode_call(&data)?;
            Ok(Some(call))
        } else {
            Ok(None)
        }
    }
}

fn decode_call(
    data: &ProposalData<AccountId, u32>,
) -> Result<xandstrate_runtime::Call, PetriError> {
    xandstrate_runtime::Call::decode(&mut data.proposed_action.as_slice())
        .map_err(|_| PetriError::ErrorDecodingProposal)
}
