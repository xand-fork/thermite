<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Petri](#petri)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Petri

Petri is a caching layer that sits on top of substrate. Certain seemingly
obviously desirable functionality is not provided by substrate when it comes
to fetching historical data. In particular, looking up an arbitrary transaction
by its hash is not provided by default. This is in part because substrate
does not necessarily guarantee transaction hashes are unique (though, in our
system, they should be) - it is in part to avoid consuming too much memory.

Petri listens to all finalized blocks, and populates a cache to make various
types of lookups easier.

Eventually, it should be replaced with something that can be persisted to disk
as well.

See rustdocs for details
