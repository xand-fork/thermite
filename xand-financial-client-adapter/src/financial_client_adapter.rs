use crate::krypt_adapter::KryptKeyStore;
use crate::substrate_key_resolver::SubstrateClientEncryptionKeyResolver;

use std::{marker::PhantomData, sync::Arc};
use tpfs_krypt::KeyManagement;

use xand_financial_client::{
    CreateManagerDeps, CreateManagerImpl, RedeemManagerDeps, RedeemManagerImpl, SendManagerDeps,
    SendManagerImpl,
};
use xand_models::ExtrinsicDecoderConfig;
use xandstrate_client::{SubstrateClient, SubstrateClientInterface};

pub fn new_create_mgr<T: SubstrateClientInterface>(
    key_mgr: Arc<dyn KeyManagement>,
    sub_client: Arc<T>,
) -> CreateManagerImpl<AdapterCreateMgrConfig<T>> {
    let key_store = KryptKeyStore { key_mgr };
    let encryption_key_resolver = SubstrateClientEncryptionKeyResolver {
        substrate_client: sub_client,
    };

    CreateManagerImpl::new(key_store.clone(), encryption_key_resolver, key_store)
}

pub struct AdapterCreateMgrConfig<T> {
    substrate_client: PhantomData<fn() -> T>,
}

impl<T> Clone for AdapterCreateMgrConfig<T> {
    fn clone(&self) -> Self {
        Self {
            substrate_client: PhantomData,
        }
    }
}

impl<T> CreateManagerDeps for AdapterCreateMgrConfig<T>
where
    T: SubstrateClientInterface,
{
    type KeyStore = KryptKeyStore;
    type EncryptionKeyResolver = SubstrateClientEncryptionKeyResolver<T>;
    type EncryptionProvider = KryptKeyStore;
}

pub fn new_redeem_mgr<T: SubstrateClientInterface>(
    key_mgr: Arc<dyn KeyManagement>,
    sub_client: Arc<T>,
) -> RedeemManagerImpl<AdapterRedeemMgrConfig<T>> {
    let key_store = KryptKeyStore { key_mgr };
    let encryption_key_resolver = SubstrateClientEncryptionKeyResolver {
        substrate_client: sub_client,
    };

    RedeemManagerImpl::new(key_store.clone(), encryption_key_resolver, key_store)
}

pub struct AdapterRedeemMgrConfig<T> {
    substrate_client: PhantomData<fn() -> T>,
}

impl<T> Clone for AdapterRedeemMgrConfig<T> {
    fn clone(&self) -> Self {
        Self {
            substrate_client: PhantomData,
        }
    }
}

impl<T> RedeemManagerDeps for AdapterRedeemMgrConfig<T>
where
    T: SubstrateClientInterface,
{
    type KeyStore = KryptKeyStore;
    type EncryptionKeyResolver = SubstrateClientEncryptionKeyResolver<T>;
    type EncryptionProvider = KryptKeyStore;
}

pub fn new_send_mgr<T: SubstrateClientInterface>(
    key_mgr: Arc<dyn KeyManagement>,
    sub_client: Arc<T>,
) -> SendManagerImpl<AdapterSendMgrConfig<T>> {
    let key_store = KryptKeyStore { key_mgr };
    let key_resolver = SubstrateClientEncryptionKeyResolver {
        substrate_client: sub_client,
    };
    let encryption_provider = key_store.clone();
    SendManagerImpl {
        key_store,
        encryption_provider,
        key_resolver,
    }
}

pub struct AdapterSendMgrConfig<T> {
    substrate_client: PhantomData<fn() -> T>,
}

impl<T> SendManagerDeps for AdapterSendMgrConfig<T>
where
    T: SubstrateClientInterface,
{
    type KeyStore = KryptKeyStore;
    type EncryptionProvider = KryptKeyStore;
    type NetworkKeyResolver = SubstrateClientEncryptionKeyResolver<T>;
}

pub struct AdapterExtDecoderConfig;

impl ExtrinsicDecoderConfig for AdapterExtDecoderConfig {
    type CreateManager = CreateManagerImpl<AdapterCreateMgrConfig<SubstrateClient>>;
    type SendManager = SendManagerImpl<AdapterSendMgrConfig<SubstrateClient>>;
    type RedeemManager = RedeemManagerImpl<AdapterRedeemMgrConfig<SubstrateClient>>;
}
