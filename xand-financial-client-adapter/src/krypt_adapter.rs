#![allow(clippy::result_large_err)]
use crate::{address_to_key_id, logging_events::LoggingEvent::KeyParseError};
use async_trait::async_trait;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use snafu::{ResultExt, Snafu};
use sp_core::crypto::Ss58Codec;
use std::convert::TryFrom;
use std::error::Error;
use std::path::PathBuf;
use std::sync::Arc;
use std::{fmt, fs};
use tpfs_krypt::config::{KeyManagerConfig, KryptConfig};
use tpfs_krypt::sp_core::Pair;
use tpfs_krypt::{
    errors::KeyManagementError, from_config, FileKeyManagerConfig, KeyIdentifier, KeyManagement,
    KeyPairValue, KeyType,
};
use tpfs_logger_port::error;
use xand_address::Address;
use xand_financial_client::errors::FinancialClientError;
use xand_financial_client::models::{Encrypted, EncryptionKey, EncryptionNonce};
use xand_financial_client::{EncryptionProvider, KeyStore};
use xand_ledger::{PrivateKey, PublicKey};
use xand_public_key::{address_to_public_key, public_key_to_address};

pub fn create_and_initialize_key_manager(
    initial_key_path: Option<PathBuf>,
    key_management_path: PathBuf,
) -> Result<Arc<dyn KeyManagement>, InitializationError> {
    if let Some(initial_key_path) = initial_key_path {
        if !key_management_path.exists() {
            fs::create_dir_all(&key_management_path).context(IoError {
                message: format!(
                    "Unable to create key management directory {:?}",
                    &key_management_path
                ),
            })?;
        }

        let files_to_copy = fs::read_dir(&initial_key_path)
            .context(IoError {
                message: format!("Could not read directory {:?}", &initial_key_path),
            })?
            .flat_map(|res| res.map(|e| e.path()))
            .filter(|path| path.is_file());

        for file in files_to_copy {
            let file_name = file.file_name().expect("invalid file name");
            let destination = key_management_path.join(file_name);

            fs::copy(&file, &destination).context(IoError {
                message: format!("Unable to copy file {:?} to {:?}", &file, &destination),
            })?;
        }
    }

    let krypt_config = KryptConfig {
        key_manager_config: KeyManagerConfig::FileKeyManager(FileKeyManagerConfig {
            keypair_directory_path: key_management_path.into_os_string().into_string()?,
        }),
    };
    Ok(Arc::from(from_config(krypt_config)?))
}

#[derive(Clone)]
pub struct KryptKeyStore {
    pub(crate) key_mgr: Arc<dyn KeyManagement>,
}

impl From<Arc<dyn KeyManagement>> for KryptKeyStore {
    fn from(key_mgr: Arc<dyn KeyManagement>) -> Self {
        KryptKeyStore { key_mgr }
    }
}

#[async_trait]
impl KeyStore for KryptKeyStore {
    async fn get_all(&self) -> Result<Vec<(PublicKey, PrivateKey)>, FinancialClientError> {
        let key_ids =
            self.key_mgr
                .get_key_ids()
                .map_err(|e| FinancialClientError::KeyManagement {
                    source: Box::new(e),
                })?;
        Ok(key_ids
            .iter()
            .map(|id| -> Result<_, Box<dyn Error>> {
                let key = self.key_mgr.get_keypair_by_id(id).map_err(|e| {
                    FinancialClientError::KeyManagement {
                        source: Box::new(e),
                    }
                })?;
                match key {
                    KeyPairValue::SubstrateSr25519(k) => {
                        let address = Address::try_from(k.key.public().to_ss58check())
                            .map_err(|e| format!("Failed to create Address: {:?}", e))?;
                        let key = sp_pair_to_private_key(&k.key)?;
                        let public_key: PublicKey = address_to_public_key(&address).ok_or_else(||
                            FinancialClientError::InvalidAddress { reason: "Could not convert address to public key".to_string() }
                        )?;
                        Ok(Some((public_key, key)))
                    }
                    KeyPairValue::SharedEncryptionX25519(_) | KeyPairValue::SubstrateEd25519(_) => {
                        Ok(None)
                    }
                }
            })
            // filter out SharedEncryptionX25519 & SubstrateEd25519
            .filter_map(|k| k.transpose())
            // log errors and filter for successfully parsed keys
            .filter_map(|r| match r {
                Ok(k) => Some(k),
                Err(e) => {
                    error!(KeyParseError(format!("{:?}", e)));
                    None
                }
            })
            .collect())
    }

    async fn has_encryption_key(&self, key: &EncryptionKey) -> Result<bool, FinancialClientError> {
        let id = encryption_key_id(key);
        self.key_mgr
            .has_key(&id)
            .map_err(|e| FinancialClientError::KeyManagement {
                source: Box::new(e),
            })
    }

    async fn get(&self, key: &PublicKey) -> Result<Option<PrivateKey>, FinancialClientError> {
        let address = public_key_to_address(key);
        match self.key_mgr.get_keypair_by_id(&address_to_key_id(&address)) {
            Ok(KeyPairValue::SubstrateSr25519(k)) => sp_pair_to_private_key(&k.key).map(Some),
            Ok(KeyPairValue::SubstrateEd25519(_)) | Ok(KeyPairValue::SharedEncryptionX25519(_)) => {
                Err(FinancialClientError::InvalidAddress {
                    reason: format!(
                        "Address {} is not associated with key of type sr25519",
                        address
                    ),
                })
            }
            Err(KeyManagementError::AddressNotFound { .. }) => Ok(None),
            Err(e) => Err(FinancialClientError::KeyManagement { source: e.into() }),
        }
    }
}

impl EncryptionProvider for KryptKeyStore {
    fn encrypt<T: Serialize>(
        &self,
        message: &T,
        sender: &EncryptionKey,
        receiver: &EncryptionKey,
        derivation_nonce: &EncryptionNonce,
    ) -> Result<Encrypted, FinancialClientError> {
        let sender_key_id = encryption_key_id(sender);
        let encrypted = tpfs_krypt::Encrypted::encrypt(
            &*self.key_mgr,
            &sender_key_id,
            receiver.as_bytes(),
            message,
            derivation_nonce.as_slice(),
        )
        .map_err(|e| FinancialClientError::KeyManagement { source: e.into() })?;
        Ok(encrypted.to_bytes().into())
    }

    fn decrypt_as_sender<T: DeserializeOwned>(
        &self,
        message: &Encrypted,
        sender: &EncryptionKey,
        receiver: &EncryptionKey,
        derivation_nonce: &EncryptionNonce,
    ) -> Result<T, FinancialClientError> {
        let encrypted =
            tpfs_krypt::Encrypted::from_bytes_unchecked(message.as_slice()).map_err(|e| {
                FinancialClientError::EncryptedPayloadParsingFailure {
                    source: Box::new(e),
                }
            })?;
        let sender_key_id = encryption_key_id(sender);
        encrypted
            .decrypt_as_sender(
                &*self.key_mgr,
                &sender_key_id,
                receiver.as_bytes(),
                derivation_nonce.as_slice(),
            )
            .map_err(|_| FinancialClientError::UnauthorizedDecryption)
    }

    fn decrypt_as_receiver<T: DeserializeOwned>(
        &self,
        message: Encrypted,
        receiver: EncryptionKey,
    ) -> Result<T, FinancialClientError> {
        let encrypted = tpfs_krypt::typed::Encrypted::from_bytes_unchecked(message.as_slice())
            .map_err(|e| FinancialClientError::KeyManagement {
                source: Box::new(e),
            })?;
        let receiver_key_id = encryption_key_id(&receiver);
        encrypted
            .decrypt_as_receiver(&*self.key_mgr, &receiver_key_id)
            .map_err(|_| FinancialClientError::UnauthorizedDecryption)
    }
}

pub fn encryption_key_id(encryption_key: &EncryptionKey) -> KeyIdentifier {
    KeyIdentifier {
        key_type: KeyType::SharedEncryptionX25519,
        value: encryption_key.to_string(),
    }
}

pub fn sp_pair_to_private_key(
    kp: &tpfs_krypt::sp_core::sr25519::Pair,
) -> Result<PrivateKey, FinancialClientError> {
    // We're just interested in the Scalar bytes from this secret (i.e. first 32 bytes), the nonce
    // is not used by xand_ledger.
    let bytes = <[u8; 32]>::try_from(&kp.as_ref().secret.to_bytes()[..32]).unwrap();
    PrivateKey::from_canonical_bytes(bytes).ok_or_else(|| FinancialClientError::KeyManagement {
        source: "Failed to create PrivateKey from bytes".into(),
    })
}

#[derive(Debug, Snafu)]
#[snafu(visibility(pub))]
pub enum InitializationError {
    #[snafu(display("IO Error occurred \"{}\" with: {}", message, source))]
    IoError {
        #[snafu(source(from(std::io::Error, Box::new)))]
        source: Box<std::io::Error>,
        message: String,
    },

    #[snafu(display("Key Management Error: {}", source))]
    #[snafu(context(false))]
    KeyManagementError {
        source: tpfs_krypt::errors::KeyManagementError,
    },

    #[snafu(display("Os String Conversion Error: {:?}", source))]
    #[snafu(context(false))]
    OsStringConversionError {
        #[snafu(source(from(std::ffi::OsString, TextError::from_dbg)))]
        source: TextError,
    },
}

//TODO: This is a duplicate. Is this struct pulling it's weight?
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TextError(String);

impl std::error::Error for TextError {}

impl fmt::Display for TextError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl TextError {
    pub(crate) fn from_dbg<T: fmt::Debug>(d: T) -> Self {
        Self(format!("{:?}", d))
    }
}

#[cfg(test)]
mod tests {
    use super::{KeyStore, KryptKeyStore};
    use std::convert::TryFrom;
    use tpfs_krypt::{
        config::{KeyManagerConfig, KryptConfig},
        KeyType,
    };
    use xand_address::Address;
    use xand_public_key::address_to_public_key;

    #[tokio::test]
    async fn private_key_can_be_retrieved_by_address() {
        let mut key_mgr = tpfs_krypt::from_config(KryptConfig {
            key_manager_config: KeyManagerConfig::InMemoryKeyManager(Default::default()),
        })
        .unwrap();
        let address = Address::try_from(
            key_mgr
                .generate_keypair(KeyType::SubstrateSr25519)
                .unwrap()
                .id
                .value,
        )
        .unwrap();
        let key_store = KryptKeyStore {
            key_mgr: key_mgr.into(),
        };
        assert!(matches!(
            key_store
                .get(&address_to_public_key(&address).unwrap())
                .await,
            Ok(Some(_))
        ));
    }
}
