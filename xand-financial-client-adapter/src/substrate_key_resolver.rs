use async_trait::async_trait;
use std::sync::Arc;

use tpfs_krypt::sp_core::crypto::Ss58Codec;
use tpfs_krypt::sp_core::sr25519;
use xand_financial_client::errors::FinancialClientError;
use xand_financial_client::models::EncryptionKey;
use xand_financial_client::NetworkKeyResolver;
use xand_ledger::PublicKey;
use xand_public_key::{address_to_public_key, public_key_to_address};
use xandstrate_client::SubstrateClientInterface;

pub struct SubstrateClientEncryptionKeyResolver<T> {
    pub(crate) substrate_client: Arc<T>,
}

impl<T> Clone for SubstrateClientEncryptionKeyResolver<T> {
    fn clone(&self) -> Self {
        Self {
            substrate_client: self.substrate_client.clone(),
        }
    }
}

#[async_trait]
impl<T> NetworkKeyResolver for SubstrateClientEncryptionKeyResolver<T>
where
    T: SubstrateClientInterface,
{
    async fn get_encryption_key(
        &self,
        public_key: &PublicKey,
    ) -> Result<EncryptionKey, FinancialClientError> {
        // TODO: once we support encryption key rotation we'll need to fetch the key based on what
        // was used at a particular block
        // see https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6654
        let address = public_key_to_address(public_key);
        let addr = address.to_string();
        let substrate_key = sr25519::Public::from_ss58check(&addr).map_err(|e| {
            FinancialClientError::UnexpectedInternalErrorWithDescription(format!("{:?}", e))
        })?;
        let key = self
            .substrate_client
            .get_encryption_key(&substrate_key)
            .await
            .map_err(|e| FinancialClientError::NetworkKey {
                source: Box::new(e),
            })?;
        Ok(key.into())
    }

    async fn get_trust_encryption_key(&self) -> Result<EncryptionKey, FinancialClientError> {
        let addr = self
            .substrate_client
            .get_trustee()
            .await
            .map_err(|e| FinancialClientError::NetworkKey {
                source: Box::new(e),
            })?
            .to_string();
        let addr = sr25519::Public::from_ss58check(&addr).map_err(|e| {
            FinancialClientError::InvalidPublicKey {
                reason: format!("{:?}", e),
            }
        })?;
        let key = self
            .substrate_client
            .get_encryption_key(&addr)
            .await
            .map_err(|e| FinancialClientError::NetworkKey {
                source: Box::new(e),
            })?;
        Ok(key.into())
    }

    async fn get_trust_key(&self) -> Result<PublicKey, FinancialClientError> {
        self.substrate_client
            .get_trustee()
            .await
            .map_err(|e| FinancialClientError::NetworkKey {
                source: Box::new(e),
            })
            .map_or_else(Err, |val| {
                address_to_public_key(&val).ok_or_else(|| FinancialClientError::InvalidPublicKey {
                    reason: "Could not convert public key to address".to_string(),
                })
            })
    }
}
