#!/usr/bin/env bash
# Note: This script is used in CI to conditionally build base docker images for faster build times.
# Must be run from the root of the thermite directory

set -e

# Confirm that we are running in root of thermite dir
DIR=$(basename $(pwd))
if [[ $DIR != "thermite" ]]
then
    echo "Script must be called from root of thermite directory. Exiting..."
    exit 1
fi

echo $(pwd)

# Load build env vars
source .ci/.env
source .ci/util.sh

# rust-base-img ##############################################################
echo "Working on" $RUST_BASE_IMG

UPSTREAM_RUST_BASE_IMG_EXISTS=$(does_manifest_exist $UPSTREAM_RUST_BASE_IMG)
RUST_BASE_IMG_EXISTS=$(does_manifest_exist $RUST_BASE_IMG)

if [[ $UPSTREAM_RUST_BASE_IMG_EXISTS == "no" ]];
then
    echo "ERROR: The specified \$UPSTREAM_RUST_BASE_IMG Docker image, ${UPSTREAM_RUST_BASE_IMG}, does not exist."
    exit 1
fi

if [[ $RUST_BASE_IMG_EXISTS == "no" ]];
then
    docker build -t $RUST_BASE_IMG -f .ci/rust-base-img/Dockerfile --build-arg UPSTREAM_RUST_BASE_IMG=$UPSTREAM_RUST_BASE_IMG --build-arg NIGHTLY_RUST_VERSION=$NIGHTLY_RUST_VERSION .ci/rust-base-img/
    docker push $RUST_BASE_IMG
else
    echo "Image manifest exists. Continuing..."
fi
