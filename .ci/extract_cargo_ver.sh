#!/usr/bin/env bash

# This script takes a path to a Cargo.toml file as the first argument, and extracts the crate's
# version from it

# Get the first line with the word "version" (package version).
# Extract it, trim whitespace and quotes
grep version $1 | head -n1 | grep -Po 'version.*=\K.*' | awk '{$1=$1};1' | tr -d "'\""
