<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Continuous Integration Tooling](#continuous-integration-tooling)
  - [Versioning Guidelines](#versioning-guidelines)
  - [Deploying Updated Images](#deploying-updated-images)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

Continuous Integration Tooling
===

Contains scripts and Dockerfiles to build images used in the Gitlab CI process.

Versioning Guidelines
-

While Gitlab or other consumers of the docker images will always pull from the latest tag, 
the images are versioned internally to guarantee the correctness of dependencies 
when building images. This is to avoid situations where potentially outdated images temporarily override
the latest tag and are used in another image's "FROM". Versioning images is also useful for the build
scripts to detect whether the image has already been built or not (e.g. when calling `does_manifest_exist`).

Versions are managed from the `.env` file. **Any** updates to images in this folder should be packaged with 
updated versions as specified in the `.env` file to ensure a complete build.

Deploying Updated Images
--

`./.ci/build_and_push_ci_images_if_not_exists.sh` is the primary mechanism to push new images. 
Ensure you've properly versioned your changes in `.env` before using this script.

