# Confirm that we are running in root of thermite dir
DIR=$(basename $(pwd))
if [[ $DIR != "thermite" ]]
then
    echo "Script must be called from root of thermite directory. Exiting..."
    exit 1
fi

echo $(pwd)

source .ci/.env

# Hacky way to check if fully qualified image exists. User must be authorized for given Container Registry
# This is used because gcr allows overwriting of tags - so we do this check so we can fail CI
# if a job tries to overwrite an existing tagged image.

# Example usage:
# `does_manifest_exist "gcr.io/xand-dev/some-image:some-tag"`
# will print "yes" or "no" to stdout
function does_manifest_exist {
    local OUTPUT;
    local RETCODE;
    # Caputre stdout and stderr to confirm if error was "no such manifest"
    OUTPUT=$(DOCKER_CLI_EXPERIMENTAL=enabled docker manifest inspect $1 2>&1 )
    RETCODE=$?
    if [[ $RETCODE -eq 0 ]]
    then
        echo "yes"
        return 0
    else
        if [[ $RETCODE -eq 1 ]] && [[ $OUTPUT =~ "no such manifest" ]]
        then
            echo "no"
            return 0
        fi
    fi
    >&2 echo "Error in checking manifest"
    >&2 echo $OUTPUT
    return 1
}

# Build and push the "build-cache" image. This is explicity called in gitlab-ci to update the build
# cache on each merge.
function build_and_push_build_cache {
    echo "Working on" $BUILD_CACHE
    docker build -t $BUILD_CACHE -f .ci/build-cache/Dockerfile --build-arg GITLAB_CI_IMG=$GITLAB_CI_IMG .
    docker push $BUILD_CACHE
}
