use async_trait::async_trait;
use derive_more::Constructor;
use futures::{StreamExt, TryStreamExt};
use xand_ledger::{
    check_txo_ownership, CorrelationId, CreateRequestRecord, CreateRequestTransaction,
    DecryptSigner, EncryptionKey, IdentityTag, OpenedTransactionOutput, PublicInputSet, PublicKey,
    TransactionBuilderErrors, TransactionInputSet,
};

use crate::errors::FinancialClientError;
use crate::errors::FinancialClientError::InvalidAmount;
use crate::models::{
    BankAccount, ConfidentialPayload, Encrypted, EncryptionNonce, FinancialTransaction,
    PendingCreate, SecretCommitmentInputs, Transaction, TransactionOutputData,
};
use crate::{EncryptionProvider, IdentityTagSelector, KeyStore, NetworkKeyResolver, TxoRepository};

#[async_trait]
pub trait CreateManager {
    async fn reveal_create_request(
        &self,
        txn: CreateRequestTransaction,
    ) -> Result<Transaction, FinancialClientError>;

    async fn reveal_create_record(
        &self,
        correlation_id: CorrelationId,
        record: CreateRequestRecord,
    ) -> Result<(Option<PublicKey>, PendingCreate), FinancialClientError>;
}

/// This is used to condense the generic trait definitions for users of the create manager.
pub trait CreateManagerDeps {
    type KeyStore: KeyStore + Send + Sync;
    type EncryptionKeyResolver: NetworkKeyResolver + Send + Sync;
    type EncryptionProvider: EncryptionProvider + Send + Sync;
}

#[derive(Clone, Debug, Constructor)]
pub struct CreateManagerImpl<T: CreateManagerDeps> {
    pub key_store: T::KeyStore,
    pub network_key_lookup: T::EncryptionKeyResolver,
    pub encryption_provider: T::EncryptionProvider,
}

#[async_trait]
impl<T: CreateManagerDeps> CreateManager for CreateManagerImpl<T> {
    async fn reveal_create_request(
        &self,
        txn: CreateRequestTransaction,
    ) -> Result<Transaction, FinancialClientError> {
        let (signer, create) = self
            .reveal_create(
                txn.core_transaction.correlation_id,
                txn.core_transaction.outputs.clone(),
                txn.core_transaction.extra.into(),
                txn.core_transaction.encrypted_sender,
            )
            .await?;
        Ok(Transaction {
            signer,
            txn: FinancialTransaction::CreateRequest(create),
            new_txos: txn
                .core_transaction
                .outputs
                .iter()
                .map(|txo| {
                    signer.as_ref().map_or(
                        TransactionOutputData::Public(txo.transaction_output),
                        |key| TransactionOutputData::Private(txo.clone(), *key),
                    )
                })
                .collect(),
            spent_txos: vec![],
            new_clear_txos: vec![],
            new_id_tag: Some(txn.core_transaction.identity_output),
            redeemed_clear_txos: vec![],
        })
    }

    async fn reveal_create_record(
        &self,
        correlation_id: CorrelationId,
        record: CreateRequestRecord,
    ) -> Result<(Option<PublicKey>, PendingCreate), FinancialClientError> {
        self.reveal_create(
            correlation_id,
            record.outputs,
            record.account_data.into(),
            record.encrypted_sender,
        )
        .await
    }
}

impl<T: CreateManagerDeps> CreateManagerImpl<T> {
    async fn reveal_create<D: DecryptSigner>(
        &self,
        correlation_id: CorrelationId,
        outputs: Vec<OpenedTransactionOutput>,
        encrypted: Encrypted,
        verified_issuer: D,
    ) -> Result<(Option<PublicKey>, PendingCreate), FinancialClientError> {
        // check if we are the sender
        let keys = self.key_store.get_all().await?;
        let trust = self.network_key_lookup.get_trust_key().await?;
        let sender = keys.iter().find_map(|key| {
            if key.0 == trust {
                let pubkey = verified_issuer.decrypt(0, key.1);
                Some(pubkey)
            } else if outputs
                .iter()
                .all(|txo| check_txo_ownership(txo.transaction_output, key.1))
            {
                Some(key.0)
            } else {
                None
            }
        });

        let amount = outputs
            .iter()
            .map(|txo| txo.value)
            .fold(Some(0), |acc, value| value.checked_add(acc?))
            .ok_or(InvalidAmount)?;

        let bank_data = match &sender {
            Some(key) => ConfidentialPayload::Decrypted(
                self.decrypt_extra_data(key, encrypted, &correlation_id)
                    .await?,
            ),
            None => ConfidentialPayload::Encrypted(encrypted),
        };

        Ok((
            sender,
            PendingCreate {
                amount_in_minor_unit: amount,
                correlation_id,
                account: bank_data,
            },
        ))
    }

    async fn decrypt_extra_data(
        &self,
        sender: &PublicKey,
        encrypted: Encrypted,
        correlation_id: &CorrelationId,
    ) -> Result<BankAccount, FinancialClientError> {
        // Get the trust encryption key
        let trust_encryption_key = self.network_key_lookup.get_trust_encryption_key().await?;

        // if trust encryption key is available, attempt receiver decryption
        if self
            .key_store
            .has_encryption_key(&trust_encryption_key)
            .await?
        {
            self.encryption_provider
                .decrypt_as_receiver(encrypted, trust_encryption_key)
        } else {
            // Perform sender decryption since the txos are owned
            let my_encryption_key = self.network_key_lookup.get_encryption_key(sender).await?;
            // Setup the nonce
            let nonce = EncryptionNonce::from(correlation_id.as_ref());
            // Perform sender decryption
            self.encryption_provider.decrypt_as_sender(
                &encrypted,
                &my_encryption_key,
                &trust_encryption_key,
                &nonce,
            )
        }
    }
}

pub(crate) async fn masking_inputs<I, T>(
    id_tag_selector: &I,
    txo_repo: &T,
    spend_count: usize,
    true_tag: &IdentityTag,
) -> Result<Vec<PublicInputSet>, FinancialClientError>
where
    I: IdentityTagSelector + ?Sized,
    T: TxoRepository + ?Sized,
{
    const DECOY_SET_COUNT: u8 = 4;
    let decoy_id_tags = id_tag_selector
        .get_decoy_id_tags(true_tag, DECOY_SET_COUNT)
        .await?;
    futures::stream::iter(decoy_id_tags)
        .then(|id_tag| async move {
            let txos = txo_repo.get_decoys(spend_count, None).await?;
            Ok(PublicInputSet::new(TransactionInputSet::new(txos), id_tag))
        })
        .try_collect()
        .await
}

pub(crate) fn generate_metadata<E>(
    encryption_provider: &E,
    opened_txo: &OpenedTransactionOutput,
    receiver_encryption_key: &EncryptionKey,
    sender_encryption_key: EncryptionKey,
) -> Result<Encrypted, TransactionBuilderErrors>
where
    E: EncryptionProvider,
{
    let data = SecretCommitmentInputs {
        blinding_factor: opened_txo.blinding_factor,
        value: opened_txo.value,
    };
    let derivation_nonce = opened_txo.transaction_output.to_hash();
    encryption_provider
        .encrypt(
            &data,
            &sender_encryption_key,
            receiver_encryption_key,
            &derivation_nonce.into(),
        )
        .map_err(|_| TransactionBuilderErrors::EncryptionError)
}

#[cfg(test)]
mod test {
    use crate::{
        models::{ConfidentialPayload, FinancialTransaction, PendingCreate},
        test_helpers::{
            managers::{generate_test_member_create_manager, generate_test_trust_create_manager},
            transactions::{construct_create_record, construct_create_request},
        },
        CreateManager,
    };
    use rand::rngs::OsRng;
    use xand_ledger::{PrivateKey, TestCreateRequestBuilder};

    #[tokio::test]
    async fn attempting_to_reveal_owned_transaction_yields_owner_address_and_bank_data() {
        let mut csprng: OsRng = OsRng::default();
        let private_key = PrivateKey::random(&mut csprng);
        let create_manager = generate_test_member_create_manager(private_key, &Default::default());

        let transaction = construct_create_request(&mut csprng, private_key, &create_manager);
        let revealed_tx = create_manager
            .reveal_create_request(transaction)
            .await
            .expect("No failure expected");
        // ensure that the transaction is revealed
        let actual_signer = revealed_tx.signer;
        assert_eq!(actual_signer, Some(private_key.into()));
        assert!(matches!(
            revealed_tx.txn,
            FinancialTransaction::CreateRequest(PendingCreate {
                account: ConfidentialPayload::Decrypted(_),
                ..
            })
        ));
    }

    #[tokio::test]
    async fn attempting_to_reveal_unowned_transaction_does_not_yield_address_or_bank_data() {
        let mut csprng: OsRng = OsRng::default();
        let private_key = PrivateKey::random(&mut csprng);
        let create_manager = generate_test_member_create_manager(private_key, &Default::default());
        let transaction = TestCreateRequestBuilder::default()
            .build(&mut csprng)
            .create_request;
        let revealed_tx = create_manager
            .reveal_create_request(transaction)
            .await
            .expect("No failure expected");
        // Ensure that the transaction is hidden.
        let actual_signer = revealed_tx.signer;
        assert_eq!(actual_signer, None);
        assert!(matches!(
            revealed_tx.txn,
            FinancialTransaction::CreateRequest(PendingCreate {
                account: ConfidentialPayload::Encrypted(_),
                ..
            })
        ));
    }

    #[tokio::test]
    async fn attempting_to_reveal_owned_record_reveals_address_and_account() {
        let mut csprng: OsRng = OsRng::default();
        let private_key = PrivateKey::random(&mut csprng);
        let create_manager = generate_test_member_create_manager(private_key, &Default::default());
        let record = construct_create_record(&mut csprng, private_key, &create_manager, 1.into());
        let revealed_record = create_manager
            .reveal_create_record(record.0, record.1)
            .await
            .expect("No failure expected");
        // Ensure that the signer is revealed to the sender
        let actual_signer = revealed_record.0;
        assert_eq!(actual_signer, Some(private_key.into()));
        assert!(matches!(
            revealed_record.1.account,
            ConfidentialPayload::Decrypted(_)
        ));
    }

    #[tokio::test]
    async fn trust_can_reveal_create_transaction_bank_data() {
        let mut csprng: OsRng = OsRng::default();
        let private_key = PrivateKey::random(&mut csprng);
        let member_create_manager =
            generate_test_member_create_manager(private_key, &Default::default());
        let request = construct_create_request(&mut csprng, private_key, &member_create_manager);

        let trust_create_manager = generate_test_trust_create_manager(Default::default());
        let transaction = trust_create_manager
            .reveal_create_request(request)
            .await
            .expect("No failure expected");
        // Ensure that the transaction is revealed to the trust
        assert!(matches!(
            transaction.txn,
            FinancialTransaction::CreateRequest(PendingCreate {
                account: ConfidentialPayload::Decrypted(_),
                ..
            })
        ));
    }

    #[tokio::test]
    async fn trust_can_reveal_create_transaction_issuer_identity() {
        let mut csprng: OsRng = OsRng::default();
        let member_private_key = PrivateKey::random(&mut csprng);
        let trust_private_key = PrivateKey::random(&mut csprng);
        let member_create_manager =
            generate_test_member_create_manager(member_private_key, &trust_private_key.into());
        let request =
            construct_create_request(&mut csprng, member_private_key, &member_create_manager);

        let trust_create_manager = generate_test_trust_create_manager(trust_private_key);
        let transaction = trust_create_manager
            .reveal_create_request(request)
            .await
            .expect("No failure expected");
        // Ensure that the transaction is revealed to the trust
        let issuer = transaction.signer.unwrap();
        assert_eq!(issuer, member_private_key.into())
    }

    #[tokio::test]
    async fn trust_can_reveal_record_bank_data() {
        let mut csprng: OsRng = OsRng::default();
        let private_key = PrivateKey::random(&mut csprng);
        let member_create_manager =
            generate_test_member_create_manager(private_key, &Default::default());
        let record =
            construct_create_record(&mut csprng, private_key, &member_create_manager, 1.into());

        let trust_create_manager = generate_test_trust_create_manager(Default::default());
        let (_, pending_create) = trust_create_manager
            .reveal_create_record(record.0, record.1)
            .await
            .expect("No failure expected");
        // Ensure that the transaction is revealed to the trust
        let actual_account = pending_create.account;
        assert!(matches!(actual_account, ConfidentialPayload::Decrypted(_)));
    }

    #[tokio::test]
    async fn trust_can_reveal_create_record_issuer_identity() {
        let mut csprng: OsRng = OsRng::default();
        let member_private_key = PrivateKey::random(&mut csprng);
        let trust_private_key = PrivateKey::random(&mut csprng);
        let member_create_manager =
            generate_test_member_create_manager(member_private_key, &trust_private_key.into());
        let record = construct_create_record(
            &mut csprng,
            member_private_key,
            &member_create_manager,
            1.into(),
        );

        let trust_create_manager = generate_test_trust_create_manager(trust_private_key);
        let (maybe_issuer, _) = trust_create_manager
            .reveal_create_record(record.0, record.1)
            .await
            .expect("No failure expected");
        // Ensure that the transaction is revealed to the trust
        let issuer = maybe_issuer.unwrap();
        assert_eq!(issuer, member_private_key.into())
    }
}
