#![allow(clippy::result_large_err)]

//! The Xand Financial Client crate handles client side / private details
//! needed to interact with the remote confidential financial transaction ledger.

pub mod clear_redeem_builder;
pub mod create_builder;
pub mod errors;
pub mod models;
mod redeem_builder;
pub mod redeems;
pub mod send;
pub mod send_builder;
#[cfg(any(feature = "test-helpers", test))]
pub mod test_helpers;
pub mod transactions;
pub mod txo_selection;

pub use errors::FinancialClientError;
pub use models::RedeemRequestParams;
pub use redeem_builder::{RedeemBuilder, RedeemBuilderDeps, RedeemBuilderImpl};
pub use redeems::{RedeemManager, RedeemManagerDeps, RedeemManagerImpl};
pub use send::{SendManager, SendManagerDeps, SendManagerImpl};
pub use send_builder::{SendBuilder, SendBuilderDeps, SendBuilderImpl, SendTxParams};
pub use transactions::{CreateManager, CreateManagerDeps, CreateManagerImpl};

use crate::txo_selection::{random_improve, MAX_CLEAR_TXOS_PER_TRANSACTION};
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use models::EncryptionNonce;
use serde::de::DeserializeOwned;
use serde::Serialize;
use std::sync::Arc;
use xand_ledger::{
    ClearTransactionOutput, Encrypted, EncryptionKey, IdentityTag, OpenedTransactionOutput,
    PrivateKey, PublicKey, TransactionOutput,
};

/// Retrieve the identity tags associated with an account managed by this instance of the Xand API.
///
/// Implementations should provide their own CSPRNG with interior mutability, e.g. `Mutex<R>`.
#[async_trait]
pub trait IdentityTagSelector: Send + Sync {
    /// Randomly select an ID tag owned by the account to use for constructing a transaction.
    async fn get_owned_id_tag(&self, key: &PublicKey) -> Result<IdentityTag, FinancialClientError>;

    /// Randomly select ID tags, except for the one specified, to use as decoys when
    /// constructing a transaction.
    async fn get_decoy_id_tags(
        &self,
        true_tag: &IdentityTag,
        num_decoys: u8,
    ) -> Result<Vec<IdentityTag>, FinancialClientError>;
}

/// Gets the encryption key registered on the chain for a given address or the trust
#[async_trait]
pub trait NetworkKeyResolver: Send + Sync {
    async fn get_encryption_key(
        &self,
        key: &PublicKey,
    ) -> Result<EncryptionKey, FinancialClientError>;

    async fn get_trust_encryption_key(&self) -> Result<EncryptionKey, FinancialClientError>;

    async fn get_trust_key(&self) -> Result<PublicKey, FinancialClientError>;
}

/// Access to the local store of secret keys
#[async_trait]
pub trait KeyStore: Send + Sync {
    async fn get_all(&self) -> Result<Vec<(PublicKey, PrivateKey)>, FinancialClientError>;

    async fn get(&self, address: &PublicKey) -> Result<Option<PrivateKey>, FinancialClientError>;

    /// Check if the encryption key exists in the key store
    async fn has_encryption_key(&self, key: &EncryptionKey) -> Result<bool, FinancialClientError>;
}

#[async_trait]
impl<T: KeyStore> KeyStore for Arc<T> {
    async fn get_all(&self) -> Result<Vec<(PublicKey, PrivateKey)>, FinancialClientError> {
        (**self).get_all().await
    }

    async fn get(&self, id: &PublicKey) -> Result<Option<PrivateKey>, FinancialClientError> {
        (**self).get(id).await
    }

    async fn has_encryption_key(&self, key: &EncryptionKey) -> Result<bool, FinancialClientError> {
        (**self).has_encryption_key(key).await
    }
}

pub trait EncryptionProvider: Send + Sync {
    fn encrypt<T: Serialize>(
        &self,
        data: &T,
        sender: &EncryptionKey,
        receiver: &EncryptionKey,
        derivation_nonce: &EncryptionNonce,
    ) -> Result<Encrypted, FinancialClientError>;

    fn decrypt_as_sender<T: DeserializeOwned>(
        &self,
        message: &Encrypted,
        sender: &EncryptionKey,
        receiver: &EncryptionKey,
        derivation_nonce: &EncryptionNonce,
    ) -> Result<T, FinancialClientError>;

    fn decrypt_as_receiver<T: DeserializeOwned>(
        &self,
        message: Encrypted,
        receiver: EncryptionKey,
    ) -> Result<T, FinancialClientError>;
}

impl<T: EncryptionProvider> EncryptionProvider for Arc<T> {
    fn encrypt<K: Serialize>(
        &self,
        data: &K,
        sender: &EncryptionKey,
        receiver: &EncryptionKey,
        derivation_nonce: &EncryptionNonce,
    ) -> Result<Encrypted, FinancialClientError> {
        (**self).encrypt(data, sender, receiver, derivation_nonce)
    }

    fn decrypt_as_sender<K: DeserializeOwned>(
        &self,
        message: &Encrypted,
        sender: &EncryptionKey,
        receiver: &EncryptionKey,
        derivation_nonce: &EncryptionNonce,
    ) -> Result<K, FinancialClientError> {
        (**self).decrypt_as_sender(message, sender, receiver, derivation_nonce)
    }

    fn decrypt_as_receiver<K: DeserializeOwned>(
        &self,
        message: Encrypted,
        receiver: EncryptionKey,
    ) -> Result<K, FinancialClientError> {
        (**self).decrypt_as_receiver(message, receiver)
    }
}

#[async_trait]
pub trait TxoRepository: Send + Sync {
    //TODO: Investigate enforcing uniqueness in txo sets via typing. Could be done with Rust std
    // sets if RistrettoPoint was new typed to be Ord or Hash

    /// Tries to find a set of unique, owned, unspent [`TransactionOutput`]s that, in total, are
    /// worth at least [`amount`].
    async fn get_spendable_txos(
        &self,
        owner: PublicKey,
        amount: u64,
    ) -> Result<Vec<OpenedTransactionOutput>, FinancialClientError>;

    /// Gets all spendable [`OpenedTransactionOutput`]s older than the given time
    async fn get_all_spendable_txos(
        &self,
        owner: PublicKey,
        until: DateTime<Utc>,
    ) -> Result<Vec<OpenedTransactionOutput>, FinancialClientError>;

    /// Gets [`count`] unique decoy [`TransactionOutput`]s which constitutes decoy input set
    /// for a transaction.
    ///
    /// When there are too few [`TransactionOutput`]s available, return an error, as it is not
    /// possible for there to be fewer TxOs available than the "real" input set.\
    ///
    /// Optionally included a `timestamp` filter on how new the TxOs can be. The `timestamp`
    /// is an inclusive cutoff, so all TxOs created on or before `DateTime` should be valid.
    async fn get_decoys(
        &self,
        count: usize,
        timestamp: Option<DateTime<Utc>>,
    ) -> Result<Vec<TransactionOutput>, FinancialClientError>;
}

#[async_trait]
pub trait ClearTxoRepository: Send + Sync {
    /// Tries to find a set of unique, owned [`ClearTransactionOutput`]s that, in total, are
    /// worth at least [`amount`].
    async fn get_redeemable_txos(
        &self,
        owner: PublicKey,
        amount: u64,
    ) -> Result<Vec<ClearTransactionOutput>, FinancialClientError> {
        let redeemable_txos: Vec<ClearTransactionOutput> =
            self.get_all_redeemable_txos(owner).await?;
        Ok(random_improve(
            amount,
            redeemable_txos.iter().collect(),
            MAX_CLEAR_TXOS_PER_TRANSACTION,
        )?
        .into_iter()
        .cloned()
        .collect())
    }

    /// Gets all redeemable [`ClearTransactionOutput`]s older than the given time
    async fn get_all_redeemable_txos(
        &self,
        owner: PublicKey,
    ) -> Result<Vec<ClearTransactionOutput>, FinancialClientError>;
}

#[async_trait]
pub trait MemberContext: Send + Sync {
    async fn get_banned_members(&self) -> Result<Vec<PublicKey>, FinancialClientError>;
}

#[async_trait]
impl<T: MemberContext> MemberContext for Arc<T> {
    async fn get_banned_members(&self) -> Result<Vec<PublicKey>, FinancialClientError> {
        (**self).get_banned_members().await
    }
}
