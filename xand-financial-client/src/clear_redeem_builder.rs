use crate::{
    ClearTxoRepository, EncryptionProvider, FinancialClientError, NetworkKeyResolver,
    RedeemRequestParams,
};
use async_trait::async_trait;
use futures::lock::Mutex;
use rand::Rng;
use xand_ledger::{ClearRedeemRequestTransaction, ClearTransactionOutput};

#[async_trait]
pub trait ClearRedeemBuilder {
    async fn build_redeem_request(
        &self,
        params: RedeemRequestParams,
    ) -> Result<ClearRedeemRequestTransaction<ClearTransactionOutput>, FinancialClientError>;
}

pub trait ClearRedeemBuilderDeps {
    type ClearTxoRepo: ClearTxoRepository;
    type EncryptionProvider: EncryptionProvider;
    type NetworkKeyResolver: NetworkKeyResolver;
    type Rng: Rng + Send;
}

pub struct ClearRedeemBuilderImpl<D: ClearRedeemBuilderDeps> {
    pub clear_txo_repo: D::ClearTxoRepo,
    pub encryption_provider: D::EncryptionProvider,
    pub network_key_resolver: D::NetworkKeyResolver,
    pub rng: Mutex<D::Rng>,
}

#[async_trait]
impl<D: ClearRedeemBuilderDeps> ClearRedeemBuilder for ClearRedeemBuilderImpl<D> {
    async fn build_redeem_request(
        &self,
        params: RedeemRequestParams,
    ) -> Result<ClearRedeemRequestTransaction<ClearTransactionOutput>, FinancialClientError> {
        let spends = self
            .clear_txo_repo
            .get_redeemable_txos(params.issuer, params.amount_in_minor_unit)
            .await?;
        let issuer_encryption_key_res = self
            .network_key_resolver
            .get_encryption_key(&params.issuer)
            .await;
        let issuer_encryption_key = issuer_encryption_key_res?;
        let trust_encryption_key = self.network_key_resolver.get_trust_encryption_key().await?;
        let extra = self.encryption_provider.encrypt(
            &params.account,
            &issuer_encryption_key,
            &trust_encryption_key,
            &params.correlation_id[..].into(),
        )?;
        let transaction = xand_ledger::construct_clear_redeem_request_transaction(
            params.issuer,
            spends,
            params.amount_in_minor_unit,
            params.correlation_id,
            extra.into(),
            &mut *self.rng.lock().await,
        )
        .map_err(|error| FinancialClientError::BuildRedeemTransactionFailure { error })?;
        Ok(transaction)
    }
}

#[cfg(test)]
mod tests {
    #![allow(non_snake_case)]

    use crate::clear_redeem_builder::{
        ClearRedeemBuilder, ClearRedeemBuilderDeps, ClearRedeemBuilderImpl,
    };
    use crate::models::BankAccount;
    use crate::test_helpers::managers::{
        FakeClearTxoRepo, TestEncryptionProvider, TestNetworkKeyResolver,
    };
    use crate::test_helpers::private_key_to_address;
    use crate::{EncryptionProvider, RedeemRequestParams};
    use rand::rngs::OsRng;
    use rand::{CryptoRng, Rng};
    use std::convert::TryFrom;
    use xand_ledger::esig_payload::ESigPayload;
    use xand_ledger::{
        ClearTransactionOutput, Encrypted, EncryptionKey, IUtxo, MonetaryValue, PrivateKey,
        PublicKey,
    };

    struct TestDeps;

    impl ClearRedeemBuilderDeps for TestDeps {
        type ClearTxoRepo = FakeClearTxoRepo;
        type EncryptionProvider = TestEncryptionProvider;
        type NetworkKeyResolver = TestNetworkKeyResolver;
        type Rng = OsRng;
    }

    struct TestContext {
        builder: ClearRedeemBuilderImpl<TestDeps>,
        issuer_priv_key: PrivateKey,
    }

    impl TestContext {
        fn new<R>(rng: &mut R) -> Self
        where
            R: Rng + CryptoRng,
        {
            let issuer_priv_key = PrivateKey::random(rng);
            let issuer_addr = private_key_to_address(&issuer_priv_key);
            let issuer = PublicKey::from(issuer_priv_key);
            const SPENDABLE_TXO_COUNT: usize = 10_000;
            let spendable_txos = vec![10; SPENDABLE_TXO_COUNT];
            let builder = ClearRedeemBuilderImpl {
                clear_txo_repo: FakeClearTxoRepo {
                    clear_txos: generate_clear_txos(issuer, spendable_txos, rng),
                },
                encryption_provider: TestEncryptionProvider::default(),
                network_key_resolver: TestNetworkKeyResolver {
                    trust_encryption_key: EncryptionKey::from(rng.gen::<[u8; 32]>()),
                    trust_key: PrivateKey::random(rng).into(),
                    other_keys: IntoIterator::into_iter([(
                        issuer_addr,
                        EncryptionKey::from(rng.gen::<[u8; 32]>()),
                    )])
                    .collect(),
                },
                rng: OsRng::default().into(),
            };
            Self {
                builder,
                issuer_priv_key,
            }
        }

        fn make_redeem_request_params<R>(&self, rng: &mut R) -> RedeemRequestParams
        where
            R: Rng,
        {
            RedeemRequestParams {
                issuer: PublicKey::from(self.issuer_priv_key),
                amount_in_minor_unit: 9,
                correlation_id: rng.gen(),
                account: BankAccount {
                    routing_number: "123".into(),
                    account_number: "456".into(),
                },
            }
        }
    }

    fn generate_clear_txos<R: Rng>(
        public_key: PublicKey,
        input_values: Vec<u64>,
        rng: &mut R,
    ) -> Vec<ClearTransactionOutput> {
        input_values
            .iter()
            .map(|val| {
                ClearTransactionOutput::new(
                    public_key,
                    MonetaryValue::try_from(*val).unwrap(),
                    rng.next_u64(),
                    ESigPayload::new_with_ueta(),
                )
            })
            .collect()
    }

    #[tokio::test]
    async fn build_redeem_request__produces_transaction_with_requested_correlation_id() {
        // Given
        let rng = &mut OsRng::default();
        let context = TestContext::new(rng);
        let redeem_params = context.make_redeem_request_params(rng);
        let expected_id = redeem_params.correlation_id;

        // When
        let request = context
            .builder
            .build_redeem_request(redeem_params)
            .await
            .unwrap();

        // Then
        let actual_id = request.correlation_id();
        assert_eq!(actual_id, expected_id);
    }

    #[tokio::test]
    async fn build_redeem_request__produces_transaction_with_requested_amount() {
        // Given
        let rng = &mut OsRng::default();
        let context = TestContext::new(rng);
        let redeem_params = context.make_redeem_request_params(rng);
        let expected_amount = redeem_params.amount_in_minor_unit;

        // When
        let request = context
            .builder
            .build_redeem_request(redeem_params)
            .await
            .unwrap();

        // Then
        let actual_amount = request.output().redeem_output.value().value();
        assert_eq!(actual_amount, expected_amount);
    }

    #[tokio::test]
    async fn build_redeem_request__produces_transaction_with_requested_encrypted_bank_account() {
        // Given
        let rng = &mut OsRng::default();
        let context = TestContext::new(rng);
        let redeem_params = context.make_redeem_request_params(rng);
        let expected_account = redeem_params.account.clone();
        let correlation_id = redeem_params.correlation_id;

        // When
        let request = context
            .builder
            .build_redeem_request(redeem_params)
            .await
            .unwrap();

        // Then
        let actual_account = context
            .builder
            .encryption_provider
            .decrypt_as_sender::<BankAccount>(
                &Encrypted::from(request.extra_data()),
                &context.builder.network_key_resolver.other_keys
                    [&private_key_to_address(&context.issuer_priv_key)],
                &context.builder.network_key_resolver.trust_encryption_key,
                &correlation_id[..].into(),
            )
            .unwrap();
        assert_eq!(actual_account, expected_account);
    }
}
