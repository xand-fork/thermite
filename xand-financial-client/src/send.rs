use crate::{
    models::{
        FinancialTransaction, SecretCommitmentInputs, SendModel, Transaction, TransactionOutputData,
    },
    EncryptionProvider, FinancialClientError, KeyStore, NetworkKeyResolver,
};
use async_trait::async_trait;
use xand_ledger::{
    check_identity_tag_ownership, check_txo_ownership, decode_one_time_key,
    find_secret_input_index, DecryptSigner, OpenedTransactionOutput, PrivateKey, PublicKey,
    SendClaimsTransaction,
};

pub trait SendManagerDeps {
    type KeyStore: KeyStore;
    type EncryptionProvider: EncryptionProvider;
    type NetworkKeyResolver: NetworkKeyResolver;
}

#[async_trait]
pub trait SendManager {
    async fn reveal_send(
        &self,
        send: SendClaimsTransaction,
    ) -> Result<Transaction, FinancialClientError>;
}

pub struct SendManagerImpl<D: SendManagerDeps> {
    pub key_store: D::KeyStore,
    pub encryption_provider: D::EncryptionProvider,
    pub key_resolver: D::NetworkKeyResolver,
}

impl<D: SendManagerDeps> SendManagerImpl<D> {
    async fn reveal_send_as_recipient(
        &self,
        keys: &[(PublicKey, PrivateKey)],
        send: SendClaimsTransaction,
    ) -> Result<Transaction, FinancialClientError> {
        let mut new_txos = Vec::new();
        let mut recipient = None;
        let mut recipient_enc_key = None;
        let mut sender = None;
        for (output_index, output) in send.core_transaction.output.into_iter().enumerate() {
            let recipient = match keys
                .iter()
                .find(|(_, key)| check_txo_ownership(output.txo, *key))
            {
                Some((new_recipient, recipient_key)) => match &recipient {
                    Some(r) if new_recipient == r => Ok(new_recipient),
                    Some(_) => Err(FinancialClientError::UnsupportedSendToMultipleRecipients),
                    None => {
                        recipient = Some(*new_recipient);
                        sender = Some(
                            send.core_transaction
                                .encrypted_sender
                                .decrypt(output_index, *recipient_key),
                        );
                        Ok(new_recipient)
                    }
                },
                None => {
                    new_txos.push(TransactionOutputData::Public(output.txo));
                    continue;
                }
            }?;
            let recipient_enc_key = match recipient_enc_key {
                Some(key) => key,
                None => {
                    let key = self.key_resolver.get_encryption_key(recipient).await?;
                    recipient_enc_key = Some(key);
                    key
                }
            };
            let SecretCommitmentInputs {
                value,
                blinding_factor,
            } = self
                .encryption_provider
                .decrypt_as_receiver(output.encrypted_metadata, recipient_enc_key)?;
            let opened_txo = OpenedTransactionOutput {
                transaction_output: output.txo,
                blinding_factor,
                value,
            };
            new_txos.push(TransactionOutputData::Private(opened_txo, *recipient));
        }
        let amount = new_txos
            .iter()
            .filter_map(|output| match output {
                TransactionOutputData::Private(txo, _) => Some(txo.value),
                TransactionOutputData::Public(_) => None,
            })
            .fold(0u64, |acc, v| acc.saturating_add(v));
        let send_txn = recipient.map(|recipient| SendModel {
            recipient,
            amount_in_minor_unit: amount,
        });
        let new_id_tag = Some(send.core_transaction.output_identity);
        Ok(Transaction {
            signer: sender,
            txn: FinancialTransaction::Send(send_txn),
            new_txos,
            new_id_tag,
            spent_txos: Vec::new(),
            new_clear_txos: Vec::new(),
            redeemed_clear_txos: Vec::new(),
        })
    }

    async fn reveal_send_as_sender(
        &self,
        sender: PublicKey,
        sender_priv_key: PrivateKey,
        send: SendClaimsTransaction,
    ) -> Result<Transaction, FinancialClientError> {
        let sender_enc_key = self.key_resolver.get_encryption_key(&sender).await?;
        let mut new_txos = Vec::new();
        let mut recipient = None;
        let mut recipient_enc_key = None;
        for (output_index, output) in send.core_transaction.output.into_iter().enumerate() {
            let new_recipient = decode_one_time_key(
                *output.txo.public_key(),
                send.core_transaction.output_identity.into(),
                output_index,
                sender_priv_key,
            )
            .ok_or(FinancialClientError::FailedToDecodeOneTimeKey {
                key: Box::from(*output.txo.public_key()),
            })?;
            let recipient = match &recipient {
                Some(r) if *r == new_recipient || new_recipient == sender => Ok(new_recipient),
                Some(_) => Err(FinancialClientError::UnsupportedSendToMultipleRecipients),
                None => {
                    recipient = Some(new_recipient);
                    Ok(new_recipient)
                }
            }?;

            let recipient_enc_key = if recipient == sender {
                sender_enc_key
            } else {
                match recipient_enc_key {
                    Some(key) => key,
                    None => {
                        let key = self.key_resolver.get_encryption_key(&recipient).await?;
                        recipient_enc_key = Some(key);
                        key
                    }
                }
            };
            let SecretCommitmentInputs {
                value,
                blinding_factor,
            } = self
                .encryption_provider
                .decrypt_as_sender::<SecretCommitmentInputs>(
                    &output.encrypted_metadata,
                    &sender_enc_key,
                    &recipient_enc_key,
                    &output.txo.to_hash().into(),
                )?;
            let opened_txo = OpenedTransactionOutput {
                transaction_output: output.txo,
                blinding_factor,
                value,
            };
            new_txos.push(TransactionOutputData::Private(opened_txo, recipient));
        }
        let amount = new_txos
            .iter()
            .filter_map(|output| match output {
                TransactionOutputData::Private(txo, recipient) if *recipient != sender => {
                    Some(txo.value)
                }
                TransactionOutputData::Private(..) | TransactionOutputData::Public(_) => None,
            })
            .fold(0u64, |acc, v| acc.saturating_add(v));
        let new_id_tag = Some(send.core_transaction.output_identity);
        let input_index = find_secret_input_index(
            &send.core_transaction.input,
            &send.core_transaction.key_images,
            sender_priv_key,
        )
        .ok_or(FinancialClientError::SenderUnableToFindSpentTxos)?;
        let spent_txos = send
            .core_transaction
            .input
            .into_iter()
            .skip(input_index)
            .map(|input| input.txos.components)
            .next()
            .unwrap_or_default();
        let send_txn = recipient.map(|recipient| SendModel {
            recipient,
            amount_in_minor_unit: amount,
        });
        Ok(Transaction {
            signer: Some(sender),
            txn: FinancialTransaction::Send(send_txn),
            new_txos,
            new_id_tag,
            spent_txos,
            new_clear_txos: Vec::new(),
            redeemed_clear_txos: Vec::new(),
        })
    }
}

#[async_trait]
impl<D: SendManagerDeps> SendManager for SendManagerImpl<D> {
    async fn reveal_send(
        &self,
        send: SendClaimsTransaction,
    ) -> Result<Transaction, FinancialClientError> {
        let keys = self.key_store.get_all().await?;
        let sender = keys.iter().find(|(_, key)| {
            check_identity_tag_ownership(send.core_transaction.output_identity, *key)
        });
        match sender {
            Some((sender, sender_priv_key)) => {
                self.reveal_send_as_sender(*sender, *sender_priv_key, send)
                    .await
            }
            None => self.reveal_send_as_recipient(&keys, send).await,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        errors::FinancialClientError,
        models::{FinancialTransaction, SendModel, Transaction},
        send::{SendManager, SendManagerImpl},
        send_builder::{SendBuilder, SendBuilderImpl, SendTxParams},
        test_helpers::{
            managers::{
                FakeKeyStore, FakeSendBuilderDeps, FakeSendManagerDeps, FakeTxoRepo,
                TestEncryptionProvider, TestNetworkKeyResolver,
            },
            private_key_to_address, FakeIdentityTagSelector, FakeMemberContext,
        },
    };
    use rand::{CryptoRng, Rng};
    use std::collections::HashMap;
    use xand_address::Address;
    use xand_ledger::{
        generate_fake_input_txos, generate_spendable_utxos, EncryptionKey, IdentityTag, PrivateKey,
        PublicKey, SendClaimsTransaction,
    };
    use xand_public_key::address_to_public_key;

    #[derive(Clone, Debug)]
    struct Member {
        private_key: PrivateKey,
        encryption_key: EncryptionKey,
    }

    impl Member {
        fn new<R: CryptoRng + Rng>(mut rng: R) -> Self {
            Self {
                private_key: PrivateKey::random(&mut rng),
                encryption_key: EncryptionKey::random(&mut rng),
            }
        }

        fn public_key(&self) -> PublicKey {
            self.private_key.into()
        }

        fn address(&self) -> Address {
            private_key_to_address(&self.private_key)
        }
    }

    struct Network {
        trust_key: PublicKey,
        trust_encryption_key: EncryptionKey,
        members: Vec<Member>,
    }

    impl Network {
        fn new<R: CryptoRng + Rng>(mut rng: R, members: Vec<Member>) -> Self {
            Network {
                trust_key: PrivateKey::random(&mut rng).into(),
                trust_encryption_key: EncryptionKey::random(&mut rng),
                members,
            }
        }

        fn key_store(&self, member: &Member) -> FakeKeyStore {
            FakeKeyStore {
                encryption_keys: vec![member.encryption_key],
                ..FakeKeyStore::new(vec![member.private_key])
            }
        }

        fn encryption_key_resolver(&self) -> TestNetworkKeyResolver {
            TestNetworkKeyResolver {
                trust_encryption_key: self.trust_encryption_key,
                trust_key: self.trust_key,
                other_keys: self
                    .members
                    .iter()
                    .map(|m| (m.address(), m.encryption_key))
                    .collect(),
            }
        }

        fn send_builder<R: CryptoRng + Rng>(
            &self,
            member: &Member,
            mut rng: R,
        ) -> SendBuilderImpl<FakeSendBuilderDeps> {
            let key_store = self.key_store(member);
            let encryption_provider = TestEncryptionProvider::default();
            let encryption_key_resolver = self.encryption_key_resolver();
            let members_and_id_tags = self
                .members
                .iter()
                .map(|m| {
                    let id_tag = IdentityTag::from_key_with_rand_base(m.public_key(), &mut rng);
                    (m, id_tag)
                })
                .collect::<Vec<_>>();
            let spendable_txo = members_and_id_tags
                .iter()
                .flat_map(|(_, id_tag)| {
                    generate_spendable_utxos(*id_tag, vec![10, 20, 30], &mut rng)
                })
                .collect::<Vec<_>>();
            let decoys = generate_fake_input_txos(4, spendable_txo.len(), &mut rng)
                .into_iter()
                .flat_map(|set| set.txos.components)
                .collect();
            SendBuilderImpl::new(
                FakeIdentityTagSelector {
                    id_tags_from_address: members_and_id_tags.iter().fold(
                        HashMap::new(),
                        |mut map, (m, id_tag)| {
                            map.entry(m.address()).or_default().push(*id_tag);
                            map
                        },
                    ),
                },
                key_store,
                FakeTxoRepo {
                    spendable_txo,
                    decoys,
                },
                encryption_provider,
                encryption_key_resolver,
                FakeMemberContext::default(),
                Default::default(),
            )
        }

        fn send_manager(&self, member: &Member) -> SendManagerImpl<FakeSendManagerDeps> {
            SendManagerImpl {
                key_store: self.key_store(member),
                encryption_provider: TestEncryptionProvider::default(),
                key_resolver: self.encryption_key_resolver(),
            }
        }

        async fn encode_send<R: CryptoRng + Rng>(
            &self,
            from: &Member,
            to: &Member,
            amount: u64,
            mut rng: R,
        ) -> SendClaimsTransaction {
            let send_params = SendTxParams {
                issuer: from.public_key(),
                amount,
                recipient: to.public_key(),
            };
            self.send_builder(from, &mut rng)
                .build_send_tx(send_params)
                .await
                .unwrap()
        }

        async fn decode_send(
            &self,
            member: &Member,
            send: SendClaimsTransaction,
        ) -> Result<Transaction, FinancialClientError> {
            self.send_manager(member).reveal_send(send).await
        }
    }

    async fn can_decode<R>(issuer: &Member, recipient: &Member, decoder: &Member, mut rng: R)
    where
        R: CryptoRng + Rng,
    {
        let expected_amount = 11;
        let network = Network::new(&mut rng, vec![issuer.clone(), recipient.clone()]);
        let encoded_send = network
            .encode_send(issuer, recipient, expected_amount, &mut rng)
            .await;
        let decoded_send = network.decode_send(decoder, encoded_send).await;
        assert!(matches!(
            decoded_send,
            Ok(Transaction {
                txn: FinancialTransaction::Send(Some(SendModel {
                    recipient: actual_recipient,
                    amount_in_minor_unit,
                })),
                ..
            }) if actual_recipient == address_to_public_key(&recipient.address()).unwrap() && amount_in_minor_unit == expected_amount,
        ));
    }

    #[tokio::test]
    async fn issuer_can_decode_their_send() {
        let mut rng = rand::thread_rng();
        let issuer = Member::new(&mut rng);
        let recipient = Member::new(&mut rng);
        can_decode(&issuer, &recipient, &issuer, &mut rng).await;
    }

    #[tokio::test]
    async fn recipient_can_decode_their_send() {
        let mut rng = rand::thread_rng();
        let issuer = Member::new(&mut rng);
        let recipient = Member::new(&mut rng);
        can_decode(&issuer, &recipient, &recipient, &mut rng).await;
    }

    #[tokio::test]
    async fn member_cannot_decode_send_they_neither_sent_nor_received() {
        let mut rng = rand::thread_rng();
        let issuer = Member::new(&mut rng);
        let recipient = Member::new(&mut rng);
        let other_member = Member::new(&mut rng);
        let network = Network::new(
            &mut rng,
            vec![issuer.clone(), recipient.clone(), other_member.clone()],
        );
        let encoded_send = network.encode_send(&issuer, &recipient, 11, &mut rng).await;
        let decoded_send = network
            .decode_send(&other_member, encoded_send)
            .await
            .unwrap();
        assert!(matches!(
            decoded_send,
            Transaction {
                txn: FinancialTransaction::Send(None),
                ..
            },
        ));
    }

    #[tokio::test]
    async fn decoding_send_to_multiple_recipients_fails() {
        let mut rng = rand::thread_rng();
        let issuer = Member::new(&mut rng);
        let recipient = Member::new(&mut rng);
        let other_recipient = Member::new(&mut rng);
        let network = Network::new(
            &mut rng,
            vec![issuer.clone(), recipient.clone(), other_recipient.clone()],
        );
        let send_builder = network.send_builder(&issuer, &mut rng);
        let mut private_inputs = send_builder
            .private_inputs(issuer.public_key(), 11, recipient.public_key())
            .await
            .unwrap();
        let other_private_inputs = send_builder
            .private_inputs(issuer.public_key(), 3, other_recipient.public_key())
            .await
            .unwrap();
        private_inputs.spends.extend(other_private_inputs.spends);
        private_inputs.outputs.extend(other_private_inputs.outputs);
        let masking_inputs = send_builder
            .masking_inputs(&private_inputs, &private_inputs.identity_tag())
            .await
            .unwrap();
        let banned_members = vec![];
        let encoded_send = xand_ledger::construct_send_claims_transaction(
            private_inputs,
            masking_inputs,
            issuer.private_key,
            banned_members,
            &mut rng,
            |opened_txo, receiver_key| {
                send_builder.generate_metadata(opened_txo, receiver_key, issuer.encryption_key)
            },
        )
        .unwrap();
        let decoded_send_as_sender = network.decode_send(&issuer, encoded_send.clone()).await;
        assert!(matches!(
            decoded_send_as_sender,
            Err(FinancialClientError::UnsupportedSendToMultipleRecipients),
        ));
        let send_manager = SendManagerImpl {
            key_store: FakeKeyStore {
                encryption_keys: vec![recipient.encryption_key, other_recipient.encryption_key],
                ..FakeKeyStore::new(vec![recipient.private_key, other_recipient.private_key])
            },
            ..network.send_manager(&recipient)
        };
        let decoded_send_as_recipient = send_manager.reveal_send(encoded_send).await;
        assert!(matches!(
            decoded_send_as_recipient,
            Err(FinancialClientError::UnsupportedSendToMultipleRecipients),
        ));
    }
}
