use std::sync::{RwLockReadGuard, RwLockWriteGuard, TryLockError};
use thiserror::Error;
use xand_ledger::{OneTimeKey, PublicKey, TransactionBuilderErrors};

#[derive(Debug, Error)]
pub enum FinancialClientError {
    #[error("Not authorized to decrypt")]
    UnauthorizedDecryption,
    #[error("data source disconnected")]
    Disconnect,
    #[error("unable to acquire lock for operation")]
    CannotAcquireLock,
    #[error("no record(s) found")]
    NotFound,
    #[error("Unable to parse encrypted payload")]
    EncryptedPayloadParsingFailure {
        source: Box<dyn std::error::Error + Send + Sync>,
    },
    #[error("Key management error: {source:?}")]
    KeyManagement {
        source: Box<dyn std::error::Error + Send + Sync>,
    },
    #[error("Problem retrieving key from the network: {source:?}")]
    NetworkKey {
        source: Box<dyn std::error::Error + Send + Sync>,
    },
    #[error("Failed to decode one-time key to public key: {key:?}")]
    FailedToDecodeOneTimeKey { key: Box<OneTimeKey> },
    #[error("Failed to parse address: {reason:?}")]
    InvalidAddress { reason: String },
    #[error("Failed to parse public key: {reason:?}")]
    InvalidPublicKey { reason: String },
    #[error("The value of this transaction is not a valid number")]
    InvalidAmount,
    #[error("Cannot sign for issuer: {issuer:?}")]
    CannotSignForIssuer { issuer: PublicKey },
    #[error("Failed to build create request transaction: {error:?}")]
    BuildCreateTransactionFailure { error: TransactionBuilderErrors },
    #[error("Failed to build redeem request transaction: {error:?}")]
    BuildRedeemTransactionFailure { error: TransactionBuilderErrors },
    #[error("Operation forbidden for transaction output not owned by caller")]
    MustOwnRealTxo,
    #[error("Insufficient claims available")]
    InsufficientClaims,
    #[error("Requested too many decoys. This is a logic error")]
    RequestedTooManyDecoys,
    #[error("Try sending or redeeming a smaller amount or try again")]
    NondeterministicTxoSelectionFailed,
    #[error("A single send to multiple recipients is not supported")]
    UnsupportedSendToMultipleRecipients,
    #[error("Sender unable to find spent transaction outputs")]
    SenderUnableToFindSpentTxos,
    #[error("Encountered an unexpected error from internal component")]
    UnexpectedInternalError(Box<dyn std::error::Error + Send + Sync>),
    #[error("Encountered an unexpected error from internal component")]
    UnexpectedInternalErrorWithDescription(String),
    #[error("Error while getting clear UTxOs: {0:?}")]
    ClearUTxOs(Box<dyn std::error::Error + Send + Sync>),
}

impl<T> From<TryLockError<RwLockReadGuard<'_, T>>> for FinancialClientError {
    fn from(_: TryLockError<RwLockReadGuard<'_, T>>) -> Self {
        Self::CannotAcquireLock
    }
}

impl<T> From<TryLockError<RwLockWriteGuard<'_, T>>> for FinancialClientError {
    fn from(_: TryLockError<RwLockWriteGuard<'_, T>>) -> Self {
        Self::CannotAcquireLock
    }
}
