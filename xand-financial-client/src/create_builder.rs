use crate::{
    errors::FinancialClientError, models::CreateRequestParams, EncryptionProvider,
    IdentityTagSelector, KeyStore, MemberContext, NetworkKeyResolver,
};
use async_trait::async_trait;
use futures::lock::Mutex;
use rand::{CryptoRng, Rng};
use xand_ledger::{CreateRequestTransaction, CreateRequestTxnInputs, PublicKey};

pub trait CreateBuilderDeps {
    type IdentityTagSelector: IdentityTagSelector + Send + Sync;
    type KeyStore: KeyStore + Send + Sync;
    type NetworkKeyResolver: NetworkKeyResolver + Send + Sync;
    type EncryptionProvider: EncryptionProvider + Send + Sync;
    type MemberContext: MemberContext + Send + Sync;
    type Rng: Rng + CryptoRng + Send;
}

#[async_trait]
pub trait CreateBuilder {
    async fn build_create_request(
        &self,
        params: &CreateRequestParams,
    ) -> Result<CreateRequestTransaction, FinancialClientError>;
}

pub struct CreateBuilderImpl<D: CreateBuilderDeps> {
    pub id_tag_selector: D::IdentityTagSelector,
    pub key_store: D::KeyStore,
    pub encryption_provider: D::EncryptionProvider,
    pub network_key_resolver: D::NetworkKeyResolver,
    pub member_context: D::MemberContext,
    pub rng: Mutex<D::Rng>,
}

const STANDARD_DECOY_COUNT: u8 = 4;

impl<D: CreateBuilderDeps> CreateBuilderImpl<D> {
    async fn get_banned_members(&self) -> Result<Vec<PublicKey>, FinancialClientError> {
        self.member_context.get_banned_members().await
    }

    async fn get_txn_inputs(
        &self,
        params: &CreateRequestParams,
    ) -> Result<CreateRequestTxnInputs, FinancialClientError> {
        let private_key = self.key_store.get(&params.issuer).await?.ok_or({
            FinancialClientError::CannotSignForIssuer {
                issuer: params.issuer,
            }
        })?;
        let true_id_tag = self
            .id_tag_selector
            .get_owned_id_tag(&params.issuer)
            .await?;
        let decoy_id_tags = self
            .id_tag_selector
            .get_decoy_id_tags(&true_id_tag, STANDARD_DECOY_COUNT)
            .await?;
        let trust_pub_key = self.network_key_resolver.get_trust_key().await?;
        let extra = self.encryption_provider.encrypt(
            &params.account,
            &self
                .network_key_resolver
                .get_encryption_key(&params.issuer)
                .await?,
            &self.network_key_resolver.get_trust_encryption_key().await?,
            &params.correlation_id[..].into(),
        )?;
        let request = CreateRequestTxnInputs {
            identity_source: true_id_tag,
            values: vec![params.amount_in_minor_unit],
            masking_identity_sources: decoy_id_tags,
            correlation_id: params.correlation_id,
            private_key,
            trust_pub_key,
            extra: extra.into(),
        };
        Ok(request)
    }
}

#[async_trait]
impl<D: CreateBuilderDeps> CreateBuilder for CreateBuilderImpl<D> {
    async fn build_create_request(
        &self,
        params: &CreateRequestParams,
    ) -> Result<CreateRequestTransaction, FinancialClientError> {
        let request = self.get_txn_inputs(params).await?;
        let banned_members = self.get_banned_members().await?;
        let mut rng = self.rng.lock().await;
        let transaction =
            xand_ledger::construct_create_request_transaction(&request, banned_members, &mut *rng)
                .map_err(|e| FinancialClientError::BuildCreateTransactionFailure { error: e })?;
        Ok(transaction)
    }
}

#[cfg(test)]
pub mod test {
    use crate::{
        create_builder::{CreateBuilder, CreateBuilderDeps, CreateBuilderImpl},
        errors::FinancialClientError,
        models::{BankAccount, CreateRequestParams, Encrypted, EncryptionKey},
        test_helpers::{
            managers::{FakeKeyStore, TestEncryptionProvider, TestNetworkKeyResolver},
            private_key_to_address, FakeIdentityTagSelector, FakeMemberContext,
        },
        EncryptionProvider,
    };
    use rand::{rngs::OsRng, Rng};
    use std::collections::HashSet;
    use std::str::FromStr;
    use xand_address::Address;
    use xand_ledger::{CreateRequestTransaction, IdentityTag, PrivateKey};
    use xand_public_key::{address_to_public_key, public_key_to_address};

    pub const TRUST_ADDRESS: &str = "5G6eTvyrydaA6fFuBVnr3A5VAzAMu7NeKKfTZ2NEWirQGCk7";

    #[tokio::test]
    async fn create_request_is_built_with_passed_in_correlation_id() {
        let context = base_test_context();
        let expected_id = context.input_params.correlation_id;
        let request = context.build().await.unwrap();
        let actual_id = request.core_transaction.correlation_id;
        assert_eq!(actual_id, expected_id);
    }

    #[tokio::test]
    async fn create_request_is_built_with_issuer_id_tag() {
        let context = base_test_context();
        let expected_tag = context.owned_id_tag;
        let request = context.build().await.unwrap();
        let actual_tags = request.core_transaction.identity_sources;
        assert!(actual_tags.contains(&expected_tag));
    }

    #[tokio::test]
    async fn create_request_is_built_with_single_output_of_requested_amount() {
        let expected_amount = 10_050;
        let context = {
            let mut building_context = base_test_context();
            building_context.input_params.amount_in_minor_unit = expected_amount;
            building_context
        };
        let request = context.build().await.unwrap();
        let actual_outputs = request.core_transaction.outputs;
        assert_eq!(
            actual_outputs
                .into_iter()
                .map(|o| o.value)
                .collect::<Vec<_>>(),
            vec![expected_amount],
        );
    }

    #[tokio::test]
    async fn create_request_is_built_with_decoys_when_available() {
        let context = base_test_context();
        let decoy_id_tags = context.decoy_id_tags.clone();
        let request = context.build().await.unwrap();
        let actual_id_tags = request.core_transaction.identity_sources;
        assert_eq!(
            actual_id_tags.into_iter().collect::<HashSet<_>>(),
            decoy_id_tags
                .into_iter()
                .chain(Some(context.owned_id_tag))
                .collect(),
        );
    }

    #[tokio::test]
    async fn create_request_is_built_with_encrypted_bank_account() {
        let context = base_test_context();
        let expected_account = context.input_params.account.clone();
        let request = context.build().await.unwrap();
        let extra = Encrypted::from(request.core_transaction.extra);
        let actual_account = context
            .builder
            .encryption_provider
            .decrypt_as_sender::<BankAccount>(
                &extra,
                &context.builder.network_key_resolver.other_keys
                    [&public_key_to_address(&context.input_params.issuer)],
                &context.builder.network_key_resolver.trust_encryption_key,
                &context.input_params.correlation_id[..].into(),
            )
            .unwrap();
        assert_eq!(actual_account, expected_account);
    }

    struct TestDeps;

    impl CreateBuilderDeps for TestDeps {
        type IdentityTagSelector = FakeIdentityTagSelector;
        type KeyStore = FakeKeyStore;
        type NetworkKeyResolver = TestNetworkKeyResolver;
        type EncryptionProvider = TestEncryptionProvider;
        type MemberContext = FakeMemberContext;
        type Rng = OsRng;
    }

    struct TestContext {
        owned_id_tag: IdentityTag,
        decoy_id_tags: Vec<IdentityTag>,
        input_params: CreateRequestParams,
        builder: CreateBuilderImpl<TestDeps>,
    }

    impl TestContext {
        async fn build(&self) -> Result<CreateRequestTransaction, FinancialClientError> {
            self.builder
                .build_create_request(&self.input_params.clone())
                .await
        }
    }

    fn base_test_context() -> TestContext {
        let mut rng = OsRng::default();
        let private_key = PrivateKey::random(&mut rng);
        let owned_id_tag = IdentityTag::from_key_with_rand_base(private_key.into(), &mut rng);
        let issuer = private_key_to_address(&private_key);
        let other_private_key = PrivateKey::random(&mut rng);
        let other_participant = private_key_to_address(&other_private_key);
        let decoy_id_tags = std::iter::repeat_with(|| {
            IdentityTag::from_key_with_rand_base(other_private_key.into(), &mut rng)
        })
        .take(4)
        .collect::<Vec<_>>();
        let input_params = CreateRequestParams {
            issuer: address_to_public_key(&issuer).unwrap(),
            amount_in_minor_unit: 1,
            correlation_id: rng.gen(),
            account: BankAccount {
                routing_number: "123".into(),
                account_number: "456".into(),
            },
        };
        let id_tag_selector = FakeIdentityTagSelector {
            id_tags_from_address: vec![
                (issuer.clone(), vec![owned_id_tag]),
                (other_participant, decoy_id_tags.clone()),
            ]
            .into_iter()
            .collect(),
        };
        let key_store = FakeKeyStore::new(vec![private_key]);
        let encryption_provider = TestEncryptionProvider::default();
        let encryption_key_resolver = TestNetworkKeyResolver {
            trust_encryption_key: EncryptionKey::from(rng.gen::<[u8; 32]>()),
            trust_key: address_to_public_key(&Address::from_str(TRUST_ADDRESS).unwrap()).unwrap(),
            other_keys: Some((issuer, EncryptionKey::from(rng.gen::<[u8; 32]>())))
                .into_iter()
                .collect(),
        };
        let member_context = FakeMemberContext::default();
        let builder = CreateBuilderImpl {
            id_tag_selector,
            key_store,
            encryption_provider,
            network_key_resolver: encryption_key_resolver,
            member_context,
            rng: rng.into(),
        };
        TestContext {
            owned_id_tag,
            decoy_id_tags,
            input_params,
            builder,
        }
    }
}
