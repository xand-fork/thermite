use crate::{
    models,
    test_helpers::{managers::TestCreateMgrConfig, private_key_to_address},
    CreateManagerImpl, EncryptionProvider,
};
use rand::{CryptoRng, Rng, RngCore};
use xand_ledger::{
    CorrelationId, CreateRequestRecord, CreateRequestTransaction, Milliseconds, PrivateKey,
    TestCreateRequestBuilder,
};

pub fn construct_create_request<R>(
    csprng: &mut R,
    private_key: PrivateKey,
    create_mgr: &CreateManagerImpl<TestCreateMgrConfig>,
) -> CreateRequestTransaction
where
    R: Rng + CryptoRng + Default,
{
    let address = private_key_to_address(&private_key);
    let bank_account = models::BankAccount::new("test".to_string(), "test".to_string());

    let mut correlation_id = [0u8; 16];
    RngCore::fill_bytes(csprng, &mut correlation_id);

    let nonce = models::EncryptionNonce::from(correlation_id.as_ref());
    let sender_key = create_mgr
        .network_key_lookup
        .other_keys
        .get(&address)
        .unwrap();
    let receiver_key = &create_mgr.network_key_lookup.trust_encryption_key;
    let enc_bank_account = &create_mgr
        .encryption_provider
        .encrypt(&bank_account, sender_key, receiver_key, &nonce)
        .unwrap();

    let trust_public_key = create_mgr.network_key_lookup.trust_key;

    TestCreateRequestBuilder::default()
        .private_key(private_key)
        .correlation_id(correlation_id)
        .extra(enc_bank_account.as_slice().to_vec())
        .trust_public_key(trust_public_key)
        .build(csprng)
        .create_request
}

pub fn construct_create_record<R>(
    csprng: &mut R,
    private_key: PrivateKey,
    create_mgr: &CreateManagerImpl<TestCreateMgrConfig>,
    expires_at: Milliseconds,
) -> (CorrelationId, CreateRequestRecord)
where
    R: Rng + CryptoRng + Default,
{
    let request = construct_create_request(csprng, private_key, create_mgr);
    (
        request.core_transaction.correlation_id,
        CreateRequestRecord {
            outputs: request.core_transaction.outputs,
            identity_output: request.core_transaction.identity_output,
            account_data: request.core_transaction.extra,
            expires_at,
            encrypted_sender: request.core_transaction.encrypted_sender.into(),
        },
    )
}
