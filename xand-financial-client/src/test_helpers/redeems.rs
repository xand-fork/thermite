pub use crate::models::*;
pub use crate::redeems::*;

use crate::redeems::RedeemManagerImpl;
use crate::test_helpers::{managers::TestRedeemMgrConfig, private_key_to_address};
use crate::EncryptionProvider;
use rand::{CryptoRng, Rng, RngCore};
use std::rc::Rc;
use xand_ledger::{
    ConfidentialRedeemRequestRecord, CorrelationId, OpenedTransactionOutput, PrivateKey,
    RedeemRequestTransaction, TestRedeemRequestBuilder, TransactionBuilderErrors,
};

pub fn construct_redeem_record<R>(
    csprng: &mut R,
    private_key: PrivateKey,
    redeem_mgr: &RedeemManagerImpl<TestRedeemMgrConfig>,
) -> (CorrelationId, ConfidentialRedeemRequestRecord)
where
    R: Rng + CryptoRng,
{
    let request = construct_redeem_request(csprng, private_key, redeem_mgr);
    let id = request.core_transaction.correlation_id;
    let record = ConfidentialRedeemRequestRecord::from(request);

    (id, record)
}

pub fn construct_redeem_request<R>(
    csprng: &mut R,
    private_key: PrivateKey,
    redeem_mgr: &RedeemManagerImpl<TestRedeemMgrConfig>,
) -> RedeemRequestTransaction
where
    R: Rng + CryptoRng,
{
    let address = private_key_to_address(&private_key);
    let bank_account = BankAccount::new("test".to_string(), "test".to_string());

    let mut correlation_id = [0u8; 16];
    RngCore::fill_bytes(csprng, &mut correlation_id);

    let nonce = EncryptionNonce::from(correlation_id.as_ref());
    let sender_key = redeem_mgr
        .network_key_lookup
        .other_keys
        .get(&address)
        .unwrap();
    let receiver_key = redeem_mgr.network_key_lookup.trust_encryption_key;
    let enc_bank_account = redeem_mgr
        .encryption_provider
        .encrypt(&bank_account, sender_key, &receiver_key, &nonce)
        .unwrap();

    let trust_public_key = redeem_mgr.network_key_lookup.trust_key;

    TestRedeemRequestBuilder::default()
        .with_private_key(private_key)
        .correlation_id(correlation_id)
        .extra(enc_bank_account.as_slice().to_vec())
        .trust_public_key(trust_public_key)
        .with_encryption_key(*sender_key)
        .with_generate_metadata(generate_metadata(
            *sender_key,
            redeem_mgr.encryption_provider.clone(),
        ))
        .build(csprng)
        .redeem_request
}

pub fn generate_metadata<E: EncryptionProvider>(
    sender_key: EncryptionKey,
    encryption_provider: E,
) -> Rc<
    impl Fn(OpenedTransactionOutput, EncryptionKey) -> Result<Encrypted, TransactionBuilderErrors>,
> {
    Rc::new(
        move |txo: OpenedTransactionOutput,
              key: EncryptionKey|
              -> Result<Encrypted, TransactionBuilderErrors> {
            Ok(encryption_provider
                .encrypt(
                    &SecretCommitmentInputs {
                        blinding_factor: txo.blinding_factor,
                        value: txo.value,
                    },
                    &sender_key,
                    &key,
                    &txo.transaction_output.to_hash().into(),
                )
                .unwrap())
        },
    )
}
