use crate::{
    errors::FinancialClientError,
    models,
    test_helpers::{private_key_to_address, FakeIdentityTagSelector, FakeMemberContext},
    ClearTxoRepository, CreateManagerDeps, CreateManagerImpl, EncryptionProvider, KeyStore,
    NetworkKeyResolver, RedeemManagerDeps, RedeemManagerImpl, SendBuilderDeps, SendManagerDeps,
    TxoRepository,
};
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use curve25519_dalek::ristretto::RistrettoPoint;
use rand::rngs::OsRng;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use std::collections::HashMap;
use xand_address::Address;
use xand_ledger::{
    ClearTransactionOutput, IUtxo, OpenedTransactionOutput, PrivateKey, PublicKey,
    TransactionOutput,
};
use xand_public_key::{address_to_public_key, public_key_to_address, ristretto_point_to_address};

pub fn generate_test_create_manager_simple() -> CreateManagerImpl<TestCreateMgrConfig> {
    let mut csprng: OsRng = OsRng::default();
    let trust_key = RistrettoPoint::default();
    generate_test_member_create_manager(PrivateKey::random(&mut csprng), &trust_key.into())
}

pub fn generate_test_redeem_manager_simple() -> RedeemManagerImpl<TestRedeemMgrConfig> {
    let mut csprng: OsRng = OsRng::default();
    let trust_key = RistrettoPoint::default();
    generate_test_member_redeem_manager(PrivateKey::random(&mut csprng), &trust_key.into())
}

pub fn generate_test_trust_create_manager(
    private_key: PrivateKey,
) -> CreateManagerImpl<TestCreateMgrConfig> {
    let trust_enc_key = models::EncryptionKey::from([1u8; 32]);
    let member_enc_key = models::EncryptionKey::from([2u8; 32]);
    let trust_address: Address = private_key_to_address(&private_key);

    generate_test_create_manager_with_keys(
        private_key,
        true,
        trust_enc_key,
        trust_address,
        vec![(private_key_to_address(&private_key), member_enc_key)],
    )
}

pub fn generate_test_trust_redeem_manager(
    private_key: PrivateKey,
) -> RedeemManagerImpl<TestRedeemMgrConfig> {
    let trust_enc_key = models::EncryptionKey::from([1u8; 32]);
    let member_enc_key = models::EncryptionKey::from([2u8; 32]);
    let trust_address: Address = private_key_to_address(&private_key);

    generate_test_redeem_manager_with_keys(
        private_key,
        true,
        trust_enc_key,
        trust_address,
        vec![(private_key_to_address(&private_key), member_enc_key)],
    )
}

pub fn generate_test_member_redeem_manager(
    private_key: PrivateKey,
    trust_public_key: &PublicKey,
) -> RedeemManagerImpl<TestRedeemMgrConfig> {
    let trust_enc_key = models::EncryptionKey::from([1u8; 32]);
    let member_enc_key = models::EncryptionKey::from([2u8; 32]);
    let trust_address: Address =
        ristretto_point_to_address(&(RistrettoPoint::from(*trust_public_key)));
    generate_test_redeem_manager_with_keys(
        private_key,
        false,
        trust_enc_key,
        trust_address,
        vec![(private_key_to_address(&private_key), member_enc_key)],
    )
}

pub fn generate_test_redeem_manager_with_keys(
    private_key: PrivateKey,
    is_trust: bool,
    trust_enc_key: models::EncryptionKey,
    trust_address: Address,
    other_enc_keys: Vec<(Address, models::EncryptionKey)>,
) -> RedeemManagerImpl<TestRedeemMgrConfig> {
    let mut test_keys = FakeKeyStore::new(vec![private_key]);
    test_keys.encryption_keys = if is_trust {
        vec![trust_enc_key]
    } else {
        vec![]
    };

    let encryption_keys = TestNetworkKeyResolver {
        trust_encryption_key: trust_enc_key,
        trust_key: address_to_public_key(&trust_address).unwrap(),
        other_keys: other_enc_keys.into_iter().collect(),
    };

    RedeemManagerImpl {
        key_store: test_keys,
        network_key_lookup: encryption_keys,
        encryption_provider: TestEncryptionProvider::default(),
    }
}

pub fn generate_test_member_create_manager(
    private_key: PrivateKey,
    trust_public_key: &PublicKey,
) -> CreateManagerImpl<TestCreateMgrConfig> {
    let trust_enc_key = models::EncryptionKey::from([1u8; 32]);
    let member_enc_key = models::EncryptionKey::from([2u8; 32]);
    let trust_address: Address =
        ristretto_point_to_address(&(RistrettoPoint::from(*trust_public_key)));
    generate_test_create_manager_with_keys(
        private_key,
        false,
        trust_enc_key,
        trust_address,
        vec![(private_key_to_address(&private_key), member_enc_key)],
    )
}

pub fn generate_test_create_manager_with_keys(
    private_key: PrivateKey,
    is_trust: bool,
    trust_enc_key: models::EncryptionKey,
    trust_address: Address,
    other_enc_keys: Vec<(Address, models::EncryptionKey)>,
) -> CreateManagerImpl<TestCreateMgrConfig> {
    let mut test_keys = FakeKeyStore::new(vec![private_key]);
    test_keys.encryption_keys = if is_trust {
        vec![trust_enc_key]
    } else {
        vec![]
    };

    let encryption_keys = TestNetworkKeyResolver {
        trust_encryption_key: trust_enc_key,
        trust_key: address_to_public_key(&trust_address).unwrap(),
        other_keys: other_enc_keys.into_iter().collect(),
    };

    CreateManagerImpl {
        key_store: test_keys,
        network_key_lookup: encryption_keys,
        encryption_provider: TestEncryptionProvider::default(),
    }
}

pub struct TestCreateMgrConfig;

impl CreateManagerDeps for TestCreateMgrConfig {
    type KeyStore = FakeKeyStore;
    type EncryptionKeyResolver = TestNetworkKeyResolver;
    type EncryptionProvider = TestEncryptionProvider;
}

pub struct TestRedeemMgrConfig;

impl RedeemManagerDeps for TestRedeemMgrConfig {
    type KeyStore = FakeKeyStore;
    type EncryptionKeyResolver = TestNetworkKeyResolver;
    type EncryptionProvider = TestEncryptionProvider;
}

#[derive(Clone, Default)]
pub struct FakeKeyStore {
    pub key_map: HashMap<Address, PrivateKey>,
    pub encryption_keys: Vec<models::EncryptionKey>,
}

impl FakeKeyStore {
    pub fn new(keys: Vec<PrivateKey>) -> Self {
        Self {
            key_map: keys
                .into_iter()
                .map(|k| (private_key_to_address(&k), k))
                .collect(),
            encryption_keys: vec![],
        }
    }
}

#[async_trait]
impl KeyStore for FakeKeyStore {
    async fn get_all(&self) -> Result<Vec<(PublicKey, PrivateKey)>, FinancialClientError> {
        Ok(self
            .key_map
            .iter()
            .map(|(k, v)| (address_to_public_key(k).unwrap(), *v))
            .collect())
    }

    async fn get(&self, key: &PublicKey) -> Result<Option<PrivateKey>, FinancialClientError> {
        Ok(self.key_map.get(&public_key_to_address(key)).cloned())
    }

    async fn has_encryption_key(
        &self,
        key: &models::EncryptionKey,
    ) -> Result<bool, FinancialClientError> {
        Ok(self.encryption_keys.contains(key))
    }
}

#[derive(Clone, Debug)]
pub struct TestNetworkKeyResolver {
    pub trust_encryption_key: models::EncryptionKey,
    pub trust_key: PublicKey,
    pub other_keys: HashMap<Address, models::EncryptionKey>,
}

#[async_trait]
impl NetworkKeyResolver for TestNetworkKeyResolver {
    async fn get_encryption_key(
        &self,
        key: &PublicKey,
    ) -> Result<models::EncryptionKey, FinancialClientError> {
        Ok(*self
            .other_keys
            .get(&public_key_to_address(key))
            .unwrap_or_else(|| {
                panic!(
                    "missing encryption key for address {}",
                    public_key_to_address(key)
                )
            }))
    }

    async fn get_trust_encryption_key(
        &self,
    ) -> Result<models::EncryptionKey, FinancialClientError> {
        Ok(self.trust_encryption_key)
    }

    async fn get_trust_key(&self) -> Result<PublicKey, FinancialClientError> {
        Ok(self.trust_key)
    }
}

#[derive(Default, Debug, Clone)]
pub struct TestEncryptionProvider;

impl EncryptionProvider for TestEncryptionProvider {
    fn encrypt<T: Serialize>(
        &self,
        data: &T,
        sender: &models::EncryptionKey,
        receiver: &models::EncryptionKey,
        derivation_nonce: &models::EncryptionNonce,
    ) -> Result<models::Encrypted, FinancialClientError> {
        let payload = TestEncryptedPayload {
            data,
            sender: *sender,
            receiver: *receiver,
            derivation_nonce: derivation_nonce.clone(),
        };
        Ok(serde_json::to_vec(&payload).unwrap().into())
    }

    fn decrypt_as_sender<T: DeserializeOwned>(
        &self,
        message: &models::Encrypted,
        sender: &models::EncryptionKey,
        receiver: &models::EncryptionKey,
        derivation_nonce: &models::EncryptionNonce,
    ) -> Result<T, FinancialClientError> {
        let payload: TestEncryptedPayload<T> =
            serde_json::from_slice(message.as_slice()).expect("failed to parse json");
        // Ensure the context used to encrypt the message matches the context to decrypt it
        assert_eq!(*sender, payload.sender);
        assert_eq!(*receiver, payload.receiver);
        assert_eq!(*derivation_nonce, payload.derivation_nonce);
        Ok(payload.data)
    }

    fn decrypt_as_receiver<T: DeserializeOwned>(
        &self,
        message: models::Encrypted,
        receiver: models::EncryptionKey,
    ) -> Result<T, FinancialClientError> {
        let payload: TestEncryptedPayload<T> =
            serde_json::from_slice(message.as_slice()).expect("failed to parse json");
        // Ensure the context used to encrypt the message matches the context to decrypt it
        if receiver != payload.receiver {
            return Err(FinancialClientError::UnauthorizedDecryption);
        }

        Ok(payload.data)
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TestEncryptedPayload<T> {
    data: T,
    sender: models::EncryptionKey,
    receiver: models::EncryptionKey,
    derivation_nonce: models::EncryptionNonce,
}

#[derive(Clone, Default)]
pub struct FakeTxoRepo {
    pub spendable_txo: Vec<OpenedTransactionOutput>,
    pub decoys: Vec<TransactionOutput>,
}

#[async_trait]
impl TxoRepository for FakeTxoRepo {
    async fn get_spendable_txos(
        &self,
        _owner: PublicKey,
        amount: u64,
    ) -> Result<Vec<OpenedTransactionOutput>, FinancialClientError> {
        let available = self.spendable_txo.iter().map(|txo| txo.value).sum::<u64>();
        if available >= amount {
            Ok(self.spendable_txo.clone())
        } else {
            Err(FinancialClientError::InsufficientClaims)
        }
    }

    async fn get_all_spendable_txos(
        &self,
        _: PublicKey,
        _: chrono::DateTime<chrono::Utc>,
    ) -> Result<Vec<OpenedTransactionOutput>, FinancialClientError> {
        Ok(self.spendable_txo.clone())
    }

    async fn get_decoys(
        &self,
        count: usize,
        _timestamp: Option<DateTime<Utc>>,
    ) -> Result<Vec<TransactionOutput>, FinancialClientError> {
        Ok(self.decoys[..count].into())
    }
}

#[derive(Clone, Default)]
pub struct FakeClearTxoRepo {
    pub clear_txos: Vec<ClearTransactionOutput>,
}

#[async_trait]
impl ClearTxoRepository for FakeClearTxoRepo {
    async fn get_redeemable_txos(
        &self,
        _owner: PublicKey,
        amount: u64,
    ) -> Result<Vec<ClearTransactionOutput>, FinancialClientError> {
        let available = self
            .clear_txos
            .iter()
            .map(|txo| txo.value().value())
            .sum::<u64>();
        if available >= amount {
            Ok(self.clear_txos.clone())
        } else {
            Err(FinancialClientError::InsufficientClaims)
        }
    }

    async fn get_all_redeemable_txos(
        &self,
        _owner: PublicKey,
    ) -> Result<Vec<ClearTransactionOutput>, FinancialClientError> {
        Ok(self.clear_txos.clone())
    }
}

#[derive(Clone, Debug)]
pub struct FakeSendBuilderDeps;

impl SendBuilderDeps for FakeSendBuilderDeps {
    type KeyStore = FakeKeyStore;
    type TxoRepo = FakeTxoRepo;
    type Rng = OsRng;
    type NetworkKeyResolver = TestNetworkKeyResolver;
    type EncryptionProvider = TestEncryptionProvider;
    type IdentityTagSelector = FakeIdentityTagSelector;
    type MemberContext = FakeMemberContext;
}

#[derive(Clone, Debug)]
pub struct FakeSendManagerDeps;

impl SendManagerDeps for FakeSendManagerDeps {
    type KeyStore = FakeKeyStore;
    type EncryptionProvider = TestEncryptionProvider;
    type NetworkKeyResolver = TestNetworkKeyResolver;
}
