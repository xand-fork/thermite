pub mod managers;
pub mod redeems;
pub mod transactions;

use crate::{FinancialClientError, IdentityTagSelector, MemberContext};
use async_trait::async_trait;
use rand::{CryptoRng, RngCore};
use std::collections::HashMap;
use xand_address::Address;
use xand_ledger::{IdentityTag, PrivateKey, PublicKey};
use xand_public_key::public_key_to_address;

pub fn private_key_to_address(private_key: &PrivateKey) -> Address {
    NetworkParticipant::from(*private_key).address()
}

#[derive(Debug, Clone)]
pub struct NetworkParticipant(PrivateKey);

impl NetworkParticipant {
    pub fn new(private_key: PrivateKey) -> Self {
        Self(private_key)
    }

    pub fn random<R>(csprng: &mut R) -> Self
    where
        R: CryptoRng + RngCore,
    {
        Self::new(PrivateKey::random(csprng))
    }

    pub fn private_key(&self) -> PrivateKey {
        self.0
    }

    pub fn public_key(&self) -> PublicKey {
        self.private_key().into()
    }

    pub fn address(&self) -> Address {
        public_key_to_address(&self.public_key())
    }
}

impl From<PrivateKey> for NetworkParticipant {
    fn from(key: PrivateKey) -> Self {
        Self::new(key)
    }
}

impl<R> From<&mut R> for NetworkParticipant
where
    R: CryptoRng + RngCore,
{
    fn from(csprng: &mut R) -> Self {
        Self::random(csprng)
    }
}

#[derive(Clone, Default)]
pub struct FakeIdentityTagSelector {
    pub id_tags_from_address: HashMap<Address, Vec<IdentityTag>>,
}

#[async_trait]
impl IdentityTagSelector for FakeIdentityTagSelector {
    async fn get_owned_id_tag(&self, key: &PublicKey) -> Result<IdentityTag, FinancialClientError> {
        Ok(self
            .id_tags_from_address
            .get(&public_key_to_address(key))
            .unwrap()[0])
    }

    async fn get_decoy_id_tags(
        &self,
        true_tag: &IdentityTag,
        num_decoys: u8,
    ) -> Result<Vec<IdentityTag>, FinancialClientError> {
        Ok(self
            .id_tags_from_address
            .values()
            .flatten()
            .filter(|tag| *tag != true_tag)
            .cloned()
            .take(num_decoys.into())
            .collect())
    }
}

#[derive(Default)]
pub struct FakeMemberContext {
    pub banned_members: Vec<PublicKey>,
}

#[async_trait]
impl MemberContext for FakeMemberContext {
    async fn get_banned_members(&self) -> Result<Vec<PublicKey>, FinancialClientError> {
        Ok(self.banned_members.clone())
    }
}
