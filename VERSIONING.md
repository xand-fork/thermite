<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Versioning](#versioning)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Versioning

Crates are used internally in the repo without being published.

The cargo workspace for this repo has many crates, each with its own version, with the versions used for convenience only. 

Do not update crate versions, unless required for a coordinated release. 

Path references for dependencies between crates should not reference versions. 

