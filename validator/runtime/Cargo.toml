[package]
authors = ['Transparent Financial Systems <support@tpfs.io>']
edition = '2021'
name = 'xandstrate-runtime'
version = "4.2.1"
description = "Crate has been yanked and should not be used"

[badges]
maintenance = { status = "deprecated" }

[dependencies]
# 1st party local
xand-runtime-models = { default_features = false, path = "../../xand-runtime-models", features = ['parity-scale-codec'] }
xandstrate-error-derive = { path = "../error-derive" }

# 1st party remote
xand_governance = { version = "2.0.0", default_features = false, registry = "tpfs" }
xand_ledger = { version = "0.62.2", default_features = false, registry = "tpfs" }
wasm-hash-verifier = { version = "0.2.2", default_features = false, registry = "tpfs" }

# 3rd party
lazy_static = { version = "1.4.0", default-features = false, features = ["spin_no_std"] }
rand = { version = "0.7.3", default-features = false }
rand_core = { version = "0.5", features = ["getrandom"] }
serde = { version = "1.0", default-features = false, features = ["derive"] }
serde-scale-wrap = { version = "0.3.0", default-features = false }

# Substrate deps
frame-benchmarking = { version = "3.0.0", default-features = false, optional = true }
frame-executive = { version = "3.0.0", default-features = false }
frame-support = { version = "3.0.0", default-features = false }
frame-system = { version = "3.0.0", default-features = false }
frame-system-benchmarking = { version = "3.0.0", default-features = false, optional = true }
frame-system-rpc-runtime-api = { version = "3.0.0", default-features = false }
pallet-authorship = { version = "3.0.0", default-features = false }
pallet-aura = { version = "3.0.0", default-features = false }
pallet-grandpa = { version = "3.0.0", default-features = false }
pallet-indices = { version = "3.0.0", default-features = false }
pallet-randomness-collective-flip = { version = "3.0.0", default-features = false }
pallet-session = { version = "3.0.0", default-features = false }
pallet-sudo = { version = "3.0.0", default-features = false }
pallet-timestamp = { version = "3.0.0", default-features = false }
pallet-transaction-payment = { version = "3.0.0", default-features = false }
parity-scale-codec = { version = '2.1', default-features = false, features = ['derive'] }
sp-api = { version = "3.0.0", default-features = false }
sp-block-builder = { version = "3.0.0", default-features = false }
sp-consensus-aura = { version = "0.9.0", default-features = false }
sp-core = { version = "3.0.0", default-features = false }
sp-externalities = { version = "0.9.0", default-features = false }
sp-inherents = { version = "3.0.0", default-features = false }
sp-io = { version = "3.0.0", default-features = false }
sp-offchain = { version = "3.0.0", default-features = false }
sp-runtime = { version = "3.0.0", default-features = false }
sp-session = { version = "3.0.0", default-features = false }
sp-std = { version = "3.0.0", default-features = false }
sp-transaction-pool = { version = "3.0.0", default-features = false }
sp-version = { version = "3.0.0", default-features = false }
sp-runtime-interface = { version = "3.0.0", default-features = false }

[build-dependencies]
substrate-wasm-builder = { version = "4.0.0", default-features = false }

[dev-dependencies]
lazy_static = "1.4.0"
num = "0.3"
proptest = "0.10"
# For testing infallible conversions into a pub key
sp-core = { version = "3.0.0", default-features = true }

xand_ledger = { version = "0.62.2", default_features = false, features = ["test-helpers"], registry = "tpfs" }

frame-support-procedural = { version = "3.0.0", default-features = false }
frame-support-procedural-tools = { version = "3.0.0", default-features = false }
frame-support-procedural-tools-derive = { version = "3.0.0", default-features = false }
sp-api-proc-macro = { version = "3.0.0", default-features = false }
sp-debug-derive = { version = "3.0.0", default-features = false }
sp-panic-handler = { version = "3.0.0", default-features = false }
sp-runtime-interface-proc-macro = { version = "3.0.0", default-features = false }
sp-tracing = { version = "3.0.0", default-features = false }
sp-utils = { version = "3.0.0", default-features = false }
substrate-prometheus-endpoint = { version = "0.9.0", default-features = false }

[features]
default = ['std']
test-upgrade-pallet = []
runtime-benchmarks = [
    "frame-benchmarking",
    "frame-support/runtime-benchmarks",
    "frame-system/runtime-benchmarks",
    "frame-system-benchmarking",
    "sp-runtime/runtime-benchmarks",
    "xand_ledger/test-helpers"
]
runtime-wasm = ["xand-runtime-models/parity-scale-codec"]
std = [
    'frame-benchmarking/std',
    'frame-executive/std',
    'frame-support/std',
    'frame-system-rpc-runtime-api/std',
    'frame-system/std',
    'pallet-aura/std',
    'pallet-grandpa/std',
    'pallet-indices/std',
    'pallet-randomness-collective-flip/std',
    'pallet-session/std',
    'pallet-sudo/std',
    'pallet-timestamp/std',
    'parity-scale-codec/std',
    'serde-scale-wrap/std',
    'sp-api/std',
    'sp-block-builder/std',
    'sp-consensus-aura/std',
    'sp-core/std',
    'sp-inherents/std',
    'sp-io/std',
    'sp-offchain/std',
    'sp-runtime/std',
    'sp-session/std',
    'sp-std/std',
    'sp-transaction-pool/std',
    'sp-version/std',
    'sp-runtime-interface/std',
    'xand_ledger/std',
    'xand-runtime-models/serialization',
    'xand-runtime-models/std',
]
