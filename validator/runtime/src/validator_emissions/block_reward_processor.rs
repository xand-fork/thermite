use core::convert::TryInto;
use core::marker::PhantomData;

use super::models::validator_emission_progress::ValidatorEmissionProgressIncrementOutcome::{
    QuotaFulfilled, QuotaUnfulfilled,
};
use crate::validator_emissions::contracts::{
    block_processor::BlockInitializationProcessor,
    events::ValidatorEmissionsEventSink,
    rewards::{BlockRewardEmitter, ValidatorProgressRepository},
};

use super::EmissionFailureReason;

pub struct BlockRewardProcessor<'a, AccountId, RewardEmitter, ProgressRepository, Events>
where
    AccountId: Clone,
    RewardEmitter: BlockRewardEmitter<AccountId>,
    ProgressRepository: ValidatorProgressRepository<AccountId>,
    Events: ValidatorEmissionsEventSink<AccountId>,
{
    emitter: &'a mut RewardEmitter,
    progress_repository: &'a mut ProgressRepository,
    events: &'a mut Events,
    account: PhantomData<AccountId>,
}

impl<
        'a,
        AccountId: Clone,
        RewardEmitter: BlockRewardEmitter<AccountId>,
        ProgressRepository: ValidatorProgressRepository<AccountId>,
        Events: ValidatorEmissionsEventSink<AccountId>,
    > BlockRewardProcessor<'a, AccountId, RewardEmitter, ProgressRepository, Events>
{
    pub fn new(
        emitter: &'a mut RewardEmitter,
        progress_repository: &'a mut ProgressRepository,
        events: &'a mut Events,
    ) -> Self {
        BlockRewardProcessor::<'a, AccountId, RewardEmitter, ProgressRepository, Events> {
            emitter,
            progress_repository,
            events,
            account: PhantomData,
        }
    }
}

impl<
        'a,
        AccountId: Clone,
        RewardEmitter: BlockRewardEmitter<AccountId>,
        ProgressRepository: ValidatorProgressRepository<AccountId>,
        Events: ValidatorEmissionsEventSink<AccountId>,
    > BlockInitializationProcessor<AccountId>
    for BlockRewardProcessor<'a, AccountId, RewardEmitter, ProgressRepository, Events>
{
    fn on_initialize(&mut self, account_id: AccountId) {
        let author_progress = self
            .progress_repository
            .get_validator_emission_progress(account_id.clone());

        match author_progress.increment() {
            QuotaUnfulfilled(new_author_progress) => {
                self.events.incrementing_counter(account_id.clone());
                self.progress_repository
                    .set_validator_emission_progress(account_id, new_author_progress);
            }
            QuotaFulfilled => {
                self.progress_repository
                    .reset_validator_emission_progress_counters(account_id.clone());

                let emission_rate = author_progress.effective_emission_rate;
                if emission_rate.is_rate_zero() {
                    self.events.no_award_emitted();
                } else {
                    let amount = emission_rate.minor_units_per_emission;
                    let award_result = self
                        .emitter
                        .issue_reward(account_id.clone(), amount.clone().try_into().unwrap());
                    match award_result {
                        Ok(_) => self.events.award_emitted(account_id, amount),
                        Err(_) => self
                            .events
                            .award_failure(account_id, EmissionFailureReason::DispatchError),
                    }
                };
            }
        }
    }
}

#[cfg(test)]
mod test {
    #![allow(non_snake_case)]

    use core::convert::TryInto;
    use std::{default::Default, marker::PhantomData};

    use crate::validator_emissions::{
        contracts::{
            block_processor::BlockInitializationProcessor, rewards::ValidatorProgressRepository,
        },
        models::{
            minor_unit_emission_rate::MinorUnitEmissionRate,
            validator_emission_progress::ValidatorEmissionProgress,
            validator_emission_rate::ValidatorEmissionRate,
        },
        tests::fakes::{
            ErroringBlockRewardEmitter, FakeBlockRewardEmitter, FakeEmissionEvents,
            FakeValidatorEmissionsEventSink, FakeValidatorProgressRepository,
        },
        EmissionFailureReason,
    };

    use super::BlockRewardProcessor;

    type AccountId = u32;

    #[test]
    fn on_initialize__when_quota_unfulfilled() {
        // Given
        let account_id: u32 = 101;
        let reward: u32 = 1;
        let block_quota: u32 = 10;
        let initial_blocks_completed: u32 = 0;
        assert!(
            (initial_blocks_completed + 1) < block_quota,
            "test precondition not valid"
        );

        let mut emitter = FakeBlockRewardEmitter::<AccountId>::default();
        let mut event_source = FakeValidatorEmissionsEventSink::<AccountId>::default();
        let mut progress_repo = FakeValidatorProgressRepository::<AccountId>::with_account_progress(
            account_id,
            ValidatorEmissionProgress {
                effective_emission_rate: ValidatorEmissionRate {
                    block_quota: block_quota.try_into().unwrap(),
                    minor_units_per_emission: MinorUnitEmissionRate::from(reward),
                },
                blocks_completed_progress: initial_blocks_completed.into(),
            },
        );

        let mut sut =
            BlockRewardProcessor::new(&mut emitter, &mut progress_repo, &mut event_source);

        // When
        sut.on_initialize(account_id);

        // Then
        // it should emit event
        event_source.assert_contains(FakeEmissionEvents::IncrementingCounter(account_id));

        // it should increment progress
        let new_progress = progress_repo.get_validator_emission_progress(account_id);
        assert_eq!(
            new_progress.blocks_completed_progress.value(),
            initial_blocks_completed + 1
        );

        // it should not emit award
        assert_eq!(emitter.get_total_award(account_id), 0);
    }

    #[test]
    fn on_initialize__when_nonzero_reward_and_quota_fulfilled() {
        // Given
        let account_id: u32 = 101;
        let reward: u64 = 1;
        let block_quota: u32 = 2;
        let initial_blocks_completed: u32 = 1;
        assert!(
            initial_blocks_completed + 1 == block_quota,
            "test precondition not valid"
        );
        let effective_emission_rate = ValidatorEmissionRate {
            block_quota: block_quota.try_into().unwrap(),
            minor_units_per_emission: MinorUnitEmissionRate::from(reward),
        };

        let mut emitter = FakeBlockRewardEmitter::<AccountId>::default();
        let mut event_source = FakeValidatorEmissionsEventSink::<AccountId>::default();
        let mut progress_repo = FakeValidatorProgressRepository::<AccountId>::with_account_progress(
            account_id,
            ValidatorEmissionProgress {
                effective_emission_rate: effective_emission_rate.clone(),
                blocks_completed_progress: initial_blocks_completed.into(),
            },
        );

        let mut sut =
            BlockRewardProcessor::new(&mut emitter, &mut progress_repo, &mut event_source);

        // When
        sut.on_initialize(account_id);

        // Then
        // it should emit event
        event_source.assert_contains(FakeEmissionEvents::AwardEmitted(
            account_id,
            effective_emission_rate.minor_units_per_emission,
        ));

        // it should reset progress
        let new_progress = progress_repo.get_validator_emission_progress(account_id);
        assert_eq!(new_progress.blocks_completed_progress.value(), 0);

        // it should emit award
        assert_eq!(emitter.get_total_award(account_id), reward);
    }

    #[test]
    fn on_initialize__when_zero_reward_and_quota_fulfilled() {
        // Given
        let account_id: u32 = 101;
        let reward: u64 = 0;
        let block_quota: u32 = 2;
        let initial_blocks_completed: u32 = 1;
        assert!(
            initial_blocks_completed + 1 == block_quota,
            "test precondition not valid"
        );
        let effective_emission_rate = ValidatorEmissionRate {
            block_quota: block_quota.try_into().unwrap(),
            minor_units_per_emission: MinorUnitEmissionRate::from(reward),
        };

        let mut emitter = FakeBlockRewardEmitter::<AccountId>::default();
        let mut event_source = FakeValidatorEmissionsEventSink::<AccountId>::default();
        let mut progress_repo = FakeValidatorProgressRepository::<AccountId>::with_account_progress(
            account_id,
            ValidatorEmissionProgress {
                effective_emission_rate,
                blocks_completed_progress: initial_blocks_completed.into(),
            },
        );

        let mut sut =
            BlockRewardProcessor::new(&mut emitter, &mut progress_repo, &mut event_source);

        // When
        sut.on_initialize(account_id);

        // Then
        // it should emit event
        event_source.assert_contains(FakeEmissionEvents::NoAwardEmitted);

        // it should reset progress
        let new_progress = progress_repo.get_validator_emission_progress(account_id);
        assert_eq!(new_progress.blocks_completed_progress.value(), 0);

        // it should not emit award
        assert_eq!(emitter.get_total_award(account_id), reward);
    }

    #[test]
    fn on_initialize__when_emit_has_an_error() {
        // Given
        const EXPECTED_ERROR: &str = "Some error";
        let account_id: u32 = 101;
        let reward: u64 = 1;
        let block_quota: u32 = 2;
        let initial_blocks_completed: u32 = 1;

        let effective_emission_rate = ValidatorEmissionRate {
            block_quota: block_quota.try_into().unwrap(),
            minor_units_per_emission: MinorUnitEmissionRate::from(reward),
        };

        let mut emitter = ErroringBlockRewardEmitter::<AccountId, &str> {
            error: EXPECTED_ERROR,
            account: PhantomData,
        };
        let mut event_source = FakeValidatorEmissionsEventSink::<AccountId>::default();
        let mut progress_repo = FakeValidatorProgressRepository::<AccountId>::with_account_progress(
            account_id,
            ValidatorEmissionProgress {
                effective_emission_rate,
                blocks_completed_progress: initial_blocks_completed.into(),
            },
        );

        let mut sut =
            BlockRewardProcessor::new(&mut emitter, &mut progress_repo, &mut event_source);

        // When
        sut.on_initialize(account_id);

        // Then
        // it should emit event
        event_source.assert_contains(FakeEmissionEvents::AwardFailure(
            account_id,
            EmissionFailureReason::DispatchError,
        ));

        // it should reset progress
        let new_progress = progress_repo.get_validator_emission_progress(account_id);
        assert_eq!(new_progress.blocks_completed_progress.value(), 0);
    }
}
