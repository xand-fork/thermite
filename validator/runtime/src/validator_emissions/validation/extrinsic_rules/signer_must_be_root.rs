use crate::{
    system::ensure_root,
    validator_emissions::{
        validation::{
            errors::{ExtrinsicRuleViolation, ValidationResult},
            Validation,
        },
        Config,
    },
};

/// Validates that the signer of this extrinsic transaction must be the root
pub struct SignerOriginMustBeRoot<T>(core::marker::PhantomData<T>);
impl<T: Config> Validation for SignerOriginMustBeRoot<T> {
    type ValidationInput = T::Origin;
    fn validate(input: Self::ValidationInput) -> ValidationResult {
        Ok(ensure_root(input).map_err(|_| ExtrinsicRuleViolation::SignerOriginMustBeRoot)?)
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
pub mod test {
    use crate::{
        system::RawOrigin,
        validator_emissions::{
            validation::{
                errors::{ExtrinsicRuleViolation, ValidationFailure},
                extrinsic_rules::signer_must_be_root::SignerOriginMustBeRoot,
                Validation,
            },
            validator_emissions_test_runtime::test_runtime::TestRuntime,
        },
    };

    #[test]
    pub fn validate__passes_for_root_origin() {
        let validation_result =
            SignerOriginMustBeRoot::<TestRuntime>::validate(RawOrigin::Root.into());

        assert!(validation_result.is_ok())
    }

    #[test]
    pub fn validate__fails_for_non_root_origin() {
        let validation_result =
            SignerOriginMustBeRoot::<TestRuntime>::validate(RawOrigin::None.into());

        assert_eq!(
            validation_result,
            Err(ValidationFailure::ExtrinsicRule(
                ExtrinsicRuleViolation::SignerOriginMustBeRoot
            )),
        )
    }
}
