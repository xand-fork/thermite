#[derive(Debug, PartialEq, Eq, Clone)]
pub enum ValidationFailure {
    StateInvariant(StateInvariantViolation),
    ExtrinsicRule(ExtrinsicRuleViolation),
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum StateInvariantViolation {
    BlockQuotaMustBeOne,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum ExtrinsicRuleViolation {
    SignerOriginMustBeRoot,
}

impl From<ExtrinsicRuleViolation> for ValidationFailure {
    fn from(val: ExtrinsicRuleViolation) -> Self {
        Self::ExtrinsicRule(val)
    }
}

impl From<StateInvariantViolation> for ValidationFailure {
    fn from(val: StateInvariantViolation) -> Self {
        Self::StateInvariant(val)
    }
}

pub type ValidationResult = Result<(), ValidationFailure>;
