use core::ops::Add;
use parity_scale_codec::{Decode, Encode};
use serde::{Deserialize, Serialize};

#[derive(Default, Clone, Copy, PartialEq, Debug, Eq, Encode, Decode, Deserialize, Serialize)]
pub struct BlockCompletionCount(u32);
impl BlockCompletionCount {
    pub fn increment(&self) -> Self {
        Self(self.value().add(1u32))
    }

    pub fn value(&self) -> u32 {
        self.0
    }
}

impl From<u32> for BlockCompletionCount {
    fn from(value: u32) -> Self {
        Self(value)
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod tests {
    use crate::validator_emissions::models::block_completion_count::BlockCompletionCount;

    #[test]
    fn from__can_be_constructed_from_u32() {
        let value = 1u32;

        let b_c_c = BlockCompletionCount::from(value);

        assert_eq!(b_c_c.value(), value)
    }

    #[test]
    fn value__returns_expected_u32_value() {
        let b_c_c = BlockCompletionCount::default();

        assert_eq!(b_c_c.value(), 0u32)
    }

    #[test]
    fn increment__returns_incremented_block_completion_count() {
        let b_c_c = BlockCompletionCount::default();

        let incremented_count = b_c_c.increment();

        assert_eq!(incremented_count.value(), 1u32)
    }

    #[test]
    fn default__value_is_zero() {
        let default = BlockCompletionCount::default();

        assert_eq!(default.value(), 0u32)
    }
}
