use core::{convert::TryFrom, num::NonZeroU32};
use parity_scale_codec::{Decode, Encode};
use serde::{Deserialize, Serialize};

use crate::validator_emissions::models::errors::ModelConstructionFailure;

use super::block_completion_count::BlockCompletionCount;

#[derive(Clone, PartialEq, Debug, Eq, Encode, Decode, Deserialize, Serialize)]
/// BlockQuota represents a quota of produced blocks that must be met for a reward emission
/// This structure prevents negative numbers and allows a reasonable bounds for a quota
pub struct BlockQuota(NonZeroU32);
impl BlockQuota {
    /// Returns the inner value stored in the BlockQuota
    pub fn value(&self) -> u32 {
        self.0.get()
    }

    pub fn is_fulfilled_by(&self, completion_count: &BlockCompletionCount) -> bool {
        let blocks_completed = completion_count.value();
        let block_quota = self.value();

        blocks_completed >= block_quota
    }
}

/// The default implementation of BlockQuota must be one per state invariant
impl Default for BlockQuota {
    fn default() -> Self {
        BlockQuota(NonZeroU32::new(1u32).unwrap())
    }
}

impl TryFrom<u32> for BlockQuota {
    type Error = ModelConstructionFailure;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        Ok(Self(NonZeroU32::new(value).ok_or(
            ModelConstructionFailure::ZeroValueSuppliedToNonZeroType,
        )?))
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod tests {
    use crate::validator_emissions::models::{
        block_completion_count::BlockCompletionCount, block_quota::BlockQuota,
        errors::ModelConstructionFailure,
    };
    use core::convert::TryFrom;

    #[test]
    fn try_from__can_be_constructed_from_u32() {
        let value = 1u32;

        let b_q = BlockQuota::try_from(value).unwrap();

        assert_eq!(b_q.value(), 1u32)
    }

    #[test]
    fn try_from__cannot_be_constructed_from_0u32() {
        let value = 0u32;

        let err = BlockQuota::try_from(value).unwrap_err();

        assert_eq!(
            err,
            ModelConstructionFailure::ZeroValueSuppliedToNonZeroType
        )
    }

    #[test]
    fn default__value_is_one() {
        let default = BlockQuota::default();

        assert_eq!(default.value(), 1u32)
    }

    #[test]
    fn is_fulfilled_by__no_completions_returns_false() {
        let quota = BlockQuota::try_from(3).unwrap();
        let completion_count = BlockCompletionCount::from(0);

        assert!(!quota.is_fulfilled_by(&completion_count));
    }

    #[test]
    fn is_fulfilled_by__one_too_small_returns_false() {
        let quota = BlockQuota::try_from(3).unwrap();
        let completion_count = BlockCompletionCount::from(2);

        assert!(!quota.is_fulfilled_by(&completion_count));
    }

    #[test]
    fn is_fulfilled_by__equal_completions_returns_true() {
        let quota = BlockQuota::try_from(3).unwrap();
        let completion_count = BlockCompletionCount::from(3);

        assert!(quota.is_fulfilled_by(&completion_count));
    }

    #[test]
    fn is_fulfilled_by__one_extra_completion_returns_true() {
        let quota = BlockQuota::try_from(3).unwrap();
        let completion_count = BlockCompletionCount::from(4);

        assert!(quota.is_fulfilled_by(&completion_count));
    }

    #[test]
    fn is_fulfilled_by__single_block_quota_can_be_false() {
        let quota = BlockQuota::try_from(1).unwrap();
        let completion_count = BlockCompletionCount::from(0);

        assert!(!quota.is_fulfilled_by(&completion_count));
    }

    #[test]
    fn is_fulfilled_by__single_block_quota_can_be_true() {
        let quota = BlockQuota::try_from(1).unwrap();
        let completion_count = BlockCompletionCount::from(1);

        assert!(quota.is_fulfilled_by(&completion_count));
    }
}
