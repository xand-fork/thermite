use core::convert::TryFrom;

use parity_scale_codec::{Decode, Encode};
use serde::{Deserialize, Serialize};
use xand_ledger::{MonetaryValue, MonetaryValueError};

use crate::validator_emissions::models::{
    block_quota::BlockQuota, minor_unit_emission_rate::MinorUnitEmissionRate,
};

#[derive(Default, Clone, PartialEq, Debug, Eq, Encode, Decode, Deserialize, Serialize)]
pub struct ValidatorEmissionRate {
    pub minor_units_per_emission: MinorUnitEmissionRate,
    pub block_quota: BlockQuota,
}

impl ValidatorEmissionRate {
    pub fn is_rate_zero(&self) -> bool {
        self.minor_units_per_emission.value() == 0
    }
}

impl TryFrom<MinorUnitEmissionRate> for MonetaryValue {
    type Error = MonetaryValueError;

    fn try_from(rate: MinorUnitEmissionRate) -> Result<Self, Self::Error> {
        MonetaryValue::try_from(rate.value())
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn is_rate_zero__true_for_zero() {
        // Given
        const RATE_VALUE: u32 = 0;
        let rate = ValidatorEmissionRate {
            minor_units_per_emission: MinorUnitEmissionRate::from(RATE_VALUE),
            block_quota: Default::default(),
        };

        // When
        let result = rate.is_rate_zero();

        // Then
        assert!(result)
    }

    #[test]
    fn is_rate_zero__false_for_nonzero() {
        // Given
        const RATE_VALUE: u32 = 1;
        let rate = ValidatorEmissionRate {
            minor_units_per_emission: MinorUnitEmissionRate::from(RATE_VALUE),
            block_quota: Default::default(),
        };

        // When
        let result = rate.is_rate_zero();

        // Then
        assert!(!result)
    }
}
