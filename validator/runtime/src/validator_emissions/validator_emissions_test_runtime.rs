pub mod constants {
    pub const VAL_1: u64 = 99;
    pub const VAL_2: u64 = 199;
    pub const VAL_3: u64 = 299;
}

mod mock_find_author {
    use frame_support::{traits::FindAuthor, ConsensusEngineId};
    use parity_scale_codec::Decode;

    use crate::validator_emissions::validator_emissions_test_runtime::test_runtime::TEST_CONSENSUS_ID;

    /// Returns the first value where the Digest's id matches TEST_CONSENSUS_ID
    pub struct MockFindAuthor;

    impl FindAuthor<u64> for MockFindAuthor {
        fn find_author<'a, I>(digests: I) -> Option<u64>
        where
            I: 'a + IntoIterator<Item = (ConsensusEngineId, &'a [u8])>,
        {
            for (id, mut data) in digests {
                if id == TEST_CONSENSUS_ID {
                    return u64::decode(&mut data).ok();
                }
            }
            None
        }
    }
}

pub mod builders {
    use super::test_runtime::TestRuntime;

    pub fn base_test_ext() -> sp_io::TestExternalities {
        let gen_config = frame_system::GenesisConfig::default()
            .build_storage::<TestRuntime>()
            .unwrap();
        gen_config.into()
    }
}

pub mod test_runtime {
    use parity_scale_codec::{Decode, Encode};
    use sp_core::H256;
    use sp_runtime::{
        testing::Header,
        traits::{BlakeTwo256, IdentityLookup},
        ConsensusEngineId,
    };

    use crate::confidentiality::reward::{RewardResult, Rewarder};
    use crate::validator_emissions as validator_emissions_pallet;
    use crate::validator_emissions::validator_emissions_test_runtime::constants::{
        VAL_1, VAL_2, VAL_3,
    };
    use crate::validator_emissions::validator_emissions_test_runtime::mock_find_author::MockFindAuthor;
    use crate::xandvalidators::queryable_xand_validators::QueryableXandValidators;
    use xand_ledger::MonetaryValue;

    /// Helper for building test headers for pallet_authorship. Used in validator_emissions testing
    pub const TEST_CONSENSUS_ID: ConsensusEngineId = [1, 2, 3, 4];

    type UncheckedExtrinsic = frame_system::mocking::MockUncheckedExtrinsic<TestRuntime>;
    type Block = frame_system::mocking::MockBlock<TestRuntime>;

    frame_support::construct_runtime!(
        pub enum TestRuntime where
            Block = Block,
            NodeBlock = Block,
            UncheckedExtrinsic = UncheckedExtrinsic,
        {
            System: frame_system::{Module, Call, Config, Storage, Event<T>},
            ValidatorEmissions: validator_emissions_pallet::{Module, Call, Storage, Event<T>},
        }
    );

    #[derive(Debug, Encode, Decode, Clone, Eq, PartialEq)]
    pub struct FakeEvent(pub String);

    impl From<()> for FakeEvent {
        fn from(_: ()) -> Self {
            FakeEvent("empty".to_string())
        }
    }

    impl From<crate::validator_emissions::Event<TestRuntime>> for FakeEvent {
        fn from(e: crate::validator_emissions::Event<TestRuntime>) -> Self {
            FakeEvent(format!("validator_emissions: {:?}", e))
        }
    }

    impl From<frame_system::Event<TestRuntime>> for FakeEvent {
        fn from(e: frame_system::Event<TestRuntime>) -> Self {
            FakeEvent(format!("{:?}", e))
        }
    }

    type AccountId = u64;

    impl frame_system::Config for TestRuntime {
        type BaseCallFilter = ();
        type BlockWeights = ();
        type BlockLength = ();
        type Origin = Origin;
        type Call = Call;
        type Index = u64;
        type BlockNumber = u64;
        type Hash = H256;
        type Hashing = BlakeTwo256;
        type AccountId = AccountId;
        type Lookup = IdentityLookup<Self::AccountId>;
        type Header = Header;
        type Event = FakeEvent;
        type BlockHashCount = ();
        type DbWeight = ();
        type Version = ();
        type PalletInfo = PalletInfo;
        type AccountData = ();
        type OnNewAccount = ();
        type OnKilledAccount = ();
        type SystemWeightInfo = ();
        type SS58Prefix = ();
    }

    impl crate::validator_emissions::Config for TestRuntime {
        type Event = FakeEvent;
    }

    impl Rewarder for TestRuntime {
        fn reward_validator_block(
            _account_id: &Self::AccountId,
            _amount: MonetaryValue,
        ) -> RewardResult {
            Ok(None)
        }
    }

    // Reference: https://github.com/substrate-developer-hub/substrate-node-template/issues/51#issuecomment-639092934
    impl pallet_authorship::Config for TestRuntime {
        type FindAuthor = MockFindAuthor;
        type UncleGenerations = ();
        type FilterUncle = ();
        type EventHandler = ();
    }

    impl QueryableXandValidators for TestRuntime {
        /// For testing purposes, TestRuntime hardcodes these validators
        fn is_validator(account: &Self::AccountId) -> bool {
            [VAL_1, VAL_2, VAL_3].contains(account)
        }
    }
}
