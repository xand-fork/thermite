use core::convert::TryInto;

use crate::validator_emissions::models::minor_unit_emission_rate::MinorUnitEmissionRate;
use crate::validator_emissions::validator_emissions_test_runtime::{
    builders::base_test_ext,
    constants::{VAL_2, VAL_3},
    test_runtime::TestRuntime,
};
use crate::validator_emissions::{
    models::{
        block_quota::BlockQuota, validator_emission_progress::ValidatorEmissionProgress,
        validator_emission_rate::ValidatorEmissionRate,
    },
    EmissionProgress, EmissionRateSetting,
};

/// Substrate docs on unit-testing
/// https://substrate.dev/docs/en/knowledgebase/runtime/tests
use super::super::Pallet;

#[test]
fn reset_validator_emission_progress_counters__creates_previously_nonexistent_entry() {
    base_test_ext().execute_with(|| {
        // Given
        let block_quota: BlockQuota = 5u32.try_into().unwrap();
        let minor_units_per_emission: MinorUnitEmissionRate = 123u64.into();
        EmissionRateSetting::<TestRuntime>::put(ValidatorEmissionRate {
            block_quota: block_quota.clone(),
            minor_units_per_emission: minor_units_per_emission.clone(),
        });

        // When
        Pallet::<TestRuntime>::reset_validator_emission_progress_counters(&VAL_2);

        // Then
        assert_eq!(
            EmissionProgress::<TestRuntime>::get(VAL_2),
            ValidatorEmissionProgress {
                blocks_completed_progress: 0u32.into(),
                effective_emission_rate: ValidatorEmissionRate {
                    minor_units_per_emission,
                    block_quota,
                },
            }
        )
    })
}

#[test]
fn reset_validator_emission_progress_counters__overwrites_existing_entry() {
    base_test_ext().execute_with(|| {
        // Given
        let new_block_quota: BlockQuota = 5u32.try_into().unwrap();
        let new_minor_units_per_emission: MinorUnitEmissionRate = 123u64.into();

        let older_block_quota: BlockQuota = 6u32.try_into().unwrap();
        let older_emission_rate: MinorUnitEmissionRate = 234u64.into();

        EmissionRateSetting::<TestRuntime>::put(ValidatorEmissionRate {
            block_quota: new_block_quota.clone(),
            minor_units_per_emission: new_minor_units_per_emission.clone(),
        });

        EmissionProgress::<TestRuntime>::insert(
            VAL_2,
            ValidatorEmissionProgress {
                blocks_completed_progress: 3u32.into(),
                effective_emission_rate: ValidatorEmissionRate {
                    block_quota: older_block_quota,
                    minor_units_per_emission: older_emission_rate,
                },
            },
        );

        // When
        Pallet::<TestRuntime>::reset_validator_emission_progress_counters(&VAL_2);

        // Then
        assert_eq!(
            EmissionProgress::<TestRuntime>::get(VAL_2),
            ValidatorEmissionProgress {
                blocks_completed_progress: 0u32.into(),
                effective_emission_rate: ValidatorEmissionRate {
                    block_quota: new_block_quota,
                    minor_units_per_emission: new_minor_units_per_emission,
                },
            }
        )
    })
}

#[test]
fn reset_validator_emission_progress_counters__leaves_other_entries_unchanged() {
    base_test_ext().execute_with(|| {
        // Given
        let block_quota: BlockQuota = 6u32.try_into().unwrap();
        let minor_units_per_emission: MinorUnitEmissionRate = 234u64.into();
        EmissionRateSetting::<TestRuntime>::put(ValidatorEmissionRate {
            block_quota: 5u32.try_into().unwrap(),
            minor_units_per_emission: 123u64.into(),
        });
        EmissionProgress::<TestRuntime>::insert(
            VAL_3,
            ValidatorEmissionProgress {
                blocks_completed_progress: 3u32.into(),
                effective_emission_rate: ValidatorEmissionRate {
                    block_quota: block_quota.clone(),
                    minor_units_per_emission: minor_units_per_emission.clone(),
                },
            },
        );

        // When
        Pallet::<TestRuntime>::reset_validator_emission_progress_counters(&VAL_2);

        // Then
        assert_eq!(
            EmissionProgress::<TestRuntime>::get(VAL_3),
            ValidatorEmissionProgress {
                blocks_completed_progress: 3u32.into(),
                effective_emission_rate: ValidatorEmissionRate {
                    minor_units_per_emission,
                    block_quota,
                },
            }
        )
    })
}
