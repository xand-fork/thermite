pub trait BlockInitializationProcessor<AccountId> {
    fn on_initialize(&mut self, account_id: AccountId);
}
