use crate::ValidatorEmissionProgress;
use xand_ledger::MonetaryValue;

pub trait BlockRewardEmitter<AccountId> {
    type Error;

    fn issue_reward(
        &mut self,
        account_id: AccountId,
        amount: MonetaryValue,
    ) -> Result<(), Self::Error>;
}

pub trait ValidatorProgressRepository<AccountId> {
    fn get_validator_emission_progress(
        &mut self,
        account_id: AccountId,
    ) -> ValidatorEmissionProgress;

    fn set_validator_emission_progress(
        &mut self,
        account_id: AccountId,
        progress: ValidatorEmissionProgress,
    );

    fn reset_validator_emission_progress_counters(&mut self, account_id: AccountId);
}
