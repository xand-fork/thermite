use crate::{
    validator_emissions::{
        models::minor_unit_emission_rate::MinorUnitEmissionRate, EmissionFailureReason,
    },
    ValidatorEmissionRate,
};

pub trait ValidatorEmissionsEventSink<AccountId> {
    fn incrementing_counter(&mut self, account_id: AccountId);
    fn unknown_author_id_in_header(&mut self, account_id: AccountId);
    fn emission_rate_rejected(&mut self, minor_units_per_emission: u64, block_quota: u32);
    fn emission_rate_set(&mut self, rate: ValidatorEmissionRate);
    fn award_emitted(&mut self, account_id: AccountId, amount: MinorUnitEmissionRate);
    fn no_award_emitted(&mut self);
    fn award_failure(&mut self, account_id: AccountId, reason: EmissionFailureReason);
}
