use crate::validator_emissions::validation::errors::ValidationResult;

/// Validation-specific errors without generics that have translations to ValidatorEmissionsFailures
pub mod errors;
/// Extrinsic Rules are optional rules that may enforced during the handling of an extrinsic
pub mod extrinsic_rules;

pub trait Validation {
    type ValidationInput;
    fn validate(input: Self::ValidationInput) -> ValidationResult;
}
