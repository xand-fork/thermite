use core::convert::TryInto;
use frame_support::dispatch::DispatchResultWithPostInfo;

use crate::validator_emissions::Error;
use crate::validator_emissions::{
    extrinsics::ValidatorEmissionExtrinsic,
    models::{
        block_quota::BlockQuota, minor_unit_emission_rate::MinorUnitEmissionRate,
        validator_emission_rate::ValidatorEmissionRate,
    },
    validation::{
        errors::ValidationResult, extrinsic_rules::signer_must_be_root::SignerOriginMustBeRoot,
        Validation,
    },
    Config, EmissionRateSetting, Event, Pallet as ValidatorEmissionsPallet,
    ValidatorEmissionsResult,
};

pub struct SetValidatorEmissionRateTxInput<T: Config> {
    pub origin: T::Origin,
    pub minor_units_per_emission: u64,
    pub block_quota: u32,
}

pub struct SetValidatorEmissionRateValidationInput<T: Config> {
    pub origin: T::Origin,
    pub block_quota: BlockQuota,
}

pub struct SetValidatorEmissionRateExtrinsic<T: Config>(core::marker::PhantomData<T>);

impl<T: Config> SetValidatorEmissionRateExtrinsic<T> {
    pub fn try_tx(
        origin: T::Origin,
        minor_units_per_emission: MinorUnitEmissionRate,
        block_quota: BlockQuota,
    ) -> ValidatorEmissionsResult<(), T> {
        Self::validate(SetValidatorEmissionRateValidationInput {
            origin,
            block_quota: block_quota.clone(),
        })?;
        let new_rate = ValidatorEmissionRate {
            minor_units_per_emission,
            block_quota,
        };
        EmissionRateSetting::<T>::set(new_rate.clone());
        ValidatorEmissionsPallet::<T>::deposit_event(Event::<T>::EmissionRateSet(new_rate));
        Ok(())
    }

    fn emit_failure(minor_units_per_emission: u64, block_quota: u32) {
        ValidatorEmissionsPallet::<T>::deposit_event(Event::<T>::EmissionRateRejected(
            minor_units_per_emission,
            block_quota,
        ));
    }
}

impl<T: Config> Validation for SetValidatorEmissionRateExtrinsic<T> {
    type ValidationInput = SetValidatorEmissionRateValidationInput<T>;

    fn validate(input: Self::ValidationInput) -> ValidationResult {
        SignerOriginMustBeRoot::<T>::validate(input.origin)?;
        Ok(())
    }
}

impl<T: Config> ValidatorEmissionExtrinsic for SetValidatorEmissionRateExtrinsic<T> {
    type ExtrinsicInput = SetValidatorEmissionRateTxInput<T>;

    fn tx(input: Self::ExtrinsicInput) -> DispatchResultWithPostInfo {
        let minor_units_per_emission = input.minor_units_per_emission;
        let block_quota = input.block_quota;
        Self::try_tx(
            input.origin,
            minor_units_per_emission.into(),
            block_quota.try_into().map_err(|e| {
                Self::emit_failure(minor_units_per_emission, block_quota);
                Error::<T>::from(e)
            })?,
        )
        .map_err(|e| {
            Self::emit_failure(minor_units_per_emission, block_quota);
            e.into()
        })
        .map(Into::into)
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
pub mod test {
    use crate::{
        system::RawOrigin,
        validator_emissions::{
            extrinsics::{
                set_validator_emission_rate::{
                    SetValidatorEmissionRateExtrinsic, SetValidatorEmissionRateTxInput,
                },
                ValidatorEmissionExtrinsic,
            },
            models::{block_quota::BlockQuota, validator_emission_rate::ValidatorEmissionRate},
            validator_emissions_test_runtime::{
                builders::base_test_ext, constants::VAL_1, test_runtime::TestRuntime,
            },
            EmissionRateSetting, Error,
        },
    };

    #[test]
    fn tx__successfully_sets_new_rate() {
        base_test_ext().execute_with(|| {
            // Given
            EmissionRateSetting::<TestRuntime>::put(ValidatorEmissionRate {
                block_quota: BlockQuota::default(),
                minor_units_per_emission: 123u64.into(),
            });

            // When
            SetValidatorEmissionRateExtrinsic::<TestRuntime>::tx(SetValidatorEmissionRateTxInput {
                origin: RawOrigin::Root.into(),
                minor_units_per_emission: 100,
                block_quota: 1u32,
            })
            .unwrap();

            // Then
            assert_eq!(
                EmissionRateSetting::<TestRuntime>::get(),
                ValidatorEmissionRate {
                    block_quota: BlockQuota::default(),
                    minor_units_per_emission: 100u64.into(),
                }
            )
        })
    }

    #[test]
    fn tx__does_not_allow_non_root_origin() {
        base_test_ext().execute_with(|| {
            // Given
            EmissionRateSetting::<TestRuntime>::put(ValidatorEmissionRate {
                block_quota: BlockQuota::default(),
                minor_units_per_emission: 123u64.into(),
            });

            // When
            let dispatch_err = SetValidatorEmissionRateExtrinsic::<TestRuntime>::tx(
                SetValidatorEmissionRateTxInput {
                    origin: RawOrigin::Signed(VAL_1).into(),
                    minor_units_per_emission: 100,
                    block_quota: 1u32,
                },
            )
            .unwrap_err();

            // Then
            assert_eq!(
                dispatch_err,
                Error::<TestRuntime>::SignerOriginMustBeRoot.into()
            )
        })
    }
}
