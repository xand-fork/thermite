use core::{fmt, fmt::Formatter};

use crate::{
    confidentiality::reward::{RewardError, Rewarder},
    validator_emissions::{
        adapters::{
            XandBlockRewardEmitter, XandValidatorEmissionsEventSink,
            XandValidatorProgressRepository,
        },
        block_reward_processor::BlockRewardProcessor,
        contracts::block_processor::BlockInitializationProcessor,
        extrinsics::{
            set_validator_emission_rate::{
                SetValidatorEmissionRateExtrinsic, SetValidatorEmissionRateTxInput,
            },
            ValidatorEmissionExtrinsic,
        },
        models::errors::ModelConstructionFailure,
        validation::errors::{ExtrinsicRuleViolation, StateInvariantViolation, ValidationFailure},
    },
    xandvalidators::queryable_xand_validators::QueryableXandValidators,
    DEFAULT_WEIGHT,
};
use models::{
    block_completion_count::BlockCompletionCount,
    validator_emission_progress::ValidatorEmissionProgress,
    validator_emission_rate::ValidatorEmissionRate,
};
use xand_ledger::MonetaryValueError;

use parity_scale_codec::{Decode, Encode};
use serde::{Deserialize, Serialize};

use self::models::minor_unit_emission_rate::MinorUnitEmissionRate;

mod adapters;
mod block_reward_processor;
mod contracts;
pub mod extrinsics;
pub mod models;
mod validation;
#[cfg(test)]
mod validator_emissions_test_runtime;

pub use pallet::*;

#[allow(clippy::unused_unit)]
#[frame_support::pallet]
pub mod pallet {
    use super::*;
    use frame_support::pallet_prelude::*;
    use frame_system::pallet_prelude::*;
    use xandstrate_error_derive::ErrorHelper;

    #[pallet::config]
    pub trait Config:
        frame_system::Config + pallet_authorship::Config + Rewarder + QueryableXandValidators
    {
        type Event: From<Event<Self>> + IsType<<Self as frame_system::Config>::Event>;
    }

    #[pallet::pallet]
    #[pallet::generate_store(pub (super) trait Store)]
    pub struct Pallet<T>(PhantomData<T>);

    #[pallet::hooks]
    impl<T: Config> Hooks<BlockNumberFor<T>> for Pallet<T> {
        fn on_initialize(_n: <T as frame_system::Config>::BlockNumber) -> Weight {
            if let Ok(author) = Pallet::<T>::get_author() {
                let mut emitter = XandBlockRewardEmitter::<T>::new();
                let mut progress_repository = XandValidatorProgressRepository::<T>::new();
                let mut event_source = XandValidatorEmissionsEventSink::<T>::new();
                let mut processor = BlockRewardProcessor::new(
                    &mut emitter,
                    &mut progress_repository,
                    &mut event_source,
                );
                processor.on_initialize(author);
            }

            Weight::default()
        }
    }

    #[pallet::call]
    impl<T: Config> Pallet<T> {
        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn set_validator_emission_rate(
            origin: OriginFor<T>,
            minor_units_per_emission: u64,
            block_quota: u32,
        ) -> DispatchResultWithPostInfo {
            SetValidatorEmissionRateExtrinsic::<T>::tx(SetValidatorEmissionRateTxInput {
                origin,
                minor_units_per_emission,
                block_quota,
            })
        }
    }

    #[pallet::event]
    #[pallet::generate_deposit(pub (super) fn deposit_event)]
    pub enum Event<T: Config> {
        IncrementingCounter(T::AccountId),
        UnknownAuthorIdInHeader(T::AccountId),
        EmissionRateRejected(u64, u32),
        EmissionRateSet(ValidatorEmissionRate),
        AwardEmitted(T::AccountId, MinorUnitEmissionRate),
        NoAwardEmitted,
        AwardFailure(T::AccountId, EmissionFailureReason),
    }

    #[pallet::error]
    #[derive(ErrorHelper)]
    pub enum Error<T> {
        IdNotInAuthoritySet,
        BlockQuotaMustBeOne,
        SignerOriginMustBeRoot,
        ZeroValueSuppliedForNonZeroType,
        InvalidMonetaryValue,
        InvalidPublicKey,
        RewardDispatchError,
    }

    /// EmissionRateSetting
    ///
    /// Stores the current minor units per emission and block quota.
    ///
    /// This value is used upon initialization of an individual validator's emission progress.
    /// Changing this value will not update the quota that a validator is working toward if they
    /// already have an existing progress.
    #[pallet::storage]
    #[pallet::getter(fn emission_rate_setting)]
    pub type EmissionRateSetting<T: Config> = StorageValue<_, ValidatorEmissionRate, ValueQuery>;

    #[pallet::type_value]
    pub fn DefaultForEmissionProgress<T: Config>() -> ValidatorEmissionProgress {
        Pallet::<T>::get_initial_emission_progress()
    }

    /// EmissionProgress
    ///
    /// Stores the current progress towards an assigned EmissionRateSetting for individual
    /// validators.
    ///
    /// A validator's progress is measured against the EmissionRateSetting which was current at
    /// the start of their progress until that rate is completed.
    /// After a validator's quota is met, a new effective_emission_rate for their progress
    /// should be set based on the current EmissionRateSetting.
    ///
    /// The default value will reflect "no progress" and have an effective emission rate pulled
    /// from the current global setting. This means that retrieving the value for an arbitrary
    /// address gives a correct emission progress when a new validator authors its first block.
    ///
    /// NOTE: Calling get() on a key which does not exist will result in a default return, not
    /// a None return. If this is undesired, check for a key's existence before querying its value.
    #[pallet::storage]
    #[pallet::getter(fn emission_progress)]
    pub type EmissionProgress<T: Config> = StorageMap<
        _,
        Blake2_128Concat,
        T::AccountId,
        ValidatorEmissionProgress,
        ValueQuery,
        DefaultForEmissionProgress<T>,
    >;

    #[pallet::genesis_config]
    #[derive(Default)]
    pub struct GenesisConfig {
        /// EmissionRateSetting
        ///
        /// Stores the current minor units per emission and block quota.
        ///
        /// This value is used upon initialization of an individual validator's emission progress.
        /// Changing this value will not update the quota that a validator is working toward if they
        /// already have an existing progress.
        pub emission_rate_setting: ValidatorEmissionRate,
    }

    #[pallet::genesis_build]
    impl<T: Config> GenesisBuild<T> for GenesisConfig {
        fn build(&self) {
            {
                let data = &self.emission_rate_setting;
                let v: &ValidatorEmissionRate = data;
                <EmissionRateSetting<T> as frame_support::storage::StorageValue<
                    ValidatorEmissionRate,
                >>::put::<&ValidatorEmissionRate>(v);
            }
        }
    }
}

pub type ValidatorEmissionsResult<T, R> = Result<T, Error<R>>;

const ID_NOT_IN_AUTHORITY_SET_FRIENDLY_MSG: &str = "The ID specified was not a current validator";
const UNKNOWN_FRIENDLY_MSG: &str = "An unknown error occurred";
const BLOCK_QUOTA_MUST_BE_ONE_FRIENDLY_MSG: &str = "The block quota must be exactly 1";
const SIGNER_ORIGIN_MUST_BE_ROOT_FRIENDLY_MSG: &str =
    "The origin of the extrinsic must be the root";
const NON_ZERO_VALUE_SUPPLIED_FOR_NON_ZERO_TYPE_FRIENDLY_MSG: &str = "The value given cannot be 0";
const INVALID_MONETARY_VALUE_FRIENDLY_MSG: &str =
    "The value supplied is not a valid monetary value.";
const INVALID_PUBLIC_KEY: &str = "The account ID supplied is not a valid public key.";
const REWARD_DISPATCH_ERROR: &str = "An error occurred during dispatching.";

impl<T: Config> fmt::Display for Error<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::IdNotInAuthoritySet => ID_NOT_IN_AUTHORITY_SET_FRIENDLY_MSG,
                Self::BlockQuotaMustBeOne => BLOCK_QUOTA_MUST_BE_ONE_FRIENDLY_MSG,
                Self::SignerOriginMustBeRoot => SIGNER_ORIGIN_MUST_BE_ROOT_FRIENDLY_MSG,
                Self::ZeroValueSuppliedForNonZeroType =>
                    NON_ZERO_VALUE_SUPPLIED_FOR_NON_ZERO_TYPE_FRIENDLY_MSG,
                Self::InvalidMonetaryValue => INVALID_MONETARY_VALUE_FRIENDLY_MSG,
                Self::InvalidPublicKey => INVALID_PUBLIC_KEY,
                Self::RewardDispatchError => REWARD_DISPATCH_ERROR,
                Self::__Ignore(_, _) => UNKNOWN_FRIENDLY_MSG,
            }
        )
    }
}

// TODO ADO 6468 - Error handling strategy for substrate
// The inner errors and this translation exist to allow our models to return validation errors
// without the required runtime generic of the error type generated from the decl_error! macro.
// The inner errors from validation *do not necessarily have to be 1:1 with the dispatched errors*

impl<T: Config> From<StateInvariantViolation> for Error<T> {
    fn from(err: StateInvariantViolation) -> Self {
        match err {
            StateInvariantViolation::BlockQuotaMustBeOne => Self::BlockQuotaMustBeOne,
        }
    }
}

impl<T: Config> From<ExtrinsicRuleViolation> for Error<T> {
    fn from(err: ExtrinsicRuleViolation) -> Self {
        match err {
            ExtrinsicRuleViolation::SignerOriginMustBeRoot => Self::SignerOriginMustBeRoot,
        }
    }
}

impl<T: Config> From<ValidationFailure> for Error<T> {
    fn from(err: ValidationFailure) -> Self {
        match err {
            ValidationFailure::ExtrinsicRule(e) => e.into(),
            ValidationFailure::StateInvariant(s) => s.into(),
        }
    }
}

impl<T: Config> From<ModelConstructionFailure> for Error<T> {
    fn from(err: ModelConstructionFailure) -> Self {
        match err {
            ModelConstructionFailure::ZeroValueSuppliedToNonZeroType => {
                Self::ZeroValueSuppliedForNonZeroType
            }
        }
    }
}

impl<T: Config> From<MonetaryValueError> for Error<T> {
    fn from(err: MonetaryValueError) -> Self {
        match err {
            MonetaryValueError::TryFromIntError => Self::InvalidMonetaryValue,
        }
    }
}

impl<T: Config> From<RewardError> for Error<T> {
    fn from(err: RewardError) -> Self {
        match err {
            RewardError::InvalidPublicKey => Self::InvalidPublicKey,
            RewardError::DispatchError => Self::RewardDispatchError,
        }
    }
}

impl<T: Config> Pallet<T> {
    pub fn get_author() -> ValidatorEmissionsResult<T::AccountId, T> {
        let author = pallet_authorship::Module::<T>::author();
        match <T as QueryableXandValidators>::is_validator(&author) {
            true => Ok(author),
            false => {
                Self::deposit_event(Event::<T>::UnknownAuthorIdInHeader(author));
                Err(Error::<T>::IdNotInAuthoritySet)
            }
        }
    }

    pub fn get_validator_emission_progress(account_id: &T::AccountId) -> ValidatorEmissionProgress {
        EmissionProgress::<T>::get(account_id)
    }

    pub fn set_validator_emission_progress(
        account_id: &T::AccountId,
        new_progress: ValidatorEmissionProgress,
    ) {
        EmissionProgress::<T>::insert(account_id, new_progress)
    }

    fn get_initial_emission_progress() -> ValidatorEmissionProgress {
        ValidatorEmissionProgress {
            blocks_completed_progress: BlockCompletionCount::default(),
            effective_emission_rate: EmissionRateSetting::<T>::get(),
        }
    }

    /// The completion counter for a validator is reset to zero.
    /// This sets the validator's progress before the first block of the next quota, after their current quota has been reached
    fn reset_validator_emission_progress_counters(block_author: &T::AccountId) {
        let new_empty_progress = Self::get_initial_emission_progress();
        Self::set_validator_emission_progress(block_author, new_empty_progress);
    }
}

#[derive(Deserialize, Serialize, Decode, Encode, Debug, Copy, Clone, PartialEq, Eq)]
pub enum EmissionFailureReason {
    InvalidMonetaryValue,
    DispatchError,
}

#[cfg(test)]
mod tests;
