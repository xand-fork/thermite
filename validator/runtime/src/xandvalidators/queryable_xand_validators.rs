use super::Config;

/// Interface against which other Xand pallets can determine validator-ship.
/// See the blanket impl below of this trait for any Runtime impl'ing XandValidators
pub trait QueryableXandValidators: frame_system::Config {
    fn is_validator(account: &Self::AccountId) -> bool;
}

impl<Runtime: Config> QueryableXandValidators for Runtime {
    fn is_validator(account: &Runtime::AccountId) -> bool {
        let current_validators = <crate::xandvalidators::Module<Runtime>>::validators();
        current_validators.contains(account)
    }
}
