use crate::ConfidentialityCall;
use crate::{
    network_voting, opaque::bytes_to_extrinsic_addr, opaque::SessionKeys, AccountId, Call,
    CheckEra, CheckGenesis, CheckNonce, CheckSpecVersion, CheckTxVersion, CheckWeight,
    EncryptionPubKey, Hash, Index, Runtime, SessionCall, SignedExtra, SignedPayload,
    UncheckedExtrinsic, XandCall, XandValidatorsCall, VERSION,
};
use frame_support::sp_runtime::generic::Era;
use parity_scale_codec::Encode;
use serde_scale_wrap::Wrap;
use sp_core::{ed25519, sr25519};
use std::convert::TryInto;
use xand_ledger::{
    CashConfirmationTransaction, ClearRedeemRequestTransaction, ClearTransactionOutput,
    CreateCancellationTransaction, CreateRequestTransaction, RedeemCancellationTransaction,
    RedeemFulfillmentTransaction, RedeemRequestTransaction, SendClaimsTransaction,
};
use xand_runtime_models::upgrade::{UpgradeWasmBlob, UpgradeXandHash};
use xand_runtime_models::CidrBlock;

pub fn confidential_create_request_extrinsic(transaction: CreateRequestTransaction) -> Call {
    Call::Confidentiality(ConfidentialityCall::request_create(Wrap(transaction)))
}

pub fn confidential_create_extrinsic(transaction: CashConfirmationTransaction) -> Call {
    Call::Confidentiality(ConfidentialityCall::confirm_create(Wrap(transaction)))
}

pub fn confidential_create_cancellation_extrinsic(
    transaction: CreateCancellationTransaction,
) -> Call {
    Call::Confidentiality(ConfidentialityCall::cancel_pending_create(Wrap(
        transaction,
    )))
}

pub fn confidential_send_extrinsic(transaction: SendClaimsTransaction) -> Call {
    Call::Confidentiality(ConfidentialityCall::send(Wrap(transaction)))
}

pub fn confidential_redeem_extrinsic(transaction: RedeemRequestTransaction) -> Call {
    Call::Confidentiality(ConfidentialityCall::redeem(Wrap(transaction)))
}

pub fn confidential_redeem_cancellation(transaction: RedeemCancellationTransaction) -> Call {
    Call::Confidentiality(ConfidentialityCall::cancel_pending_redeem(Wrap(
        transaction,
    )))
}

pub fn confidential_redeem_fulfillment(transaction: RedeemFulfillmentTransaction) -> Call {
    Call::Confidentiality(ConfidentialityCall::redeem_fulfill(Wrap(transaction)))
}

pub fn confidential_clear_redeem_extrinsic(
    transaction: ClearRedeemRequestTransaction<ClearTransactionOutput>,
) -> Call {
    Call::Confidentiality(ConfidentialityCall::validator_redeem(Wrap(transaction)))
}

pub fn register_member_extrinsic(register_this: AccountId, pub_key: EncryptionPubKey) -> Call {
    Call::XandStrate(XandCall::register_account_as_member(register_this, pub_key))
}

pub fn remove_member_extrinsic(remove_this: AccountId) -> Call {
    Call::XandStrate(XandCall::remove_member(remove_this))
}

pub fn exit_member_extrinsic(remove_this: AccountId) -> Call {
    Call::XandStrate(XandCall::exit_member(remove_this))
}

pub fn set_limited_agent_id_extrinsic(register_this: Option<AccountId>) -> Call {
    Call::XandStrate(XandCall::set_limited_agent_id(register_this))
}

pub fn set_validator_emission_rate_extrinsic(
    minor_units_per_emission: u64,
    block_quota: u32,
) -> Call {
    Call::ValidatorEmissions(
        crate::validator_emissions::Call::set_validator_emission_rate(
            minor_units_per_emission,
            block_quota,
        ),
    )
}

pub fn set_trust_node_id_extrinsic(register_this: AccountId, pub_key: EncryptionPubKey) -> Call {
    Call::XandStrate(XandCall::set_trust_node_id(register_this, pub_key))
}

pub fn add_authority_key_extrinsic(register_this: AccountId) -> Call {
    Call::XandValidators(XandValidatorsCall::add_authority_key(register_this))
}

pub fn remove_authority_key_extrinsic(unregister_this: AccountId) -> Call {
    Call::XandValidators(XandValidatorsCall::remove_authority_key(unregister_this))
}

/// Note that this is *not* a xandstrate transaction, but is a transaction predefined by the
/// `session` substrate module.
pub fn register_session_keys_extrinsic(
    aura_pubkey: sr25519::Public,
    grandpa_pubkey: ed25519::Public,
) -> Call {
    let keys = SessionKeys {
        aura: aura_pubkey.into(),
        grandpa: grandpa_pubkey.into(),
    };
    Call::Session(SessionCall::set_keys(keys, vec![]))
}

pub fn allowlist_cidr_block(cidr_block: CidrBlock) -> Call {
    Call::XandStrate(XandCall::allowlist_cidr_block(cidr_block))
}

pub fn remove_allowlist_cidr_block(cidr_block: CidrBlock) -> Call {
    Call::XandStrate(XandCall::remove_allowlist_cidr_block(cidr_block))
}

pub fn root_allowlist_cidr_block(account: AccountId, cidr_block: CidrBlock) -> Call {
    Call::XandStrate(XandCall::root_allowlist_cidr_block(account, cidr_block))
}

pub fn root_remove_allowlist_cidr_block(account: AccountId, cidr_block: CidrBlock) -> Call {
    Call::XandStrate(XandCall::root_remove_allowlist_cidr_block(
        account, cidr_block,
    ))
}

pub fn withdraw_from_network() -> Call {
    Call::XandStrate(XandCall::remove_self())
}

pub fn set_member_encryption_key(key: EncryptionPubKey) -> Call {
    Call::XandStrate(XandCall::set_member_encryption_key(key))
}

pub fn set_trust_encryption_key(key: EncryptionPubKey) -> Call {
    Call::XandStrate(XandCall::set_trust_encryption_key(key))
}

pub fn submit_proposal_extrinsic(proposed_action: Call) -> Call {
    Call::NetworkVoting(network_voting::Call::submit_proposal(Box::new(
        proposed_action,
    )))
}

pub fn vote_on_proposal_extrinsic(id: u32, vote: bool) -> Call {
    Call::NetworkVoting(network_voting::Call::vote_proposal(id, vote))
}

pub fn set_pending_create_expire_time_extrinsic(expire_in_milliseconds: u64) -> Call {
    Call::Confidentiality(ConfidentialityCall::set_pending_create_expire_time(
        expire_in_milliseconds,
    ))
}

pub fn upgrade_runtime(wasm_blob: UpgradeWasmBlob, hash: UpgradeXandHash) -> Call {
    Call::XandStrate(XandCall::upgrade_runtime(wasm_blob, hash))
}

/// An extrinsic which has all the data it needs to be submitted, but has not yet been signed.
/// Note that it is possible for these to go "stale" if the account index inside of them becomes
/// old due to another extrinsic being submtited first by the same account.
///
/// This is like substrate's `UncheckedExtrinsic` except it *definitely* has not been signed yet,
/// whereas `UncheckedExtrinsic` cannot both be unsigned, and contain necessary information
/// like the index and genesis hash.
#[derive(Clone, Debug)]
pub struct UnsignedExtrinsic {
    pub acct_ix: Index,
    pub issuer: sr25519::Public,
    pub call: Call,
    pub genesis_hash: Hash,
    pub era: Era,
}

/// Simplest trait possible for signing extrinsic data, specific to this runtime.
pub trait ExtrinsicSigner {
    type Err: std::error::Error;

    fn sign(&self, signer: &sr25519::Public, data: &[u8]) -> Result<Vec<u8>, Self::Err>;
}

impl UnsignedExtrinsic {
    /// Builds a new immortal unsigned extrinsic
    pub fn new(call: Call, issuer: sr25519::Public, acct_ix: Index, genesis_hash: Hash) -> Self {
        Self {
            call,
            issuer,
            acct_ix,
            genesis_hash,
            era: Era::Immortal,
        }
    }

    pub fn sign<E: std::error::Error>(
        self,
        ext_signer: &dyn ExtrinsicSigner<Err = E>,
    ) -> Result<UncheckedExtrinsic, E> {
        let extra: SignedExtra = (
            CheckSpecVersion::<Runtime>::new(),
            CheckTxVersion::<Runtime>::new(),
            CheckGenesis::<Runtime>::new(),
            CheckEra::<Runtime>::from(self.era),
            CheckNonce::<Runtime>::from(self.acct_ix),
            CheckWeight::<Runtime>::new(),
        );
        let raw_payload = SignedPayload::from_raw(
            self.call,
            extra,
            (
                VERSION.spec_version as u32,
                VERSION.transaction_version,
                self.genesis_hash,
                self.genesis_hash,
                (),
                (),
            ),
        );
        let iss_ref = &self.issuer;
        let signature = raw_payload.using_encoded(|payload| ext_signer.sign(iss_ref, payload))?;
        let raw_signature: &[u8] = signature.as_ref();
        let sr_signature: sr25519::Signature = raw_signature
            .try_into()
            .expect("We just created the signature");
        let (function, extra, _) = raw_payload.deconstruct();

        let account_bytes: [u8; 32] = self.issuer.0;
        Ok(UncheckedExtrinsic::new_signed(
            function,
            bytes_to_extrinsic_addr(account_bytes),
            sr_signature.into(),
            extra,
        ))
    }
}

pub trait CallExt {
    fn into_extrinsic(
        self,
        account_index: Index,
        genesis_hash: Hash,
        submitter: &sr25519::Public,
    ) -> UnsignedExtrinsic;

    fn into_sudo(self) -> Self;
}

impl CallExt for Call {
    fn into_extrinsic(
        self,
        account_index: Index,
        genesis_hash: Hash,
        signer: &sr25519::Public,
    ) -> UnsignedExtrinsic {
        UnsignedExtrinsic::new(self, *signer, account_index, genesis_hash)
    }

    fn into_sudo(self) -> Self {
        Call::Sudo(pallet_sudo::Call::sudo(Box::new(self)))
    }
}
