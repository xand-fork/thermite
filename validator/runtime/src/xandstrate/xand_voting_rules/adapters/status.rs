use crate::network_voting::VoteStatus;
use xand_governance::models::proposal::ProposalStatus;

impl From<VoteStatus> for ProposalStatus {
    fn from(vs: VoteStatus) -> Self {
        match vs {
            VoteStatus::Open => Self::Open,
            VoteStatus::Rejected => Self::Rejected,
            VoteStatus::Accepted => Self::Accepted,
        }
    }
}

impl From<ProposalStatus> for VoteStatus {
    fn from(ps: ProposalStatus) -> Self {
        match ps {
            ProposalStatus::Open => Self::Open,
            ProposalStatus::Rejected => Self::Rejected,
            ProposalStatus::Accepted => Self::Accepted,
        }
    }
}
