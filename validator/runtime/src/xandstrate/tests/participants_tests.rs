use frame_support::{assert_err, assert_ok};
use frame_system as system;
use sp_runtime::{testing::UintAuthorityId, traits::Dispatchable};

use crate::confidentiality::XandstrateStateTrait;
use crate::xandstrate::tests::run_to_block;
use crate::xandstrate::xandstrate_test_runtime::test_runtime::BLOCKS_UNTIL_AUTO_EXIT;
use crate::xandstrate::BannedMembers;
use crate::{
    xandstrate::{
        xandstrate_test_runtime::{
            constants::{LIMITED_AGENT, MEM_1, MEM_2, TRUST, VAL_1, VAL_2, VAL_3},
            test_runtime::{
                base_test_ext, empty_test_ext, multi_vals_test_ext, Call, Origin, TestModule,
                TestRuntime, ValidatorMod,
            },
        },
        EncryptionPubKey, Error, LimitedAgentId, NodeEncryptionKey, RegisteredMembers, TrustNodeId,
    },
    xandvalidators::XandValidatorsErr,
    XandstrateStateAdapter,
};
use xand_runtime_models::BannedState;

#[test]
fn only_root_can_register_members() {
    empty_test_ext().execute_with(|| {
        assert_err!(
            TestModule::register_account_as_member(
                Origin::signed(MEM_1),
                MEM_2,
                Default::default()
            ),
            Error::<TestRuntime>::TransactionMustBeDoneByRoot
        );
        assert!(!RegisteredMembers::<TestRuntime>::get(MEM_2));
        assert_ok!(TestModule::register_account_as_member(
            Origin::root(),
            MEM_2,
            Default::default()
        ));
        assert!(RegisteredMembers::<TestRuntime>::get(MEM_2));
    });
}

#[test]
fn adding_member_increments_refs_to_account() {
    empty_test_ext().execute_with(|| {
        let account = MEM_2;
        let before_refs = system::Account::<TestRuntime>::get(account).providers;
        TestModule::register_account_as_member(Origin::root(), account, Default::default())
            .unwrap();
        let after_refs = system::Account::<TestRuntime>::get(account).providers;
        assert_eq!(after_refs, before_refs + 1);
    })
}

#[test]
fn registering_existing_member_fails() {
    empty_test_ext().execute_with(|| {
        let account = MEM_1;
        let pub_key = EncryptionPubKey::from([1u8; 32]);
        TestModule::register_account_as_member(Origin::root(), account, pub_key).unwrap();
        assert_err!(
            TestModule::register_account_as_member(Origin::root(), account, pub_key),
            Error::<TestRuntime>::AlreadyRegistered,
        );
    })
}

#[test]
fn non_root_cannot_call_set_limited_agent() {
    base_test_ext().execute_with(|| {
        // Given
        let original_la_id = Some(LIMITED_AGENT);
        let new_la_id = Some(1000);
        assert_ne!(original_la_id, new_la_id);

        // When
        let mem_call = TestModule::set_limited_agent_id(Origin::signed(MEM_1), new_la_id);
        let val_call = TestModule::set_limited_agent_id(Origin::signed(VAL_1), new_la_id);
        let trust_call = TestModule::set_limited_agent_id(Origin::signed(TRUST), new_la_id);

        // Then
        assert_err!(mem_call, Error::<TestRuntime>::TransactionMustBeDoneByRoot);
        assert_err!(val_call, Error::<TestRuntime>::TransactionMustBeDoneByRoot);
        assert_err!(
            trust_call,
            Error::<TestRuntime>::TransactionMustBeDoneByRoot
        );
        assert_eq!(LimitedAgentId::<TestRuntime>::get(), original_la_id);
    })
}

#[test]
fn root_can_update_limited_agent_id_to_new_id() {
    base_test_ext().execute_with(|| {
        // Given
        let new_la = Some(1000);

        // When
        TestModule::set_limited_agent_id(Origin::root(), new_la).unwrap();

        // Then
        assert_eq!(LimitedAgentId::<TestRuntime>::get(), new_la);
    })
}

#[test]
fn root_can_update_limited_agent_id_to_none() {
    base_test_ext().execute_with(|| {
        // Given
        let new_la = None;

        // When
        TestModule::set_limited_agent_id(Origin::root(), new_la).unwrap();

        // Then
        assert_eq!(LimitedAgentId::<TestRuntime>::get(), None);
    })
}

#[test]
fn registered_member_has_associated_node_encryption_key() {
    let expected_pub_key: EncryptionPubKey = [99u8; 32].into();

    empty_test_ext().execute_with(|| {
        let account = MEM_2;
        TestModule::register_account_as_member(Origin::root(), account, expected_pub_key).unwrap();
        let actual_pub_key = NodeEncryptionKey::<TestRuntime>::get(account);
        assert_eq!(actual_pub_key, expected_pub_key);
    })
}

// FIXME: Genesis config should require members and trust to have a public key for encryption.
//   Needed for 5680 and 5681.

#[test]
fn only_root_can_mark_member_for_removal() {
    base_test_ext().execute_with(|| {
        assert_err!(
            TestModule::remove_member(Origin::signed(MEM_1), MEM_2),
            Error::<TestRuntime>::TransactionMustBeDoneByRoot
        );
        assert!(RegisteredMembers::<TestRuntime>::get(MEM_2));
        assert_ok!(TestModule::remove_member(Origin::root(), MEM_2));
        assert!(!RegisteredMembers::<TestRuntime>::get(MEM_2));
    })
}

#[test]
fn root_can_immediately_exit_member() {
    base_test_ext().execute_with(|| {
        // Given
        assert!(RegisteredMembers::<TestRuntime>::get(MEM_2));
        let banned_list_before = XandstrateStateAdapter::<TestRuntime>::get_banned_members();
        assert!(banned_list_before.iter().all(|&(p, _)| p != MEM_2));

        // When
        assert_ok!(TestModule::remove_member(Origin::root(), MEM_2));
        assert_ok!(TestModule::exit_member(Origin::root(), MEM_2));
        assert!(!RegisteredMembers::<TestRuntime>::get(MEM_2));
        let banned_list_after = XandstrateStateAdapter::<TestRuntime>::get_banned_members();

        //Then
        assert!(banned_list_after.contains(&(MEM_2, BannedState::Exited)));
    })
}

#[test]
fn member_can_immediately_exit_self_as_member() {
    base_test_ext().execute_with(|| {
        // Given
        assert_err!(
            TestModule::exit_member(Origin::signed(MEM_1), MEM_2),
            Error::<TestRuntime>::TransactionMustBeDoneByRootOrAccount
        );
        assert!(RegisteredMembers::<TestRuntime>::get(MEM_2));
        let banned_list_before = XandstrateStateAdapter::<TestRuntime>::get_banned_members();
        assert!(banned_list_before.iter().all(|&(p, _)| p != MEM_2));
        assert_ok!(TestModule::remove_member(Origin::root(), MEM_2)); // Signing as root, as `remove_member` is not the SUT

        // When
        assert_ok!(TestModule::exit_member(Origin::signed(MEM_2), MEM_2));
        assert!(!RegisteredMembers::<TestRuntime>::get(MEM_2));
        let banned_list_after = XandstrateStateAdapter::<TestRuntime>::get_banned_members();

        // Then
        assert!(banned_list_after.contains(&(MEM_2, BannedState::Exited)));
    })
}

#[test]
fn member_cannot_immediately_exit_another_member() {
    base_test_ext().execute_with(|| {
        // Given
        let banned_block = 10;
        run_to_block(banned_block - 1);
        assert!(RegisteredMembers::<TestRuntime>::get(MEM_1));
        assert!(RegisteredMembers::<TestRuntime>::get(MEM_2)); // Targeted Member we will attempt to exit
        let banned_list_before = XandstrateStateAdapter::<TestRuntime>::get_banned_members();
        assert!(banned_list_before.iter().all(|&(p, _)| p != MEM_2)); // Targeted Member is not banned

        // When
        assert_ok!(TestModule::remove_member(Origin::root(), MEM_2)); // Signing as root, as `remove_member` is not the SUT
        let banned_list_after = XandstrateStateAdapter::<TestRuntime>::get_banned_members();
        assert!(banned_list_after.contains(&(MEM_2, BannedState::Exiting(banned_block)))); // Targeted member not yet Exited
        assert!(!RegisteredMembers::<TestRuntime>::get(MEM_2));

        // Then
        assert_err!(
            TestModule::exit_member(Origin::signed(MEM_1), MEM_2),
            Error::<TestRuntime>::TransactionMustBeDoneByRootOrAccount
        );
        let banned_list_after = XandstrateStateAdapter::<TestRuntime>::get_banned_members();
        assert!(banned_list_after.contains(&(MEM_2, BannedState::Exiting(banned_block))));
        // Targeted member still not yet Exited
    })
}

#[test]
fn exiting_member_decrements_refs_to_account() {
    base_test_ext().execute_with(|| {
        let account = MEM_2;
        let before_refs = system::Account::<TestRuntime>::get(account).providers;
        TestModule::remove_member(Origin::root(), account).unwrap();
        TestModule::exit_member(Origin::root(), account).unwrap();
        let after_refs = system::Account::<TestRuntime>::get(account).providers;
        assert_eq!(after_refs, before_refs - 1);
    })
}

/// Decrementing only happens upon `exit_member()`
#[test]
fn marking_member_for_exit_doesnt_decrement_refs_to_account() {
    base_test_ext().execute_with(|| {
        let account = MEM_2;
        TestModule::remove_member(Origin::root(), account).unwrap();
        let before_refs = system::Account::<TestRuntime>::get(account).providers;
        let after_refs = system::Account::<TestRuntime>::get(account).providers;
        assert_eq!(after_refs, before_refs);
    })
}

#[test]
fn node_encryption_key_remains_intact_after_member_marked_for_removal() {
    empty_test_ext().execute_with(|| {
        // Given
        let account = MEM_2;
        TestModule::register_account_as_member(Origin::root(), account, Default::default())
            .unwrap();
        assert!(NodeEncryptionKey::<TestRuntime>::contains_key(account));

        // When
        TestModule::remove_member(Origin::root(), account).unwrap();
        // Then
        assert!(NodeEncryptionKey::<TestRuntime>::contains_key(account));
    })
}

#[test]
fn node_encryption_key_removed_after_member_exit() {
    empty_test_ext().execute_with(|| {
        // Given
        let account = MEM_2;
        TestModule::register_account_as_member(Origin::root(), account, Default::default())
            .unwrap();
        assert!(NodeEncryptionKey::<TestRuntime>::contains_key(account));

        // When
        TestModule::remove_member(Origin::root(), account).unwrap();
        TestModule::exit_member(Origin::root(), account).unwrap();

        // Then
        assert!(!NodeEncryptionKey::<TestRuntime>::contains_key(account));
    })
}

#[test]
fn registered_member_list_excludes_member_marked_for_removal() {
    base_test_ext().execute_with(|| {
        // Given When
        TestModule::remove_member(Origin::root(), MEM_2).unwrap();

        //Then
        assert!(!RegisteredMembers::<TestRuntime>::contains_key(MEM_2));
        assert!(RegisteredMembers::<TestRuntime>::contains_key(MEM_1)); // Other member not affected
    })
}

#[test]
fn cannot_exit_member_unmarked_for_removal_or_not_on_banned_list() {
    base_test_ext().execute_with(|| {
        // Given When
        let banned_list = XandstrateStateAdapter::<TestRuntime>::get_banned_members();
        let member_in_exiting_state_pre = banned_list.contains(&(MEM_2, BannedState::Exited));
        assert!(!member_in_exiting_state_pre);
        let member_is_on_banned_list = banned_list.iter().any(|&(p, _)| p == MEM_2);
        assert!(!member_is_on_banned_list);

        // Then
        // We don't call `remove_member()` so this member is not yet marked for removal
        assert_err!(
            TestModule::exit_member(Origin::root(), MEM_2),
            Error::<TestRuntime>::MemberNotInExitingState
        );
        let member_in_exiting_state = banned_list.contains(&(MEM_2, BannedState::Exited));
        assert!(!member_in_exiting_state);
    })
}

#[test]
fn registered_member_list_excludes_exited_member() {
    base_test_ext().execute_with(|| {
        // Given
        TestModule::remove_member(Origin::root(), MEM_2).unwrap();

        // When
        TestModule::exit_member(Origin::root(), MEM_2).unwrap();

        //Then
        assert!(!RegisteredMembers::<TestRuntime>::contains_key(MEM_2));
        assert!(RegisteredMembers::<TestRuntime>::contains_key(MEM_1)); // Other member not affected
    })
}

#[test]
fn exiting_member_automatically_exited_after_30_days() {
    base_test_ext().execute_with(|| {
        // Given
        TestModule::remove_member(Origin::root(), MEM_2).unwrap();

        run_to_block(BLOCKS_UNTIL_AUTO_EXIT - 1);

        assert_eq!(
            BannedMembers::<TestRuntime>::get(MEM_2),
            BannedState::Exiting(0)
        );

        run_to_block(BLOCKS_UNTIL_AUTO_EXIT);

        //Then
        assert_eq!(
            BannedMembers::<TestRuntime>::get(MEM_2),
            BannedState::Exited
        );
    })
}

#[test]
fn banned_member_list_defaults_to_empty() {
    base_test_ext().execute_with(|| {
        let banned_list = XandstrateStateAdapter::<TestRuntime>::get_banned_members();
        assert!(banned_list.is_empty());
    })
}

#[test]
fn banned_member_list_includes_member_marked_for_removal() {
    base_test_ext().execute_with(|| {
        let banned_block = 10;
        run_to_block(banned_block - 1);
        TestModule::remove_member(Origin::root(), MEM_2).unwrap();

        let banned_list = XandstrateStateAdapter::<TestRuntime>::get_banned_members();
        assert!(banned_list.contains(&(MEM_2, BannedState::Exiting(banned_block))));
        assert!(banned_list.iter().all(|&(p, _)| p != MEM_1));
    })
}

#[test]
fn banned_member_list_includes_exited_member() {
    base_test_ext().execute_with(|| {
        // Given
        TestModule::remove_member(Origin::root(), MEM_2).unwrap();

        // When
        TestModule::exit_member(Origin::root(), MEM_2).unwrap();
        let banned_list = XandstrateStateAdapter::<TestRuntime>::get_banned_members();

        //Then
        assert!(banned_list.contains(&(MEM_2, BannedState::Exited)));
        assert!(banned_list.iter().all(|&(p, _)| p != MEM_1)); // Other member isn't affected
    })
}

#[test]
fn genesis_members_have_refs() {
    base_test_ext().execute_with(|| {
        assert_eq!(system::Account::<TestRuntime>::get(MEM_1).providers, 1);
        assert_eq!(system::Account::<TestRuntime>::get(MEM_2).providers, 1);
    });
}

#[test]
fn new_members_have_refs() {
    empty_test_ext().execute_with(|| {
        assert_eq!(system::Account::<TestRuntime>::get(MEM_1).providers, 0);
        assert_ok!(TestModule::register_account_as_member(
            Origin::root(),
            MEM_1,
            Default::default()
        ));
        assert_eq!(system::Account::<TestRuntime>::get(MEM_1).providers, 1);
    });
}

#[test]
fn only_root_can_add_authority() {
    base_test_ext().execute_with(|| {
        assert_err!(
            ValidatorMod::add_authority_key(Origin::signed(MEM_1), VAL_2),
            XandValidatorsErr::<TestRuntime>::TransactionMustBeDoneByRoot
        );
        assert_ok!(ValidatorMod::add_authority_key(Origin::root(), VAL_2));
        Call::Session(pallet_session::Call::set_keys(
            UintAuthorityId(VAL_2).into(),
            vec![],
        ))
        .dispatch(Origin::signed(VAL_2))
        .unwrap();
        assert!(ValidatorMod::validators().contains(&VAL_2));
    })
}

#[test]
fn only_root_can_remove_authority() {
    multi_vals_test_ext().execute_with(|| {
        assert_err!(
            ValidatorMod::remove_authority_key(Origin::signed(MEM_1), VAL_1),
            XandValidatorsErr::<TestRuntime>::TransactionMustBeDoneByRoot
        );
        assert_ok!(ValidatorMod::remove_authority_key(Origin::root(), VAL_1));
        assert!(!ValidatorMod::validators().contains(&VAL_1));
    })
}

#[test]
fn validator_cant_be_double_added() {
    base_test_ext().execute_with(|| {
        assert_ok!(ValidatorMod::add_authority_key(Origin::root(), VAL_2));
        Call::Session(pallet_session::Call::set_keys(
            UintAuthorityId(VAL_2).into(),
            vec![],
        ))
        .dispatch(Origin::signed(VAL_2))
        .unwrap();
        assert_err!(
            ValidatorMod::add_authority_key(Origin::root(), VAL_2),
            XandValidatorsErr::<TestRuntime>::ValidatorAlreadyInSet
        );
        assert_eq!(ValidatorMod::validators().len(), 2);
    })
}

#[test]
fn validator_can_be_added_before_session_key() {
    base_test_ext().execute_with(|| {
        assert_ok!(ValidatorMod::add_authority_key(Origin::root(), VAL_2));
        assert_eq!(ValidatorMod::validators().len(), 2);

        Call::Session(pallet_session::Call::set_keys(
            UintAuthorityId(VAL_2).into(),
            vec![],
        ))
        .dispatch(Origin::signed(VAL_2))
        .unwrap();
        assert_eq!(ValidatorMod::validators().len(), 2);
    })
}

#[test]
fn validator_cannot_be_added_after_session_key() {
    base_test_ext().execute_with(|| {
        let res = Call::Session(pallet_session::Call::set_keys(
            UintAuthorityId(VAL_2).into(),
            vec![],
        ))
        .dispatch(Origin::signed(VAL_2));
        assert_err!(res, pallet_session::Error::<TestRuntime>::NoAccount);
        assert_eq!(ValidatorMod::validators().len(), 1);
    })
}

#[test]
fn can_only_remove_validator_that_exists() {
    multi_vals_test_ext().execute_with(|| {
        assert_err!(
            ValidatorMod::remove_authority_key(Origin::root(), 420),
            XandValidatorsErr::<TestRuntime>::ValidatorNotInSet
        );
        assert_ok!(ValidatorMod::remove_authority_key(Origin::root(), VAL_1));
        assert!(!ValidatorMod::validators().contains(&VAL_1));
        assert!(ValidatorMod::validators().contains(&VAL_2));
        assert!(ValidatorMod::validators().contains(&VAL_3));
    })
}

#[test]
fn only_root_can_set_trust_node() {
    empty_test_ext().execute_with(|| {
        assert_err!(
            TestModule::set_trust_node_id(Origin::signed(MEM_1), MEM_2, Default::default()),
            Error::<TestRuntime>::TransactionMustBeDoneByRoot
        );
        assert_eq!(TrustNodeId::<TestRuntime>::get(), 0);
        assert_ok!(TestModule::set_trust_node_id(
            Origin::root(),
            MEM_2,
            Default::default()
        ));
        assert_eq!(TrustNodeId::<TestRuntime>::get(), MEM_2);
    });
}

#[test]
fn trust_has_associated_node_encryption_key() {
    let expected_pub_key: EncryptionPubKey = [101u8; 32].into();

    empty_test_ext().execute_with(|| {
        let account = MEM_2;
        TestModule::set_trust_node_id(Origin::root(), account, expected_pub_key).unwrap();

        let actual_pub_key = NodeEncryptionKey::<TestRuntime>::get(account);
        assert_eq!(actual_pub_key, expected_pub_key);
    })
}

#[test]
fn resetting_trust_replaces_node_encryption_key() {
    let original_key: EncryptionPubKey = [101u8; 32].into();
    let new_key: EncryptionPubKey = [7u8; 32].into();

    empty_test_ext().execute_with(|| {
        // Given
        let account = MEM_1;
        TestModule::set_trust_node_id(Origin::root(), account, original_key).unwrap();

        // When
        TestModule::set_trust_node_id(Origin::root(), account, new_key).unwrap();

        // Then
        assert_eq!(NodeEncryptionKey::<TestRuntime>::get(account), new_key);
    })
}

#[test]
fn cannot_remove_last_validator() {
    base_test_ext().execute_with(|| {
        assert_err!(
            ValidatorMod::remove_authority_key(Origin::root(), VAL_1),
            XandValidatorsErr::<TestRuntime>::CannotRemoveLastRemainingValidator
        );
        assert!(ValidatorMod::validators().contains(&VAL_1));
    })
}

#[test]
fn member_can_mark_themself_for_removal() {
    base_test_ext().execute_with(|| {
        let signer = MEM_1;
        assert!(RegisteredMembers::<TestRuntime>::get(signer));
        assert_ok!(TestModule::remove_self(Origin::signed(signer)));
        assert!(!RegisteredMembers::<TestRuntime>::get(signer));
        assert!(BannedMembers::<TestRuntime>::contains_key(signer));
        assert!(matches!(
            BannedMembers::<TestRuntime>::get(signer),
            BannedState::Exiting(..)
        ));
    })
}

#[test]
fn validator_can_remove_themself() {
    multi_vals_test_ext().execute_with(|| {
        let signer = VAL_1;
        assert!(ValidatorMod::validators().contains(&signer));
        assert_ok!(TestModule::remove_self(Origin::signed(signer)));
        assert!(!ValidatorMod::validators().contains(&signer));
    })
}

#[test]
fn non_member_validator_errors_when_removes_themself() {
    multi_vals_test_ext().execute_with(|| {
        let signer: u64 = 123456;
        assert_err!(
            TestModule::remove_self(Origin::signed(signer)),
            Error::<TestRuntime>::SignerNotAValidMemberOrValidator
        );
    })
}

#[test]
fn trust_errors_when_removes_themself() {
    multi_vals_test_ext().execute_with(|| {
        let signer = TRUST;
        assert_err!(
            TestModule::remove_self(Origin::signed(signer)),
            Error::<TestRuntime>::SignerNotAValidMemberOrValidator
        );
    })
}
