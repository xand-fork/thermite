use crate::confidentiality::ledger_adapters::NewPublicKey;
use crate::xandvalidators::queryable_xand_validators::QueryableXandValidators;
use crate::{
    confidentiality::ledger_adapters::member_repo::NewIdentityTag,
    xandstrate::{self, BannedMembers},
    RegisteredMembers, TrustNodeId, XandstrateStateTrait,
};
use core::convert::From;
use sp_core::{
    crypto::AccountId32,
    sp_std::{
        convert::{TryFrom, TryInto},
        marker::PhantomData,
        vec::Vec,
    },
};
use xand_ledger::{curve25519_dalek::ristretto::RistrettoPoint, PublicKey};
use xand_runtime_models::BannedState;

pub struct XandstrateStateAdapter<T: xandstrate::Config> {
    runtime_type: PhantomData<T>,
}

impl<T> XandstrateStateTrait<T> for XandstrateStateAdapter<T>
where
    T: xandstrate::Config,
    <T as frame_system::Config>::AccountId:
        TryFrom<NewIdentityTag, Error = ()> + TryInto<NewPublicKey> + TryFrom<NewPublicKey>,
    <T as frame_system::Config>::BlockNumber: Into<u64>,
{
    type MemberAccountId = T::AccountId;
    type BannedMemberAccountId = T::AccountId;
    type TrustAccountId = T::AccountId;
    type ValidatorAccountId = T::AccountId;

    fn is_member(address: &Self::MemberAccountId) -> bool {
        RegisteredMembers::<T>::get(address)
    }

    fn get_banned_members() -> Vec<(Self::BannedMemberAccountId, BannedState<T::BlockNumber>)> {
        BannedMembers::<T>::iter().collect()
    }

    fn get_trust_id() -> Self::TrustAccountId {
        TrustNodeId::<T>::get()
    }

    fn is_validator(address: &Self::ValidatorAccountId) -> bool {
        <T as QueryableXandValidators>::is_validator(address)
    }
}

impl TryFrom<NewIdentityTag> for AccountId32 {
    type Error = ();

    fn try_from(value: NewIdentityTag) -> Result<Self, Self::Error> {
        let id = value.0;
        PublicKey::try_from(id)
            .map_err(|_| ())
            .map(RistrettoPoint::from)
            .map(|p| ristretto_pt_to_account(&p))
    }
}

impl From<NewPublicKey> for AccountId32 {
    fn from(value: NewPublicKey) -> Self {
        ristretto_pt_to_account(&RistrettoPoint::from(PublicKey::from(value)))
    }
}

fn ristretto_pt_to_account(pt: &RistrettoPoint) -> AccountId32 {
    let compressed = pt.compress();
    let bytes = compressed.0;
    bytes.into()
}

#[cfg(test)]
mod tests {
    #![allow(non_snake_case)]

    use super::*;
    use crate::xandstrate::xandstrate_test_runtime::{constants::*, test_runtime::*};
    use crate::xandstrate::GenesisConfig;
    use sp_runtime::testing::UintAuthorityId;
    use xand_runtime_models::BannedState;

    impl TryFrom<u64> for NewPublicKey {
        type Error = ();

        fn try_from(_: u64) -> Result<Self, Self::Error> {
            Err(())
        }
    }

    impl TryFrom<NewPublicKey> for u64 {
        type Error = ();

        fn try_from(_: NewPublicKey) -> Result<Self, Self::Error> {
            Err(())
        }
    }

    fn make_state_test_extrinsics(
        registered_members: Vec<(u64, bool)>,
        banned_members: Vec<(
            u64,
            BannedState<<TestRuntime as frame_system::Config>::BlockNumber>,
        )>,
        trust_node_id: u64,
        validators: Vec<u64>,
    ) -> sp_io::TestExternalities {
        let genesis = GenesisConfig::<TestRuntime> {
            banned_members,
            trust_node_id,
            registered_members,
            ..base_xandstrate_genesis()
        };
        new_test_extrinsics(
            genesis,
            // Configure validators, whose account key and signing key are both the provided id
            validators
                .into_iter()
                .map(|id| (id, id, UintAuthorityId(id)))
                .collect(),
        )
    }

    impl TryFrom<NewIdentityTag> for u64 {
        type Error = ();

        fn try_from(_value: NewIdentityTag) -> Result<Self, Self::Error> {
            unimplemented!()
        }
    }

    #[test]
    fn member_exists_returns_true() {
        let registered_members = vec![(MEM_1, true), (MEM_2, true)];
        let banned_members = vec![];
        let trust_node_id = TRUST;
        let validators = vec![VAL_1];
        make_state_test_extrinsics(
            registered_members,
            banned_members,
            trust_node_id,
            validators,
        )
        .execute_with(|| {
            let exists = XandstrateStateAdapter::<TestRuntime>::is_member(&MEM_1);
            assert!(exists)
        })
    }

    #[test]
    fn member_does_not_exist_returns_false() {
        let registered_members = vec![(MEM_2, true)];
        let banned_members = vec![];
        let trust_node_id = TRUST;
        let validators = vec![VAL_1];
        make_state_test_extrinsics(
            registered_members,
            banned_members,
            trust_node_id,
            validators,
        )
        .execute_with(|| {
            let exists = XandstrateStateAdapter::<TestRuntime>::is_member(&MEM_1);
            assert!(!exists)
        })
    }

    #[test]
    fn get_trust_id__is_correct() {
        let registered_members = vec![];
        let banned_members = vec![];
        let expected = TRUST;
        let validators = vec![VAL_1];
        make_state_test_extrinsics(registered_members, banned_members, expected, validators)
            .execute_with(|| {
                let actual = XandstrateStateAdapter::<TestRuntime>::get_trust_id();
                assert_eq!(expected, actual)
            })
    }

    #[test]
    fn can_get_all_the_banned_members() {
        let registered_members = vec![];
        let banned_members = vec![(MEM_1, BannedState::Exiting(0))];
        let expected = banned_members.clone();
        let trust_node_id = TRUST;
        let validators = vec![VAL_1];
        make_state_test_extrinsics(
            registered_members,
            banned_members,
            trust_node_id,
            validators,
        )
        .execute_with(|| {
            let got_banned = XandstrateStateAdapter::<TestRuntime>::get_banned_members();
            assert_eq!(got_banned, expected)
        })
    }

    #[test]
    fn is_validator__returns_false_with_unknown_address() {
        // Given
        let registered_members = vec![];
        let banned_members = vec![];
        let trust_node_id = TRUST;
        let validators = vec![VAL_1];
        let other_validator_id = VAL_2;
        make_state_test_extrinsics(
            registered_members,
            banned_members,
            trust_node_id,
            validators,
        )
        .execute_with(|| {
            // When
            let exists = XandstrateStateAdapter::<TestRuntime>::is_validator(&other_validator_id);
            // Then
            assert!(!exists)
        })
    }

    #[test]
    fn is_validator__returns_true_with_valid_address() {
        // Given
        let registered_members = vec![];
        let banned_members = vec![];
        let trust_node_id = TRUST;
        let validator_id = VAL_1;
        let validators = vec![validator_id];
        make_state_test_extrinsics(
            registered_members,
            banned_members,
            trust_node_id,
            validators,
        )
        .execute_with(|| {
            // When
            let exists = XandstrateStateAdapter::<TestRuntime>::is_validator(&validator_id);
            // Then
            assert!(exists)
        })
    }
}
