use crate::confidentiality::{
    ledger_adapters::NewPublicKey,
    test_constants::{MEM_1, TRUST},
    testing_and_benchmarking_shared::create_random_redeem_transaction,
    tests::{new_test_externalities, ConfidentialityPallet, Origin, Test},
    Error, GenesisConfig,
};
use frame_support::{assert_err, assert_ok};
use frame_system::RawOrigin;
use rand::{prelude::StdRng, SeedableRng};
use serde_scale_wrap::Wrap;
use xand_ledger::{
    PublicKey, RedeemCancellationReason, RedeemCancellationTransaction,
    RedeemFulfillmentTransaction,
};

#[test]
fn redeem_happy_path() {
    let mut csprng = StdRng::seed_from_u64(42);
    let trust_pubkey: PublicKey = NewPublicKey::new(TRUST.clone()).unwrap().into();
    let redeem_tx = create_random_redeem_transaction(&mut csprng, trust_pubkey);

    // initialize genesis with identity tags
    let genesis = GenesisConfig::<Test> {
        identity_tags: redeem_tx.identity_set.clone(),
        txos: redeem_tx.input_txos.clone(),
        ..Default::default()
    };

    new_test_externalities(genesis, Default::default()).execute_with(|| {
        let result = ConfidentialityPallet::redeem(RawOrigin::None.into(), Wrap(redeem_tx.tx));
        assert_ok!(result);
    });
}

#[test]
fn redeem_fails_if_signed() {
    let mut csprng = StdRng::seed_from_u64(42);
    let trust_pubkey: PublicKey = NewPublicKey::new(TRUST.clone()).unwrap().into();
    let redeem_tx = create_random_redeem_transaction(&mut csprng, trust_pubkey);

    // initialize genesis with identity tags
    let genesis = GenesisConfig::<Test> {
        identity_tags: redeem_tx.identity_set.clone(),
        txos: redeem_tx.input_txos.clone(),
        ..Default::default()
    };

    new_test_externalities(genesis, Default::default()).execute_with(|| {
        let result =
            ConfidentialityPallet::redeem(Origin::signed(MEM_1.clone()), Wrap(redeem_tx.tx));
        assert_err!(
            result,
            Error::<Test>::CanNotSubmitConfidentialTransactionWithSignature
        );
    });
}

#[test]
fn fulfill_redeem_happy_path() {
    let mut csprng = StdRng::seed_from_u64(42);
    let trust_pubkey: PublicKey = NewPublicKey::new(TRUST.clone()).unwrap().into();
    let redeem_tx = create_random_redeem_transaction(&mut csprng, trust_pubkey);
    let correlation_id = redeem_tx.tx.core_transaction.correlation_id;
    let fulfill = RedeemFulfillmentTransaction { correlation_id };

    // initialize genesis with identity tags
    let genesis = GenesisConfig::<Test> {
        identity_tags: redeem_tx.identity_set.clone(),
        txos: redeem_tx.input_txos.clone(),
        ..Default::default()
    };

    new_test_externalities(genesis, Default::default()).execute_with(|| {
        let req = ConfidentialityPallet::redeem(RawOrigin::None.into(), Wrap(redeem_tx.tx));
        assert_ok!(req);
        let result =
            ConfidentialityPallet::redeem_fulfill(Origin::signed(TRUST.clone()), Wrap(fulfill));
        assert_ok!(result);
    });
}

#[test]
fn fulfill_redeem_fails_if_not_sent_by_trust() {
    let mut csprng = StdRng::seed_from_u64(42);
    let trust_pubkey: PublicKey = NewPublicKey::new(TRUST.clone()).unwrap().into();
    let redeem_tx = create_random_redeem_transaction(&mut csprng, trust_pubkey);
    let correlation_id = redeem_tx.tx.core_transaction.correlation_id;
    let fulfill = RedeemFulfillmentTransaction { correlation_id };

    // initialize genesis with identity tags
    let genesis = GenesisConfig::<Test> {
        identity_tags: redeem_tx.identity_set.clone(),
        txos: redeem_tx.input_txos.clone(),
        ..Default::default()
    };

    new_test_externalities(genesis, Default::default()).execute_with(|| {
        let req = ConfidentialityPallet::redeem(RawOrigin::None.into(), Wrap(redeem_tx.tx));
        assert_ok!(req);
        let result =
            ConfidentialityPallet::redeem_fulfill(Origin::signed(MEM_1.clone()), Wrap(fulfill));
        assert_err!(result, Error::<Test>::OnlyTrustMayFulfillRedeems);
    });
}

#[test]
fn cancel_pending_redeem_happy_path() {
    let mut csprng = StdRng::seed_from_u64(42);
    let trust_pubkey: PublicKey = NewPublicKey::new(TRUST.clone()).unwrap().into();
    let redeem_tx = create_random_redeem_transaction(&mut csprng, trust_pubkey);
    let correlation_id = redeem_tx.tx.core_transaction.correlation_id;
    let reason = RedeemCancellationReason::InvalidData;
    let cancel = RedeemCancellationTransaction {
        correlation_id,
        reason,
    };

    // initialize genesis with identity tags
    let genesis = GenesisConfig::<Test> {
        identity_tags: redeem_tx.identity_set.clone(),
        txos: redeem_tx.input_txos.clone(),
        ..Default::default()
    };

    new_test_externalities(genesis, Default::default()).execute_with(|| {
        let req = ConfidentialityPallet::redeem(RawOrigin::None.into(), Wrap(redeem_tx.tx));
        assert_ok!(req);
        let result = ConfidentialityPallet::cancel_pending_redeem(
            Origin::signed(TRUST.clone()),
            Wrap(cancel),
        );
        assert_ok!(result);
    });
}

#[test]
fn cancel_pending_redeem_fails_if_not_sent_by_trust() {
    let mut csprng = StdRng::seed_from_u64(42);
    let trust_pubkey: PublicKey = NewPublicKey::new(TRUST.clone()).unwrap().into();
    let redeem_tx = create_random_redeem_transaction(&mut csprng, trust_pubkey);
    let correlation_id = redeem_tx.tx.core_transaction.correlation_id;
    let reason = RedeemCancellationReason::InvalidData;
    let cancel = RedeemCancellationTransaction {
        correlation_id,
        reason,
    };

    // initialize genesis with identity tags
    let genesis = GenesisConfig::<Test> {
        identity_tags: redeem_tx.identity_set.clone(),
        txos: redeem_tx.input_txos.clone(),
        ..Default::default()
    };

    new_test_externalities(genesis, Default::default()).execute_with(|| {
        let req = ConfidentialityPallet::redeem(RawOrigin::None.into(), Wrap(redeem_tx.tx));
        assert_ok!(req);
        let result = ConfidentialityPallet::cancel_pending_redeem(
            Origin::signed(MEM_1.clone()),
            Wrap(cancel),
        );
        assert_err!(result, Error::<Test>::OnlyTrustMayCancelRedeems);
    });
}
