#![allow(non_snake_case)]

use super::*;
use crate::confidentiality::{
    tests::{new_test_externalities, Test},
    GenesisConfig,
};
use xand_ledger::esig_payload::ESigPayload;
use xand_ledger::{
    ClearTransactionOutput, ClearTransactionOutputRepository, MonetaryValue, PublicKey, Salt,
};

#[test]
fn reward__succeeds_if_issued_by_root() {
    let genesis = GenesisConfig::<Test>::default();
    new_test_externalities(genesis, Default::default()).execute_with(|| {
        // Given
        let repo = ClearUtxoRepo::<Test> {
            _module_marker: PhantomData,
        };
        let utxo = test_utxo(PublicKey::default(), 1_u32.try_into().unwrap(), 0);
        let reward_tx = RewardTransaction::new(utxo);

        // When
        let result = ConfidentialityPallet::reward(Origin::root(), Wrap(reward_tx));

        // Then
        result.unwrap();
        let utxo_exists = repo.contains(&utxo);
        assert!(utxo_exists);
    });
}

#[test]
fn reward__fails_if_not_root() {
    let genesis = GenesisConfig::<Test>::default();
    new_test_externalities(genesis, Default::default()).execute_with(|| {
        // Given
        let reward_tx = RewardTransaction::new(test_utxo(
            PublicKey::default(),
            1_u32.try_into().unwrap(),
            0,
        ));

        // When
        let result = ConfidentialityPallet::reward(Origin::signed(MEM_1.clone()), Wrap(reward_tx));

        // Then
        let err: DispatchError = result.unwrap_err().error;
        assert_eq!(err, DispatchError::BadOrigin);
    });
}

#[test]
fn reward__fails_if_txo_already_exists() {
    let genesis = GenesisConfig::<Test>::default();
    new_test_externalities(genesis, Default::default()).execute_with(|| {
        // Given
        let utxo = test_utxo(PublicKey::default(), 1_u32.try_into().unwrap(), 0);
        let reward_tx_1 = RewardTransaction::new(utxo);
        let reward_tx_2 = RewardTransaction::new(utxo);
        ConfidentialityPallet::reward(Origin::root(), Wrap(reward_tx_1)).unwrap();

        // When - issue duplicate reward
        let result = ConfidentialityPallet::reward(Origin::root(), Wrap(reward_tx_2));

        // Then
        assert!(
            matches!(
                result.unwrap_err().error,
                DispatchError::Module {
                    message: Some("TxoAlreadyExists"),
                    ..
                }
            ),
            "Found {:?}",
            result
        );
    });
}

#[test]
fn reward__subsequent_reward_succeeds() {
    let genesis = GenesisConfig::<Test>::default();
    new_test_externalities(genesis, Default::default()).execute_with(|| {
        // Given
        let utxo_1 = test_utxo(PublicKey::default(), 1_u32.try_into().unwrap(), 0);
        let reward_tx_1 = RewardTransaction::new(utxo_1);
        ConfidentialityPallet::reward(Origin::root(), Wrap(reward_tx_1)).unwrap();

        let utxo_2 = test_utxo(PublicKey::default(), 1_u32.try_into().unwrap(), 1);
        let reward_tx_2 = RewardTransaction::new(utxo_2);

        // When
        let result = ConfidentialityPallet::reward(Origin::root(), Wrap(reward_tx_2));

        // Then
        assert!(result.is_ok(), "Found {:?}", result);
    });
}

#[test]
fn reward__increments_total_created() {
    let genesis = GenesisConfig::<Test>::default();
    new_test_externalities(genesis, Default::default()).execute_with(|| {
        // Given
        let utxo = test_utxo(PublicKey::default(), 5_u32.try_into().unwrap(), 0);
        let reward_tx = RewardTransaction::new(utxo);

        // When
        ConfidentialityPallet::reward(Origin::root(), Wrap(reward_tx)).unwrap();

        // Then
        assert_eq!(5, TotalCreated::<Test>::get());
    });
}

fn test_utxo(public_key: PublicKey, value: MonetaryValue, salt: Salt) -> ClearTransactionOutput {
    ClearTransactionOutput::new(public_key, value, salt, ESigPayload::new_with_ueta())
}
