use crate::confidentiality::testing_and_benchmarking_shared::create_random_create_transaction;
use crate::confidentiality::{
    test_constants::{MEM_1, NON_EXISTENT_TRUST, TRUST},
    tests::Origin,
    tests::{new_test_externalities, ConfidentialityPallet, System, Test},
    tests::{run_to_block, SLOT_DURATION},
    Error, GenesisConfig, PendingCreates, UsedCorrelationIds,
};
use frame_support::{assert_err, assert_noop, assert_ok};
use frame_system::RawOrigin;
use rand::{prelude::StdRng, SeedableRng};
use serde_scale_wrap::Wrap;
use xand_ledger::CashConfirmationTransaction;

#[test]
fn create_request_happy_path() {
    let mut csprng = StdRng::seed_from_u64(42);
    let create_tx = create_random_create_transaction(&mut csprng);

    // initialize genesis with identity tags
    let genesis = GenesisConfig::<Test> {
        identity_tags: create_tx.identity_set.clone(),
        ..Default::default()
    };
    new_test_externalities(genesis, Default::default()).execute_with(|| {
        let result =
            ConfidentialityPallet::request_create(RawOrigin::None.into(), Wrap(create_tx.tx));
        assert_ok!(result);
    });
}

#[test]
fn create_request_fails_if_signed_txn() {
    let mut csprng = StdRng::seed_from_u64(42);
    let create_tx = create_random_create_transaction(&mut csprng);

    // initialize genesis with identity tags
    let genesis = GenesisConfig::<Test> {
        identity_tags: create_tx.identity_set.clone(),
        ..Default::default()
    };

    new_test_externalities(genesis, Default::default()).execute_with(|| {
        let result = ConfidentialityPallet::request_create(
            Origin::signed(MEM_1.clone()),
            Wrap(create_tx.tx),
        );
        assert_err!(
            result,
            Error::<Test>::CanNotSubmitConfidentialTransactionWithSignature
        );
    });
}

#[test]
fn cash_confirmation_happy_path() {
    let mut csprng = StdRng::seed_from_u64(42);
    let create_tx = create_random_create_transaction(&mut csprng);

    let total_value = create_tx
        .tx
        .core_transaction
        .outputs
        .iter()
        .fold(0u64, |acc, output| acc + output.value);

    // initialize genesis with identity tags
    let genesis = GenesisConfig::<Test> {
        identity_tags: create_tx.identity_set.clone(),
        ..Default::default()
    };
    new_test_externalities(genesis, Default::default()).execute_with(|| {
        // submit pending create
        let result =
            ConfidentialityPallet::request_create_tx(RawOrigin::None.into(), create_tx.tx.clone());
        assert_ok!(result);
        // submit cash confirmation
        let result = ConfidentialityPallet::confirm_create_tx(
            Origin::signed(TRUST.clone()),
            CashConfirmationTransaction::new(create_tx.tx.core_transaction.correlation_id),
        );
        assert_ok!(result);
        // assert total created matches total of create outputs
        assert_eq!(ConfidentialityPallet::total_created(), total_value);
        // assert identity output is now valid for future use
        // assert that identity tags exist
        assert!(ConfidentialityPallet::identity_tags(Wrap(
            create_tx.tx.core_transaction.identity_output
        )));
    });
}

#[test]
fn expired_creates_cancelled_and_event_emitted() {
    let mut csprng = StdRng::seed_from_u64(42);
    let create_tx = create_random_create_transaction(&mut csprng);

    let genesis = GenesisConfig::<Test> {
        identity_tags: create_tx.identity_set.clone(),
        pending_create_expire_time: 3 * SLOT_DURATION / 2,
        ..Default::default()
    };
    new_test_externalities(genesis, Default::default()).execute_with(|| {
        run_to_block(1);
        assert_ok!(ConfidentialityPallet::request_create_tx(
            RawOrigin::None.into(),
            create_tx.tx
        ));
        run_to_block(3);
        assert!(PendingCreates::<Test>::iter().next().is_none());
        let events = System::events();
        assert!(events.iter().any(|er| er.event.0.contains("Expired")));
    });
}

#[test]
fn correlation_ids_are_cleared_after_window() {
    let mut csprng = StdRng::seed_from_u64(42);
    let create_tx = create_random_create_transaction(&mut csprng);

    let mut create_tx_2 = create_random_create_transaction(&mut csprng);
    create_tx_2.tx.core_transaction.correlation_id = create_tx.tx.core_transaction.correlation_id;

    // combine the identity tags of both create txs
    let mut identity_tags = create_tx.identity_set.clone();
    identity_tags.append(&mut create_tx_2.identity_set.clone());

    let genesis = GenesisConfig::<Test> {
        identity_tags,
        pending_create_expire_time: 100u64 * 6000,
        used_correlation_id_expire_time: 10,
        ..Default::default()
    };
    new_test_externalities(genesis, Default::default()).execute_with(|| {
        run_to_block(1);
        assert_ok!(ConfidentialityPallet::request_create_tx(
            RawOrigin::None.into(),
            create_tx.tx.clone()
        ));
        // TODO: also create a pending redeem
        assert_eq!(PendingCreates::<Test>::iter().count(), 1);
        assert_eq!(UsedCorrelationIds::<Test>::iter().count(), 1);
        assert_ok!(ConfidentialityPallet::confirm_create_tx(
            Origin::signed(TRUST.clone()),
            CashConfirmationTransaction::new(create_tx.tx.core_transaction.correlation_id),
        ));
        // TODO: also fulfill a redeem
        assert_eq!(UsedCorrelationIds::<Test>::iter().count(), 1);
        run_to_block(2);
        // In block '2' the requests have been fulfilled, but the correlation_id is still reserved
        assert_eq!(UsedCorrelationIds::<Test>::iter().count(), 1);
        // Verify new requests cannot be created w/ reserved correlation ids
        assert_err!(
            ConfidentialityPallet::request_create_tx(RawOrigin::None.into(), create_tx_2.tx),
            Error::<Test>::CorrelationIdCannotBeReused
        );
        // TODO: also verify redeem correlation id can't be reused

        run_to_block(3);
        assert_eq!(UsedCorrelationIds::<Test>::iter().count(), 0);
    });
}

#[test]
fn correlation_ids_are_not_cleared_if_past_window_but_still_pending() {
    let mut csprng = StdRng::seed_from_u64(42);
    let create_tx = create_random_create_transaction(&mut csprng);

    let genesis = GenesisConfig::<Test> {
        identity_tags: create_tx.identity_set.clone(),
        pending_create_expire_time: 100u64 * 6000,
        used_correlation_id_expire_time: 10,
        ..Default::default()
    };

    // Setup confidential runtime
    new_test_externalities(genesis, Default::default()).execute_with(|| {
        run_to_block(1);
        assert_ok!(ConfidentialityPallet::request_create_tx(
            RawOrigin::None.into(),
            create_tx.tx.clone()
        ));
        // TODO: Create a pending redeem
        assert_eq!(UsedCorrelationIds::<Test>::iter().count(), 1);
        run_to_block(2);
        assert_eq!(UsedCorrelationIds::<Test>::iter().count(), 1);
        assert_ok!(ConfidentialityPallet::confirm_create_tx(
            Origin::signed(TRUST.clone()),
            CashConfirmationTransaction::new(create_tx.tx.core_transaction.correlation_id),
        ));
        assert_eq!(UsedCorrelationIds::<Test>::iter().count(), 0);
    });
}

#[test]
fn confirm_create_fails_if_signed_by_non_trust_entity() {
    // submit conf create
    // sign the conf create by another acct_id that is not registered in the test externalities as the trust
    // assert the error returned when attempting to confirm create

    let mut csprng = StdRng::seed_from_u64(42);
    let create_tx = create_random_create_transaction(&mut csprng);

    // initialize genesis with identity tags
    let genesis = GenesisConfig::<Test> {
        identity_tags: create_tx.identity_set.clone(),
        ..Default::default()
    };

    new_test_externalities(genesis, Default::default()).execute_with(|| {
        // submit pending create
        let create_req_result =
            ConfidentialityPallet::request_create_tx(RawOrigin::None.into(), create_tx.tx.clone());
        assert_ok!(create_req_result);
        // submit cash confirmation with id that is not the trust
        let confirm_create_result = ConfidentialityPallet::confirm_create_tx(
            Origin::signed(NON_EXISTENT_TRUST.clone()),
            CashConfirmationTransaction::new(create_tx.tx.core_transaction.correlation_id),
        );
        // assert error from call
        assert_noop!(
            confirm_create_result,
            Error::<Test>::OnlyTrustMayConfirmCreates
        );
        // assert no Xand created
        assert_eq!(ConfidentialityPallet::total_created(), 0);
        // assert identity output is invalid in that identity tags do not exist
        assert!(!ConfidentialityPallet::identity_tags(Wrap(
            create_tx.tx.core_transaction.identity_output
        )));
    });
}
