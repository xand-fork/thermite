use crate::confidentiality::{
    test_constants::MEM_1,
    testing_and_benchmarking_shared::create_random_send_transaction,
    tests::{new_test_externalities, ConfidentialityPallet, Origin, Test},
    Error, GenesisConfig, IdentityTags, TXOs, UsedKeyImages,
};
use frame_support::{assert_err, assert_ok};
use frame_system::RawOrigin;
use rand::{prelude::StdRng, SeedableRng};
use serde_scale_wrap::Wrap;

#[test]
fn send_fails_if_signed_txn() {
    let mut csprng = StdRng::seed_from_u64(42);
    let send_tx = create_random_send_transaction(&mut csprng);

    // initialize genesis with identity tags
    let genesis = GenesisConfig::<Test> {
        identity_tags: send_tx.input_identities.clone(),
        ..Default::default()
    };

    new_test_externalities(genesis, Default::default()).execute_with(|| {
        let result = ConfidentialityPallet::send(Origin::signed(MEM_1.clone()), Wrap(send_tx.send));
        assert_err!(
            result,
            Error::<Test>::CanNotSubmitConfidentialTransactionWithSignature
        );
    });
}

#[test]
fn send_key_images_are_recorded() {
    let mut csprng = StdRng::seed_from_u64(42);
    let send_tx = create_random_send_transaction(&mut csprng);

    // initialize genesis with identity tags and input txos
    let genesis = GenesisConfig::<Test> {
        identity_tags: send_tx.input_identities.clone(),
        txos: send_tx.input_txos.clone(),
        ..Default::default()
    };

    new_test_externalities(genesis, Default::default()).execute_with(|| {
        let result = ConfidentialityPallet::send(RawOrigin::None.into(), Wrap(send_tx.send));

        // check that all key images from send are stored
        assert_ok!(result);
        assert_eq!(
            UsedKeyImages::<Test>::iter().count(),
            send_tx.key_images.len()
        )
    });
}

#[test]
fn send_output_id_tag_is_stored() {
    let mut csprng = StdRng::seed_from_u64(42);
    let send_tx = create_random_send_transaction(&mut csprng);

    // initialize genesis with identity tags and input txos
    let genesis = GenesisConfig::<Test> {
        identity_tags: send_tx.input_identities.clone(),
        txos: send_tx.input_txos.clone(),
        ..Default::default()
    };

    new_test_externalities(genesis, Default::default()).execute_with(|| {
        let result =
            ConfidentialityPallet::send(RawOrigin::None.into(), Wrap(send_tx.send.clone()));

        // check that all key images from send are stored
        assert_ok!(result);
        assert!(IdentityTags::<Test>::get(Wrap(
            send_tx.send.core_transaction.output_identity
        )))
    });
}

#[test]
fn send_output_txos_are_stored() {
    let mut csprng = StdRng::seed_from_u64(42);
    let send_tx = create_random_send_transaction(&mut csprng);

    // initialize genesis with identity tags and input txos
    let genesis = GenesisConfig::<Test> {
        identity_tags: send_tx.input_identities.clone(),
        txos: send_tx.input_txos.clone(),
        ..Default::default()
    };

    new_test_externalities(genesis, Default::default()).execute_with(|| {
        // ensure input txos are stored
        assert_eq!(TXOs::<Test>::iter().count(), send_tx.input_txos.len());

        let result =
            ConfidentialityPallet::send(RawOrigin::None.into(), Wrap(send_tx.send.clone()));

        assert_ok!(result);
        // ensure output txos are stored after send
        assert_eq!(
            TXOs::<Test>::iter().count(),
            send_tx.input_txos.len() + send_tx.send.core_transaction.output.len()
        );
    });
}
