use crate::confidentiality::ledger_adapters::NewPublicKey;
use crate::confidentiality::{Config, XandstrateStateTrait};
use sp_core::sp_std::{convert::TryInto, marker::PhantomData};
use xand_ledger::{PublicKey, ValidatorRepository};

pub(crate) struct XandstrateValidatorRepo<T> {
    pub(crate) _module_marker: PhantomData<T>,
}

impl<T> ValidatorRepository for XandstrateValidatorRepo<T>
where
    T: Config,
{
    type PublicKey = PublicKey;

    fn is_validator(&self, public_key: &Self::PublicKey) -> bool {
        let new_public_key = NewPublicKey(*public_key);
        let address_opt = new_public_key.try_into().ok();
        if let Some(address) = address_opt {
            <T as Config>::XandstrateState::is_validator(&address)
        } else {
            false
        }
    }
}

#[cfg(test)]
mod tests {
    #![allow(non_snake_case)]

    use crate::confidentiality::{
        ledger_adapters::{validator_repo::XandstrateValidatorRepo, NewPublicKey},
        tests::{new_test_externalities, Test, XandstrateStateMock},
        GenesisConfig,
    };
    use rand::prelude::StdRng;
    use rand_core::SeedableRng;
    use sp_core::crypto::AccountId32;
    use std::marker::PhantomData;
    use xand_ledger::{PrivateKey, PublicKey, ValidatorRepository};

    #[test]
    fn is_validator__returns_true_with_participating_address() {
        // Given a random public key
        let mut csprng = StdRng::seed_from_u64(42);
        let some_private_key = PrivateKey::random(&mut csprng);
        let some_public_key = PublicKey::from(some_private_key);

        // And a validator repo
        let repo = XandstrateValidatorRepo::<Test> {
            _module_marker: PhantomData,
        };

        // And a test context with default test genesis, confidentiality flag set to false, and a
        // participating validator with the above address
        new_test_externalities(
            GenesisConfig::<Test>::default(),
            XandstrateStateMock {
                validators: vec![AccountId32::from(NewPublicKey(some_public_key))],
                ..Default::default()
            },
        )
        .execute_with(|| {
            // When
            let is_validator = repo.is_validator(&some_public_key);

            // Then
            assert!(is_validator);
        });
    }

    #[test]
    fn is_validator__returns_false_with_nonparticipating_address() {
        // Given a random public key
        let mut csprng = StdRng::seed_from_u64(42);
        let some_private_key = PrivateKey::random(&mut csprng);
        let some_public_key = PublicKey::from(some_private_key);

        // And a validator repo
        let repo = XandstrateValidatorRepo::<Test> {
            _module_marker: PhantomData,
        };

        // And a test context with default test genesis, confidentiality flag set to false, and no
        // participating validators
        new_test_externalities(
            GenesisConfig::<Test>::default(),
            XandstrateStateMock::default(),
        )
        .execute_with(|| {
            // When
            let is_validator = repo.is_validator(&some_public_key);

            // Then
            assert!(!is_validator);
        });
    }
}
