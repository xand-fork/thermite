use crate::confidentiality;
use core::marker::PhantomData;
use serde_scale_wrap::Wrap;
use xand_ledger::{KeyImage, KeyImageRepository};

pub(crate) struct KeyImageRepo<T> {
    pub(crate) _module_marker: PhantomData<T>,
}

impl<T: confidentiality::Config> KeyImageRepository for KeyImageRepo<T> {
    fn is_used(&self, image: &KeyImage) -> bool {
        confidentiality::UsedKeyImages::<T>::get(Wrap(*image))
    }

    fn insert_key_image(&self, image: KeyImage) {
        confidentiality::UsedKeyImages::<T>::insert(Wrap(image), true)
    }
}

#[cfg(test)]
mod tests {
    use crate::confidentiality::{
        ledger_adapters::key_image_repo::KeyImageRepo,
        tests::{new_test_externalities, Test},
        GenesisConfig,
    };
    use rand::{prelude::StdRng, SeedableRng};
    use serde_scale_wrap::Wrap;
    use std::marker::PhantomData;
    use xand_ledger::{KeyImage, KeyImageRepository};

    #[test]
    fn cant_retrieve_non_existent_key_image() {
        let mut csprng = StdRng::seed_from_u64(42);
        let tag = KeyImage::random(&mut csprng);
        let genesis = GenesisConfig::<Test>::default();
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let repo = KeyImageRepo::<Test> {
                _module_marker: PhantomData,
            };
            assert!(!repo.is_used(&tag));
        });
    }

    #[test]
    fn can_retrieve_key_image() {
        let mut csprng = StdRng::seed_from_u64(42);
        let key_image = KeyImage::random(&mut csprng);

        let genesis = GenesisConfig::<Test> {
            used_key_images: vec![(Wrap(key_image), true)],
            ..Default::default()
        };

        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let repo = KeyImageRepo::<Test> {
                _module_marker: PhantomData,
            };
            assert!(repo.is_used(&key_image));
        });
    }

    #[test]
    fn can_store_key_image() {
        let mut csprng = StdRng::seed_from_u64(42);
        let genesis = GenesisConfig::<Test>::default();
        let key_image = KeyImage::random(&mut csprng);

        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let repo = KeyImageRepo::<Test> {
                _module_marker: PhantomData,
            };
            repo.insert_key_image(key_image);
            assert!(repo.is_used(&key_image));
        });
    }
}
