use crate::confidentiality::ledger_adapters::NewPublicKey;
use crate::{
    confidentiality::{Config, XandstrateStateTrait},
    Vec,
};
use sp_core::sp_std::{convert::TryInto, marker::PhantomData};
use xand_ledger::{IdentityTag, MemberRepository, PublicKey};

pub(crate) struct XandstrateMemberRepo<T> {
    pub(crate) _module_marker: PhantomData<T>,
}

impl<T> MemberRepository<T::BlockNumber> for XandstrateMemberRepo<T>
where
    T: Config,
{
    fn is_valid_member(&self, id: &IdentityTag) -> bool {
        let new_tag = NewIdentityTag(*id);
        if let Some(address) = convert_id_tag_to_generic_account::<T>(new_tag) {
            <T as Config>::XandstrateState::is_member(&address)
        } else {
            false
        }
    }

    fn get_banned_members(&self) -> Vec<(PublicKey, xand_ledger::BannedState<T::BlockNumber>)> {
        <T as Config>::XandstrateState::get_banned_members()
            .into_iter()
            .flat_map(|(pubkey, state)| {
                TryInto::<NewPublicKey>::try_into(pubkey).map(|pk| (pk, state.into()))
            })
            .map(|(pubkey, state)| (pubkey.into(), state))
            .collect()
    }
}

pub struct NewIdentityTag(pub IdentityTag);

fn convert_id_tag_to_generic_account<T>(
    id: NewIdentityTag,
) -> Option<<<T as Config>::XandstrateState as XandstrateStateTrait<T>>::MemberAccountId>
where
    T: Config,
{
    id.try_into().ok()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::confidentiality::{
        tests::{new_test_externalities, Test, XandstrateStateMock},
        GenesisConfig,
    };
    use rand::{prelude::StdRng, SeedableRng};
    use sp_core::crypto::AccountId32;
    use xand_ledger::curve25519_dalek::ristretto::RistrettoPoint;
    use xand_runtime_models::BannedState;

    #[test]
    fn return_false_when_id_is_not_in_members() {
        // initialize genesis with identity tags
        let genesis = GenesisConfig::<Test>::default();

        // setup test context with confidentiality flag set to false
        new_test_externalities(genesis, XandstrateStateMock::default()).execute_with(|| {
            let repo = XandstrateMemberRepo::<Test> {
                _module_marker: PhantomData,
            };
            let mut csprng = StdRng::seed_from_u64(42);
            let some_id = IdentityTag::random(&mut csprng);
            assert!(!repo.is_valid_member(&some_id));
        });
    }

    #[test]
    fn return_true_when_id_is_in_members() {
        let genesis = GenesisConfig::<Test>::default();

        let mut csprng = StdRng::seed_from_u64(42);

        let ristretto: RistrettoPoint = RistrettoPoint::random(&mut csprng);
        let member: AccountId32 = ristretto.compress().0.into();
        let id = IdentityTag::from_key_with_generator_base(ristretto.into());

        // setup test context with confidentiality flag set to false
        // add member
        new_test_externalities(
            genesis,
            XandstrateStateMock {
                members: vec![member],
                ..Default::default()
            },
        )
        .execute_with(|| {
            let repo = XandstrateMemberRepo::<Test> {
                _module_marker: PhantomData,
            };
            assert!(repo.is_valid_member(&id));
        });
    }

    #[test]
    fn can_get_all_banned_members() {
        let genesis = GenesisConfig::<Test>::default();

        let mut csprng = StdRng::seed_from_u64(42);

        let ristretto: RistrettoPoint = RistrettoPoint::random(&mut csprng);
        let member: AccountId32 = ristretto.compress().0.into();

        let pubkey: PublicKey = TryInto::<NewPublicKey>::try_into(member.clone())
            .unwrap()
            .into();
        let expected = vec![(pubkey, xand_ledger::BannedState::Exiting(0))];

        new_test_externalities(
            genesis,
            XandstrateStateMock {
                banned_members: vec![(member, BannedState::Exiting(0))],
                ..Default::default()
            },
        )
        .execute_with(|| {
            let repo = XandstrateMemberRepo::<Test> {
                _module_marker: PhantomData,
            };
            assert_eq!(repo.get_banned_members(), expected);
        });
    }
}
