use crate::confidentiality::ledger_adapters::NewPublicKey;
use crate::confidentiality::{Config, XandstrateStateTrait};
use sp_core::sp_std::convert::TryInto;
use sp_core::sp_std::marker::PhantomData;
use xand_ledger::{PublicKey, TrustMetadata};

pub(crate) struct TrustMetadataRepo<T> {
    pub(crate) _module_marker: PhantomData<T>,
}

impl<T: Config> TrustMetadata for TrustMetadataRepo<T>
where
    <<T as Config>::XandstrateState as XandstrateStateTrait<T>>::TrustAccountId:
        TryInto<NewPublicKey>,
{
    fn get_trust_key(&self) -> Option<PublicKey> {
        let wrapper: Option<NewPublicKey> = <T as Config>::XandstrateState::get_trust_id()
            .try_into()
            .ok();
        wrapper.map(Into::into)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::confidentiality::{
        test_constants::*,
        tests::{new_test_externalities, Test, XandstrateStateMock},
        GenesisConfig,
    };
    use sp_core::sp_std::convert::TryFrom;

    #[test]
    fn can_get_id() {
        // initialize genesis with identity tags
        let genesis = GenesisConfig::<Test>::default();

        // setup test context with confidentiality flag set to false
        new_test_externalities(
            genesis,
            XandstrateStateMock {
                trust_node_id: TRUST.clone(),
                ..Default::default()
            },
        )
        .execute_with(|| {
            let repo = TrustMetadataRepo::<Test> {
                _module_marker: PhantomData,
            };
            let actual = repo.get_trust_key().unwrap();
            let expected: PublicKey = NewPublicKey::try_from(TRUST.clone()).unwrap().into();
            assert_eq!(actual, expected);
        });
    }
}
