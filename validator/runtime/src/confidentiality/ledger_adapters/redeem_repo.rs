use crate::confidentiality;
use core::marker::PhantomData;
use frame_support::IterableStorageMap;
use serde_scale_wrap::Wrap;
use xand_ledger::{
    ClearTransactionOutput, CorrelationId, RedeemRequestRecord, RedeemRequestRepository,
};

pub(crate) struct RedeemRepo<T> {
    pub(crate) _module_marker: PhantomData<T>,
}

#[cfg(test)]
impl<T> RedeemRepo<T> {
    pub fn new() -> RedeemRepo<T> {
        RedeemRepo {
            _module_marker: PhantomData,
        }
    }
}

impl<T: confidentiality::Config> RedeemRequestRepository for RedeemRepo<T> {
    type Utxo = ClearTransactionOutput;
    type Iter = PendingRedeemMapUnwrapper<T>;

    fn get_unfulfilled_redeem_request(
        &self,
        correlation_id: [u8; 16],
    ) -> Option<RedeemRequestRecord> {
        confidentiality::PendingRedeems::<T>::get(correlation_id).map(|x| x.0)
    }

    fn store_redeem_request(&self, correlation_id: [u8; 16], record: RedeemRequestRecord) {
        confidentiality::PendingRedeems::<T>::insert(correlation_id, Wrap(record))
    }

    fn remove_redeem_request(&self, correlation_id: [u8; 16]) {
        confidentiality::PendingRedeems::<T>::remove(correlation_id)
    }

    fn all_unfulfilled_redeem_requests(&self) -> Self::Iter {
        PendingRedeemMapUnwrapper {
            inner: confidentiality::PendingRedeems::<T>::iter(),
        }
    }
}

pub(crate) struct PendingRedeemMapUnwrapper<T: confidentiality::Config> {
    inner: <confidentiality::PendingRedeems<T> as IterableStorageMap<
        CorrelationId,
        Wrap<RedeemRequestRecord>,
    >>::Iterator,
}

impl<T: confidentiality::Config> Iterator for PendingRedeemMapUnwrapper<T> {
    type Item = (CorrelationId, RedeemRequestRecord);

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().map(|(a, b)| (a, b.0))
    }
}

#[cfg(test)]
pub(crate) mod tests {
    #![allow(non_snake_case)]

    use super::*;
    use crate::confidentiality::{tests::*, GenesisConfig, PendingRedeems};
    use rand::{prelude::StdRng, SeedableRng};
    use rand::{CryptoRng, RngCore};
    use std::convert::TryFrom;
    use xand_ledger::esig_payload::ESigPayload;
    use xand_ledger::{
        ClearRedeemRequestRecord, ConfidentialRedeemRequestRecord, MonetaryValue, OneTimeKey,
        OpenedTransactionOutput, PrivateKey, PublicKey, RedeemSigner, TransactionOutput,
        VerifiableEncryptionOfSignerKey,
    };

    pub fn random_confidential_redeem_record<R>(csprng: &mut R) -> RedeemRequestRecord
    where
        R: RngCore + CryptoRng,
    {
        RedeemRequestRecord::Confidential(Box::new(ConfidentialRedeemRequestRecord {
            redeem_output: OpenedTransactionOutput {
                transaction_output: TransactionOutput::new(
                    Default::default(),
                    OneTimeKey::random(csprng),
                    ESigPayload::new_with_ueta(),
                ),
                blinding_factor: Default::default(),
                value: 0,
            },
            account_data: vec![],
            amount: 0,
            signer: RedeemSigner::EncryptedSigner(
                VerifiableEncryptionOfSignerKey::random(csprng).into(),
            ),
        }))
    }

    pub fn random_clear_redeem_record<R>(csprng: &mut R) -> RedeemRequestRecord
    where
        R: RngCore + CryptoRng,
    {
        let some_public_key = PublicKey::from(PrivateKey::random(csprng));
        let amount = MonetaryValue::try_from(100u64).unwrap();
        RedeemRequestRecord::Clear(ClearRedeemRequestRecord {
            redeem_output: ClearTransactionOutput::new(
                some_public_key,
                amount,
                csprng.next_u64(),
                ESigPayload::new_with_ueta(),
            ),
            account_data: vec![],
            amount,
        })
    }

    #[test]
    fn can_get_unfulfilled_redeem() {
        let mut csprng = StdRng::seed_from_u64(42);
        let repo = RedeemRepo::<Test>::new();
        let id = [0u8; 16];
        let expected = random_confidential_redeem_record(&mut csprng);
        let genesis = GenesisConfig::<Test> {
            pending_redeems: vec![(id, Wrap(expected.clone()))],
            ..Default::default()
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let actual = repo.get_unfulfilled_redeem_request(id).unwrap();
            assert_eq!(actual, expected);
        })
    }

    #[test]
    fn can_store_redeem() {
        let genesis = GenesisConfig::<Test>::default();
        let mut csprng = StdRng::seed_from_u64(42);
        let repo = RedeemRepo::<Test>::new();
        let id = [0u8; 16];
        let expected = random_confidential_redeem_record(&mut csprng);
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            assert!(confidentiality::PendingRedeems::<Test>::get(id).is_none());
            repo.store_redeem_request(id, expected.clone());
            let actual = confidentiality::PendingRedeems::<Test>::get(id).unwrap();
            assert_eq!(actual, Wrap(expected));
        })
    }

    #[test]
    fn can_remove_redeem() {
        let mut csprng = StdRng::seed_from_u64(42);
        let repo = RedeemRepo::<Test>::new();
        let id = [0u8; 16];
        let expected = random_confidential_redeem_record(&mut csprng);
        let genesis = GenesisConfig::<Test> {
            pending_redeems: vec![(id, Wrap(expected))],
            ..Default::default()
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            repo.remove_redeem_request(id);
            assert!(confidentiality::PendingRedeems::<Test>::get(id).is_none());
        })
    }

    #[test]
    fn can_get_all_unfulfilled_redeems() {
        let mut csprng = StdRng::seed_from_u64(42);
        let repo = RedeemRepo::<Test>::new();
        let id_one = [1u8; 16];
        let id_two = [2u8; 16];
        let id_three = [3u8; 16];
        let rec_one = random_confidential_redeem_record(&mut csprng);
        let rec_two = random_confidential_redeem_record(&mut csprng);
        let rec_three = random_confidential_redeem_record(&mut csprng);
        let expected = vec![(id_one, rec_one), (id_two, rec_two), (id_three, rec_three)];
        let pending_redeems = expected
            .clone()
            .into_iter()
            .map(|(x, y)| (x, Wrap(y)))
            .collect();
        let genesis = GenesisConfig::<Test> {
            pending_redeems,
            ..Default::default()
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let actual: Vec<_> = repo.all_unfulfilled_redeem_requests().collect();
            assert_eq!(actual, expected);
        })
    }

    #[test]
    fn get_unfulfilled_redeem__returns_none_when_no_such_record_exists() {
        // Given
        let repo = RedeemRepo::<Test>::new();
        let id = [0u8; 16];
        new_test_externalities(GenesisConfig::<Test>::default(), Default::default()).execute_with(
            || {
                // When
                let result = repo.get_unfulfilled_redeem_request(id);

                // Then
                assert!(result.is_none());
            },
        )
    }

    #[test]
    fn get_unfulfilled_redeem__returns_correct_clear_record() {
        // Given
        let mut csprng = StdRng::seed_from_u64(42);
        let repo = RedeemRepo::<Test>::new();
        let id = [0u8; 16];
        let expected = random_clear_redeem_record(&mut csprng);
        let genesis = GenesisConfig::<Test> {
            pending_redeems: vec![(id, Wrap(expected.clone()))],
            ..Default::default()
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            // When
            let actual = repo.get_unfulfilled_redeem_request(id);

            // Then
            assert_eq!(actual.unwrap(), expected);
        })
    }

    #[test]
    fn store_redeem__puts_clear_record_into_storage() {
        // Given
        let genesis = GenesisConfig::<Test>::default();
        let mut csprng = StdRng::seed_from_u64(42);
        let repo = RedeemRepo::<Test>::new();
        let id = [0u8; 16];
        let expected = random_clear_redeem_record(&mut csprng);
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            // When store_redeem is called in a context where there is no record with the given id
            assert!(PendingRedeems::<Test>::get(id).is_none());
            repo.store_redeem_request(id, expected.clone());

            // Then
            let actual = PendingRedeems::<Test>::get(id).unwrap();
            assert_eq!(actual, Wrap(expected));
        })
    }

    #[test]
    fn remove_redeem__removes_clear_record_from_storage() {
        // Given
        let mut csprng = StdRng::seed_from_u64(42);
        let repo = RedeemRepo::<Test>::new();
        let id = [0u8; 16];
        let expected = random_clear_redeem_record(&mut csprng);
        let genesis = GenesisConfig::<Test> {
            pending_redeems: vec![(id, Wrap(expected))],
            ..Default::default()
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            // When store_redeem is called in a context where a record with the given id exists
            assert!(PendingRedeems::<Test>::get(id).is_some());
            repo.remove_redeem_request(id);

            // Then
            assert!(PendingRedeems::<Test>::get(id).is_none());
        })
    }

    #[test]
    fn get_all_unfulfilled_redeems__returns_an_empty_iterator_when_no_records_exist() {
        // Given
        let repo = RedeemRepo::<Test>::new();
        let expected = vec![];
        new_test_externalities(GenesisConfig::<Test>::default(), Default::default()).execute_with(
            || {
                // When
                let actual: Vec<_> = repo.all_unfulfilled_redeem_requests().collect();

                // Then
                assert_eq!(actual, expected);
            },
        )
    }

    #[test]
    fn get_all_unfulfilled_redeems__returns_multiple_clear_redeem_records_when_they_exist() {
        // Given
        let mut csprng = StdRng::seed_from_u64(42);
        let repo = RedeemRepo::<Test>::new();
        let id_one = [1u8; 16];
        let id_two = [2u8; 16];
        let id_three = [3u8; 16];
        let rec_one = random_clear_redeem_record(&mut csprng);
        let rec_two = random_clear_redeem_record(&mut csprng);
        let rec_three = random_clear_redeem_record(&mut csprng);
        let expected = vec![(id_one, rec_one), (id_two, rec_two), (id_three, rec_three)];
        let pending_redeems = expected
            .clone()
            .into_iter()
            .map(|(x, y)| (x, Wrap(y)))
            .collect();
        let genesis = GenesisConfig::<Test> {
            pending_redeems,
            ..Default::default()
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            // When
            let actual: Vec<_> = repo.all_unfulfilled_redeem_requests().collect();

            // Then
            assert_eq!(actual, expected);
        })
    }

    #[test]
    fn get_all_unfulfilled_redeems__returns_both_clear_and_confidential_redeem_records_when_they_exist(
    ) {
        // Given
        let mut csprng = StdRng::seed_from_u64(42);
        let repo = RedeemRepo::<Test>::new();
        let id_one = [1u8; 16];
        let id_two = [2u8; 16];
        let id_three = [3u8; 16];
        let rec_one = random_clear_redeem_record(&mut csprng);
        let rec_two = random_confidential_redeem_record(&mut csprng);
        let rec_three = random_clear_redeem_record(&mut csprng);
        let expected = vec![(id_one, rec_one), (id_two, rec_two), (id_three, rec_three)];
        let pending_redeems = expected
            .clone()
            .into_iter()
            .map(|(x, y)| (x, Wrap(y)))
            .collect();
        let genesis = GenesisConfig::<Test> {
            pending_redeems,
            ..Default::default()
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            // When
            let actual: Vec<_> = repo.all_unfulfilled_redeem_requests().collect();

            // Then
            assert_eq!(actual, expected);
        })
    }
}
