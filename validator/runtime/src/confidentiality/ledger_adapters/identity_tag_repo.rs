use crate::confidentiality;
use serde_scale_wrap::Wrap;
use sp_std::marker::PhantomData;
use xand_ledger::{IdentityTag, IdentityTagRepository};

pub(crate) struct IdentityTagRepo<T> {
    pub(crate) _module_marker: PhantomData<T>,
}

impl<T: confidentiality::Config> IdentityTagRepository for IdentityTagRepo<T> {
    fn exists(&self, id: &IdentityTag) -> bool {
        confidentiality::IdentityTags::<T>::get(Wrap(*id))
    }

    fn store_id_tag(&self, id: IdentityTag) {
        confidentiality::IdentityTags::<T>::insert(Wrap(id), true)
    }
}

#[cfg(test)]
mod tests {
    use crate::confidentiality::ledger_adapters::identity_tag_repo::IdentityTagRepo;
    use crate::confidentiality::tests::{new_test_externalities, Test};
    use crate::confidentiality::GenesisConfig;
    use rand::{prelude::StdRng, SeedableRng};
    use serde_scale_wrap::Wrap;
    use sp_std::marker::PhantomData;
    use xand_ledger::{IdentityTag, IdentityTagRepository};

    #[test]
    fn cant_retrieve_nonexistent_id_tags() {
        let mut csprng = StdRng::seed_from_u64(42);
        let tag = IdentityTag::random(&mut csprng);
        let genesis = GenesisConfig::<Test>::default();
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let repo = IdentityTagRepo::<Test> {
                _module_marker: PhantomData,
            };
            assert!(!repo.exists(&tag));
        });
    }

    #[test]
    fn can_retrieve_id_tag() {
        let mut csprng = StdRng::seed_from_u64(42);
        let tag = IdentityTag::random(&mut csprng);

        let genesis = GenesisConfig::<Test> {
            identity_tags: vec![(Wrap(tag), true)],
            ..Default::default()
        };

        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let repo = IdentityTagRepo::<Test> {
                _module_marker: PhantomData,
            };
            assert!(repo.exists(&tag));
        });
    }

    #[test]
    fn can_store_id_tag() {
        let mut csprng = StdRng::seed_from_u64(42);
        let genesis = GenesisConfig::<Test>::default();
        let tag = IdentityTag::random(&mut csprng);

        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let repo = IdentityTagRepo::<Test> {
                _module_marker: PhantomData,
            };
            repo.store_id_tag(tag);
            assert!(repo.exists(&tag));
        });
    }
}
