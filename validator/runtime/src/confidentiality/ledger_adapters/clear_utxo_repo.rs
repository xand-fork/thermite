use crate::confidentiality;
use core::marker::PhantomData;
use serde_scale_wrap::Wrap;
use xand_ledger::{ClearTransactionOutput, ClearTransactionOutputRepository};

pub(crate) struct ClearUtxoRepo<T> {
    pub(crate) _module_marker: PhantomData<T>,
}

impl<T: confidentiality::Config> ClearTransactionOutputRepository for ClearUtxoRepo<T> {
    type Utxo = ClearTransactionOutput;

    /// Returns true if this TXO exists in storage
    fn contains(&self, output: &ClearTransactionOutput) -> bool {
        confidentiality::ClearUTXOs::<T>::contains_key(Wrap(*output))
    }
    /// Store a TXO into storage
    fn add(&self, output: ClearTransactionOutput) {
        confidentiality::ClearUTXOs::<T>::insert(Wrap(output), ())
    }

    fn remove(&self, output: Self::Utxo) -> bool {
        if self.contains(&output) {
            confidentiality::ClearUTXOs::<T>::remove(Wrap(output));
            true
        } else {
            false
        }
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod tests {
    use super::*;
    use crate::confidentiality::{
        tests::{new_test_externalities, Test},
        GenesisConfig,
    };
    use std::convert::TryFrom;
    use xand_ledger::esig_payload::ESigPayload;
    use xand_ledger::{ClearTransactionOutput, MonetaryValue, PublicKey};

    #[test]
    fn lookup__returns_false_when_utxo_is_not_in_storage() {
        let genesis = GenesisConfig::<Test>::default();
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            // Given
            let repo = ClearUtxoRepo::<Test> {
                _module_marker: PhantomData,
            };
            let public_key = PublicKey::default();
            let value = MonetaryValue::try_from(1u32).unwrap();
            let salt = 0;
            let esig_payload = ESigPayload::new_with_ueta();
            let some_clear_txo = ClearTransactionOutput::new(public_key, value, salt, esig_payload);

            // When
            let exists = repo.contains(&some_clear_txo);

            // Then
            assert!(!exists);
        });
    }

    #[test]
    fn lookup__returns_true_when_utxo_is_added_to_storage() {
        let genesis = GenesisConfig::<Test>::default();
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            // Given
            let repo = ClearUtxoRepo::<Test> {
                _module_marker: PhantomData,
            };
            let public_key = PublicKey::default();
            let value = MonetaryValue::try_from(1u32).unwrap();
            let salt = 0;
            let esig_payload = ESigPayload::new_with_ueta();
            let some_clear_txo = ClearTransactionOutput::new(public_key, value, salt, esig_payload);
            confidentiality::ClearUTXOs::<Test>::insert(Wrap(some_clear_txo), ());

            // When
            let exists = repo.contains(&some_clear_txo);

            // Then
            assert!(exists);
        });
    }

    #[test]
    fn lookup__nonempty_storage_returns_false_for_new_utxo() {
        let genesis = GenesisConfig::<Test>::default();
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            // Given
            let repo = ClearUtxoRepo::<Test> {
                _module_marker: PhantomData,
            };
            let public_key = PublicKey::default();
            let value = MonetaryValue::try_from(1u32).unwrap();
            let salt = 0;
            let esig_payload = ESigPayload::new_with_ueta();
            let old_utxo = ClearTransactionOutput::new(public_key, value, salt, esig_payload);
            confidentiality::ClearUTXOs::<Test>::insert(Wrap(old_utxo), ());
            let new_utxo = ClearTransactionOutput::new(public_key, value, salt + 1, esig_payload);

            // When
            let exists = repo.contains(&new_utxo);

            // Then
            assert!(!exists);
        });
    }

    #[test]
    fn add__successfully_adds_utxo_to_storage() {
        let genesis = GenesisConfig::<Test>::default();
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            // Given
            let repo = ClearUtxoRepo::<Test> {
                _module_marker: PhantomData,
            };
            let public_key = PublicKey::default();
            let value = MonetaryValue::try_from(1u32).unwrap();
            let salt = 0;
            let esig_payload = ESigPayload::new_with_ueta();
            let some_clear_txo = ClearTransactionOutput::new(public_key, value, salt, esig_payload);

            // When
            repo.add(some_clear_txo);

            // Then
            assert!(confidentiality::ClearUTXOs::<Test>::contains_key(Wrap(
                some_clear_txo
            )));
        });
    }

    #[test]
    fn remove__successfully_removes_utxo_from_storage() {
        let genesis = GenesisConfig::<Test>::default();
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            // Given
            let repo = ClearUtxoRepo::<Test> {
                _module_marker: PhantomData,
            };
            let public_key = PublicKey::default();
            let value = MonetaryValue::try_from(1u32).unwrap();
            let salt = 0;
            let esig_payload = ESigPayload::new_with_ueta();
            let some_clear_txo = ClearTransactionOutput::new(public_key, value, salt, esig_payload);
            confidentiality::ClearUTXOs::<Test>::insert(Wrap(some_clear_txo), ());

            // When
            let removed = repo.remove(some_clear_txo);

            // Then
            assert!(removed);
            assert!(!confidentiality::ClearUTXOs::<Test>::contains_key(Wrap(
                some_clear_txo
            )));
        });
    }

    #[test]
    fn remove__returns_false_when_no_txo_exists_to_remove() {
        let genesis = GenesisConfig::<Test>::default();
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            // Given
            let repo = ClearUtxoRepo::<Test> {
                _module_marker: PhantomData,
            };
            let public_key = PublicKey::default();
            let value = MonetaryValue::try_from(1u32).unwrap();
            let salt = 0;
            let esig_payload = ESigPayload::new_with_ueta();
            let some_clear_txo = ClearTransactionOutput::new(public_key, value, salt, esig_payload);

            // When
            let removed = repo.remove(some_clear_txo);

            // Then
            assert!(!removed);
        });
    }
}
