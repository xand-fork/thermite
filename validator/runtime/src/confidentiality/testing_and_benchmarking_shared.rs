use crate::confidentiality::test_constants::TRUST;
use crate::confidentiality::NewPublicKey;
use core::convert::TryFrom;
use rand::CryptoRng;
use serde_scale_wrap::Wrap;
use sp_core::sp_std::vec::Vec;
use xand_ledger::{
    CreateRequestTransaction, IdentityTag, KeyImage, PublicKey, RedeemRequestTransaction,
    SendClaimsTransaction, TestCreateRequestBuilder, TestRedeemRequestBuilder,
    TestSendClaimsBuilder, TransactionOutput, TxoStatus,
};

/// Create Request transaction with relevant context for setting up test genesis configuration
pub struct TestCreateTransaction {
    pub tx: CreateRequestTransaction,
    pub identity_set: Vec<(Wrap<IdentityTag>, bool)>,
}

/// Creates a randomly generated create transaction
pub(crate) fn create_random_create_transaction<R: rand::Rng + CryptoRng>(
    csprng: &mut R,
) -> TestCreateTransaction {
    let create_tx: CreateRequestTransaction = TestCreateRequestBuilder::default()
        .trust_public_key(NewPublicKey::try_from(TRUST.clone()).unwrap().into())
        .build(csprng)
        .create_request;
    let identity_tags = create_tx
        .core_transaction
        .identity_sources
        .iter()
        .map(|tag| (Wrap(*tag), true))
        .collect();
    TestCreateTransaction {
        tx: create_tx,
        identity_set: identity_tags,
    }
}

#[allow(clippy::type_complexity)]
/// Send transaction with relevant context for setting up test genesis configuration
pub struct TestSendTransaction {
    pub send: SendClaimsTransaction,
    pub key_images: Vec<(Wrap<KeyImage>, bool)>,
    pub input_identities: Vec<(Wrap<IdentityTag>, bool)>,
    pub input_txos: Vec<(Wrap<TransactionOutput>, (Wrap<TxoStatus>, u64))>,
}

/// Creates a randomly generated send transaction
pub(crate) fn create_random_send_transaction<R>(csprng: &mut R) -> TestSendTransaction
where
    R: rand::Rng + CryptoRng,
{
    let send = TestSendClaimsBuilder::default().build(csprng);

    let key_images = send
        .core_transaction
        .key_images
        .iter()
        .map(|image| (Wrap(*image), true))
        .collect();
    let identity_tags = send
        .core_transaction
        .input
        .iter()
        .map(|input| (Wrap(input.identity_input), true))
        .collect();
    let input_txos = send
        .core_transaction
        .input
        .iter()
        .flat_map(|input| input.txos.components.iter())
        .map(|txo| (Wrap(*txo), (Wrap(TxoStatus::Confirmed), 0)))
        .collect();

    TestSendTransaction {
        send,
        input_identities: identity_tags,
        key_images,
        input_txos,
    }
}

#[allow(clippy::type_complexity)]
pub struct TestRedeemTransaction<BlockNumber> {
    pub tx: RedeemRequestTransaction,
    pub identity_set: Vec<(Wrap<IdentityTag>, bool)>,
    pub input_txos: Vec<(Wrap<TransactionOutput>, (Wrap<TxoStatus>, BlockNumber))>,
    pub trust_pubkey: PublicKey,
}

pub(crate) fn create_random_redeem_transaction<R, BlockNumber>(
    csprng: &mut R,
    trust: PublicKey,
) -> TestRedeemTransaction<BlockNumber>
where
    R: rand::Rng + CryptoRng,
    BlockNumber: From<u32>,
{
    let built = TestRedeemRequestBuilder::default()
        .trust_public_key(trust)
        .build(csprng);
    let tx = built.redeem_request;
    let identity_set = tx
        .core_transaction
        .input
        .iter()
        .map(|set| (Wrap(set.identity_input), true))
        .collect();
    let input_txos = tx
        .core_transaction
        .input
        .iter()
        .flat_map(|input| input.txos.components.iter())
        .map(|txo| (Wrap(*txo), (Wrap(TxoStatus::Confirmed), (0_u32).into())))
        .collect();
    let trust_pubkey = built.trust_pub_key;
    TestRedeemTransaction {
        tx,
        identity_set,
        input_txos,
        trust_pubkey,
    }
}
