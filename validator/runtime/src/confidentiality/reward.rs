use super::{Call, Config as ConfidentialityTrait};
use frame_support::{traits::UnfilteredDispatchable, weights::Weight};
use parity_scale_codec::Encode;
use serde_scale_wrap::Wrap;
use sp_runtime::SaturatedConversion;
use xand_ledger::{
    curve25519_dalek::ristretto::CompressedRistretto, esig_payload::ESigPayload,
    ClearTransactionOutput, MonetaryValue, PublicKey, RewardTransaction,
};

#[derive(Clone, Copy)]
pub enum RewardError {
    InvalidPublicKey,
    DispatchError,
}

pub type RewardResult = Result<Option<Weight>, RewardError>;

pub trait Rewarder: frame_system::Config {
    /// This creates a clear-UTXO to reward a validator for mining a block.
    fn reward_validator_block(account_id: &Self::AccountId, amount: MonetaryValue) -> RewardResult;
}

impl<T: ConfidentialityTrait> Rewarder for T
where
    T::BlockNumber: Into<u64>,
{
    fn reward_validator_block(account_id: &T::AccountId, amount: MonetaryValue) -> RewardResult {
        let public_key =
            get_public_key_for_account(account_id).ok_or(RewardError::InvalidPublicKey)?;
        let salt = frame_system::Module::<T>::block_number().saturated_into::<u64>();
        let txo =
            ClearTransactionOutput::new(public_key, amount, salt, ESigPayload::new_with_ueta());
        let rtx = RewardTransaction::new(txo);
        let call = Call::<T>::reward(Wrap(rtx));

        call.dispatch_bypass_filter(frame_system::RawOrigin::Root.into())
            .map(|post_dispatch_info| post_dispatch_info.actual_weight)
            .map_err(|_| RewardError::DispatchError)
    }
}

fn get_public_key_for_account<T: Encode>(account_id: T) -> Option<PublicKey> {
    let bytes = account_id.encode();
    let compressed_ristretto = CompressedRistretto::from_slice(&bytes);
    compressed_ristretto.decompress().map(Into::into)
}
