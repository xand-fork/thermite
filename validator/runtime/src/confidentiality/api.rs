#![allow(clippy::too_many_arguments)]
// This is needed because the codegen from decl_runtime_apis! fails newer versions of clippy
#![allow(clippy::unnecessary_mut_passed)]

use serde_scale_wrap::Wrap;
use sp_api::decl_runtime_apis;
use sp_std::vec::Vec;
use xand_ledger::{ClearTransactionOutput, CreateRequestRecord, RedeemRequestRecord};
use xand_runtime_models::FiatReqCorrelationId;

decl_runtime_apis! {
    pub trait ConfidentialPendingApi {
        fn get_confidential_pending_creates() -> Vec<(FiatReqCorrelationId, Wrap<CreateRequestRecord>)>;
        fn get_confidential_pending_redeems() -> Vec<(FiatReqCorrelationId, Wrap<RedeemRequestRecord>)>;
        fn get_clear_utxos() -> Vec<Wrap<ClearTransactionOutput>>;
        fn get_create_expire_time() -> u64;
    }
}
