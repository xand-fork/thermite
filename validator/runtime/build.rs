// Copyright 2019 Parity Technologies (UK) Ltd.
// This file is part of Substrate.

// Substrate is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Substrate is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Substrate.  If not, see <http://www.gnu.org/licenses/>.

#![forbid(unsafe_code)]

use std::{env, fs, process::Command};
use substrate_wasm_builder::WasmBuilder;

fn main() {
    let ci_env_path = concat!(env!("CARGO_MANIFEST_DIR"), "/.env");
    let line_prefix = "NIGHTLY_RUST_VERSION=";
    let nightly_version = fs::read_to_string(ci_env_path)
        .unwrap_or_else(|e| {
            panic!(
                "Failed to read {} to get nightly rust version: {}",
                ci_env_path, e
            );
        })
        .lines()
        .find_map(|line| line.strip_prefix(line_prefix).map(|s| s.to_string()))
        .unwrap_or_else(|| {
            panic!(
                "Expected to find a line starting with {} in {}",
                line_prefix, ci_env_path
            );
        });
    let nightly_version = format!("nightly-{}", nightly_version);
    ensure_pinned_rust_nightly_is_installed(&nightly_version);
    println!(
        "Running substrate wasm builder with rust {}",
        nightly_version
    );
    env::set_var("WASM_BUILD_TOOLCHAIN", nightly_version);
    WasmBuilder::new()
        .with_current_project()
        .export_heap_base()
        .import_memory()
        .build()
}

fn ensure_pinned_rust_nightly_is_installed(nightly_version: &str) {
    let out = Command::new("rustup")
        .args([
            "target",
            "list",
            "--installed",
            "--toolchain",
            nightly_version,
        ])
        .output()
        .unwrap_or_else(|e| panic!("{} Failed to run rustup: {}", ERROR_MARKER, e));
    if !out.status.success() {
        panic!("{}", missing_pinned_rust_nightly(nightly_version));
    }
    let out = String::from_utf8(out.stdout)
        .unwrap_or_else(|e| panic!("{} Invalid UTF-8 in rustup output: {}", ERROR_MARKER, e));
    let found_target = out.lines().any(|line| line == "wasm32-unknown-unknown");
    if !found_target {
        panic!("{}", missing_wasm_target(nightly_version));
    }
}

fn missing_pinned_rust_nightly(nightly_version: &str) -> String {
    format!(
        "\n\n{} Rust {} is needed.\nPlease run the following:\n{}\n{}\n\n",
        ERROR_MARKER,
        nightly_version,
        rust_installation_instructions(nightly_version),
        wasm_installation_instructions(nightly_version),
    )
}

fn missing_wasm_target(nightly_version: &str) -> String {
    format!(
        "\n\n{} Missing wasm target.\nPlease run the following:\n{}\n\n",
        ERROR_MARKER,
        wasm_installation_instructions(nightly_version),
    )
}

fn rust_installation_instructions(nightly_version: &str) -> String {
    format!(
        "{} rustup toolchain install {}",
        COMMAND_MARKER, nightly_version,
    )
}

fn wasm_installation_instructions(nightly_version: &str) -> String {
    format!(
        "{} rustup target add --toolchain {} wasm32-unknown-unknown",
        COMMAND_MARKER, nightly_version,
    )
}

const ERROR_MARKER: &str = "❌";
const COMMAND_MARKER: &str = "❱";
