use crate::{hash_substrate_encodeable, ExtrinsicHash, SubstrateClientErrors};
use std::collections::HashMap;
use xandstrate_runtime::{fixup_events, Event, EventRecord, Phase, SignedBlock};

/// Number type for extrinsic index within a block
type ExtrinsicIndex = u32;

/// Given a block and related information, [BlockView] transforms and merges the data
/// together to facilitate querying for information of interest
pub struct BlockView {
    /// Mapping of the extrinsic hash to its index in the block
    ext_hash_to_idx: HashMap<ExtrinsicHash, ExtrinsicIndex>,
    /// Mapping of extrinsic index to any Events that were associated with it in the block
    apply_ext_events: HashMap<ExtrinsicIndex, Vec<Event>>,
}

impl BlockView {
    /// Transforms and merges data to build a [BlockView]
    pub fn load(block: SignedBlock, events: Vec<EventRecord>) -> Self {
        // Build map of extrinsic hash to its index in the block
        let ext_hash_to_idx: HashMap<ExtrinsicHash, _> = block
            .block
            .extrinsics
            .iter()
            .map(hash_substrate_encodeable)
            .zip(0..) // Attaches index to each extrinsic hash
            .collect();

        // Filter events for the ApplyExtrinsic phase and System events and get the extrinsicIndex they reference.
        let mut apply_ext_events = HashMap::new();
        for event in events.into_iter() {
            match event.phase {
                Phase::ApplyExtrinsic(idx) => match event.event {
                    e @ Event::pallet_sudo(_) | e @ Event::system(_) => {
                        apply_ext_events.entry(idx).or_insert_with(Vec::new).push(e);
                    }
                    _ => (),
                },
                Phase::Finalization | Phase::Initialization => (),
            };
        }

        BlockView {
            ext_hash_to_idx,
            apply_ext_events,
        }
    }

    /// Returns all events (associated with extrinsic application, but not others) in the block
    pub fn all_events(&self) -> impl Iterator<Item = &Event> {
        self.apply_ext_events.values().flatten()
    }

    /// Returns the events for the extrinsic, or an Error if extrinsic is not found.
    pub fn get_events_for_ext(
        &self,
        ext_hash: &ExtrinsicHash,
    ) -> Result<Vec<Event>, SubstrateClientErrors> {
        // Get index for given hash
        let ext_idx = match self.ext_hash_to_idx.get(ext_hash) {
            Some(i) => i,
            None => {
                return Err(SubstrateClientErrors::ExtrinsicNotInBlock { hash: *ext_hash });
            }
        };

        // Get system event for that index
        match self.apply_ext_events.get(ext_idx).cloned() {
            Some(mut events) => {
                fixup_events(events.iter_mut());
                Ok(events)
            }
            None => {
                Err(SubstrateClientErrors::SystemEventNotFoundForExtrinsicIndex { index: *ext_idx })
            }
        }
    }
}
