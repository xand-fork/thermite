//! Code related to the handling of substrate's `Event`s

use crate::{BlockHash, CommittedExtrinsic, SubstrateClientErrors};
use xandstrate_runtime::Event;

#[async_trait::async_trait]
pub trait SubstrateEventProvider {
    /// Given a finalized extrinsic, retrieves the corresponding SystemEvent during the
    /// ApplyExtrinsic phase for that extrinsic. The event should indicate successful or failed
    /// application of the extrinsic.
    async fn get_events_for(
        &self,
        finalized_ext: CommittedExtrinsic,
    ) -> Result<Vec<Event>, SubstrateClientErrors>;

    /// Fetch all the events for the given block
    async fn get_events_for_block(
        &self,
        finalized_ext: &BlockHash,
    ) -> Result<Vec<Event>, SubstrateClientErrors>;

    /// Given a finalized extrinsic, checks the events in the block that extrinsic was finalized in
    /// to confirm that it was finalized successfully
    async fn is_extrinsic_success(
        &self,
        finalized_ext: CommittedExtrinsic,
    ) -> Result<bool, SubstrateClientErrors>;
}
