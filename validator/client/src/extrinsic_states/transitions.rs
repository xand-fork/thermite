use crate::{
    extrinsic_states::{CommittedWithSystemEvent, ErroredExtrinsic, InvalidExtrinsic},
    BlockHash, CommittedExtrinsic, SubmittedExtrinsic,
};
use xandstrate_runtime::{Runtime, SystemEvent};

/// Submitted -> Finalized
impl From<(&SubmittedExtrinsic, BlockHash)> for CommittedExtrinsic {
    fn from((sub_ext, block_hash): (&SubmittedExtrinsic, BlockHash)) -> Self {
        CommittedExtrinsic {
            hash: sub_ext.hash,
            committed_in_block: block_hash,
        }
    }
}

/// Submitted -> Errored
impl From<&SubmittedExtrinsic> for ErroredExtrinsic {
    fn from(sub_ext: &SubmittedExtrinsic) -> Self {
        ErroredExtrinsic { hash: sub_ext.hash }
    }
}

/// Submitted -> Invalid
impl From<&SubmittedExtrinsic> for InvalidExtrinsic {
    fn from(sub_ext: &SubmittedExtrinsic) -> Self {
        InvalidExtrinsic { hash: sub_ext.hash }
    }
}

/// Finalized -> FinalizedWithSytemEvent
impl From<(CommittedExtrinsic, SystemEvent<Runtime>)> for CommittedWithSystemEvent {
    fn from((ext, sys_event): (CommittedExtrinsic, SystemEvent<Runtime>)) -> Self {
        CommittedWithSystemEvent { ext, sys_event }
    }
}
