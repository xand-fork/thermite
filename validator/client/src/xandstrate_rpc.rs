use jsonrpc_core::Result;
use jsonrpc_derive::rpc;
use xand_address::Address;
use xand_models::EncryptionKey;
use xand_runtime_models::CidrBlockArray;
use xandstrate_runtime::{
    network_voting::PropIndex,
    validator_emissions::models::{
        validator_emission_progress::ValidatorEmissionProgress,
        validator_emission_rate::ValidatorEmissionRate,
    },
    xandstrate::xandstrate_api::ProposalData,
    AccountId,
};

pub use gen_client::Client as XandstrateClient;

#[rpc]
pub trait XandstrateRpcApi {
    /// Get the complete list of allowlisted CIDR blocks
    #[rpc(name = "xandstrate_allowlistCidrBlock", alias("allowlist_cidrBlock"))]
    fn allowlist(&self) -> Result<Vec<(Address, CidrBlockArray)>>;
    /// Get a information on a specific non-expired Proposal
    #[rpc(name = "xandstrate_getProposal")]
    fn get_proposal(&self, id: PropIndex) -> Result<Option<ProposalData<Address, u32>>>;
    /// Get list of all non-expired Proposals
    #[rpc(name = "xandstrate_getAllProposals")]
    fn get_all_proposals(&self) -> Result<Vec<ProposalData<Address, u32>>>;
    /// Get Trust Public Address
    #[rpc(name = "xandstrate_getTrust")]
    fn get_trustee(&self) -> Result<Address>;
    #[rpc(name = "xandstrate_getLimitedAgent")]
    fn get_limited_agent(&self) -> Result<Option<Address>>;
    /// Get list of members
    #[rpc(name = "xandstrate_getMembers")]
    fn get_members(&self) -> Result<Vec<Address>>;
    /// Get list of banned members as of latest block
    #[rpc(name = "xandstrate_getBannedMembers")]
    fn get_banned_members(&self) -> Result<Vec<Address>>;
    /// Get list of validators
    #[rpc(name = "xandstrate_getAuthorityKeys")]
    fn get_authority_keys(&self) -> Result<Vec<Address>>;
    /// Get encryption key for an account
    #[rpc(name = "xandstrate_getEncryptionKey")]
    fn get_encryption_key(&self, acct: AccountId) -> Result<Option<EncryptionKey>>;
    /// Get total issuance
    #[rpc(name = "xandstrate_getTotalIssuance")]
    fn get_total_issuance(&self) -> Result<u64>;
    /// Get the current emission rate for validators
    #[rpc(
        name = "xandstrate_getValidatorEmissionRate",
        alias("get_validator_emission_rate")
    )]
    fn get_validator_emission_rate(&self) -> Result<ValidatorEmissionRate>;
    /// Get the current emission progress for validators
    #[rpc(
        name = "xandstrate_getValidatorEmissionProgress",
        alias("get_validator_emission_progress")
    )]
    fn get_validator_emission_progress(
        &self,
        acct: AccountId,
    ) -> Result<Option<ValidatorEmissionProgress>>;
}
