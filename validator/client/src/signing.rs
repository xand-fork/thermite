use crate::{sr25519::Public, KeyMgmtErr, SubstrateClientErrors};
use derive_more::Constructor;
use snafu::ResultExt;
use sp_core::crypto::Ss58Codec;
use std::sync::Arc;
use tpfs_krypt::{KeyIdentifier, KeyManagement, KeyType};
use xandstrate_runtime::extrinsic_builder::ExtrinsicSigner;

/// Wrapper type to implement lower-level signing trait for tpfs-krypt key manager
#[derive(Constructor)]
pub struct KeyMgrSigner(pub Arc<dyn KeyManagement>);
impl ExtrinsicSigner for KeyMgrSigner {
    type Err = SubstrateClientErrors;

    fn sign(&self, signer: &Public, data: &[u8]) -> Result<Vec<u8>, SubstrateClientErrors> {
        let key_id = KeyIdentifier {
            value: signer.to_ss58check(),
            key_type: KeyType::SubstrateSr25519,
        };
        self.0
            .sign(&key_id, data)
            .map(|x| x.as_ref().as_ref().to_vec())
            .context(KeyMgmtErr {
                msg: "While signing extrinsic",
            })
    }
}
