<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Xandstrate Client](#xandstrate-client)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Xandstrate Client

The xandstrate client is our method of talking directly to substrate via it's
websocket-based RPC api. Unfortunately, this API (despite being the primary
way of interacting with substrate) isn't very well documented outside of
the JS Client which is semi Polkadot specific.

The docs for that client, which also include docs about the default APIs
exposed by some substrate pallets is located [here](https://polkadot.js.org/api/substrate/rpc.html).
As far as I am aware, this is the only non-code documentation of the RPC APIs.
We also define some custom RPC methods that are obviously not documented there,
refer to the client code for what's available.

For more details, see the rustdocs.
