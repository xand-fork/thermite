use std::fmt;

use futures::Future;

use crate::error::Error;

// Given two futures meant to run indefinitely:
//
// 1. create a tokio runtime
// 2. create an OS thread to isolate the runtime
// 3. spawn the two tasks in the runtime
// 4. return output value from the first task to panic or complete
//
// The intention is to make the process exit when either service,
// which is meant to run indefinitely, panics or completes.
// Production kubernetes monitoring should handle starting the
// process again after it exits.
//
// This can be adapted to support more tasks, but is currently
// hardcoded to support two. The use of `tokio::select!` makes
// it difficult to support an arbitrary number of tasks.
pub(crate) fn run_service_tasks<F1, F2, Out1, Out2>(
    future_1: F1,
    future_2: F2,
) -> Result<Either<Out1, Out2>, Error>
where
    F1: Future<Output = Out1> + Send + 'static,
    F2: Future<Output = Out2> + Send + 'static,
    Out1: std::marker::Send + fmt::Debug + 'static,
    Out2: std::marker::Send + fmt::Debug + 'static,
{
    let tokio_runtime = tokio::runtime::Runtime::new()?;

    let either = std::thread::Builder::new()
        .name("tokio-parent".into())
        .spawn(move || {
            let handle_1 = tokio_runtime.spawn(future_1);
            let handle_2 = tokio_runtime.spawn(future_2);
            tokio_runtime.block_on(async move {
                tokio::select! {
                    output_1 = handle_1 => {
                        Either::Left(output_1)
                    },
                    output_2 = handle_2 => {
                        Either::Right(output_2)
                    },
                }
            })
        })?
        .join()
        .map_err(|e| {
            Error::Unknown(format!(
                "Must be able to get a response from tokio-parent thread: {:?}",
                e
            ))
        })?;

    match either {
        Either::Left(val_1) => Ok(val_1.map(Either::Left)?),
        Either::Right(val_2) => Ok(val_2.map(Either::Right)?),
    }
}

// Models a value of one of two generic types. Useful when the type
// system forces you to use one type, but you'd like to use two.
#[derive(Debug, Eq, PartialEq)]
pub enum Either<L, R> {
    Left(L),
    Right(R),
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use super::*;

    #[test]
    fn panic_in_task_a_and_not_task_b_yields_error_from_parent_task() {
        // Given a task A that will panic on startup, and a task B that runs for a longer time
        let task_a = task_panics();
        let task_b = long_task_loops();

        // When the functions run in the existing runtime
        let res = run_service_tasks(task_a, task_b);

        // Then the process exits with an Error
        assert!(matches!(res, Err(Error::TaskPanicked(_))));
    }

    #[test]
    fn premature_completion_in_task_a_yields_shutdown_of_task_b_and_exit_from_parent_task() {
        // Given a task A that completes immediately, and a task B that runs for a longer time
        let task_a = task_completes();
        let task_b = long_task_loops();

        // When the functions run in the existing runtime
        let output = run_service_tasks(task_a, task_b).unwrap();

        // Then the process exits with an Ok and both tasks are cleaned up
        assert_eq!(output, Either::Left(()));
    }

    #[test]
    fn panic_in_task_b_and_not_task_a_yields_error_from_parent_task() {
        // Given a task B that will panic on startup, and a task A that runs for a longer time
        let task_a = long_task_loops();
        let task_b = task_panics();

        // When the functions run in the existing runtime
        let res = run_service_tasks(task_a, task_b);

        // Then the process exits with an Error
        assert!(matches!(res, Err(Error::TaskPanicked(_))));
    }

    #[test]
    fn premature_completion_in_task_b_yields_shutdown_of_task_a_and_exit_from_parent_task() {
        // Given a task B that completes immediately, and a task A that runs for a longer time
        let task_a = long_task_loops();
        let task_b = task_completes();

        // When the functions run in the existing runtime
        let output = run_service_tasks(task_a, task_b).unwrap();

        // Then the process exits with an Ok and both tasks are cleaned up
        assert_eq!(output, Either::Right(()));
    }

    async fn task_completes() {
        println!("Task starting");
    }

    async fn task_panics() {
        loop {
            panic!("Task panicked");
        }
    }

    async fn long_task_loops() {
        tokio::time::sleep(Duration::from_millis(1000)).await;
        println!("Long Task starting");
        for _ in 0..15 {
            println!("Long Task running");
        }
        println!("Long Task finished");
    }
}
