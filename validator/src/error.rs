use std::any::Any;
use std::fmt;
use thiserror::Error;

#[allow(clippy::large_enum_variant)]
#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    Io(#[from] std::io::Error),
    #[error(transparent)]
    SubstrateCli(#[from] sc_cli::Error),
    TaskPanicked(Box<dyn Any + Send + 'static>),
    TaskCancelled(std::io::Error),
    CompiledWithoutBenchmarking,
    Unknown(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::CompiledWithoutBenchmarking => write!(
                f,
                "Benchmarking wasn't enabled when building the node. \
                You can enable it with `--features runtime-benchmarks`."
            ),
            _ => write!(f, "{:?}", self),
        }
    }
}

impl From<sc_service::Error> for Error {
    fn from(e: sc_service::Error) -> Self {
        sc_cli::Error::from(e).into()
    }
}

impl From<tokio::task::JoinError> for Error {
    fn from(e: tokio::task::JoinError) -> Self {
        if e.is_panic() {
            Self::TaskPanicked(e.into_panic())
        } else if e.is_cancelled() {
            Self::TaskCancelled(e.into())
        } else {
            Self::Io(e.into())
        }
    }
}
