//! A simple binary to be run just after a new deployment. It can handle some of the more
//! complex things needed during validator setup, like initializing the keystore properly

#![forbid(unsafe_code)]

use sc_keystore::LocalKeystore as Keystore;
use sp_core::{ed25519, sr25519, Pair};
use sp_keystore::SyncCryptoStore;
use std::io::{Error as IOError, ErrorKind};
use std::{
    fs::{read_dir, File},
    io::Read,
    path::{Path, PathBuf},
};
use structopt::StructOpt;
use xand_models::ToAddress;

#[derive(Clone, StructOpt, Debug)]
struct Args {
    /// Path to a directory containing keys that will be placed into the substrate keystore.
    /// There must be one key that starts with "aura" and one key that starts with "grandpa".
    /// The contents of the key files must be the secret phrase for the key.
    key_file_directory: PathBuf,
    /// Path to a substrate keystore that we will populate with the provided session keys.
    /// It should in turn be used as the `--keystore-path` argument to the validator.
    substrate_keystore: PathBuf,
}

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let args = Args::from_args();
    println!("Provided args: {:?}", &args);

    put_keys_in_keystore(&args)?;

    Ok(())
}

/// Puts the keys we expect each validator to have into the substrate keystore. We do this because
/// the validator expects to have this keystore directory populated with keys following
/// a very specific naming scheme, one that is liable to change. By using the Keystore
/// implementation from substrate, we guarantee that the keys will appear as required.
fn put_keys_in_keystore(args: &Args) -> Result<()> {
    // Find the keys first
    let (aura_path, grandpa_path) = find_relevant_keys(&args.key_file_directory)?;
    // Load session keys and then store them into the keystore.
    let aura_k = load_file_content(&aura_path)?;
    let grandpa_k = load_file_content(&grandpa_path)?;
    let keystore = Keystore::open(&args.substrate_keystore, None)?;
    let key_pair = sr25519::Pair::from_string(aura_k.as_str(), None)
        .map_err(|e| Box::new(IOError::new(ErrorKind::Other, format!("{:?}", e))))?;
    keystore
        .insert_unknown(
            sp_core::crypto::key_types::AURA,
            aura_k.as_str(),
            key_pair.public().as_ref(),
        )
        .map_err(|_| {
            Box::new(IOError::new(
                ErrorKind::Other,
                format!("While saving key: {:?}", key_pair.public().to_address()),
            ))
        })?;

    let key_pair = ed25519::Pair::from_string(grandpa_k.as_str(), None)
        .map_err(|e| Box::new(IOError::new(ErrorKind::Other, format!("{:?}", e))))?;
    keystore
        .insert_unknown(
            sp_core::crypto::key_types::GRANDPA,
            grandpa_k.as_str(),
            key_pair.public().as_ref(),
        )
        .map_err(|_| {
            Box::new(IOError::new(
                ErrorKind::Other,
                format!("While saving key: {:?}", key_pair.public().to_address()),
            ))
        })?;

    Ok(())
}

/// Find the aura and grandpa keys in the key files directory
/// Returns (aura key path, grandpa key path)
fn find_relevant_keys(in_me: &Path) -> Result<(PathBuf, PathBuf)> {
    let mut aura_key = None;
    let mut grandpa_key = None;
    for entry in read_dir(in_me)? {
        let entry = entry?;
        let path = entry.path();
        if path.is_file() {
            println!("File in keys directory: {}", path.as_path().display());
            if path
                .file_name()
                .unwrap()
                .to_string_lossy()
                .starts_with("validator-session-produce-blocks")
            {
                aura_key = Some(path.to_path_buf())
            }
            if path
                .file_name()
                .unwrap()
                .to_string_lossy()
                .starts_with("validator-session-finalize-blocks")
            {
                grandpa_key = Some(path.to_path_buf())
            }
        }
    }
    if aura_key.is_none() {
        return Err(
            "AURA key not found (must be a key file starting with `validator-session-aura`)".into(),
        );
    }
    if grandpa_key.is_none() {
        return Err(
            "Grandpa key not found (must be a key file starting with `validator-session-grandpa`)"
                .into(),
        );
    }
    Ok((aura_key.unwrap(), grandpa_key.unwrap()))
}

fn load_file_content(path: &Path) -> Result<String> {
    let mut file = File::open(path)?;
    let mut content = String::new();
    file.read_to_string(&mut content)?;
    Ok(content.trim().to_string())
}
