//! Service and ServiceFactory implementation. Specialized wrapper over Substrate service.
//!
//! Most of the code was lifted from this version Substrate's runtime and service definitions:
//! https://github.com/paritytech/substrate/blob/master/node/cli/src/service.rs
mod runner;

use crate::error::Error;
use petri::petri_persisted_state::PetriPersistedState;
use petri::snapshot::file_snapshotter::{
    FileManagerImpl, FileReaderImpl, FileSnapshotter, FileWriterImpl, SchedulerImpl,
};
use petri::snapshot::Snapshotter;
use petri::{IdTagCache, Petri, TransactionCache, TxoCache};
use rand::rngs::OsRng;
use runner::{run_service_tasks, Either};
use sc_client_api::{ExecutorProvider, RemoteBackend};
use sc_consensus_slots::BackoffAuthoringOnFinalizedHeadLagging;
use sc_executor::native_executor_instance;
pub use sc_executor::NativeExecutor;
use sc_finality_grandpa::SharedVoterState;
use sc_service::{error::Error as ServiceError, Configuration, TaskManager};
use sp_consensus_aura::sr25519::AuthorityPair as AuraPair;
use sp_inherents::InherentDataProviders;
use std::path::PathBuf;
use std::sync::Arc;
use std::time::Duration;
use url::Url;
use xand_api::XandApiConfig;
use xand_financial_client_adapter::financial_client_adapter::{
    new_redeem_mgr, AdapterExtDecoderConfig,
};
use xand_financial_client_adapter::krypt_adapter::{
    create_and_initialize_key_manager, KryptKeyStore,
};
use xand_financial_client_adapter::{new_create_mgr, new_send_mgr};
use xand_models::ExtrinsicDecoderImpl;
use xandstrate_client::SubstrateClient;
use xandstrate_runtime::{
    self, confidentiality::runtime_interface::crypto, opaque::Block, RuntimeApi,
};

// Our native executor instance.
native_executor_instance!(
    pub Executor,
    xandstrate_runtime::api::dispatch,
    xandstrate_runtime::native_version,
    (frame_benchmarking::benchmarking::HostFunctions, crypto::HostFunctions),
);

pub type FullClient = sc_service::TFullClient<Block, RuntimeApi, Executor>;
type FullBackend = sc_service::TFullBackend<Block>;
type FullSelectChain = sc_consensus::LongestChain<FullBackend, Block>;

type NewPartial = sc_service::PartialComponents<
    FullClient,
    FullBackend,
    FullSelectChain,
    sp_consensus::DefaultImportQueue<Block, FullClient>,
    sc_transaction_pool::FullPool<Block, FullClient>,
    (
        sc_consensus_aura::AuraBlockImport<
            Block,
            FullClient,
            sc_finality_grandpa::GrandpaBlockImport<
                FullBackend,
                Block,
                FullClient,
                FullSelectChain,
            >,
            AuraPair,
        >,
        sc_finality_grandpa::LinkHalf<Block, FullClient, FullSelectChain>,
    ),
>;

/// Starts a `ServiceBuilder` for a full service.
///
/// Use this function if you don't actually need the full service, but just the builder in order to
/// be able to perform chain operations.
pub fn new_partial(config: &Configuration) -> Result<NewPartial, ServiceError> {
    let inherent_data_providers = sp_inherents::InherentDataProviders::new();

    let (client, backend, keystore, task_manager) =
        sc_service::new_full_parts::<Block, RuntimeApi, Executor>(config)?;
    let client = Arc::new(client);

    let select_chain = sc_consensus::LongestChain::new(backend.clone());

    let transaction_pool = sc_transaction_pool::BasicPool::new_full(
        config.transaction_pool.clone(),
        config.role.is_authority().into(),
        config.prometheus_registry(),
        task_manager.spawn_handle(),
        client.clone(),
    );

    let (grandpa_block_import, grandpa_link) = sc_finality_grandpa::block_import(
        client.clone(),
        &(client.clone() as Arc<_>),
        select_chain.clone(),
    )?;

    let aura_block_import = sc_consensus_aura::AuraBlockImport::<_, _, _, AuraPair>::new(
        grandpa_block_import.clone(),
        client.clone(),
    );

    let import_queue = sc_consensus_aura::import_queue::<_, _, _, AuraPair, _, _>(
        sc_consensus_aura::slot_duration(&*client)?,
        aura_block_import.clone(),
        Some(Box::new(grandpa_block_import)),
        client.clone(),
        inherent_data_providers.clone(),
        &task_manager.spawn_handle(),
        config.prometheus_registry(),
        sp_consensus::CanAuthorWithNativeVersion::new(client.executor().clone()),
    )?;

    Ok(sc_service::PartialComponents {
        client,
        backend,
        task_manager,
        import_queue,
        keystore_container: keystore,
        select_chain,
        transaction_pool,
        inherent_data_providers,
        other: (aura_block_import, grandpa_link),
    })
}

/// Creates a full service from the configuration.
/// Builds a new service for a full client.
pub fn new_full(mut config: Configuration) -> Result<(TaskManager, Arc<FullClient>), ServiceError> {
    let sc_service::PartialComponents {
        client,
        backend,
        mut task_manager,
        import_queue,
        keystore_container: keystore,
        select_chain,
        transaction_pool,
        inherent_data_providers,
        other: (block_import, grandpa_link),
    } = new_partial(&config)?;

    config
        .network
        .extra_sets
        .push(sc_finality_grandpa::grandpa_peers_set_config());

    let (network, network_status_sinks, system_rpc_tx, network_starter) =
        sc_service::build_network(sc_service::BuildNetworkParams {
            config: &config,
            client: client.clone(),
            transaction_pool: transaction_pool.clone(),
            spawn_handle: task_manager.spawn_handle(),
            import_queue,
            on_demand: None,
            block_announce_validator_builder: None,
        })?;

    if config.offchain_worker.enabled {
        sc_service::build_offchain_workers(
            &config,
            backend.clone(),
            task_manager.spawn_handle(),
            client.clone(),
            network.clone(),
        );
    }

    let role = config.role.clone();
    let force_authoring = config.force_authoring;
    let name = config.network.node_name.clone();
    let enable_grandpa = !config.disable_grandpa;
    let prometheus_registry = config.prometheus_registry().cloned();

    let rpc_extensions_builder = {
        let client = client.clone();
        let pool = transaction_pool.clone();

        Box::new(move |deny_unsafe, _| {
            let deps = crate::rpc::FullDeps {
                client: client.clone(),
                pool: pool.clone(),
                deny_unsafe,
            };

            crate::rpc::create_full(deps)
        })
    };

    let (_, telemetry_connection_sinks) = sc_service::spawn_tasks(sc_service::SpawnTasksParams {
        network: network.clone(),
        client: client.clone(),
        keystore: keystore.sync_keystore(),
        task_manager: &mut task_manager,
        transaction_pool: transaction_pool.clone(),
        rpc_extensions_builder,
        on_demand: None,
        remote_blockchain: None,
        backend,
        network_status_sinks,
        system_rpc_tx,
        config,
    })?;

    if role.is_authority() {
        let proposer = sc_basic_authorship::ProposerFactory::new(
            task_manager.spawn_handle(),
            client.clone(),
            transaction_pool,
            prometheus_registry.as_ref(),
        );

        let can_author_with =
            sp_consensus::CanAuthorWithNativeVersion::new(client.executor().clone());

        let aura = sc_consensus_aura::start_aura::<_, _, _, _, _, AuraPair, _, _, _, _>(
            sc_consensus_aura::slot_duration(&*client)?,
            client.clone(),
            select_chain,
            block_import,
            proposer,
            network.clone(),
            inherent_data_providers,
            force_authoring,
            Some(BackoffAuthoringOnFinalizedHeadLagging::default()), // TODO: Investigate if this is correct: https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6843
            keystore.sync_keystore(),
            can_author_with,
        )?;

        // the AURA authoring task is considered essential, i.e. if it
        // fails we take down the service with it.
        task_manager
            .spawn_essential_handle()
            .spawn_blocking("aura", aura);
    }

    // if the node isn't actively participating in consensus then it doesn't
    // need a keystore, regardless of which protocol we use below.
    let keystore = if role.is_authority() {
        Some(keystore)
    } else {
        None
    };

    let grandpa_config = sc_finality_grandpa::Config {
        // FIXME #1578 make this available through chainspec
        gossip_duration: Duration::from_millis(333),
        justification_period: 512,
        name: Some(name),
        observer_enabled: false,
        keystore: keystore.map(|s| s.sync_keystore()),
        is_authority: role.is_network_authority(),
    };

    if enable_grandpa {
        // start the full GRANDPA voter
        // NOTE: non-authorities could run the GRANDPA observer protocol, but at
        // this point the full voter should provide better guarantees of block
        // and vote data availability than the observer. The observer has not
        // been tested extensively yet and having most nodes in a network run it
        // could lead to finality stalls.
        let grandpa_config = sc_finality_grandpa::GrandpaParams {
            config: grandpa_config,
            link: grandpa_link,
            network,
            telemetry_on_connect: telemetry_connection_sinks.map(|s| s.on_connect_stream()),
            voting_rule: sc_finality_grandpa::VotingRulesBuilder::default().build(),
            prometheus_registry,
            shared_voter_state: SharedVoterState::empty(),
        };

        // the GRANDPA voter task is considered infallible, i.e.
        // if it fails we take down the service with it.
        task_manager.spawn_essential_handle().spawn_blocking(
            "grandpa-voter",
            sc_finality_grandpa::run_grandpa_voter(grandpa_config)?,
        );
    }

    network_starter.start_network();
    Ok((task_manager, client))
}

/// Builds a new service for a light client.
pub fn new_light(config: Configuration) -> Result<TaskManager, ServiceError> {
    let (client, backend, keystore, mut task_manager, on_demand) =
        sc_service::new_light_parts::<Block, RuntimeApi, Executor>(&config)?;

    let transaction_pool = Arc::new(sc_transaction_pool::BasicPool::new_light(
        config.transaction_pool.clone(),
        config.prometheus_registry(),
        task_manager.spawn_handle(),
        client.clone(),
        on_demand.clone(),
    ));
    let select_chain = sc_consensus::LongestChain::new(backend.clone());
    let (grandpa_block_import, _) = sc_finality_grandpa::block_import(
        client.clone(),
        &(client.clone() as Arc<_>),
        select_chain,
    )?;

    let import_queue = sc_consensus_aura::import_queue::<_, _, _, AuraPair, _, _>(
        sc_consensus_aura::slot_duration(&*client)?,
        grandpa_block_import,
        None,
        client.clone(),
        InherentDataProviders::new(),
        &task_manager.spawn_handle(),
        config.prometheus_registry(),
        sp_consensus::NeverCanAuthor,
    )?;

    let (network, network_status_sinks, system_rpc_tx, network_starter) =
        sc_service::build_network(sc_service::BuildNetworkParams {
            config: &config,
            client: client.clone(),
            transaction_pool: transaction_pool.clone(),
            spawn_handle: task_manager.spawn_handle(),
            import_queue,
            on_demand: Some(on_demand.clone()),
            block_announce_validator_builder: None,
        })?;

    if config.offchain_worker.enabled {
        sc_service::build_offchain_workers(
            &config,
            backend.clone(),
            task_manager.spawn_handle(),
            client.clone(),
            network.clone(),
        );
    }

    let light_deps = crate::rpc::LightDeps {
        remote_blockchain: backend.remote_blockchain(),
        fetcher: on_demand.clone(),
        client: client.clone(),
        pool: transaction_pool.clone(),
    };

    let rpc_extensions = crate::rpc::create_light(light_deps);

    sc_service::spawn_tasks(sc_service::SpawnTasksParams {
        remote_blockchain: Some(backend.remote_blockchain()),
        transaction_pool,
        task_manager: &mut task_manager,
        on_demand: Some(on_demand),
        rpc_extensions_builder: Box::new(sc_service::NoopRpcExtensionBuilder(rpc_extensions)),
        config,
        client,
        keystore: keystore.sync_keystore(),
        backend,
        network,
        network_status_sinks,
        system_rpc_tx,
    })?;

    network_starter.start_network();

    Ok(task_manager)
}

pub struct XandServicesConfig {
    pub key_config: KeyConfig,
    pub validator_ws_url: Url,
    pub transaction_limit: Option<usize>,
    pub xand_api_config: XandApiConfig,
    pub chain_client: Arc<crate::service::FullClient>,
    pub snapshot_config: Option<SnapshotConfig>,
}

pub struct KeyConfig {
    pub initial_key_path: Option<PathBuf>,
    pub key_management_path: PathBuf,
}

pub struct SnapshotConfig {
    pub snapshot_dir: PathBuf,
    pub snapshot_frequency: Duration,
}

/// Used to startup petri and the xand api
pub fn start_xand_services(cfg: XandServicesConfig) -> Result<(), Error> {
    // setup key management
    let key_manager = create_and_initialize_key_manager(
        cfg.key_config.initial_key_path,
        cfg.key_config.key_management_path,
    )
    .expect("Unable to initialize key manager");
    let key_store: Arc<KryptKeyStore> = Arc::new(Arc::clone(&key_manager).into());
    // setup substrate client
    let substrate_client = Arc::new(
        futures::executor::block_on(SubstrateClient::connect(cfg.validator_ws_url))
            .expect("Bad substrate connection config"),
    );
    // setup create manager
    let create_manager = new_create_mgr(key_manager.clone(), substrate_client.clone());
    // setup redeem manager
    let redeem_manager = new_redeem_mgr(key_manager.clone(), substrate_client.clone());
    // setup send manager
    let send_manager = new_send_mgr(key_manager.clone(), substrate_client.clone());
    // setup txn cache

    let extrinsic_decoder = ExtrinsicDecoderImpl::<AdapterExtDecoderConfig>::new(
        create_manager.clone(),
        send_manager,
        redeem_manager.clone(),
    );

    let file_snapshotter = build_snapshotter(&cfg.snapshot_config);

    let state = get_petri_state(cfg.transaction_limit, &file_snapshotter);
    let state_for_api = state.clone();
    let mut petri = Petri::new(
        cfg.chain_client,
        extrinsic_decoder,
        Default::default(),
        file_snapshotter,
        state,
    );

    let xand_api_config = cfg.xand_api_config;

    let start_petri = async move {
        petri.start().await;
    };

    // Substrate sets a panic hook that forces a process exit. That
    // obviously breaks the idea of having the xand_api live forever
    // so we must un-set it.
    let _ = std::panic::take_hook();

    let start_xand_api_server = async move {
        log::info!("Xand API server starting on {:?}", &xand_api_config.port);
        xand_api::container::start_server(
            xand_api_config.clone(),
            state_for_api.identity_tag_store.clone(),
            state_for_api.transaction_cache.clone(),
            substrate_client.clone(),
            key_manager.clone(),
            state_for_api.utxo_store.clone(),
            key_store.clone(),
            create_manager.clone(),
            redeem_manager.clone(),
        )
        .await
        .expect("xand-api must run");
    };

    match run_service_tasks(start_petri, start_xand_api_server)? {
        Either::Left(petri_output) => log::info!("Petri service task exited: {:?}", petri_output),
        Either::Right(xand_api_output) => {
            log::info!("Xand API service task exited: {:?}", xand_api_output)
        }
    }

    Ok(())
}

fn get_petri_state<S: Snapshotter<PetriPersistedState<OsRng>>>(
    transaction_limit: Option<usize>,
    maybe_snapshotter: &Option<S>,
) -> PetriPersistedState<OsRng> {
    maybe_snapshotter
        .as_ref()
        .or_else(|| {
            log::info!("No snapshotter included.");
            None
        })
        .and_then(|snapshotter| snapshotter.get_snapshot())
        .map(|snapshot| {
            log::info!(
                "Found Petri snapshot. Loading and then resuming processing on block #{}.",
                snapshot.next_block_to_process
            );
            if let Some(limit) = transaction_limit {
                snapshot
                    .transaction_cache
                    .set_transaction_limit(limit)
                    .expect("This should never be poisoned");
            }
            snapshot
        })
        .unwrap_or_else(|| {
            log::info!("Starting with empty petri caches.");
            empty_petri_state(transaction_limit)
        })
}

fn empty_petri_state(transaction_limit: Option<usize>) -> PetriPersistedState<OsRng> {
    let transaction_cache = Arc::new(transaction_limit.map_or_else(Default::default, |limit| {
        TransactionCache::with_transaction_limit(limit)
    }));
    let txo_cache = TxoCache::default();
    let id_tag_cache = IdTagCache::new(OsRng::default());
    PetriPersistedState::new(transaction_cache, txo_cache, id_tag_cache)
}

fn build_snapshotter(
    snapshot_cfg: &Option<SnapshotConfig>,
) -> Option<FileSnapshotter<FileWriterImpl, FileReaderImpl, FileManagerImpl, SchedulerImpl>> {
    snapshot_cfg
        .as_ref()
        .map(|cfg| FileSnapshotter::new_standard(cfg.snapshot_dir.clone(), cfg.snapshot_frequency))
}
