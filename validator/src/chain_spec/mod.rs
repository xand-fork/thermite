//! Handles configuration and production of the substrate "chain spec" which defines the
//! initial parameters of the chain / genesis information

use sc_finality_grandpa::AuthorityId as GrandpaId;
use sc_keystore::LocalKeystore as Keystore;
use sc_service::ChainType;
use sp_consensus_aura::sr25519::AuthorityId as AuraId;
use sp_core::{ed25519, sr25519, Pair, Public};
use sp_keystore::SyncCryptoStore;
use sp_runtime::traits::{IdentifyAccount, Verify};
#[cfg(feature = "runtime-benchmarks")]
use std::convert::TryFrom;
use std::path::PathBuf;
use std::{
    iter::repeat,
    str::FromStr,
    sync::atomic::{AtomicUsize, Ordering},
};
use xand_runtime_models::CidrBlockArray;
#[cfg(feature = "runtime-benchmarks")]
use xandstrate_runtime::confidentiality::benchmarking::{CREATE_TX, REDEEM_TX, SEND_TX};
#[cfg(feature = "runtime-benchmarks")]
use xandstrate_runtime::confidentiality::test_constants;
use xandstrate_runtime::{
    opaque::SessionKeys,
    validator_emissions::models::validator_emission_rate::ValidatorEmissionRate, AccountId,
    AuraConfig, ConfidentialityConfig, EncryptionPubKey, GenesisConfig, GrandpaConfig,
    NetworkVotingConfig, SessionConfig, Signature, SudoConfig, SystemConfig,
    ValidatorEmissionsConfig, XandStrateConfig, XandValidatorsConfig, EPOCH_DURATION_IN_BLOCKS,
    MILLISECS_PER_BLOCK, SHORT_TEST_EPOCH_DURATION_IN_BLOCKS, SLOT_DURATION, WASM_BINARY,
};

// Because substrate dumbly only accepts function pointers for the function that generates
// the genesis config, doing so dynamically requires us to stash data somewhere we can access
// from a no-parameter non-closure function
pub static NUM_NODES: AtomicUsize = AtomicUsize::new(5);

// The value for emission rewards used if an alternative chainspec needs to be created that allows emission rates.
// TODO ADO 7788 - currently there is a race condition bug which makes it difficult to set values individually, refactor this in the output of the ADO item listed.
pub const ALT_CHAINSPEC_VALIDATOR_REWARDS_MINOR_UNITS: u32 = 100u32;

/// Specialized `ChainSpec`. This is a specialization of the general Substrate ChainSpec type.
pub type ChainSpec = sc_service::GenericChainSpec<GenesisConfig>;

// 30s expiration time, (short enough for integration tests)
pub const DEFAULT_TESTNET_PENDING_CREATE_EXPIRE_TIME_MILLISECS: u64 = 30_000;
// 30 days of blocks
const DEFAULT_BLOCKS_UNTIL_AUTO_EXIT: u32 = (30 * 24 * 60 * 60 * 1000 / MILLISECS_PER_BLOCK) as u32;

type NumNodes = usize;
type UseEmissionRewards = bool;

/// The chain specification option. This is expected to come in from the CLI and
/// is little more than one of a number of alternatives which can easily be converted
/// from a string (`--chain=...`) into a `ChainSpec`.
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Alternative {
    /// "Xand1-N" as root authority, "Trust" as trust account, "2Pac" and "Biggie" as Members.
    Standard(NumNodes, UseEmissionRewards),
    #[cfg(feature = "runtime-benchmarks")]
    Benchmarks(NumNodes),
    Template,
}

/// Fetches the root (sudo) key that we use during testing
pub fn dev_root_key() -> sr25519::Pair {
    sr25519::Pair::from_string("//Xand", None).expect("Constant can't break")
}

/// Returns a keypair using the dev mnemonic, using the provided string as a derivation path
pub fn dev_authority_keypair(s: &str) -> ed25519::Pair {
    ed25519::Pair::from_string(&format!("//{}", s), None).expect("static values are valid; qed")
}

pub fn dev_account_key(s: &str) -> AccountId {
    sr25519::Pair::from_string(&format!("//{}", s), None)
        .expect("static values are valid; qed")
        .public()
        .0
        .into()
}

pub fn dev_encryption_key(s: &str) -> EncryptionPubKey {
    EncryptionPubKey::from_str(s).unwrap()
}

impl FromStr for Alternative {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        fn parse_num_after_dash(s: &str) -> Result<usize, String> {
            let num_str = s
                .split('-')
                .nth(1)
                .ok_or_else(|| "Nothing after the dash".to_string())?;
            usize::from_str(num_str).map_err(|e| e.to_string())
        }

        match s {
            // Default 5-node network
            "confidential" => Ok(Alternative::Standard(5, false)),
            #[cfg(feature = "runtime-benchmarks")]
            "benchmarks" => Ok(Alternative::Benchmarks(5)),
            // Allow specifying a spec like "dev-3" for a 3 node dev deployment
            other_str if other_str.starts_with("confidential-") => {
                let num_nodes = parse_num_after_dash(other_str)?;
                Ok(Alternative::Standard(num_nodes, false))
            }
            // Returns the chain spec with no identities filled in
            "template" => Ok(Alternative::Template),
            _ => Err("Chain spec must be specified".into()),
        }
    }
}

impl Alternative {
    fn get_genesis_constructor(self) -> fn() -> GenesisConfig {
        match self {
            Self::Standard(_, use_rewards) => {
                if use_rewards {
                    Self::testnet_confidentiality_genesis_with_nodes_and_rewards
                } else {
                    Self::testnet_confidentiality_genesis_with_nodes
                }
            }
            Self::Template => base_genesis,
            #[cfg(feature = "runtime-benchmarks")]
            Self::Benchmarks(_) => Self::testnet_confidentiality_genesis_with_nodes_benchmark,
        }
    }

    /// Get an actual chain config from one of the alternatives.
    pub fn load(self) -> ChainSpec {
        let mut props = serde_json::map::Map::new();
        // Ensure our default "unit" size in the UI is reasonable (2 our "cents" will show up
        // as "dollars" in the UI by default)
        props.insert("tokenDecimals".to_string(), 2.into());
        // Give a nice little X symbol for our currency! I hope you parse unicode!
        props.insert("tokenSymbol".to_string(), "🅧".into());

        match self {
            Alternative::Standard(num_nodes, _) => {
                NUM_NODES.store(num_nodes, Ordering::SeqCst);
                ChainSpec::from_genesis(
                    &format!("Local Confidentiality Testnet - {} Nodes", num_nodes),
                    "confidential",
                    ChainType::Development,
                    self.get_genesis_constructor(),
                    vec![],
                    None,
                    None,
                    Some(props),
                    None,
                )
            }
            #[cfg(feature = "runtime-benchmarks")]
            Alternative::Benchmarks(num_nodes) => {
                NUM_NODES.store(num_nodes, Ordering::SeqCst);
                let name = &format!("Local Confidentiality Testnet - {} Nodes", num_nodes);
                let id = "confidential";
                let chain_type = ChainType::Development;
                let constructor = self.get_genesis_constructor();
                let boot_nodes = vec![];
                let telemetry_endpoints = None;
                let protocol_id = None;
                let properties = Some(props);
                let extentions = None;
                ChainSpec::from_genesis(
                    name,
                    id,
                    chain_type,
                    constructor,
                    boot_nodes,
                    telemetry_endpoints,
                    protocol_id,
                    properties,
                    extentions,
                )
            }
            Alternative::Template => ChainSpec::from_genesis(
                "Xand Net",
                "template",
                ChainType::Development,
                self.get_genesis_constructor(),
                vec![],
                None,
                None,
                Some(props),
                None,
            ),
        }
    }

    pub(crate) fn testnet_confidentiality_genesis_with_nodes() -> GenesisConfig {
        const PAC_ENCRYPTION_KEY: &str = "E8dqeGXb6mmbGVr9anBYP4CukgPC2z9REB4ed2z6m8iP";
        const BIGGIE_ENCRYPTION_KEY: &str = "2YafchPNHsesJe29KeSsB2z5MmPANAzTtnTdYrXjmDSJ";
        const TRUST_ENCRYPTION_KEY: &str = "FrbTj38hA43D4cazHAuF8CQ8SmavEzV9M9vuVjFKAUWA";

        let auth_keys = (1..=NUM_NODES.load(Ordering::SeqCst))
            .zip(repeat("Xand"))
            .map(|(i, s)| get_authority_keys_from_phrase(&format!("//{}{}", s, i)))
            .collect();
        testnet_genesis(
            auth_keys,
            dev_root_key().public().0.into(),
            vec![dev_account_key("2Pac"), dev_account_key("Biggie")],
            dev_account_key("Trust"),
            None,
            vec![],
            vec![
                (
                    dev_account_key("2Pac"),
                    dev_encryption_key(PAC_ENCRYPTION_KEY),
                ),
                (
                    dev_account_key("Biggie"),
                    dev_encryption_key(BIGGIE_ENCRYPTION_KEY),
                ),
                (
                    dev_account_key("Trust"),
                    dev_encryption_key(TRUST_ENCRYPTION_KEY),
                ),
            ],
            false,
        )
    }

    pub(crate) fn testnet_confidentiality_genesis_with_nodes_and_rewards() -> GenesisConfig {
        const PAC_ENCRYPTION_KEY: &str = "E8dqeGXb6mmbGVr9anBYP4CukgPC2z9REB4ed2z6m8iP";
        const BIGGIE_ENCRYPTION_KEY: &str = "2YafchPNHsesJe29KeSsB2z5MmPANAzTtnTdYrXjmDSJ";
        const TRUST_ENCRYPTION_KEY: &str = "FrbTj38hA43D4cazHAuF8CQ8SmavEzV9M9vuVjFKAUWA";
        const XAND_ENCRYPTION_KEY: &str = "5y8g94FZsyTfJf6EeVdu733guRZGenXdTdftUSawaPiR";
        const XAND1_ENCRYPTION_KEY: &str = "HASJE7dXifTMGP835rS27rdXMY62EmuDZyGjHXjCrBJ1";

        let auth_keys = (1..=NUM_NODES.load(Ordering::SeqCst))
            .zip(repeat("Xand"))
            .map(|(i, s)| get_authority_keys_from_phrase(&format!("//{}{}", s, i)))
            .collect();
        testnet_genesis(
            auth_keys,
            dev_root_key().public().0.into(),
            vec![dev_account_key("2Pac"), dev_account_key("Biggie")],
            dev_account_key("Trust"),
            None,
            vec![],
            vec![
                (
                    dev_account_key("2Pac"),
                    dev_encryption_key(PAC_ENCRYPTION_KEY),
                ),
                (
                    dev_account_key("Biggie"),
                    dev_encryption_key(BIGGIE_ENCRYPTION_KEY),
                ),
                (
                    dev_account_key("Trust"),
                    dev_encryption_key(TRUST_ENCRYPTION_KEY),
                ),
                (
                    dev_account_key("Xand"),
                    dev_encryption_key(XAND_ENCRYPTION_KEY),
                ),
                (
                    dev_account_key("Xand1"),
                    dev_encryption_key(XAND1_ENCRYPTION_KEY),
                ),
            ],
            true,
        )
    }

    #[cfg(feature = "runtime-benchmarks")]
    pub(crate) fn testnet_confidentiality_genesis_with_nodes_benchmark() -> GenesisConfig {
        const PAC_ENCRYPTION_KEY: &str = "E8dqeGXb6mmbGVr9anBYP4CukgPC2z9REB4ed2z6m8iP";
        const BIGGIE_ENCRYPTION_KEY: &str = "2YafchPNHsesJe29KeSsB2z5MmPANAzTtnTdYrXjmDSJ";
        const TRUST_ENCRYPTION_KEY: &str = "FrbTj38hA43D4cazHAuF8CQ8SmavEzV9M9vuVjFKAUWA";

        let auth_keys = (1..=NUM_NODES.load(Ordering::SeqCst))
            .zip(repeat("Xand"))
            .map(|(i, s)| get_authority_keys_from_phrase(&format!("//{}{}", s, i)))
            .collect();
        testnet_genesis(
            auth_keys,
            dev_root_key().public().0.into(),
            vec![dev_account_key("2Pac"), dev_account_key("Biggie")],
            test_constants::TRUST.clone(),
            None,
            vec![],
            vec![
                (
                    dev_account_key("2Pac"),
                    dev_encryption_key(PAC_ENCRYPTION_KEY),
                ),
                (
                    dev_account_key("Biggie"),
                    dev_encryption_key(BIGGIE_ENCRYPTION_KEY),
                ),
                (
                    test_constants::TRUST.clone(),
                    dev_encryption_key(TRUST_ENCRYPTION_KEY),
                ),
            ],
            false,
        )
    }
}

pub type AuthorityKeys = (AccountId, AuraId, GrandpaId);
type AccountPublic = <Signature as Verify>::Signer;

pub fn get_from_phrase<TPublic: Public>(phrase: &str) -> <TPublic::Pair as Pair>::Public {
    TPublic::Pair::from_string(phrase, None)
        .expect("static values are valid; qed")
        .public()
}

pub fn get_account_id_from_phrase<TPublic: Public>(phrase: &str) -> AccountId
where
    AccountPublic: From<<TPublic::Pair as Pair>::Public>,
{
    AccountPublic::from(get_from_phrase::<TPublic>(phrase)).into_account()
}

pub fn get_authority_keys_from_phrase(phrase: &str) -> AuthorityKeys {
    (
        get_account_id_from_phrase::<sr25519::Public>(phrase),
        get_from_phrase::<AuraId>(phrase),
        get_from_phrase::<GrandpaId>(phrase),
    )
}

// TODO: This is exposed publicly, but it unwraps Result. We either need to hide this behide a
//       feature flag or handle these errors. The problem with the feature flag is it will
//       cause extra build time for the different features, and this is one of the heaviest
//       pieces of code, so we want to avoid that.
//       https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6845
/// Only for testing - use to easily save keys into a keystore from a seed path
/// See https://github.com/paritytech/substrate/blob/e98b829287313502fce0cde6c1d09439b0f2a9f7/test-utils/chain-spec-builder/src/main.rs#L134
pub fn generate_and_save_keys<T: Into<PathBuf>>(
    seed: &str,
    keystore_path: T,
) -> Result<(), sc_keystore::Error> {
    let keystore = Keystore::open(keystore_path, None)?;
    // TODO: Ok to unwrap? This code is just for tests.

    let secret_uri = format!("//{}", seed);
    let aura_key_pair =
        sr25519::Pair::from_string(secret_uri.as_str(), None).expect("Generates key pair");
    keystore
        .insert_unknown(
            sp_core::crypto::key_types::AURA,
            secret_uri.as_str(),
            aura_key_pair.public().as_ref(),
        )
        .expect("Inserts unknown key");

    let grandpa_key_pair =
        ed25519::Pair::from_string(secret_uri.as_str(), None).expect("Generates key pair");
    keystore
        .insert_unknown(
            sp_core::crypto::key_types::GRANDPA,
            secret_uri.as_str(),
            grandpa_key_pair.public().as_ref(),
        )
        .expect("Inserts unknown key");

    Ok(())
}

fn session_keys(aura: AuraId, grandpa: GrandpaId) -> SessionKeys {
    SessionKeys { aura, grandpa }
}

/// This returns a [GenesisConfig] with no identiies, and empty configs for runtime modules that
/// require identities (notably xandstrate and sudo)
fn base_genesis() -> GenesisConfig {
    // This bit of code is a workaround for the fact that epoch duration isn't
    // part of the chainspec in the way it should be, this fixes that. It allows us to turn down
    // epoch length during consensus related testing.
    let epoch_duration_blocks = if std::env::var(SHORT_EPOCH_ENV_FLAG).is_ok() {
        SHORT_TEST_EPOCH_DURATION_IN_BLOCKS
    } else {
        EPOCH_DURATION_IN_BLOCKS
    };
    let epoch_duration = {
        let slot_fill_rate = MILLISECS_PER_BLOCK as f64 / SLOT_DURATION as f64;
        (f64::from(epoch_duration_blocks) * slot_fill_rate) as u32
    };
    GenesisConfig {
        pallet_aura: Some(AuraConfig {
            authorities: vec![],
        }),
        system: Some(SystemConfig {
            // TODO: refactor this call chain to support a result when loading wasm to avoid panic
            code: WASM_BINARY
                .expect("No WASM available to generate chainspec")
                .to_vec(),
            changes_trie_config: Default::default(),
        }),
        pallet_sudo: None, // Must be filled in
        pallet_grandpa: Some(GrandpaConfig {
            authorities: vec![],
        }),
        pallet_session: Some(SessionConfig {
            // Must be set to (account id, session key) pairs, where there is a corresponding
            // account id in `validators`
            keys: vec![],
        }),
        xandstrate: None, // Must be filled in
        xandvalidators: Some(XandValidatorsConfig {
            epoch_duration,
            // Must be populated with account ids matching those passed to the session pallet
            validators: vec![],
        }),
        network_voting: None,

        validator_emissions: Some(ValidatorEmissionsConfig {
            emission_rate_setting: ValidatorEmissionRate {
                minor_units_per_emission: Default::default(),
                block_quota: Default::default(),
            },
        }),
        confidentiality: None,
    }
}

pub const SHORT_EPOCH_ENV_FLAG: &str = "SHORT_EPOCHS_CONSENSUS_TESTING";

#[allow(clippy::too_many_arguments)]
/// Builds a testnet genesis block, endowing particular identities with a starting balance
pub fn testnet_genesis(
    initial_authorities: Vec<AuthorityKeys>,
    root_key: AccountId,
    ap_accounts: Vec<AccountId>,
    trust_account: AccountId,
    limited_agent_id: Option<AccountId>,
    allowlisted_cidrs: Vec<(AccountId, CidrBlockArray)>,
    node_encryption_keys: Vec<(AccountId, EncryptionPubKey)>,
    use_emission_rewards: bool,
) -> GenesisConfig {
    let mut config = base_genesis();

    // Set authorities in session module. Aura and Grandpa's initial settings are derived from this
    let mut session_cfg = config.pallet_session.take().unwrap();

    session_cfg.keys = initial_authorities
        .iter()
        .map(|x| {
            (
                x.0.clone(),
                x.0.clone(),
                session_keys(x.1.clone(), x.2.clone()),
            )
        })
        .collect();
    config.pallet_session = Some(session_cfg);

    config.pallet_sudo = Some(SudoConfig { key: root_key });

    // XAND specific configuration
    config.xandstrate = Some(XandStrateConfig {
        registered_members: ap_accounts.into_iter().map(|k| (k, true)).collect(),
        banned_members: vec![],
        blocks_until_auto_exit: DEFAULT_BLOCKS_UNTIL_AUTO_EXIT,
        trust_node_id: trust_account,
        limited_agent_id: limited_agent_id.unwrap_or_default(),
        allow_listed_cidr_blocks: allowlisted_cidrs,
        node_encryption_key: node_encryption_keys,
    });

    #[cfg(feature = "runtime-benchmarks")]
    let identity_tags = {
        let mut tags = CREATE_TX.identity_set.clone();
        tags.append(&mut SEND_TX.input_identities.clone());
        tags.append(&mut REDEEM_TX.identity_set.clone());
        tags
    };

    #[cfg(feature = "runtime-benchmarks")]
    let txos = {
        let mut txos = SEND_TX.input_txos.clone();
        txos.append(&mut REDEEM_TX.input_txos.clone());
        txos.into_iter()
            .map(|(x, (y, block))| (x, (y, u32::try_from(block).unwrap())))
            .collect()
    };

    config.confidentiality = Some(ConfidentialityConfig {
        pending_create_expire_time: DEFAULT_TESTNET_PENDING_CREATE_EXPIRE_TIME_MILLISECS,
        #[cfg(feature = "runtime-benchmarks")]
        identity_tags,
        #[cfg(feature = "runtime-benchmarks")]
        txos,
        ..Default::default()
    });

    let mut xvcfg = config.xandvalidators.take().unwrap();
    xvcfg.validators = initial_authorities.into_iter().map(|x| x.0).collect();
    config.xandvalidators = Some(xvcfg);

    config.network_voting = Some(NetworkVotingConfig {
        voting_period: 1000, // TODO: Don't hardcode
    });

    // NOTE: Validator emissions only implemented fully for confidential networks.
    if use_emission_rewards {
        config.validator_emissions = Some(ValidatorEmissionsConfig {
            emission_rate_setting: ValidatorEmissionRate {
                minor_units_per_emission: ALT_CHAINSPEC_VALIDATOR_REWARDS_MINOR_UNITS.into(),
                block_quota: Default::default(),
            },
        });
    } else {
        config.validator_emissions = Some(ValidatorEmissionsConfig {
            emission_rate_setting: ValidatorEmissionRate {
                minor_units_per_emission: Default::default(),
                block_quota: Default::default(),
            },
        })
    }

    config
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn default_blocks_until_auto_exit_represents_30_days() {
        let thirty_days_in_millisecs = 30 * 24 * 60 * 60 * 1000;
        let expected = thirty_days_in_millisecs / MILLISECS_PER_BLOCK as u32;
        assert_eq!(DEFAULT_BLOCKS_UNTIL_AUTO_EXIT, expected);
    }
}
